<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_Extension extends Model
{
    protected $table="task_extension";

    function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }
}
