<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseResponse extends Model
{
    //
    protected $table = 'case_responses';

    public function case(){
        
        return $this->belongsTo('App\SupportCase', 'case_id', 'id' );
    }
    public function user(){

        return $this->belongsTo('App\People',  'response_by', 'id');
    }
}
