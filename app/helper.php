<?php

use Carbon\Carbon;
use App\Classes\table;
use Illuminate\Support\Facades\Auth;
use App\Branch;
use App\People;
use App\Task;
use App\Device;
use App\Training;
use Illuminate\Support\Facades\DB;



function week_days_this_month($startDate, $endDate)
{
    $resultDays = array(
        'monday' => 0,
        'tuesday' => 0,
        'wednesday' => 0,
        'thursday' => 0,
        'friday' => 0,
        'saturday' => 0,
        'sunday' => 0
    );

    // change string to date time object 
    $startDate = new DateTime($startDate);
    $endDate = new DateTime($endDate);

    // iterate over start to end date 
    while ($startDate <= $endDate) {
        // find the timestamp value of start date 
        $timestamp = strtotime($startDate->format('d-m-Y'));

        // find out the day for timestamp and increase particular day 
        $weekDay = date('l', $timestamp);
        $resultDays[strtolower($weekDay)] = $resultDays[strtolower($weekDay)] + 1;

        // increase startDate by 1 
        $startDate->modify('+1 day');
    }

    return $resultDays;
}

function schedule_template($schedule_time)
{

    $time = explode("-", $schedule_time);
    $start = date('h:i a', strtotime($time[0]));
    $end = date('h:i a', strtotime($time[1]));
    return $start . ' - ' . $end;
}

function remainingLeaves($people_id)
{
    $current_user = table::people()->where('id', $people_id)->first();
    // Leave count starts

    $cur_year = date('Y');
    $pre_year = $cur_year - 1;
    $start_date = $pre_year . "-12-01 00:00:00";
    $end_date = $cur_year . "-12-31 00:00:00";

    $taken_array = table::leaves()->where('reference', $current_user->id)->whereBetween('leavefrom', [$start_date, $end_date])->where('status','Approved')->get();

    $auth_schedule = table::schedules()->where('branch_id',$current_user->branch_id)->where('reference', $current_user->id)->first();
    if($auth_schedule)
    {
        $auth_schedule_template = table::sch_template()->where('id', $auth_schedule->schedule_id)->first();

        $taken_leaves = 0;
        // dd($taken_array);
        foreach ($taken_array as $taken) {
            $start = new Carbon($taken->leavefrom);
            $end = (new Carbon($taken->leaveto))->addDays(1);
    

            for ($i = $start; $i < $end; $i->addDays(1)) {
                // check the leave day is holiday or  weekend or not
    
                if (($i->format('Y')) == $cur_year) {
    
                    $cur_month_holidays_dates = table::holidays()->where('month', $i->format('m'))->select('dates')->get();

                    if (count($cur_month_holidays_dates) != 0) {
                        $cur_month_holidays = explode(",", $cur_month_holidays_dates[0]->dates);
                        // dd($i->format('d'),$cur_month_holidays);
                        if (!in_array($i->format('d'), $cur_month_holidays)) {
                            $day = strtolower($i->format('l'));
                            // dd($auth_schedule_template->$day);
                            if ($auth_schedule_template->$day != Null) {
                                $taken_leaves += 1;
                            }
                        }
                    } else {
                        $day = strtolower($i->format('l'));
                        // dd($auth_schedule_template->$day);
                        if ($auth_schedule_template->$day != Null) {
                            $taken_leaves += 1;
                        }
                    }
                }
            }
        }

        // Leave count ends
        $total_leaves = table::leavetypes()->where('id', $current_user->leaveprivilege)->first();
        if ($total_leaves != NULL) {
            $available_leaves = $total_leaves->limit - $taken_leaves;
        } else $available_leaves = 0;
    
        return $available_leaves;

    }
    else{
        return "No Schedule!";
    }
    
}

 function available_leaves($people){
   $current_user = table::people()->where('id', $people)->first();
   $taken_leaves = table::leaves()->where('reference', $people)->whereYear('leavefrom', Carbon::now()->year)->whereYear('leaveto', Carbon::now()->year)->where('status','Approved')->sum('leave_counter');
  $total_leaves = table::leavetypes()->where('id', $current_user->leaveprivilege)->first();
        if ($total_leaves != NULL) {
            $available_leaves = $total_leaves->limit - $taken_leaves;
        } else $available_leaves = 0;
        
  return $available_leaves;
 }
 
 function taken_leaves($people){
   $current_user = table::people()->where('id', $people)->first();
   $taken_leaves = table::leaves()->where('reference', $people)->whereYear('leavefrom', Carbon::now()->year)->whereYear('leaveto', Carbon::now()->year)->where('status','Approved')->sum('leave_counter');
  
        
  return $taken_leaves;
 }
 
 
 function is_owner($branch_id){

    $branch = Branch::where('branch_id',$branch_id)->where('owner_id' , Auth::user()->id)->first();

    if($branch){
        return True;
    }
    else{
        return false;
    }

 }

 function branch_number($admin_id){
    $branch_number = Branch::where('owner_id' , $admin_id)->count();
    return $branch_number;
 }
 
 function people_number($admin_id){
    //  dd($admin_id);
    $branches = Branch::where('owner_id' , $admin_id)->get();
    $branch_ids = [];
    foreach($branches as $branch){
       
        array_push($branch_ids,$branch->id);
    }
    $people = People::whereIn('branch_id',$branch_ids )->where('role_id', 1)->where('acc_type' , 1)->count();

   
    return $people;

 }

 function task_eligible($id){

    $task = Task::where('reference' , $id)->where('done_status', false)->count();

    if($task > 6){
        return false;
    }
    else{
        return true;
    }


 }
 
 function employee_schedule($employee_id){
 
 return table::schedules()->where('reference',$employee_id)->where('active_status', '=', '1')->first();
 
 }
 
 function package_stage($branch_id){
     $owner_id = Branch::where('id' , $branch_id)->value('owner_id');
     $end_date_old = DB::table('user_subscriptions')->where('user_id',$owner_id)->where('status','active')->value('plan_period_end');
     $end_date = new DateTime($end_date_old);
     $cur_date = new DateTime('now');;
     if($cur_date > $end_date){
         return 0;
     }
     else{
        return 1; 
     }


    
 }


function employee_count($branch_id){

    $branches = Branch::Join('people', 'branches.id', '=', 'people.branch_id')->where('people.acc_type', 1)->where('people.branch_id', $branch_id)->count();
    if($branches ){
        return $branches ;
    }
    else{
        return 0;
    }
}


function admin_count($branch_id){

    $branches = Branch::Join('people', 'branches.id', '=', 'people.branch_id')->where('people.acc_type', 2)->where('people.branch_id', $branch_id)->count();
    if($branches ){
        return $branches ;
    }
    else{
        return 0;
    }
}

function device_count($branch_id){

    $device = Device::where('branch_id',$branch_id)->count();

    if($device ){
        return $device ;
    }
    else{
        return 0;
    }
}

function traing_time($branch_id){

    $timezone = DB::table('settings')->where('branch_id', $branch_id )->value('timezone');

    $train = Training::where('branch_id',$branch_id)->orderby('created_at', 'DESC')->first();
    
    if($train != null){
        $now =  \Carbon\Carbon::now($timezone);
        $train_time =  \Carbon\Carbon::parse($train->created_at) ;

        $time = $train_time->diffInDays($now) . ' Days' ;

        if($time == 0 ){
            $time =$valid_time->diffInHours($now) . ' Hours' ;
        }

        if( $time){
            return $time;
        }
        else{
            return 0;
        } 
    }else{
        return 0;
    }
    
       
}
