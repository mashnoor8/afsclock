<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //
    protected $table="people";

    function tasks()
    {
        return $this->hasMany(Task::class, 'reference', 'id');
    }
    function job_title()
    {
        return $this->hasOne(Job_Title::class,'id','job_title_id');
    }
    public function verifyUser()
    {
    return $this->hasOne('App\VerifyUser');
    }


}
