<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    
    public function noticeBY(){
        return $this->hasOne('App\People', 'id', 'notice_by');
    }
    public function noticeFor(){
        return $this->hasOne('App\People', 'id', 'reference');
    }
}
