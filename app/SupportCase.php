<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportCase extends Model
{
    //
    protected $table= "cases";

    public function responses(){
        return $this->hasMany('App\CaseResponse',  'case_id' ,  'id' );
    }

}
