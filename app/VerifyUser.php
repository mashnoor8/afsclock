<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    //
    protected $guarded = [];
 
    public function user()
    {
        return $this->belongsTo(People::class, 'user_id', 'id');
    }
}
