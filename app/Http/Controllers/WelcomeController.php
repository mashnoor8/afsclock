<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\People;
use App\VerifyUser;
use Carbon\Carbon;
use App\Classes\table;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
   

    public function index(){

        if (Auth::user()) {   


            if(Auth::user()->role_id == 1)
            {
                return Redirect::to(url('personal/dashboard'));
            }
            elseif(Auth::user()->role_id == 2){
               return Redirect::to(url('dashboard') );
            }
            elseif(Auth::user()->role_id == 3){
            return Redirect::to(url('/branches') );

            }
            else{
                return view('welcome.index');
            }
        }
        else {
            return view('welcome.index');
        }

    }
    public function register()
    {
        return view('welcome.register');
    }
    public function features()
    {
        return view('welcome.features');
    }

    public function pricing()
    {
        $packages = DB::table('packages')->where('status', 1)->where('currency', 'USD')->get();
        return view('welcome.pricing' , compact('packages'));
    }

    public function solution()
    {
        return view('welcome.solution');
    }

    public function trial()
    {
        
        return view('welcome.trial');
    }

    public function faqs()
    {
        return view('welcome.faq');
    }

    public function manual()
    {
        
        return view('welcome.user_manual');
    }

    public function remoteStaff()
    {
        return view('welcome.staff_management');
    }

    public function callCenter()
    {
        return view('welcome.call_center');
    }

    public function contat()
    {
        return view('welcome.contact');
    }

    public function warehouseStaff()
    {
        return view('welcome.warehouse_staff');
    }
    public function send( Request $request)
    {

        // dd($request->all());

        Mail::to('info@attendancekeeper.net')->send(new ContactFormMail ($request));
        // dd($request->all());
        return view('thanks');

    }
    public function super_admin_registration(Request $request){
     //dd($request->country);

    $validatedData = $request->validate([
        
        'first_name' => ['required'],
        'last_name' => ['required'],
        'email' => ['unique:people,companyemail'],
        'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
        
    ]);

    $exist_country = DB::table('countries')->where('name',$request->country)->first();
    
    if($exist_country != null){
    $country_id = $exist_country;
    }
    else{
   
    DB::table('countries')->insert(['name'=>$request->country]);
    $country_id = DB::table('countries')->where('name',$request->country)->first();
    }
    
    $super_admin = new People();
    $super_admin->firstname = $request->first_name;
    $super_admin->lastname = $request->last_name;
    $super_admin->country_id = $country_id->id;
    $super_admin->companyemail = $request->email;
    $super_admin->employmentstatus = "Active";
    $super_admin->employmenttype = "Regular";
    $super_admin->password = Hash::make($request->password);
    $super_admin->role_id = "3";
    $super_admin->acc_type = "3";
    

    $super_admin->status = "1";

    $super_admin->save();
    

    DB::table('user_subscriptions')->insert([
        'user_id' => $super_admin->id,
        'package_id' => '1',
        'stripe_subscription_id'=> 'trial',
        'stripe_customer_id' => 'trial',
        'plan_interval_count' => '1',
        'payer_email' => $super_admin->companyemail,
        'status' =>  'active',
        'plan_period_start' => Carbon::now(),
        'plan_period_end' =>Carbon::now()->addDays(60),
        'created_at' => Carbon::now()
    ]);


    $verifyUser = VerifyUser::create([
        'user_id' => $super_admin->id,
        'token' => sha1(time())
      ]);
   
        $token = VerifyUser::where('user_id', $super_admin->id)->first();

   

    $error = Null ;
    try{
 Mail::send('email.verifyUser', ['firstname' => $super_admin->firstname, 'lastname' => $super_admin->lastname, 'companyemail' => $super_admin->companyemail , 'token' => $token->token ], function ($m) use ($super_admin) {
        $m->from('info@attendancekeeper.net', 'Attendance Keeper');
        $m->to($super_admin->companyemail, $super_admin->firstname)->subject('Email Verification-Attendance Keeper');
    });
}
catch(\Exception $e){
    $error = $e;
}
    
    
    if($error == Null){
    return redirect('/login')->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
    }
    
    else{
    return redirect('/login')->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.Cannot send email');
    }

    

    }

    public function viewPassword()
	{
		return view('super_admin.branches.update_password');
    }
    


    public function updatePassword(Request $request)
	{
		//if($request->sh == 2){return redirect()->route('updatePassword');}
        // dd($request->all());

		$v = $request->validate([
            'currentpassword' => 'required|max:100',
            'newpassword' => 'required|min:8|max:100',
            'confirmpassword' => 'required|min:8|max:100',
        ]);
        
		$branch_id = Auth::user()->branch_id;

        $id = Auth::id();
        // dd($id);
		$p = Auth::user()->password;
		$c_password = $request->currentpassword;
		$n_password = $request->newpassword;
		$c_p_password = $request->confirmpassword;

		if($id == null)
        {
            return redirect('super_admin/update-password')->with('error', 'Whoops! Please fill the form completely.');
		}

		if($n_password != $c_p_password)
		{
			return redirect('super_admin/update-password')->with('error', 'New password does not match!');
		}

		if(Hash::check($c_password, $p))
		{
			table::people()->where('branch_id',$branch_id)->where('id', $id)->update([
				'password' => Hash::make($n_password),
			]);

			return redirect('super_admin/update-password')->with('success', 'Password Has been Changed Successfully');
		} else {
			return redirect('super_admin/update-password')->with('error', 'Oops! current password does not match.');
		}
    }

    public function verifyUser($token)
    {
    $verifyUser = VerifyUser::where('token', $token)->first();
    
    if(isset($verifyUser) ){
        $people = $verifyUser->user;
        
        if(!$people->verified) {
            
            $people->verified = 1;
            $people->save();
        
            $status = "Your e-mail is verified. You can now login.";
            } 
            else {
            $status = "Your e-mail is already verified. You can now login.";
            }

        } 
        else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
    return redirect('/login')->with('status', $status);
    }
    
    public function packages_search(Request $request){
      $country = $request->get('country');
      
      $country_europe = ['Andorra','Austria','Belarus','Belgium','Bosnia and Herzegovina','Bulgaria','Croatia','Cyprus','Czech Republic','Denmark','Estonia','Finland','France','Germany','Greece','Hungary','Iceland','Ireland','Italy','Latvia','Liechtenstein','Lithuania','Luxembourg','Malta','Moldova','Monaco','Montenegro','Netherlands','North Macedonia','Norway','Poland','Portugal','Romania','San Marino','Serbia','Slovakia','Slovenia','Spain','Sweden','Switzerland','Turkey','Ukraine'] ;
        if($country == 'United Kingdom'){
              $currency = "GBP";
          }
        elseif(in_array($country, $country_europe)){
              $currency = "EUR";
          }
        else{
               $currency = "USD";
          }
       
        $packages = DB::table('packages')->where('currency',$currency)->where('status', 1)->get();
        return view('welcome.pricing-list', compact('packages'))->render();
    }


        
    
    
}
