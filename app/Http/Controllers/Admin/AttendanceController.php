<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;




class AttendanceController extends Controller
{
    // The following controller function
    // queries employee attendance information and sends to the view file.
    public function index()
    {
       if (permission::permitted('attendance')=='fail'){ return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;


        $data = table::attendance()->where('branch_id',$branch_id)->orderBy('timein', 'desc')->paginate(13);
        $missing = table::applications()->where('branch_id',$branch_id)->where('AdminApproved',0)->count();
        $cc = table::settings()->where('branch_id',$branch_id)->value('clock_comment');
        $title = "attendances";
        return view('admin.attendance', compact('data','title', 'cc','missing'));
    }

    // Clock in / out view file represented
    // by this controller function.
    public function clock()
    {
        return view('clock');
    }


    public function applications_for_missing_attendances()
    {
        $branch_id = Auth::user()->branch_id;

    
        $applications = table::applications()->where('branch_id',$branch_id)->where('AdminApproved','Applied')->orderBy('created_at', 'desc')->get();
        $title = "attendances";
        
        return view('admin.applications-missing-attendance',compact('applications','title'));
      
    }
    public function application_details($id)
    {
        $branch_id = Auth::user()->branch_id;

    
        $application = table::applications()->where('branch_id',$branch_id)->where('id',$id)->first();
        
        $applicant = table::people()->where('branch_id',$branch_id)->where('id',$application->reference)->first();
        $company = table::company()->where('branch_id',$branch_id)->where('id',$applicant->company_id)->value('company');
        $department = table::department()->where('branch_id',$branch_id)->where('id',$applicant->department_id)->value('department');
        $jobtitle = table::jobtitle()->where('branch_id',$branch_id)->where('id',$applicant->job_title_id)->value('jobtitle');
        $missing = table::applications()->where('branch_id',$branch_id)->where('AdminApproved',0)->count();
        $attendance_exist = table::attendance()->where('branch_id',$branch_id)->where('reference',$application->reference)->where('date',$application->date)->first();
        
        $assigned_schedule_id = table::schedules()->where('branch_id',$branch_id)->where([['reference', $application->reference],['active_status', 1]] )->value('schedule_id');
        $day_date = date('l', strtotime($application->date));
        $day = strtolower($day_date);
        $schedule_template = table::sch_template()->where('branch_id',$branch_id)->where('id', $assigned_schedule_id)->value($day);
        if($schedule_template != Null){
            $str_arr = explode ("-", $schedule_template);
            $schedule_timein_24 = $str_arr[0];
            $schedule_timeout_24 = $str_arr[1];
            $schedule_timein = date('h:i:s a',strtotime($schedule_timein_24));
            $schedule_timeout = date('h:i:s a',strtotime($schedule_timeout_24));
        }
        else{
            $schedule_timein = "Weekend";
            $schedule_timeout = "Weekend";

        }


        $title = "attendances";
        
        return view('admin.application-missing-attendance',compact('application','title','applicant','schedule_timein','schedule_timeout','attendance_exist','company','department','missing','jobtitle'));

      
    }
    
    public function  approval_missing_attendance_post(Request $request)
    {
      $id = $request->id;
      $comment = $request->comment;
      $branch_id = Auth::user()->branch_id;

      $attendance = table::applications()->where('branch_id',$branch_id)->where('id', $id)->first();
      $attendance_exist = table::attendance()->where('branch_id',$branch_id)->where('reference',$attendance->reference)->where('date',$attendance->date)->first();
      $curtime = Carbon::now();
      $timeIN = date("Y-m-d h:i:s A", strtotime($attendance->timein));
      $timeOUT = date("Y-m-d h:i:s A", strtotime($attendance->timeout));


        $assigned_schedule_id = table::schedules()->where('branch_id',$branch_id)->where([['reference', $attendance->reference],['active_status', 1]] )->value('schedule_id');

        $schedule_template = table::sch_template()->where('branch_id',$branch_id)->where('id', $assigned_schedule_id)->first();
        $t1 = Carbon::createFromFormat("Y-m-d h:i:s A", $timeIN);
        $t2 = Carbon::createFromFormat("Y-m-d h:i:s A", $timeOUT);
        $th = $t1->diffInHours($t2);
        $tm = floor(($t1->diffInMinutes($t2) - (60 * $th)));
        $totalhour = $th.".".$tm;
        
        $day_date = date('l', strtotime($attendance->date));
        $day = strtolower($day_date);
        $day_today = $schedule_template->$day;
        if ($day_today) {
          $str_arr = explode ("-", $day_today);
          $in_time = $str_arr[0];
          $out_time = $str_arr[1];
        }else{
          $in_time = NULL;
          $out_time = NULL;
        }

        if($in_time == null)
        {
            $status_in = "Weekend";
        } else {
            $sched_clock_in_time_24h = date("H.i", strtotime($in_time));
            $time_in_24h = date("H.i", strtotime($timeIN));

            if ($time_in_24h <= $sched_clock_in_time_24h)
            {
                $status_in = 'In Time';
            } else {
                $status_in = 'Late Arrival';
            }
        }

        // $sched_out_time = table::schedules()->where([
        //     ['reference', '=', $reference],
        //     ['archive','=','0'],
        // ])->value('outime');
        if ($day_today) {
            $str_arr = explode ("-", $day_today);
            $in_time = $str_arr[0];
            $out_time = $str_arr[1];
            $time11 = explode(':',$in_time);
            $time22 = explode(':',$out_time);
            $hours1 = $time11[0];
            $hours2 = $time22[0];
            $mins1 = $time11[1];
            $mins2 = $time22[1];
            $hours = $hours2 - $hours1;
            $mins = 0;
            if($hours < 0)
            {
            $hours = 24 + $hours;
            }
            if($mins2 >= $mins1) { 
            $mins = (int)$mins2 - (int)$mins1;
            // dd($mins2,$mins1, $mins);
            }
            else {
            $mins = ($mins2 + 60) - $mins1;
            $hours--;
            }
            if($mins < 9)
            {
            $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);
            }
            if($hours < 9)
            {
            $hours =str_pad($hours, 2, '0', STR_PAD_LEFT);
            }
            $total_scheduled_time_minutes = ($hours * 60) + $mins;
            $total_scheduled_time = $total_scheduled_time_minutes/60;
            $total_office_work_time = floatval($totalhour) ;

            

            // return $out_time;
          }else{
            $in_time = NULL;
            $out_time = NULL;
          }
          
          if( $in_time != Null)
          {
                if($total_office_work_time >= $total_scheduled_time)
                {
                    $status_out = 'On Time';
                }
                else
                {
                    $status_out = 'Early Departure';
                }
          }
          else{
            $status_out = 'Weekend';
          }

    if($attendance_exist != Null){

        table::attendance()->where('branch_id',$branch_id)->where('id', $attendance_exist->id)->update([
            'timein' => $attendance->timein,
            'timeout' => $attendance->timeout,
            'comment' => $comment,
            'totalhours' => $totalhour,
            'status_timein' => $status_in,
            'status_timeout' => $status_out,
        ]);
        table::applications()->where('branch_id',$branch_id)->where('id',$id)->update([
            'AdminApproved' => 'Approved',
            'comment' => $comment,
            'updated_at' =>  $curtime,

        ]);
    }
    else
    {
        table::attendance()->insert([
            'reference' => $attendance->reference,
            'branch_id' => $branch_id,
            'idno' => $attendance->idno,
            'date' =>$attendance->date,
            'employee' => $attendance->employee,
            'timein' => $attendance->timein,
            'timeout' => $attendance->timeout,
            'totalhours' => $totalhour,
            'status_timein' => $status_in,
            'status_timeout' => $status_out,
            'comment' => $comment,
            'schedule_id' => $assigned_schedule_id,
        ]);

        table::applications()->where('branch_id',$branch_id)->where('id',$id)->update([
            'AdminApproved' => 'Approved',
            'comment' => $comment,
            'updated_at' =>  $curtime,

        ]);

    }
     
    return redirect('applications/missing_attendances')->with('success', 'Application For Missing Attendance Approved Successfully!');
         
    }

    // public function  deny_missing_attendance_post($id)
    // {
    //     $comment = "Denied";
    //     table::applications()->update([
    //         'comment' => $comment,
    //         'AdminApproved' => 1,
    //     ]);
    //     return redirect('applications/missing_attendances')->with('success', 'Application For Missing Attendance Denied Successfully!');
    // }
    
    public function  deny_missing_attendance_reason(Request $request)
    {
        $id = $request->id;
        $comment = $request->comment;
        $curtime = Carbon::now();
        $branch_id = Auth::user()->branch_id;

        table::applications()->where('branch_id',$branch_id)->where('id',$id)->update([
            'comment' => $comment,
            'AdminApproved' => 'Denied',
            'updated_at' =>  $curtime,
        ]);
        return redirect('applications/missing_attendances')->with('success', 'Application For Missing Attendance Denied Successfully!');
    }  


    public function  attendance_search(Request $request)
    {
      $query = $request->get('query');
      $start = $request->get('start');
      $end = $request->get('end');
    $branch_id = Auth::user()->branch_id;

    //   dd($query,$start,$end);
      $cur_date = date('Y-m-d');

      // return response()->json([$start, $end]);

    if($query == '' && $start == '' && $end == '' )
        {
          $data = table::attendance()->where('branch_id',$branch_id)->orderBy('timein', 'desc')->paginate(13);
           
    } 
    elseif ($query != '' && $start == '' && $end == '' ) 
    {
      $data = table::attendance()->where('branch_id',$branch_id)->where('employee', 'like', '%'.$query.'%')->orderBy('timein', 'desc')->paginate(13);
           
        }

    elseif ($query != '' && $start != '' && $end == '' ) 
    {
      $data = table::attendance()->where('branch_id',$branch_id)->where('employee', 'like', '%'.$query.'%')->whereBetween('date', [$start, $cur_date])->orderBy('timein', 'desc')->paginate(13);
          
        }
    elseif ($query != '' && $start == '' && $end != '' ) 
    {
      $data = table::attendance()->where('branch_id',$branch_id)->where('employee', 'like', '%'.$query.'%')->where('date','<=', $end)->orderBy('timein', 'desc')->paginate(13);
           
        }

    elseif ($query == '' && $start != '' && $end == '' ) 
    {
        $data = table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $cur_date])->orderBy('timein', 'desc')->paginate(13);
            
        }
    
    elseif ($query == '' && $start != '' && $end != '' ) 
    {
        $data = table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->orderBy('timein', 'desc')->paginate(13);
        }
    elseif ($query == '' && $start == '' && $end != '' ) 
    {
        $data = table::attendance()->where('branch_id',$branch_id)->where('date','<=', $end)->orderBy('timein', 'desc')->paginate(13);
        }
    elseif ($query != '' && $start != '' && $end != '' ) 
    {
        $data = table::attendance()->where('branch_id',$branch_id)->where('employee', 'like', '%'.$query.'%')->whereBetween('date', [$start, $end])->orderBy('timein', 'desc')->paginate(13);
        }

    else
    {
        $data = table::attendance()->where('branch_id',$branch_id)->orderBy('timein', 'desc')->paginate(13);
    

	}

    return view('admin.attendance-table', compact('data'))->render();
}




    // The following controller fucntion allows
    // admin to edit attendace information of any employee.
    public function edit($id, Request $request)
    {
       if (permission::permitted('attendance-edit')=='fail'){ return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;

        $a = table::attendance()->where('branch_id',$branch_id)->where('id', $id)->first();
        $e_id = ($a->id == null) ? 0 : Crypt::encryptString($a->id) ;
        $title = "attendances";
        return view('admin.edits.edit-attendance', compact('a','title', 'e_id'));
    }

    // The delete function allows admin to delete
    // attendace information for any specific employee.
    // Takes an attendace id as an argument.
    public function delete($id)
    {
       if (permission::permitted('attendance-delete')=='fail'){ return redirect()->route('denied'); }
    //     if($request->sh == 2){return redirect()->route('attendance');}
        $branch_id = Auth::user()->branch_id;
       
      
        table::attendance()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('attendance')->with('success', 'Attendance Deleted Successfully!');
    }

    // Allow admin to update attendace information.
    public function update(Request $request)
    {
       if (permission::permitted('attendance-edit')=='fail') { return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('attendance');}
        $branch_id = Auth::user()->branch_id;

        $v = $request->validate([
            'id' => 'required|max:200',
            'idno' => 'required|max:100',
            'timein' => 'required|max:15',
            'timeout' => 'required|max:15',
            'reason' => 'required|max:255',
        ]);

        $id = Crypt::decryptString($request->id);
        $idno = $request->idno;
        $timeIN = date("Y-m-d h:i:s A", strtotime($request->timein_date." ".$request->timein));
        $timeOUT = date("Y-m-d h:i:s A", strtotime($request->timeout_date." ".$request->timeout));
        $reason = $request->reason;
        $reference=table::attendance()->where('branch_id',$branch_id)->where('id', $id)->select('reference')->first();
      

        // $sched_in_time = table::schedules()->where([
        //     ['reference', '=', $reference],
        //     ['archive', '=', '0'],
        // ])->value('intime');
        $assigned_schedule_id = table::schedules()->where('branch_id',$branch_id)->where([['reference', $reference->reference],['active_status', 1]] )->value('schedule_id');
        
        $schedule_template = table::sch_template()->where('branch_id',$branch_id)->where('id', $assigned_schedule_id)->first();

        $today = Carbon::now();
        $day = strtolower($today->isoFormat('dddd'));

        $day_today = $schedule_template->$day;
        if ($day_today) {
          $str_arr = explode ("-", $day_today);
          $in_time = $str_arr[0];
          $out_time = $str_arr[1];
        }else{
          $in_time = NULL;
          $out_time = NULL;
        }

        if($in_time == null)
        {
            $status_in = "Ok";
        } else {
            $sched_clock_in_time_24h = date("H.i", strtotime($in_time));
            $time_in_24h = date("H.i", strtotime($timeIN));

            if ($time_in_24h <= $sched_clock_in_time_24h)
            {
                $status_in = 'In Time';
            } else {
                $status_in = 'Late Arrival';
            }
        }

        // $sched_out_time = table::schedules()->where([
        //     ['reference', '=', $reference],
        //     ['archive','=','0'],
        // ])->value('outime');

        if($out_time == null)
        {
            $status_out = "Ok";
        } else {
            $sched_clock_out_time_24h = date("H.i", strtotime($out_time));
            $time_out_24h = date("H.i", strtotime($timeOUT));

            if($time_out_24h >= $sched_clock_out_time_24h)
            {
                $status_out = 'On Time';
            } else {
                $status_out = 'Early Departure';
            }
        }

        $t1 = Carbon::createFromFormat("Y-m-d h:i:s A", $timeIN);
        $t2 = Carbon::createFromFormat("Y-m-d h:i:s A", $timeOUT);
        $th = $t1->diffInHours($t2);
        $tm = floor(($t1->diffInMinutes($t2) - (60 * $th)));
        $totalhour = $th.".".$tm;

        table::attendance()->where('branch_id',$branch_id)->where('id', $id)->update([
            'timein' => $timeIN,
            'timeout' => $timeOUT,
            'comment' => $reason,
            'totalhours' => $totalhour,
            'status_timein' => $status_in,
            'status_timeout' => $status_out,
        ]);

        return redirect('attendance')->with('success','Employee Attendance has been updated!');
    }

    // The following controller function allow admin to view the attendance
    // information in details on a modal.
    public function details(Request $request){

       if (permission::permitted('attendance-edit')=='fail') { return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;

        $attendanceID = request()->attendanceID;
        $theAttendance = table::attendance()->where('branch_id',$branch_id)->where('id', $attendanceID)->first();

        if ($theAttendance) {
          $all_breaks = table::daily_breaks()->where('branch_id',$branch_id)->where([['reference', $theAttendance->reference],['attendance_id', $theAttendance->id]])->get();
          if (empty($all_breaks)) {
            return response()-json(NULL);
          }else {
            return response()->json( $all_breaks);
          }
        }else {
          return response()->json(NULL);
        }



    }

}
