<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ExportsController extends Controller
{

	function company(Request $request)
	{
		if (permission::permitted('company')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('company');}

		$date = date('Y-m-d');
        $time = date('h-i-sa');
		$file = 'companies-'.$date.'T'.$time.'.csv';
		$branch_id = Auth::user()->branch_id;

		$c = table::company()->where('branch_id',$branch_id)->get();

		Storage::put($file, '', 'private');

		foreach ($c as $d)
		{
		    Storage::prepend($file, $d->id .','. $d->company);
		}

		Storage::prepend($file, '"ID"' .','. 'COMPANY');

		return Storage::download($file);
    }

	function department(Request $request)
	{
		if (permission::permitted('departments')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('department');}
		$branch_id = Auth::user()->branch_id;

		$d = table::department()->where('branch_id',$branch_id)->get();

		$date = date('Y-m-d');
        $time = date('h-i-sa');
		$file = 'departments-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($d as $i)
		{
		    Storage::prepend($file, $i->id .','. $i->department);
		}

		Storage::prepend($file, '"ID"' .','. 'DEPARTMENT');

		return Storage::download($file);
    }

	function jobtitle(Request $request)
	{
		if (permission::permitted('jobtitles')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('jobtitle');}
		$branch_id = Auth::user()->branch_id;

		$j = table::jobtitle()->join('department','department.id','=','jobtitle.dept_code')->where('department.branch_id',$branch_id)->select( 'jobtitle.id','jobtitle.jobtitle','department.department')->get();

		$date = date('Y-m-d');
        $time = date('h-i-sa');
		$file = 'jobtitles-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($j as $d)
		{
		    Storage::prepend($file, $d->id .','. $d->jobtitle .','. $d->department);
		}

		Storage::prepend($file, '"ID"' .','. 'DEPARTMENT' .','. 'DEPARTMENT');

		return Storage::download($file);
    }

	function leavetypes(Request $request)
	{
		if (permission::permitted('leavetypes')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('leavetype');}
		$branch_id = Auth::user()->branch_id;

		$l = table::leavetypes()->where('branch_id',$branch_id)->get();

		$date = date('Y-m-d');
        $time = date('h-i-sa');
		$file = 'leavetypes-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($l as $d)
		{
		    Storage::prepend($file, $d->id .','. $d->leavetype .','. $d->limit .','. $d->percalendar);
		}

		Storage::prepend($file, '"ID"' .','. 'LEAVE TYPE' .','. 'LIMIT' .','. 'TYPE');

		return Storage::download($file);
    }

	function employeeList()
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$today = date('Y-m-d');
		$branch_id = Auth::user()->branch_id;

		$p = table::people()->where('branch_id',$branch_id)->get();
		
		$date = date('Y-m-d');
        $time = date('h-i-sa');
		$file = 'employee-lists-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($p as $d)
		{
			$formatted_dt1=Carbon::parse($today);

           $formatted_dt2=Carbon::parse($d->birthday);

            $days=$formatted_dt1->diffInDays($formatted_dt2);
			
			$age = (int)($days/365);

			
		    Storage::prepend($file, $d->id .','. $d->lastname.' '.$d->firstname.' '.$d->mi .','.$age .','. $d->gender .','. $d->civilstatus .','. $d->mobileno .','. $d->emailaddress .','. $d->employmenttype .','. $d->employmentstatus);
		}

		Storage::prepend($file, '"ID"' .','. 'EMPLOYEE' .','. 'AGE' .','. 'GENDER' .','. 'CIVILSTATUS' .','. 'MOBILE NUMBER' .','. 'EMAIL ADDRESS' .','. 'EMPLOYMENT TYPE' .','. 'EMPLOYMENT STATUS');

		return Storage::download($file);
	}





	
	// Employee attendance report CSV file export function.
	function attendanceReport(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		// $id =(int)$request->emp_id;

		$query = $request->employee;
		$start = $request->start;
		$end = $request->end;
		
// dd($request->all());
		$cur_date = date('Y-m-d');
		$branch_id = Auth::user()->branch_id;
       

		if($query !== null AND $start == null AND $end == null )
		{
		 	$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->select('id','idno', 'date', 'employee', 'status_timein','status_timeout','timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
			
		} elseif ($query !== null AND $start !== null AND $end !== null) {
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $end])->select('id','idno', 'date', 'status_timein','status_timeout','employee', 'timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
			
		} elseif ($query == null AND $start !== null AND $end !== null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->select('id','idno', 'date', 'employee', 'status_timein','status_timeout','timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
			
		}
		
	    elseif ($query == null AND $start == null AND $end !== null) {
		$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('date','<', $end)->select('id','idno', 'date', 'employee', 'status_timein','status_timeout','timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
		
	    }
        elseif ($query == null AND $start !== null AND $end == null) {
	    $employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $cur_date])->select('id','idno', 'date', 'employee', 'status_timein','status_timeout','timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
	
		}
		elseif ($query !== null AND $start == null AND $end !== null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->where('date','<=', $end)->select('id','idno', 'date', 'employee','status_timein','status_timeout', 'timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
			
		}
		elseif ($query !== null AND $start !== null AND $end == null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $cur_date])->select('id','idno', 'date', 'employee','status_timein','status_timeout', 'timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
		
		}
		else{
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->select('id','idno', 'date', 'employee', 'status_timein','status_timeout','timein', 'timeout', 'totalhours','overtime_mins')->orderBy('date', 'DESC')->get();
			
		}



		$date = date('Y-m-d');
		$time = date('h-i-sa');

		$file = 'attendance-reports-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');
	

		foreach ($employeeAttendance as $d)
		{
			
			$b = str_replace( ',', '', $d->employee );
			if($d->totalhours != Null){
			$time = explode('.',$d->totalhours);
			if($time[0] == 0){
			   $th = $time[1] .' '. 'min';
			}
			elseif($time[1] == 0){
			   $th = $time[0] .' '. 'hr';
			}
			elseif($time[0] != 0 && $time[1] != 0){
			   $th = $time[0] .' '. 'hr' .' '.$time[1] .' '. 'min';
			}
			}
			else{
			  $th = 'N/A';
			}
			Storage::prepend($file, $d->idno .','. date('d-m-Y', strtotime($d->timein)) .','. $b .','. date('H:i:s', strtotime($d->timein)) .','. date('H:i:s', strtotime($d->timeout)) .','. $th .','. $d->overtime_mins .','. $d->status_timein .','. $d->status_timeout);
	
		}

		// dd($file);
		Storage::prepend($file, '"ID NO"' .','. 'DATE' .','. 'EMPLOYEE' .','. 'TIME IN' .','. 'TIME OUT' .','. 'TOTAL HOURS' .','. 'Over Time' .','. 'STATUS-IN' .','. 'STATUS-OUT');

		return Storage::download($file);

		






		// $name = $request->smartsearch;
		// $datefrom = $request->datefrom;
		// $dateto = $request->dateto;

	

		// return redirect('reports/employee-attendance')->with('error', 'Whoops! Please provide date range or select employee.');
	}

	function leavesReport(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$id = $request->emp_id;
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;
		$cur_date = date('Y-m-d');
		$branch_id = Auth::user()->branch_id;

		if ($id == null AND $datefrom == null AND $dateto == null)
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->orderby('leaves.id','desc')->get();
				   
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}

		elseif ($id !== null AND $datefrom !== null AND $dateto !== null)
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->whereBetween('leaves.leavefrom', [$datefrom, $dateto])
				   ->orderby('leaves.id','desc')->get();
			
			
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}

		elseif($id !== null AND $datefrom == null AND $dateto == null )
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->orderby('leaves.id','desc')->get();

			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}
		elseif($id !== null AND $datefrom !== null AND $dateto == null )
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->whereBetween('leaves.leavefrom', [$datefrom, $cur_date])
				   ->orderby('leaves.id','desc')->get();

			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}
		elseif($id !== null AND $datefrom == null AND $dateto !== null )
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->where('leaves.leavefrom','<=',$dateto)
				   ->orderby('leaves.id','desc')->get();

			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}

		elseif ($id == null AND $datefrom !== null AND $dateto !== null)
		{

			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->whereBetween('leaves.leavefrom', [$datefrom, $dateto])
				   ->orderby('leaves.id','desc')->get();
		
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}
		elseif ($id == null AND $datefrom !== null AND $dateto == null)
		{

			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->whereBetween('leaves.leavefrom', [$datefrom, $cur_date])
				   ->orderby('leaves.id','desc')->get();
		
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}
		elseif ($id == null AND $datefrom == null AND $dateto !== null)
		{

			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('leaves.leavefrom','<=' ,$dateto)
				   ->where('people.branch_id',$branch_id)
				   ->orderby('leaves.id','desc')->get();
		
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}
		else
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->orderby('leaves.id','desc')->get();
				   
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'leave-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				$employee = "$d->firstname"." "."$d->lastname";
				Storage::prepend($file, $d->id .','. $d->idno .','. '"'.$employee.'"' .','. $d->type .','. $d->leavefrom .','. $d->leaveto .','. $d->reason .','. $d->status);
			}

			Storage::prepend($file, '"ID"' .','. 'IDNO' .','. 'EMPLOYEE' .','. 'TYPE' .','. 'LEAVE FROM' .','. 'LEAVE TO' .','. 'REASON' .','. 'STATUS');

			return Storage::download($file);
		}


		return redirect('reports/employee-leaves')->with('error', 'Whoops! Please provide date range or select employee.');
	}

	function birthdaysReport()
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;

		$c = DB::table('people')
		->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
		->join('department', 'department.id', '=', 'people.department_id')
		->select('people.idno','people.firstname','people.lastname','people.mi', 'jobtitle.jobtitle', 'department.department','people.mobileno','people.birthday')
		->where('people.branch_id',$branch_id)
		->orderby('people.firstname')->get();

		$date = date('Y-m-d');
		$time = date('h-i-sa');
		$file = 'employee-birthdays-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($c as $d)
		{
		    Storage::prepend($file, $d->idno .','. $d->lastname.' '.$d->firstname.' '.$d->mi .','. $d->department .','. $d->jobtitle .','. $d->birthday .','. $d->mobileno);
		}

		Storage::prepend($file, '"ID"' .','. 'EMPLOYEE NAME' .','. 'DEPARTMENT' .','. 'POSITION' .','. 'BIRTHDAY' .','. 'MOBILE NUMBER' );

		return Storage::download($file);
	}

	function accountReport()
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;

		$u = table::people()->where('branch_id',$branch_id)->get();

		$date = date('Y-m-d');
		$time = date('h-i-sa');
		$file = 'employee-accounts-'.$date.'T'.$time.'.csv';

		Storage::put($file, '', 'private');

		foreach ($u as $a)
		{
			if($a->acc_type == 2)
			{
				$a_type = 'Admin';
			} else {
				$a_type = 'Employee';
			}

			if($a->status == 1)
			{
				$status = 'Active';
			} else {
				$a_type = 'Irregular';
			}
			Storage::prepend($file, $a->firstname .','. $a->emailaddress .','. $a_type.','. $status);
		
		}
		Storage::prepend($file, 'EMPLOYEE NAME' .','. 'EMAIL' .','. 'ACCOUNT TYPE'.','. 'STATUS');

		return Storage::download($file);
	}

	function scheduleReport(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$id = $request->emp_id;
		$branch_id = Auth::user()->branch_id;

		if ($id == null)
		{
			$data = table::schedules()->where('branch_id',$branch_id)->get();
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'schedule-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				Storage::prepend($file, $d->idno .',"'. $d->employee .'",'. $d->intime .','. '"'.$d->outime.'"' .','. $d->datefrom .','. $d->dateto .','. $d->hours .',"'. $d->restday .'",'. $d->archive);
			}

			Storage::prepend($file, '"IDNO"' .','. 'EMPLOYEE' .','. 'START TIME' .','. 'OFF TIME' .','. 'DATE FROM' .','. 'DATE TO' .','. 'HOURS' .','. 'RESTDAY' .','. 'STATUS');

			return Storage::download($file);
		}

		if ($id !== null)
		{
			$data = table::schedules()->where('branch_id',$branch_id)->where('idno', $id)->get();
			$date = date('Y-m-d');
			$time = date('h-i-sa');
			$file = 'schedule-reports-'.$date.'T'.$time.'.csv';

			Storage::put($file, '', 'private');

			foreach ($data as $d)
			{
				Storage::prepend($file, $d->idno .',"'. $d->employee .'",'. $d->intime .','. '"'.$d->outime.'"' .','. $d->datefrom .','. $d->dateto .','. $d->hours .',"'. $d->restday .'",'. $d->archive);
			}

			Storage::prepend($file, '"IDNO"' .','. 'EMPLOYEE' .','. 'START TIME' .','. 'OFF TIME' .','. 'DATE FROM' .','. 'DATE TO' .','. 'HOURS' .','. 'RESTDAY' .','. 'STATUS');

			return Storage::download($file);
		}

		return redirect('reports/employee-schedule')->with('error', 'Whoops! Please select employee.');
	}

}
