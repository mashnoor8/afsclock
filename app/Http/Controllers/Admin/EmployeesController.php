<?php

namespace App\Http\Controllers\admin;

use App\EmployeeFace;
use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Company;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Employee
{

	public $id = '';
	public $idno = '';
	public $name = '';
	public $company = '';
	public $department = '';
	public $jobtitle = '';
	public $status = '';

	public function __construct($id, $idno, $name, $company, $department, $jobtitle, $status)
	{
		$this->id = $id;
		$this->idno = $idno;
		$this->name = $name;
		$this->company = $company;
		$this->department = $department;
		$this->jobtitle = $jobtitle;
		$this->status = $status;
	}
}

class EmployeesController extends Controller
{

	// Finds all employee data and displays them in the view file.
	public function index()
	{
		if (permission::permitted('employees') == 'fail') {
			return redirect()->route('denied');
		}

		$branch_id = Auth::user()->branch_id;

		$emp_typeR = table::people()
		    ->where('branch_id',$branch_id)
			->where('employmenttype', 'Regular')
			->where('employmentstatus', 'Active')
			->count();

		$roles = table::roles()->get();

		$emp_typeT = table::people()
		    ->where('branch_id',$branch_id)
			->where('employmenttype', 'Trainee')
			->where('employmentstatus', 'Active')
			->count();

		$emp_genderM = table::people()
	     	->where('branch_id',$branch_id)
			->where('gender', 'Male')
			->count();

		$emp_genderR = table::people()
		    ->where('branch_id',$branch_id)
			->where('gender', 'Female')
			->count();

		$emp_allActive = table::people()
		    ->where('branch_id',$branch_id)
			->where('employmentstatus', 'Active')
			->count();

		$emp_allArchive = table::people()
		    ->where('branch_id',$branch_id)
			->where('employmentstatus', 'Archive')
			->count();
		$departments = table::department()
			->where('branch_id',$branch_id)
			->get();
	
		$jobtitles = table::jobtitle()
			->where('branch_id',$branch_id)
			->get();
			// dd($departments, $jobtitles);
		$data = DB::table('people')
			->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
			->join('department', 'department.id', '=', 'people.department_id')
			->join('company', 'company.id', '=', 'people.company_id')
			->select('people.firstname', 'people.lastname', 'people.mi', 'people.employmentstatus', 'people.idno', 'people.status', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
			->where('people.role_id', 1)
			->where('people.branch_id',$branch_id)
			->orderby('people.firstname')->paginate(12);
		

		$emp_file = table::people()->where('branch_id',$branch_id)->count();


		if ($emp_allArchive != null or $emp_allActive != null or $emp_allArchive >= 1 or $emp_allActive >= 1) {
			$number1 = $emp_allArchive / $emp_allActive * 100;
		} else {
			$number1 = null;
		}

		$title = "employees";
		return view('admin.employees', compact('data', 'title', 'emp_typeR', 'emp_typeT', 'emp_genderM', 'emp_genderR', 'emp_allActive', 'emp_file', 'emp_allArchive', 'roles', 'departments','jobtitles'));
	}

	// Following controller function prepare company employee related information
	// and send them to the view to be displayed in the add new employee form.
	public function new()
	{
		if (permission::permitted('employees-add') == 'fail') {
			return redirect()->route('denied');
		}
		$branch_id = Auth::user()->branch_id;


		// check the peopl employee limit 
		$branch_owner = DB::table('branches')->where('id',$branch_id)->value('owner_id');
		$subsciption = DB::table('user_subscriptions')->where('user_id',$branch_owner)->where('status','active')->orderby('created_at','Desc')->first();
		$package_limit = DB::table('packages')->where('id',$subsciption->package_id)->value('limit');
		$limit= explode("-",$package_limit);
		$limit_upper = (int)$limit[1]; 
		
		$super_admin_id =DB::table('people')
					->join('branches', 'people.id' ,'=' , 'branches.owner_id')
					->where('branches.id', $branch_id)
					->value('people.id');

        $employee_number = people_number($super_admin_id) ;
		
 		if( $employee_number >= $limit_upper){
			return redirect()->back()->with('error', 'You have exceeded your employee limit. Please upgrade your package to increase employee limit.'); 
		}

		$employees = table::people()->where('branch_id',$branch_id)->get();
		$company = table::company()->where('branch_id',$branch_id)->first();
		// dd($company);
		$department = table::department()->where('branch_id',$branch_id)->get();
		$jobtitle = table::jobtitle()->where('branch_id',$branch_id)->get();
		$leavegroup = table::leavetypes()->where('branch_id',$branch_id)->get();
		
	
		$roles = table::roles()->whereIn('id', [2,1] )->get();

		return view('admin.new-employee', compact('roles', 'company', 'department', 'jobtitle', 'employees', 'leavegroup'));
	}

	//
	public function faceregistration($id)
	{
		$branch_id = Auth::user()->branch_id;
		$employee_photos = table::employee_faces()->where('branch_id',$branch_id)->where('reference', $id)->get();
		// dd($employee_photos);
		return view('admin.employee-face-registration', compact('id', 'employee_photos'));
	}

	public function employee_search(Request $request)
	{
		$query = $request->get('query');
		$department = $request->get('department');
		$job = $request->get('job');
		$branch_id = Auth::user()->branch_id;
		
		if($query != Null){
		$ids =  DB::table('people')->where('firstname', 'like', '%' . $query . '%')->orWhere('mi', 'like', '%' . $query . '%')->orWhere('lastname', 'like', '%' . $query . '%')->where('branch_id','=' ,$branch_id)->pluck('id');
		}
		
		

		//dd($query , $department ,$job);
		if ($query == Null && $department == "all" && $job == "all" ) {
		
			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname','people.lastname', 'people.mi', 'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->where('people.branch_id','=' ,$branch_id)
				->orderby('people.idno')->paginate(12);
		} elseif ($query != Null && $department == "all" && $job == "all" ) {

			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->whereIn('people.id', $ids)
				->where('people.branch_id',$branch_id)
				->orderby('people.idno')->paginate(12);
		} elseif($query == Null && $department != "all" && $job == "all" ){
			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->where('people.branch_id',$branch_id)
				->where('department.department',$department)
				->orderby('people.idno')->paginate(12);

		}elseif ($query != Null && $department != "all" && $job == "all" ) {

			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->whereIn('people.id', $ids)
				->where('people.branch_id',$branch_id)
				->where('department.department',$department)
				->orderby('people.idno')->paginate(12);
		} elseif($query == Null && $department == "all" && $job != "all" ){
			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->where('people.branch_id',$branch_id)
				->where('jobtitle.jobtitle',$job)
				->orderby('people.idno')->paginate(12);

		}elseif ($query != Null && $department == "all" && $job != "all" ) {

			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->whereIn('people.id', $ids)
				->where('people.branch_id',$branch_id)
				->where('jobtitle.jobtitle',$job)
				->orderby('people.idno')->paginate(12);
		}elseif($query == Null && $department != "all" && $job != "all" ){
			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->where('people.branch_id',$branch_id)
				->where('jobtitle.jobtitle',$job)
				->where('department.department',$department)
				->orderby('people.idno')->paginate(12);

		}elseif ($query != Null && $department != "all" && $job != "all" ) {

			$data = DB::table('people')
				->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
				->join('department', 'department.id', '=', 'people.department_id')
				->join('company', 'company.id', '=', 'people.company_id')
				->select('people.firstname', 'people.lastname', 'people.mi',  'people.idno', 'people.employmentstatus', 'people.id', 'jobtitle.jobtitle', 'department.department', 'company.company', 'people.mobileno', 'people.birthday')
				->where('people.acc_type', 1)
				->whereIn('people.id', $ids)
				->where('people.branch_id',$branch_id)
				->where('jobtitle.jobtitle',$job)
				->where('department.department',$department)
				->orderby('people.idno')->paginate(12);
		}


		return view('admin.employee-table', compact('data'))->render();
	}



	public function employeeImages($id)
	{
		$branch_id = Auth::user()->branch_id;

		$all_photos = table::employee_faces()->where('branch_id',$branch_id)->where('reference', $id)->count();
		if ($all_photos > 1) {
			$employee_photos = table::employee_faces()->where('branch_id',$branch_id)->where('reference', $id)->latest()->first();
			return response()->json($employee_photos);
		} elseif ($all_photos == 1) {
			$employee_photos = table::employee_faces()->where('branch_id',$branch_id)->where('reference', $id)->first();
			return response()->json($employee_photos);
		}
	}

	public function deleteImage($id)
	{
		$branch_id = Auth::user()->branch_id;

		$image = table::employee_faces()->where('branch_id',$branch_id)->delete($id);
		return back()->with('success', 'Image deleted successfully!');
	}


	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	// Face Registration Controller Function.
	public function registerface($id, Request $request)
	{

		$image_name = $this->generateRandomString() . ".jpg";
		$img_data = $request->img_data;
		$img_data = str_replace('data:image/jpeg;base64,', '', $img_data);
		$img_data = str_replace(' ', '+', $img_data);

		$img_data = base64_decode($img_data);

		$destinationPath = public_path() . '/assets/faces/' . $image_name;
		file_put_contents($destinationPath, $img_data);
		$employeeFace = new EmployeeFace();
		$employeeFace->reference = $id;
		$employeeFace->branch_id = Auth::user()->branch_id;
		$employeeFace->image_name = $image_name;
		$employeeFace->save();
		$msg = "This is a simple message.";
		return response()->json(array('msg' => $msg), 200);
	}

	// Add the employee to the database.
	// Data received from the add employee table and
	// validates them before storing in the database.
	public function add(Request $request)
	{
		

		if (permission::permitted('employees-add') == 'fail') {
			return redirect()->route('denied');
		}
		$data = $request->all();
		
		// dd(  $data);

		$v = $request->validate([
			'lastname' => 'required|alpha_dash_space|max:155',
			'firstname' => 'required|alpha_dash_space|max:155',
			// 'mi' => 'required|alpha_dash_space|max:155',
			// 'age' => 'required|digits_between:0,199|max:3',
			// 'gender' => 'required|alpha|max:155',
			'emailaddress' => ['required', 'unique:people,emailaddress'],
			// 'civilstatus' => 'required|alpha|max:155',
			// 'height' => 'required|digits_between:0,299|max:3',
			// 'weight' => 'required|digits_between:0,999|max:3',
			// 'mobileno' => 'required|max:155',
			// 'birthday' => 'required|date|max:155',
			// 'nationalid' => 'required|max:155',
			// 'birthplace' => 'required|max:255',
			// 'homeaddress' => 'required|max:255',
			// 'company' => 'required|alpha_dash_space|max:100',
			// 'department' => 'required|alpha_dash_space|max:100',
			// 'jobposition' => 'required|alpha_dash_space|max:100',
			'companyemail' => ['required', 'unique:people,companyemail'],
			// 'leaveprivilege' => 'required|max:155',
			'idno' => 'required|max:155',
			// 'employmenttype' => 'required|alpha_dash_space|max:155',
			'employmentstatus' => 'required|alpha_dash_space|max:155',
			// 'startdate' => 'required|date|max:155',
			// 'dateregularized' => 'required|date|max:155'
			// 'password' => 'required|min:8|max:100|confirmed',
			'password' => 'required||confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',

			'status' => 'required|boolean|max:1',
		]);




		
		$branch_id = Auth::user()->branch_id;

		if ($request->password != $request->password_confirmation) {
			return redirect('employees')->with('error', 'Whoops! Password confirmation does not match!');
		}

		$is_user_exist = table::people()->where('branch_id',$branch_id)->where('companyemail', $request->companyemail)->whereNotNull('password')->first();

		if ($is_user_exist) {
			return redirect('employees')->with('error', 'Whoops! this user already exist');
		}

		$lastname = mb_strtoupper($request->lastname);
		$firstname = mb_strtoupper($request->firstname);
		$mi = mb_strtoupper($request->mi);

		$gender = mb_strtoupper($request->gender);
		$emailaddress = mb_strtolower($request->emailaddress);
		$civilstatus = mb_strtoupper($request->civilstatus);

		$mobileno = $request->mobileno;
		$birthday = date("Y-m-d", strtotime($request->birthday));
		$nationalid = mb_strtoupper($request->nationalid);
		$birthplace = mb_strtoupper($request->birthplace);
		$homeaddress = mb_strtoupper($request->homeaddress);
		$company = mb_strtoupper($request->company);
		$department = mb_strtoupper($request->department);
		$jobposition = mb_strtoupper($request->jobtitle);
		$companyemail = mb_strtolower($request->companyemail);
		$leaveprivilege = $request->leaveprivilege;
		$idno = mb_strtoupper($request->idno);
		$employmenttype = $request->employmenttype;
		$employmentstatus = $request->employmentstatus;
		$startdate = date("Y-m-d", strtotime($request->startdate));
		$dateregularized = date("Y-m-d", strtotime($request->dateregularized));
		$role = $request->role;
		$acc_type = $request->acc_type;
		$status = $request->status;
		$password = $request->password;
		$password_confirmation = $request->password_confirmation;




		// Creates department dynamically if does not exist in database.
		$existing_department = table::department()->where('branch_id',$branch_id)->where('department', $department)->first();
		if ($existing_department) {
			$department = $existing_department->id;
		} else {
			table::department()->insert(['branch_id'=>$branch_id,'department' => $department]);
			$new_department = table::department()->where('branch_id',$branch_id)->where('department', $department)->first();
			$department = $new_department->id;
		}


		// Creates company dynamically if does not exist in database.
		$existing_company = table::company()->where('branch_id',$branch_id)->where('company', $company)->first();
		if ($existing_company) {
			$company = $existing_company->id;
		} else {
			table::company()->insert(['branch_id'=>$branch_id,'company' => $company]);
			$new_company = table::company()->where('branch_id',$branch_id)->where('company', $company)->first();
			$company = $new_company->id;
		}


		// Creates jobtittle dynamically if does not exist in database.
		$existing_jobtitle = table::jobtitle()->where('branch_id',$branch_id)->where('jobtitle', $jobposition)->first();
		if ($existing_jobtitle) {
			$jobposition = $existing_jobtitle->id;
		} else {
			table::jobtitle()->insert(['branch_id'=>$branch_id,'jobtitle' => $jobposition, 'dept_code' => $department]);
			$new_jobtittle = table::jobtitle()->where('branch_id',$branch_id)->where('jobtitle', $jobposition)->first();
			$jobposition = $new_jobtittle->id;
		}


		$is_idno_taken = table::people()->where('branch_id',$branch_id)->where('idno', $idno)->exists();

		if ($is_idno_taken == 1) {
			return redirect('employees-new')->with('error', 'Whoops! the ID Number is already taken.');
		}

		$file = $request->file('image');
		if ($file != null) {

			$filename =$firstname .strtoupper(substr(md5(time()), 0, 6)). '.png';
            $resize_image = Image::make($file)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/images/faces/';
            $resize_image->save("$destinationPath" . "$filename");
			// $name = $request->file('image')->getClientOriginalName();
			// $resize_image = Image::make($file)->resize(320, 320);
			// $destinationPath = public_path() . '/assets/faces/';
			// $resize_image->save("$destinationPath" . "$name");
		} else {
			$filename = '';
		}
		
		$passport = $request->file('pass_image');
		if ($passport != null) {

			$pass_name ='Pass'.$firstname .strtoupper(substr(md5(time()), 0, 6)). '.png';
            $resize_image = Image::make($passport)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$pass_name");

		} else {
			$pass_name = '';
		}
		$drive_image = $request->file('drive_image');
		if ($drive_image != null) {

			$drive_image_name ='Drive'.$firstname .strtoupper(substr(md5(time()), 0, 6)). '.png';
            $resize_image = Image::make($drive_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$drive_image_name");
		
		} else {
			$drive_image_name = '';
		}
		$bpr_image = $request->file('bpr_image');
		if ($bpr_image != null) {

			$bpr_image_name ='BRP'.$firstname .strtoupper(substr(md5(time()), 0, 6)). '.png';
            $resize_image = Image::make($bpr_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$bpr_image_name");
			
		} else {
			$bpr_image_name = '';
		}

		$doc_image = $request->file('doc_image');
		if ($doc_image != null) {

			$doc_image_name ='Doc'.$firstname .strtoupper(substr(md5(time()), 0, 6)). '.png';
            $resize_image = Image::make($doc_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$doc_image_name");
			
		} else {
			$doc_image_name = '';
		}
// dd($filename, $pass_name,$drive_image_name,$bpr_image_name,$doc_image_name);
		$new_employee = table::people()->insert([
			[
				'branch_id' => $branch_id,
				'lastname' => $lastname,
				'firstname' => $firstname,
				'mi' => $mi,
				'gender' => $gender,
				'emailaddress' => $emailaddress,
				'civilstatus' => $civilstatus,
				'mobileno' => $mobileno,
				'birthday' => $birthday,
				'birthplace' => $birthplace,
				'nationalid' => $nationalid,
				'homeaddress' => $homeaddress,
				'employmenttype' => $employmenttype,
				'employmentstatus' => $employmentstatus,
				'avatar' => $filename,
				'company_id' => $company,
				'department_id' => $department,
				'job_title_id' => $jobposition,
				'companyemail' => $companyemail,
				'leaveprivilege' => $leaveprivilege,
				'idno' => $idno,
				'startdate' => $startdate,
				'dateregularized' => $dateregularized,
				'role_id' => $role,
				'acc_type' => $acc_type,
				'password' => Hash::make($password),
				'status' => $status,
				'passport_number' => $request->passport_number,
				'passport_issue_date' => $request->passport_issue_date,
				'passport_ex_date' => $request->passport_ex_date,
				'pass_image' => $pass_name,
				'd_license_number' => $request->d_license_number,
				'drive_issue_date' => $request->drive_issue_date,
				'drive_ex_date' => $request->drive_ex_date,
				'drive_image' => $drive_image_name,
				'bpr_number' => $request->bpr_number,
				'bpr_issue_date' => $request->bpr_issue_date,
				'bpr_ex_date' => $request->bpr_ex_date,
				'bpr_issue_date' => $request->bpr_issue_date,
				'bpr_image' => $bpr_image_name,
				'checked' => $request->checked,
				'current_status' => $request->current_status,
				'document_name' => $request->document_name,
				'document_issue_date' => $request->document_issue_date,
				'document_ex_date' => $request->document_ex_date,
				'document_image' => $doc_image_name,
				'comment' => $request->comment,
				'verified' => True,
				'created_at' => Carbon::now()
			],
		]);

		
		

		$refId = DB::getPdo()->lastInsertId();

		$notification = array(
			'message' => 'Employee has been added! Please assign a schedule for this employee otherwise no attendance will be counted for this employee.',
			'alert-type' => 'success'
		);

		return redirect('schedules')->with($notification);
	}
}
