<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\People;
use App\Task;
use Illuminate\Http\Request;
use App\Classes\table;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Classes\permission;
use Mail;
use Carbon\Carbon;
use DateTime;

class TasksHistory{

    public $id = '';
    public $datetime ='';
    public $reason = '';
    public $new_deadline = '';


    public function __construct($id, $datetime, $reason, $new_deadline)
    {
        $this->id = $id;
        $this->datetime = $datetime;
        $this->reason = $reason;
        $this->new_deadline = $new_deadline;
    }

}

class TasksController extends Controller
{
    public function index()
    {
        $branch_id = Auth::user()->branch_id;

        $data = Task::where('branch_id',$branch_id )->orderby('created_at','DESC')->paginate(12);

       

        // $data = DB::table('people')
		// ->join('tasks', 'tasks.reference', '=', 'people.id')
		// ->select('people.firstname','people.lastname','people.mi','tasks.reference','tasks.assigned_by', 'tasks.id','tasks.title','tasks.description','tasks.deadline','tasks.finishdate','tasks.comment','tasks.done_status','tasks.created_at')
		// ->orderby('tasks.created_at','desc')->paginate(12);

        $employee = People::where('branch_id',$branch_id)->where('role_id','!=','3')->get();

        $title = "task-manager";

        // dd( $employee , Auth::user()->id);

        return view('admin.taskmanager', compact('data', 'title','employee'));
    }

    public function edit($id)
    {
        $branch_id = Auth::user()->branch_id;

        $task_old = DB::table('tasks')
        ->join('branches', 'branches.id','=', 'tasks.branch_id')
        ->join('people','people.id','=','tasks.reference')
        ->leftjoin('task_extension', 'task_extension.task_id', '=', 'tasks.id')
        ->select('people.firstname','people.lastname','tasks.id','tasks.title','tasks.description','tasks.deadline','tasks.finishdate','tasks.comment','task_extension.new_deadline','tasks.done_status')
        ->where('tasks.branch_id',$branch_id )
        ->where('tasks.id','=',$id)->get();
        $task = $task_old[0];

        // $e_id = Crypt::encryptString($id);




        return view('admin.edits.edit-task', compact('task', 'id'));

    }

    

    public function view($id)
    {
        $branch_id = Auth::user()->branch_id;

        
        $task_old = DB::table('tasks')
        ->join('branches', 'branches.id','=', 'tasks.branch_id')
        ->leftjoin('people','people.id','=','tasks.reference')
        ->leftjoin('task_extension', 'task_extension.task_id', '=', 'tasks.id')
        ->select('people.firstname','people.lastname','people.mi', 'tasks.id','tasks.title','tasks.description','tasks.deadline','tasks.finishdate','tasks.comment','task_extension.new_deadline','tasks.done_status')
        ->where('tasks.id','=',$id)
        ->where('tasks.branch_id',$branch_id )->get();
        $task = $task_old[0];
        return view('admin.edits.view-task', compact('task'));

    }
    
	
	public function task_search(Request $request)
	{
        $branch_id = Auth::user()->branch_id;

        $query = $request->get('query');
        $situation = $request->get('situation');
        $owner = $request->get('owner');
        // dd( $query,$situation,$owner);
        if($query != Null)
        {
            $people_id = table::people()->where('branch_id',$branch_id )->where('firstname', 'like', '%'.$query.'%')
            ->orWhere('mi', 'like', '%'.$query.'%')
            ->orWhere('lastname', 'like', '%'.$query.'%')->value('id');
        }
        // dd($people_id);
        

        $admin_id = Auth::id();
        // dd($admin_id);
        //if query is not exist
		if($query == "" && $situation == "all" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->orderby('created_at','DESC')->paginate(12);

        } 
        elseif($query == "" && $situation == "all" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);
        	
        } 
        elseif($query == "" && $situation == "all" && $owner == "employees")
        {
           
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);
         	
        } 
        elseif($query == '' && $situation == "completed" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 1)->orderby('created_at','desc')->paginate(12);
         
        } 
        elseif($query == '' && $situation == "completed" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 1)->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);    
        } 
        elseif($query == '' && $situation == "completed" && $owner == "employees")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 1)->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);
        } 
        elseif($query == '' && $situation == "pending" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 0)->orderby('created_at','desc')->paginate(12);			
        } 
        elseif($query == '' && $situation == "pending" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 0)->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);

			
        } 
        elseif($query == '' && $situation == "pending" && $owner == "employees")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=', 0)->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);			
        }
// if query exist
        elseif($query != '' && $situation == "all" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->orderby('created_at','desc')->paginate(12);	
        } 
        elseif($query != '' && $situation == "all" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);

        } 
        elseif($query != '' && $situation == "all" && $owner == "employees")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);			
        } 
        elseif($query != '' && $situation == "completed" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 1)->orderby('created_at','desc')->paginate(12);			
        } 
        elseif($query != '' && $situation == "completed" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 1)->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);			
        } 
        elseif($query != '' && $situation == "completed" && $owner == "employees")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 1)->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);	
        } 
        elseif($query != '' && $situation == "pending" && $owner == "all")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 0)->orderby('created_at','desc')->paginate(12);		
        }
        elseif($query != '' && $situation == "pending" && $owner == "admin")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 0)->where('assigned_by','=', $admin_id)->orderby('created_at','desc')->paginate(12);
        } 
        elseif($query != '' && $situation == "pending" && $owner == "employees")
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->where('reference','=', $people_id)->where('done_status','=', 0)->where('assigned_by','!=', $admin_id)->orderby('created_at','desc')->paginate(12);
        }
         else 
        {
            $data = Task::where('reference', '!=', Auth::user()->id)->where('branch_id',$branch_id )->orderby('created_at','DESC')->paginate(12);
            
        }


		return view('admin.taskmanager-table', compact('data'))->render();


    }
    
    public function myTasks()
    {
        $branch_id = Auth::user()->branch_id;
       
        $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->paginate(12);
        $title="task-manager";
        return view('admin.admin-my-tasks', compact('title','data'));

    }
    public function markdone(Request $request)
    {
        if (permission::permitted('leaves-edit')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('leave');}
        $branch_id = Auth::user()->branch_id;

        $v = $request->validate([

            'comment' => 'required'
        ]);

       
       $id = $request->id;

        $task = Task::find($id);

        $task->comment = $request->comment;
        $task->finishdate = date('Y-m-d');
        $task->done_status = 1;
        $task->save();

        return redirect('admin/mytasks')->with('success','Task has been updated!');

    }
    public function mytasks_search(Request $request)
	{
        $branch_id = Auth::user()->branch_id;

        $query = $request->get('query');
        $situation = $request->get('situation');
    //    dd($query,$situation);
            if($query != Null)
                {
                    $people_id = table::people()->where('branch_id',$branch_id )
                    ->where('firstname', 'like', '%'.$query.'%')
                    ->orWhere('mi', 'like', '%'.$query.'%')
                    ->orWhere('lastname', 'like', '%'.$query.'%')->value('id');
                }

        if($query == "" && $situation == "all")
        {
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->paginate(12);

        } 
        elseif($query == "" && $situation == "completed")
        {
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=',1)->paginate(12);
        	
        } 
        elseif($query == "" && $situation == "pending")
        {
           
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=',0)->paginate(12);
         	
        } 
        elseif($query != "" && $situation == "all")
        {
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->where('assigned_by', $people_id)->paginate(12);

        } 
        elseif($query != "" && $situation == "completed")
        {
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->where('assigned_by', $people_id)->where('done_status','=',1)->paginate(12);	
        } 
        elseif($query != "" && $situation == "pending")
        {
           
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->where('done_status','=',0)->where('assigned_by', $people_id)->paginate(12);	
        } 
        else
        {
            $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->paginate(12);   
        }
        return view('admin.admin-my-tasks-table', compact('data'))->render();

    }
    public function extend_deadline($id){
        
        $task = Task::find($id);
        return view('admin.tasks-extend-deadline', compact('task'));
      }
  
  
      public function update_deadline(Request $request){
          
        $branch_id = Auth::user()->branch_id;
  
        $v = $request->validate([
            'reason' => 'required',
            'new_deadline' => 'required',
        ]);
  
        $task_id = $request->task_id;
        $new_deadline = $request->new_deadline;
        $reason = $request->reason;
  
  $exist = table::task_extension()->where('task_id',$task_id)->first();
      if($exist != Null){
      table::task_extension()->where('branch_id', $branch_id)->where('task_id',$task_id)->update(
          ['new_deadline' => $new_deadline, 'reason' => $reason, 'updated_at' => Carbon::now()]
      );
      }else{

      table::task_extension()->insert(
          ['branch_id' => $branch_id,'task_id' => $task_id, 'new_deadline' => $new_deadline, 'reason' => $reason, 'created_at' => Carbon::now()]
      );
      }
        
  
  
        return redirect('taskmanager')->with('success','New deadline for this task has been updated!');
      }


    public function update(Request $request)
    {
        $branch_id = Auth::user()->branch_id;

        
        $v = $request->validate([
            'id' => 'required|max:200',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date'
        ]);

       

        table::tasks()
        ->where('branch_id',$branch_id)
        ->where('id', $request->id)
        ->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'deadline' => $request->deadline,
                    'comment' => $request->comment
        ]);

        return redirect('taskmanager')->with('success','Task has been updated!');



    }

    public function delete($id)
    {
        $branch_id = Auth::user()->branch_id;


       Task::where('id', $id)->where('branch_id',$branch_id )->delete();

        return redirect('taskmanager')->with('success', 'Deleted!');
    }


    public function add(Request $request)
    {

        // dd($request->all());
        $branch_id = Auth::user()->branch_id;

        if (permission::permitted('schedules-add')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('schedule');}

        $v = $request->validate([
            'employee' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required',
        ]);

        $id = $request->employee;
        $assigned_by = Auth::id();
        
        $title = $request->title;
        $descriptopn = $request->description;
        $deadline = $request->deadline;
        $deadline_mail_datetime = new DateTime($deadline);
        $deadline_mail = $deadline_mail_datetime->format('Y-m-d');


        $task = new Task();
        $task->branch_id = $branch_id;
        $task->reference = $id;
        $task->assigned_by = $assigned_by;      
        $task->title = $title;
        $task->description = $descriptopn;
        $task->deadline = $deadline;
        $task->done_status = 0;

        $task->save();

        $assign_to_info = table::people()->where('branch_id',$branch_id )->where('id', '=', $id)->first();
        $assign_by_info = table::people()->where('branch_id',$branch_id )->where('id', '=', $assigned_by)->first();
        
        $assign_to = $assign_to_info->firstname." ".$assign_to_info->lastname;
        $assign_by = $assign_by_info->firstname." ".$assign_by_info->lastname;

        Mail::send('email.task', ['assigned_to' => $assign_to,'assigned_by' => $assign_by,'title' => $title, 'deadline' => $deadline ], function ($m) use ($assign_to_info) {
            $m->from('info@attendancekeeper.net', 'Attendance Keeper');

            $m->to($assign_to_info->companyemail, $assign_to_info->firstname)->subject('Task Reminder!');
        });
        
      

        return redirect('taskmanager')->with('success', 'Task assigned successfully!');
    }


    public function task_details($id){
     if (permission::permitted('dashboard')=='fail'){ return redirect()->route('denied'); }

      $branch_id = Auth::user()->branch_id;

      $task = Task::find($id);

      $task_history = collect([]);


        $extended_deadlines = table::task_extension()->where('branch_id',$branch_id )->where('task_id', $task->id)->get();

        $assigned_to = User::find($task->reference);
        $assigned_by = User::find($task->assigned_by);

        // dd($extended_deadlines);

        if ($extended_deadlines) {
          foreach ($extended_deadlines as $data) {
            $task_history->push(new TasksHistory($task->id, $data->created_at, $data->reason, $data->new_deadline));
          }
        }

        // dd($task_history);

      return view('admin.task_details', compact('task', 'task_history', 'assigned_to', 'assigned_by'));
    }

    function task_eligible($id){

        $task = Task::where('reference' , $id)->where('done_status', false)->count();
        $deadline = Task::where('reference' , $id)->where('done_status', false)->orderBy('deadline', 'DESC')->first();
        $date = date('d-M-Y' , strtotime($deadline->deadline )); 
        if($task >= 6){
            $response = [
                'status' => 1,
                'deadline' => $date,
                'task' => $task 
            ];
            
            return response()->json($response);   
        }
        else{
            // return true;
            return response()->json('0');
        }       
    }
}
