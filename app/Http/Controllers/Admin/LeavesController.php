<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class Leaves{

    public $id = '';
    public $employee ='';
    public $type = '';
    public $leavefrom = '';
    public $leaveto = '';
    public $returndate = '';
    public $comment = '';
    public $status = '';


    public function __construct($id, $employee, $type, $leavefrom,$leaveto,$returndate, $comment,$status )
    {
        $this->id = $id;
        $this->employee = $employee;
        $this->type = $type;
        $this->leavefrom = $leavefrom;
        $this->leaveto = $leaveto;
        $this->returndate = $returndate;
        $this->comment = $comment;
        $this->status = $status;
    }

}

class LeavesController extends Controller
{
    public function index()
    {
//        if (permission::permitted('leaves')=='fail'){ return redirect()->route('denied'); }

        $leaves_collection = collect([]);

		$branch_id = Auth::user()->branch_id;

        $employee = table::people()->where('branch_id',$branch_id)->get();
        

        $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('people.branch_id',$branch_id)
            ->orderby('people.firstname')->paginate(12);

        // foreach($leaves as $leave){
        //   $the_employee = User::find($leave->reference);
        //   $leaves_collection->push(new Leaves($leave->id, $the_employee->firstname." ".$the_employee->lastname, $leave->type, $leave->leavefrom, $leave->leaveto, $leave->returndate, $leave->comment,$leave->status));
        // }

        $leave_types = table::leavetypes()->where('branch_id',$branch_id)->get();
        $title = "leave";
        return view('admin.leaves', compact('employee', 'title', 'leave_types', 'data'));
    }

    public function edit($id, Request $request)
    {
//        if (permission::permitted('leaves-edit')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;

        $l = table::leaves()->where('branch_id',$branch_id)->where('id', $id)->first();
        $l->leavefrom = date('M d, Y', strtotime($l->leavefrom));
        $l->leaveto = date('M d, Y', strtotime($l->leaveto));
        $l->returndate = date('M d, Y', strtotime($l->returndate));
        $leave_types = table::leavetypes()->where('branch_id',$branch_id)->get();
        $e_id = ($l->id == null) ? 0 : Crypt::encryptString($l->id) ;
        $employee = User::find($l->reference);
        $emp_name = $employee->firstname." ".$employee->lastname;
        $title = "leave";
        return view('admin.edits.edit-leaves', compact('l', 'leave_types', 'e_id','emp_name', 'title'));
    }
    //leave_search
    public function leave_search(Request $request)
    {
        $query = $request->get('query');
        $situation = $request->get('situation');
		$branch_id = Auth::user()->branch_id;


		
		if(!$query  &&  $situation == 'all')
        {
          
            //  dd($query,$situation );
			$data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('people.branch_id',$branch_id)
            ->orderby('people.firstname')->paginate(12);

        } 
         elseif ($query  && $situation == 'all') 
        {
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('people.branch_id',$branch_id)
            ->where('people.firstname', 'like', '%'.$query.'%')
			->orWhere('people.mi', 'like', '%'.$query.'%')
			->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->orderby('people.firstname')->paginate(12);

			
            
        }
        elseif(!$query && $situation == 'Approved'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            // ->where('people.firstname', 'like', '%'.$query.'%')
			// ->orWhere('people.mi', 'like', '%'.$query.'%')
            // ->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->where('people.branch_id',$branch_id)
            ->where('leaves.status', 'like' , '%'.$situation.'%' )
            ->orderby('people.firstname')->paginate(12);
        }
        elseif(!$query && $situation == 'Pending'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            // ->where('people.firstname', 'like', '%'.$query.'%')
			// ->orWhere('people.mi', 'like', '%'.$query.'%')
            // ->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->where('people.branch_id',$branch_id)
            ->where('leaves.status', 'like' , '%'.$situation.'%' )
            ->orderby('people.firstname')->paginate(12);
        }
        elseif($query && $situation == 'Approved'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('leaves.status', '=' , 'Approved' )
            ->where('people.branch_id',$branch_id)
            ->where('people.firstname', 'like', '%'.$query.'%')
			->orWhere('people.mi', 'like', '%'.$query.'%')
            ->orWhere('people.lastname', 'like', '%'.$query.'%')
            // ->where('leaves.status', '=' , 'Approved' )
            ->orderby('people.firstname')->paginate(12);
        }
        elseif($query && $situation == 'Pending'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('leaves.status', '=' , 'Pending' )
            ->where('people.branch_id',$branch_id)
            ->where('people.firstname', 'like', '%'.$query.'%')
			->orWhere('people.mi', 'like', '%'.$query.'%')
            ->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->orderby('people.firstname')->paginate(12);
        }
        elseif(!$query && $situation == 'Decline'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            // ->where('people.firstname', 'like', '%'.$query.'%')
			// ->orWhere('people.mi', 'like', '%'.$query.'%')
            // ->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->where('people.branch_id',$branch_id)
            ->where('leaves.status', 'like' , '%'.$situation.'%' )
            ->orderby('people.firstname')->paginate(12);
        }
        elseif($query && $situation == 'Decline'){
            // dd($query,$situation );

            $data = DB::table('people')
			->join('leaves', 'leaves.reference', '=', 'people.id')
			->select('people.firstname','people.lastname','people.mi', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.comment','leaves.status')
            ->where('leaves.status', '=' , 'Declined' )
            ->where('people.branch_id',$branch_id)
            ->where('people.firstname', 'like', '%'.$query.'%')
			->orWhere('people.mi', 'like', '%'.$query.'%')
            ->orWhere('people.lastname', 'like', '%'.$query.'%')
            ->orderby('people.firstname')->paginate(12);
        }


		return view('admin.leaves-table', compact('data'))->render();

    }



    public function update(Request $request)
    {
//        if (permission::permitted('leaves-edit')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('leave');}
       
        $v = $request->validate([
            'id' => 'required|max:200',
            'status' => 'required|max:100',
            'comment' => 'max:255',
        ]);

        $id = Crypt::decryptString($request->id);
        $status = $request->status;
        $comment = mb_strtoupper($request->comment);
	$branch_id = Auth::user()->branch_id;
	
	if ($status == 'Approved') {
		    $leave = table::leaves()->where('id', $id)->first();
		    $start = new Carbon($leave->leavefrom);
		    $end = (new Carbon($leave->leaveto))->addDays(1);
		    // Find Schedule
		    $auth_schedule = table::schedules()->where('branch_id', Auth::user()->branch_id)->where('active_status','1')->where('reference', $leave->reference)->first();
		    if ($auth_schedule) {
			$auth_schedule_template = table::sch_template()->where('id', $auth_schedule->schedule_id)->first();
			$taken_leaves = 0;
			for ($i = $start; $i < $end; $i->addDays(1)) {
			    // check the leave day is holiday or  weekend or not
			    $cur_month_holidays_dates = table::holidays()->where('month', $i->format('m'))->select('dates')->value('dates');


			    if ($cur_month_holidays_dates != Null) {
				$cur_month_holidays = explode(",", $cur_month_holidays_dates);

				if (!in_array($i->format('d'), $cur_month_holidays)) {
				    $day = strtolower($i->format('l'));
				    // dd($auth_schedule_template->$day);
				    if ($auth_schedule_template->$day != Null) {
				        $taken_leaves += 1;
				    }
				}
			    } else {
				$day = strtolower($i->format('l'));
				if ($auth_schedule_template->$day != Null) {
				    $taken_leaves += 1;
				}
			    }
			    
			    
			}
			
			table::leaves()
			->where('branch_id', $branch_id)
			->where('id', $id)
			->update([
			    'status' => $status,
			    'comment' => $comment,
			    'leave_counter' => $taken_leaves
			]);
			
			 return redirect('/leaves')->with('success','Employee leave has been updated!');
		    } else {
		    return redirect('/leaves')->with('error','Schedule of this Employee does not exist!');
		    }
		} else {

		    table::leaves()
			->where('branch_id', $branch_id)
			->where('id', $id)
			->update([
			    'status' => $status,
			    'comment' => $comment
			]);
		}

        return redirect('/leaves')->with('success','Employee leave has been updated!');
    }


    public function delete($id, Request $request)
    {
//        if (permission::permitted('leaves-delete')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('leave');}
		$branch_id = Auth::user()->branch_id;

        table::leaves()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('leaves')->with('success','Deleted!');
    }

}
