<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\DB;
use DateTimeZone;
use DateTime;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;


class ReportsController extends Controller
{
	public function index()
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;
		
		$lastviews = table::reportviews()->where('branch_id',$branch_id)->get();
		$title = "reports";
    	return view('admin.reports',compact('title'), ['lastviews' => $lastviews]);
    }

	public function empList(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$branch_id = Auth::user()->branch_id;

		$today = date('M, d Y');
		$empList = table::people()->where('branch_id',$branch_id)->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 1)->update(['last_viewed' => $today]);
		$title = "reports";
		return view('admin.reports.report-employee-list', compact('title','empList'));
	}



	// Smart employee attendance report search.
	// Performs ajax request from "report employee attendance" view.
	public function employeeReportSearch(Request $request){
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;

		// Data provided in the search field
		$searchContent = request()->searchContent;
		$datefrom = request()->datefrom;
		$dateto = request()->dateto;
		$date_from = Carbon::parse($datefrom);
		$date_to = Carbon::parse($dateto);
		$date_to = $date_to->addDays(1);

		// If there exist search content data only.
		// Date range data are not available.
		if($searchContent == "" && $datefrom == "" && $dateto == ""){
			$searchResults = table::attendance()->where('branch_id',$branch_id)->get();
		}
		elseif($searchContent != "" && $datefrom == "" && $dateto == ""){
			$searchResults = table::attendance()->where('branch_id',$branch_id)->where('employee','LIKE','%'.$searchContent.'%')->orWhere('idno','LIKE','%'.$searchContent.'%')->get();
		}
		// When all the variable values available, executes this block.
		elseif ($searchContent !== "" && $datefrom !== "" && $dateto !== "") {
			$searchResults = table::attendance()->where('branch_id',$branch_id)->where('employee','LIKE','%'.$searchContent.'%')->whereBetween('timein', [$date_from, $date_to])->orWhere('idno','LIKE','%'.$searchContent.'%')->whereBetween('timein', [$datefrom, $dateto])->get();
		}
		// Else performs this block.
		else{
			$searchResults = table::attendance()->where('branch_id',$branch_id)->get();
		}

		return response()->json($searchResults);

	}

	
	public function empAtten(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		
		//if($request->sh == 2){return redirect()->route('reports');}

		$branch_id = Auth::user()->branch_id;

		$today = date('M, d Y');
		// $empAtten = table::attendance()->get();
		$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->paginate(15);
		$employee = table::people()->where('branch_id',$branch_id)->where('employmentstatus', 'Active')->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 2)->update(array('last_viewed' => $today));
		$title = "reports";
		return view('admin.reports.report-employee-attendance', compact('title','employeeAttendance', 'employee'));
	}






	public function attendance_search(Request $request)
	{
		$query = $request->get('query');
		$start = $request->get('start');
		$end = $request->get('end');
		$cur_date = date('Y-m-d');
		$branch_id = Auth::user()->branch_id;

		// dd($query);
        // if ($query == null AND $start == null AND $end == null)
		// {
		// 	$employeeAttendance = table::attendance()->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->paginate(10);
			
		// }

		if($query !== null AND $start == null AND $end == null )
		{
		 	$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		} elseif ($query !== null AND $start !== null AND $end !== null) {
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $end])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		} elseif ($query == null AND $start !== null AND $end !== null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}
		
	    elseif ($query == null AND $start == null AND $end !== null) {
		$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('date','<', $end)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
		
	    }
        elseif ($query == null AND $start !== null AND $end == null) {
	    $employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $cur_date])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
	
		}
		elseif ($query !== null AND $start == null AND $end !== null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->where('date','<=', $end)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}
		elseif ($query !== null AND $start !== null AND $end == null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $cur_date])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
		
		}
		else{
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}


		// return response()->json([$query, $start, $end]);

		return view('admin.reports.report-employee-attendance-table', compact('employeeAttendance'))->render();
	}






	public function attendance_search_by_name(Request $request)
	{
		
		$query = $request->get('query');
		$start = $request->get('start');
		$end = $request->get('end');
		$branch_id = Auth::user()->branch_id;

		// if($start == $end){
		// 	$carbon=Carbon::parse($end)->addDays(1);
		// 	$end=$carbon;
			
		// }
		
		// dd($query);
		$cur_date = date('Y-m-d');
        // if ($query == null AND $start == null AND $end == null)
		// {
		// 	$employeeAttendance = table::attendance()->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->paginate(10);
			
		// }
		// dd($query ,$start,$end);

		if($query !== null AND $start == null AND $end == null )
		{
		 	$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}
		 elseif ($query !== null AND $start !== null AND $end !== null) {
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $end])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		} 
		elseif ($query == null AND $start !== null AND $end !== null) {



			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}
		
	    elseif ($query == null AND $start == null AND $end !== null) {
		$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('date','<', $end)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
		
	    }
        elseif ($query == null AND $start !== null AND $end == null) {
			// dd($start);
	    $employeeAttendance= table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $cur_date])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
	
		}
		elseif ($query !== null AND $start == null AND $end !== null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->where('date','<=', $end)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}
		elseif ($query !== null AND $start !== null AND $end == null) {
			$employeeAttendance= table::attendance()->where('branch_id',$branch_id)->where('reference', $query)->whereBetween('date', [$start, $cur_date])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
		
		}
		else{
			$employeeAttendance = table::attendance()->where('branch_id',$branch_id)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours','overtime_mins')->paginate(15);
			
		}

		return view('admin.reports.report-employee-attendance-table', compact('employeeAttendance'))->render();
	}







	public function empLeaves(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$today = date('M, d Y');
		$branch_id = Auth::user()->branch_id;

		$employee = table::people()->where('branch_id',$branch_id)->where('employmentstatus', 'Active')->get();



		// $empLeaves = table::leaves()->get();
		// $i = 0;
		// for($i = 0 ; $i< count($empLeaves); $i++)
		// {
		// 	$emp = table::people()->where('id', $empLeaves[$i]->reference)->first();
		// 	$empLeaves[$i]->employee =  $emp->firstname . " " . $emp->lastname;
		// }
        $empLeaves = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->orderby('people.firstname')->get();

		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 3)->update(array('last_viewed' => $today));
		$title = "reports";
		return view('admin.reports.report-employee-leaves', compact('title','empLeaves', 'employee'));
	}

	public function empSched(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$branch_id = Auth::user()->branch_id;

		$today = date('M, d Y');
		$empSched = table::schedules()->where('branch_id',$branch_id)->orderBy('archive', 'ASC')->get();
		$employee = table::people()->where('branch_id',$branch_id)->where('employmentstatus', 'Active')->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 4)->update(array('last_viewed' => $today));
		$title = "reports";
		return view('admin.reports.report-employee-schedule', compact('title','empSched', 'employee'));
	}

	public function orgProfile(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$today = date('M, d Y');
		$branch_id = Auth::user()->branch_id;

		$ed = table::people()->where('branch_id',$branch_id)->where('employmentstatus', 'Active')->get();



		// $age_18_24 = table::people()->where([['age', '>=', '18'], ['age', '<=', '24']])->count();
		// $age_25_31 = table::people()->where([['age', '>=', '25'], ['age', '<=', '31']])->count();
		// $age_32_38 = table::people()->where([['age', '>=', '32'], ['age', '<=', '38']])->count();
		// $age_39_45 = table::people()->where([['age', '>=', '39'], ['age', '<=', '45']])->count();
		// $age_46_100 = table::people()->where('age', '>=', '46')->count();

		$age_18_24 = 0;
		$age_25_31 = 0;
		$age_32_38 = 0;
		$age_39_45 = 0;
		$age_46_100 = 0;



		for($i = 0; $i < count($ed); $i ++)
		{
			$now = new DateTime();
			$bday = new DateTime($ed[$i]->birthday);

			$difference = $now->diff($bday);

			$age = $difference->format("%y");

			if($age >= '18' && $age <= '24') $age_18_24++;
			else if($age >= '25' && $age <= '31') $age_25_31++;
			else if($age >= '32' && $age <= '38') $age_32_38++;
			else if($age >= '39' && $age <= '45') $age_39_45++;
			else if($age >= '46' && $age <= '100') $age_46_100++;

			//Set the company name for the employee
			if($ed[$i]->company_id != NULL)
			{
				$curr_comp = table::company()->where('branch_id',$branch_id)->where('id', $ed[$i]->company_id)->first();
				if($curr_comp)
				$ed[$i]->company = $curr_comp->company;
				else
				$ed[$i]->company = "N/A";
				
			}
			else
			{
				$ed[$i]->company = "NULL";
			}

			if($ed[$i]->department_id != NULL)
			{
				$curr_dept = table::department()->where('branch_id',$branch_id)->where('id', $ed[$i]->department_id)->first();

				$ed[$i]->department = $curr_dept->department;
			}
			else
			{
				$ed[$i]->department = "NULL";
			}



		}


		if($age_18_24 == null) {$age_18_24 = 0;};
		if($age_25_31 == null) {$age_25_31 = 0;};
		if($age_32_38 == null) {$age_32_38 = 0;};
		if($age_39_45 == null) {$age_39_45 = 0;};
		if($age_46_100 == null) {$age_46_100 = 0;};

		$age_group = $age_18_24.','.$age_25_31.','.$age_32_38.','.$age_39_45.','.$age_46_100;
		$dcc = null;
		$dpc = null;
		$dgc = null;
		$csc = null;
		$yhc = null;

		foreach ($ed as $c) { $comp[] = $c->company; $dcc = array_count_values($comp); }
		$cc = ($dcc == null) ? null : implode(', ', $dcc) . ',' ;

		foreach ($ed as $d) { $dept[] = $d->department; $dpc = array_count_values($dept); }
		$dc = ($dpc == null) ? null : implode(', ', $dpc) . ',' ;

		foreach ($ed as $g) { $gender[] = $g->gender; $dgc = array_count_values($gender); }
		$gc = ($dgc == null) ? null : implode(', ', $dgc) . ',' ;

		foreach ($ed as $cs) { $civilstatus[] = $cs->civilstatus; $csc = array_count_values($civilstatus); }
		$cg = ($csc == null) ? null : implode(', ', $csc) . ',' ;

		foreach ($ed as $yearhired) {
			$year[] = date("Y", strtotime($yearhired->startdate));
			asort($year);
			$yhc = array_count_values($year);
		}
		$yc = ($yhc == null) ? null : implode(', ', $yhc) . ',' ;

		$orgProfile = table::people()->where('branch_id',$branch_id)->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 5)->update(array('last_viewed' => $today));
		$title = "reports";
		return view('admin.reports.report-organization-profile', compact('orgProfile', 'title','age_group', 'gc', 'dgc', 'cg', 'csc', 'yc', 'yhc', 'dc', 'dpc', 'dcc', 'cc'));
	}

	public function empBday(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$today = date('M, d Y');
		$branch_id = Auth::user()->branch_id;

		$empBday = DB::table('people')
		->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
		->join('department', 'department.id', '=', 'people.department_id')
		->select('people.firstname','people.lastname','people.mi', 'jobtitle.jobtitle', 'department.department','people.mobileno','people.birthday')
		->where('people.branch_id',$branch_id)
		->orderby('people.firstname')->get();



		// $empBday = table::people()->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 7)->update(['last_viewed' => $today]);
		$title = "reports";
		return view('admin.reports.report-employee-birthdays', compact('title','empBday'));
	}

	public function userAccs(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		//if($request->sh == 2){return redirect()->route('reports');}
		$branch_id = Auth::user()->branch_id;

		$today = date('M, d Y');
		$userAccs = table::people()->where('branch_id',$branch_id)->get();
		table::reportviews()->where('branch_id',$branch_id)->where('report_id', 6)->update(['last_viewed' => $today]);
		$title = "reports";
		return view('admin.reports.report-user-accounts', compact('title','userAccs'));
	}

	public function getEmpAtten(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$branch_id = Auth::user()->branch_id;

		$id = $request->id;
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		if ($id == null AND $datefrom == null AND $dateto == null)
		{
			$data = table::attendance()->where('branch_id',$branch_id)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->get();
			return response()->json($data);
		}

		if($id !== null AND $datefrom == null AND $dateto == null )
		{
		 	$data = table::attendance()->where('branch_id',$branch_id)->where('idno', $id)->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->get();
			return response()->json($data);
		} elseif ($id !== null AND $datefrom !== null AND $dateto !== null) {
			$data = table::attendance()->where('branch_id',$branch_id)->where('idno', $id)->whereBetween('date', [$datefrom, $dateto])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->get();
			return response()->json($data);
		} elseif ($id == null AND $datefrom !== null AND $dateto !== null) {
			$data = table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$datefrom, $dateto])->select('idno', 'date', 'employee', 'timein', 'timeout', 'totalhours')->get();
			return response()->json($data);
		}
	}
	


	public function getEmpLeav(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$id = $request->id;
		$cur_date = date('Y-m-d');
		$branch_id = Auth::user()->branch_id;
		
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		if ($id == null AND $datefrom == null AND $dateto == null)
		{

			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->orderby('people.firstname')->get();
				  

			// $data = table::leaves()->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		}

		elseif($id !== null AND $datefrom == null AND $dateto == null )
		{
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->where('idno', $id)->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		} elseif ($id !== null AND $datefrom !== null AND $dateto !== null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->whereBetween('leaves.leavefrom', [$datefrom, $dateto])
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->where('idno', $id)->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		} 
		elseif ($id !== null AND $datefrom !== null AND $dateto == null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->whereBetween('leaves.leavefrom', [$datefrom, $cur_date])
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->where('idno', $id)->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		} 
		elseif ($id !== null AND $datefrom == null AND $dateto !== null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.idno', $id)
				   ->where('people.branch_id',$branch_id)
				   ->where('leaves.leavefrom', '<=' , $dateto)
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->where('idno', $id)->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		} 
		
		
		
		
		elseif ($id == null AND $datefrom !== null AND $dateto !== null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->whereBetween('leaves.leavefrom', [$datefrom, $dateto])
				   ->where('people.branch_id',$branch_id)
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		}
		elseif ($id == null AND $datefrom !== null AND $dateto == null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->whereBetween('leaves.leavefrom', [$datefrom, $cur_date])
				   ->where('people.branch_id',$branch_id)
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		}
		elseif ($id == null AND $datefrom == null AND $dateto !== null) {
			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('leaves.leavefrom','<=' , $dateto)
				   ->where('people.branch_id',$branch_id)
				   ->orderby('people.firstname')->get();
			// $data = table::leaves()->whereBetween('leavefrom', [$datefrom, $dateto])->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		}
		else
		{

			$data = DB::table('people')
				   ->join('leaves', 'leaves.reference', '=', 'people.id')
				   ->select('people.idno','people.firstname','people.mi', 'people.lastname', 'leaves.type','leaves.id','leaves.leavefrom','leaves.leaveto','leaves.returndate', 'leaves.reason','leaves.status')
				   ->where('people.branch_id',$branch_id)  
				   ->orderby('people.firstname')->get();
				  

			// $data = table::leaves()->select('idno', 'employee', 'type', 'leavefrom', 'leaveto', 'status', 'reason')->get();
			return response()->json($data);
		}
	}

	public function getEmpSched(Request $request)
	{
		if (permission::permitted('reports')=='fail'){ return redirect()->route('denied'); }
		$id = $request->id;
		$branch_id = Auth::user()->branch_id;

		if ($id == null)
		{
			$data = table::schedules()->where('branch_id',$branch_id)->select('reference', 'employee', 'intime', 'outime', 'datefrom', 'dateto', 'hours', 'restday', 'archive')->orderBy('archive', 'ASC')->get();
			return response()->json($data);
		}

		if($id !== null)
		{
		 	$data = table::schedules()->where('branch_id',$branch_id)->where('idno', $id)->select('reference', 'employee', 'intime', 'outime', 'datefrom', 'dateto', 'hours', 'restday', 'archive')->orderBy('archive', 'ASC')->get();
			return response()->json($data);
		}
	}
}
