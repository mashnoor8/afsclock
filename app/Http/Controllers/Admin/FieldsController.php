<?php


namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FieldsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Company
    |--------------------------------------------------------------------------
    */

    // Finds all the companies and displays them in the view file.
    // This controller and view file is only accessible by the admin.
    public function company()
    {
//        if (permission::permitted('company') == 'fail') {
//            return redirect()->route('denied');
//        }
		$branch_id = Auth::user()->branch_id;
       
        
        
        $data = table::company()->where('branch_id',$branch_id)->get();
        return view('admin.fields.company', compact('data'));
    }

    // Allows admin to add new company.
    public function addCompany(Request $request)
    {
//        if (permission::permitted('company-add') == 'fail') {
//            return redirect()->route('denied');
//        }

        $v = $request->validate([
            'company' => 'required|unique:company|alpha_dash_space|max:100',
        ]);
        $branch_id = Auth::user()->branch_id;
        $company = mb_strtoupper($request->company);

        table::company()->insert([
            'branch_id' => $branch_id,
            'company' => $company,
        ]);

        return redirect('fields/company')->with('success', 'New Company has been saved.');
    }


    public function deleteCompany($id, Request $request)
    {
//        if (permission::permitted('company-delete')=='fail'){ return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;
        
        $pcompany = table::people()->where('branch_id',$branch_id)->pluck('company_id');
		return view('admin.edits.delete-company', compact('pcompany','id'));
   	}

  
    // Allows admin to delete company from the database.
    public function clearCompany( Request $request)
    {
//        if (permission::permitted('company-delete') == 'fail') {
//            return redirect()->route('denied');
//        }
       
       $id = $request->id;
      
        //if($request->sh == 2){return redirect()->route('company');}

        $branch_id = Auth::user()->branch_id;
        table::company()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('fields/company')->with('success', 'Company has been Deleted!');
    }
// Allows admin to edit company from the database.

public function editCompany($id, Request $request)
{
//    if (permission::permitted('company-delete')=='fail'){ return redirect()->route('denied'); }
    $branch_id = Auth::user()->branch_id;
    
    $company = table::company()->where('branch_id',$branch_id)->where('id','=',$id)->first();
    return view('admin.edits.edit-company', compact('company'));
    }

public function updateCompany( Request $request)
{

   
//    if (permission::permitted('company-delete') == 'fail') {
//        return redirect()->route('denied');
//    }
    //if($request->sh == 2){return redirect()->route('company');}
    $company = mb_strtoupper($request->company);
   $id = $request->id;
   $branch_id = Auth::user()->branch_id;
 
    table::company()->where('branch_id',$branch_id)->where('id', $id)->update(['company' => $company]);

    return redirect('fields/company')->with('success', 'Company has been Updated!');
}

    /*
    |--------------------------------------------------------------------------
    | Department
    |--------------------------------------------------------------------------
    */
    public function department()
    {
//        if (permission::permitted('departments') == 'fail') {
//            return redirect()->route('denied');
//        }
        $branch_id = Auth::user()->branch_id;

        $pdepartment = table::people()->where('branch_id',$branch_id)->pluck('department_id');
        $data = table::department()->where('branch_id',$branch_id)->get();
        
        return view('admin.fields.department', compact('pdepartment','data'));
    }

    public function addDepartment(Request $request)
    {
//        if (permission::permitted('departments-add') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('department');}

        // $v = $request->validate([
        //     'department' => 'required|alpha_dash_space|max:100',
        // ]);

        $department = mb_strtoupper($request->department);
        $branch_id = Auth::user()->branch_id;

        $data = table::department()->where('branch_id',$branch_id)->pluck('department');
        if($data->contains($department)){
            return redirect('fields/department')->with('error', 'The department has already been taken');
        }
        table::department()->insert([
            'branch_id' => $branch_id,
            'department' => $department
        ]);

        return redirect('fields/department')->with('success', 'New Department has been saved.');
    }


   public function deleteDepartment($id, Request $request)
    {
//        if (permission::permitted('department-delete')=='fail'){ return redirect()->route('denied'); }
        
        $branch_id = Auth::user()->branch_id;
        
        $pdepartment = table::people()->where('branch_id',$branch_id)->pluck('department_id');
		return view('admin.edits.delete-department', compact('pdepartment','id'));
   	}

  
    // Allows admin to delete department from the database.
    public function clearDepartment( Request $request)
    {
//        if (permission::permitted('department-delete') == 'fail') {
//            return redirect()->route('denied');
//        }
       
       $id = $request->id;
       $branch_id = Auth::user()->branch_id;
      
        //if($request->sh == 2){return redirect()->route('company');}

        table::department()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('fields/department')->with('success', 'Department has been Deleted!');
    }
// Allows admin to edit department from the database.

public function editDepartment($id, Request $request)
{
//    if (permission::permitted('department-delete')=='fail'){ return redirect()->route('denied'); }
    $branch_id = Auth::user()->branch_id;
    
    $department = table::department()->where('branch_id',$branch_id)->where('id','=',$id)->first();
    return view('admin.edits.edit-department', compact('department'));
    }

public function updateDepartment( Request $request)
{

   
//    if (permission::permitted('department-delete') == 'fail') {
//        return redirect()->route('denied');
//    }
    //if($request->sh == 2){return redirect()->route('company');}
    $department = mb_strtoupper($request->department);
   $id = $request->id;
   $branch_id = Auth::user()->branch_id;
 
    table::department()->where('branch_id',$branch_id)->where('id', $id)->update(['department' => $department]);

    return redirect('fields/department')->with('success', 'Department has been Updated!');
}


    /*
    |--------------------------------------------------------------------------
    | Job Title or Position
    |--------------------------------------------------------------------------
    */
    public function jobtitle()
    {
//        if (permission::permitted('jobtitles') == 'fail') {
//            return redirect()->route('denied');
//        }
        $branch_id = Auth::user()->branch_id;

        $data = table::jobtitle()->where('branch_id',$branch_id)->get();
        $d = table::department()->where('branch_id',$branch_id)->get();

        return view('admin.fields.jobtitle', compact('data', 'd'));
    }

    public function addJobtitle(Request $request)
    {
//        if (permission::permitted('jobtitles-add') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('jobtitle');}

        // $v = $request->validate([
        //     'jobtitle' => 'required|unique:jobtitle|alpha_dash_space|max:100',
        // ]);

        $jobtitle = mb_strtoupper($request->jobtitle);
        $dept_code = $request->dept_code;
        $branch_id = Auth::user()->branch_id;

        $data = table::jobtitle()->where('branch_id',$branch_id)->pluck('jobtitle');
       
        if($data->contains($jobtitle)){
            return redirect('fields/jobtitle')->with('error', 'The department has already been taken');
        }

        table::jobtitle()->insert([
            [
                'branch_id' => $branch_id,
                'jobtitle' => $jobtitle,
                'dept_code' => $dept_code
            ],
        ]);

        return redirect('fields/jobtitle')->with('success', 'New Job Title has been saved.');
    }

   
    public function deleteJobtitle($id, Request $request)
    {
//        if (permission::permitted('jobtitle-delete')=='fail'){ return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;
        
        $pjobtitle = table::people()->where('branch_id',$branch_id)->pluck('job_title_id');
		return view('admin.edits.delete-jobtitle', compact('pjobtitle','id'));
   	}

  
    // Allows admin to delete Jobtitle from the database.
    public function clearJobtitle( Request $request)
    {
//        if (permission::permitted('jobtitle-delete') == 'fail') {
//            return redirect()->route('denied');
//        }
       
       $id = $request->id;
       $branch_id = Auth::user()->branch_id;
      
        //if($request->sh == 2){return redirect()->route('company');}

        table::jobtitle()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('fields/jobtitle')->with('success', 'Jobtitle has been Deleted!');
    }
// Allows admin to edit Jobtitle from the database.

public function editJobtitle($id, Request $request)
{
//    if (permission::permitted('jobtitle-delete')=='fail'){ return redirect()->route('denied'); }
    $branch_id = Auth::user()->branch_id;
    
   
    $jobtitle = DB::table('jobtitle')
    ->leftjoin('department', 'jobtitle.dept_code', '=', 'department.id')
    ->select('jobtitle.id','jobtitle.jobtitle','department.department')
    ->where('jobtitle.branch_id',$branch_id)
    ->where('jobtitle.id','=',$id)->first();
    $d = table::department()->where('branch_id',$branch_id)->get();
   
    return view('admin.edits.edit-jobtitle', compact('jobtitle','d'));
    }

public function updateJobtitle( Request $request)
{
   
   
//    if (permission::permitted('jobtitle-delete') == 'fail') {
//        return redirect()->route('denied');
//    }
    $branch_id = Auth::user()->branch_id;

    // $v = $request->validate([
    //     'jobtitle' => 'required|unique:jobtitle|alpha_dash_space|max:100',
    // ]);
    $id = $request->id;
    $jobtitle = mb_strtoupper($request->jobtitle);
    if($request->dept_code != Null)
    {
    $dept_code = $request->dept_code;
    table::jobtitle()->where('branch_id',$branch_id)->where('id', $id)->update([
            'jobtitle' => $jobtitle,
            'dept_code' => $dept_code
    ]);

    }
    else
    {
        table::jobtitle()->where('branch_id',$branch_id)->where('id', $id)->update([
                'jobtitle' => $jobtitle   
        ]);

    }
    

  
 
    

    return redirect('fields/jobtitle')->with('success', 'Jobtitle has been Updated!');
}



    /*
    |--------------------------------------------------------------------------
    | Leave Type
    |--------------------------------------------------------------------------
    */
    public function leavetype()
    {
//        if (permission::permitted('leavetypes') == 'fail') {
//            return redirect()->route('denied');
//        }
        $branch_id = Auth::user()->branch_id;

        $data = table::leavetypes()->where('branch_id',$branch_id)->get();

        $title = "leave";

        return view('admin.fields.leavetype', compact('data', 'title' ));
    }

    public function addLeavetype(Request $request)
    {
//        if (permission::permitted('leavetypes-add') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('leavetype');}
        $branch_id = Auth::user()->branch_id;
        $v = $request->validate([
            'leavetype' => 'required|alpha_dash_space|max:100',
            'credits' => 'required|digits_between:0,365|max:3',
            'term' => 'required|max:100',
        ]);
        $leavetype = mb_strtoupper($request->leavetype);
        $data = table::leavetypes()->where('branch_id',$branch_id)->pluck('leavetype');
        if($data->contains($leavetype)){
            return redirect('fields/leavetype')->with('error', 'The Leave Type has already been taken');
        }
        

        
        $credits = $request->credits;
        $term = $request->term;

        table::leavetypes()->insert([
            ['branch_id'=>$branch_id,'leavetype' => $leavetype, 'limit' => $credits, 'percalendar' => $term]
        ]);

        return redirect('fields/leavetype')->with('success', 'New Leave Type has been saved.');
    }

   
    public function deleteLeavetype($id, Request $request)
    {
//        if (permission::permitted('leavetypes-delete')=='fail'){ return redirect()->route('denied'); }
        $branch_id = Auth::user()->branch_id;
        
        $pleavetype = table::people()->where('branch_id',$branch_id)->pluck('leaveprivilege');
		return view('admin.edits.delete-leavetype', compact('pleavetype','id'));
   	}

  
    // Allows admin to delete department from the database.
    public function clearLeavetype( Request $request)
    {
//        if (permission::permitted('leavetypes-delete') == 'fail') {
//            return redirect()->route('denied');
//        }
       
       $id = $request->id;
       $branch_id = Auth::user()->branch_id;
      
        //if($request->sh == 2){return redirect()->route('company');}

        table::leavetypes()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('fields/leavetype')->with('success', 'Leavetype has been Deleted!');
    }
// Allows admin to edit department from the database.

public function editLeavetype($id, Request $request)
{
//    if (permission::permitted('leavetypes-delete')=='fail'){ return redirect()->route('denied'); }
    $branch_id = Auth::user()->branch_id;
    
    $leavetype = table::leavetypes()->where('branch_id',$branch_id)->where('id','=',$id)->first();
   
    return view('admin.edits.edit-leavetype', compact('leavetype'));
    }

public function updateLeavetype( Request $request)
{

  
//    if (permission::permitted('leavetypes-delete') == 'fail') {
//        return redirect()->route('denied');
//    }
    //if($request->sh == 2){return redirect()->route('company');}
   
   $id = $request->id;
   $branch_id = Auth::user()->branch_id;

   $leavetype = mb_strtoupper($request->leavetype);
   $credits = $request->credits;
   $term = $request->term;
 
   table::leavetypes()->where('branch_id',$branch_id)->where('id', $id)->update([
    'leavetype' => $leavetype, 'limit' => $credits, 'percalendar' => $term,
]);

    return redirect('fields/leavetype')->with('success', 'Leavetype has been Updated!');
}



    /*
    |--------------------------------------------------------------------------
    | Leave Groups
    |--------------------------------------------------------------------------
    */
    public function leaveGroups()
    {
//        if (permission::permitted('leavegroups') == 'fail') {
//            return redirect()->route('denied');
//        }
        $branch_id = Auth::user()->branch_id;

        $lt = table::leavetypes()->where('branch_id',$branch_id)->get();
        $lg = table::leavegroup()->where('branch_id',$branch_id)->get();

        return view('admin.fields.leave-groups', compact('lt', 'lg'));
    }

    public function addLeaveGroups(Request $request)
    {
//        if (permission::permitted('leavegroup-add') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('leavegroup');}

        $v = $request->validate([
            'leavegroup' => 'required|alpha_dash_space|max:100',
            'description' => 'required|alpha_dash_space|max:155',
            'status' => 'required|boolean|max:1',
            'leaveprivileges' => 'required|max:255',
        ]);
        $branch_id = Auth::user()->branch_id;

        $leavegroup = strtoupper($request->leavegroup);
        $description = strtoupper($request->description);
        $status = $request->status;
        $leaveprivileges = implode(',', $request->leaveprivileges);

        table::leavegroup()->insert([
            ["branch_id"=> $branch_id,"leavegroup" => $leavegroup, "description" => $description, "leaveprivileges" => $leaveprivileges, "status" => $status]
        ]);

        return redirect('fields/leavetype/leave-groups')->with('success', 'New Leave Group has been saved!');
    }

    public function editLeaveGroups($id)
    {
//        if (permission::permitted('leavegroup-edit') == 'fail') {
//            return redirect()->route('denied');
//        }
        $branch_id = Auth::user()->branch_id;

        $lt = table::leavetypes()->where('branch_id',$branch_id)->get();
        $lg = table::leavegroup()->where('branch_id',$branch_id)->where("id", $id)->first();
        $e_id = ($lg->id == null) ? 0 : Crypt::encryptString($lg->id);

        return view('admin.edits.edit-leavegroups', compact('lg', 'lt', 'e_id'));
    }

    public function updateLeaveGroups(Request $request)
    {
//        if (permission::permitted('leavegroup-edit') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('leavegroup');}

        $v = $request->validate([
            'leavegroup' => 'required|alpha_dash_space|max:100',
            'description' => 'required|alpha_dash_space|max:155',
            'status' => 'required|boolean|max:1',
            'leaveprivileges' => 'required|max:255',
            'id' => 'required|max:200'
        ]);
        $branch_id = Auth::user()->branch_id;

        $leavegroup = strtoupper($request->leavegroup);
        $description = strtoupper($request->description);
        $status = $request->status;
        $leaveprivileges = implode(',', $request->leaveprivileges);
        $id = Crypt::decryptString($request->id);

        table::leavegroup()->where('branch_id',$branch_id)->where('id', $id)->update([
            "leavegroup" => $leavegroup,
            "description" => $description,
            "leaveprivileges" => $leaveprivileges,
            "status" => $status
        ]);

        return redirect('fields/leavetype/leave-groups')->with('success', 'Leave group has been update!');
    }

    public function deleteLeaveGroups($id, Request $request)
    {
//        if (permission::permitted('leavegroup-delete') == 'fail') {
//            return redirect()->route('denied');
//        }
        //if($request->sh == 2){return redirect()->route('leavegroup');}
        $branch_id = Auth::user()->branch_id;

        table::leavegroup()->where('branch_id',$branch_id)->where('id', $id)->delete();

        return redirect('fields/leavetype/leave-groups')->with('success', 'Deleted!');
    }

    /*
    |--------------------------------------------------------------------------
    | Common Task
    |--------------------------------------------------------------------------
    */

    public function common_task()
    {
        $branch_id = Auth::user()->branch_id;

        $data = table::common_task()->where('branch_id',$branch_id)->get();
        return view('admin.fields.common_task', compact('data'));
    }

    public function addCommonTask(Request $request)
    {
        $title = $request->get('task_title');
        $branch_id = Auth::user()->branch_id;

        $description = $request->get('task_description');

        $v = $request->validate([
            'branch_id' => $branch_id,
            'task_title' => 'required',
            'task_description' => 'required'
        ]);

        table::common_task()->insert([
            'title' => $title,
            'description' => $description
        ]);

        return redirect('fields/common_task')->with('success', 'Task added successfully!');
    }


}
