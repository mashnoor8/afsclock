<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;


class ProfileController extends Controller
{

	public function view($id, Request $request)
	{
		if (permission::permitted('employees-view') == 'fail') {
			return redirect()->route('denied');
		}

		$branch_id = Auth::user()->branch_id;

		$p = table::people()->where('branch_id', $branch_id)->where('id', $id)->first();
		$company_data = table::company()->where('branch_id', $branch_id)->where('id', $p->company_id)->first();
		$profile_photo = table::people()->where('branch_id', $branch_id)->select('avatar')->where('id', $p->id)->value('avatar');
		$department = table::department()->where('branch_id', $branch_id)->where('id', $p->department_id)->first();
		$job_title = table::jobtitle()->where('branch_id', $branch_id)->where('id', $p->job_title_id)->first();
		$leavetype = table::leavetypes()->where('branch_id', $branch_id)->where('id', $p->leaveprivilege)->first();
		$leavegroup = table::leavegroup()->where('branch_id', $branch_id)->get();
		

		if ($leavetype) {
			$total_leaves = $leavetype->limit;
		} else {
			$total_leaves = 0;
		}



		return view('admin.profile-view', compact('p', 'company_data','total_leaves', 'profile_photo', 'department', 'leavetype', 'job_title', 'leavegroup'));
	}

	public function delete($id, Request $request)
	{
		if (permission::permitted('employees-delete') == 'fail') {
			return redirect()->route('denied');
		}

		return view('admin.delete-employee', compact('id'));
	}

	public function clear(Request $request)
	{
		if (permission::permitted('employees-delete') == 'fail') {
			return redirect()->route('denied');
		}
		//if($request->sh == 2){return redirect()->route('employees');}
		$branch_id = Auth::user()->branch_id;

		$id = $request->id;
		table::people()->where('branch_id', $branch_id)->where('id', $id)->delete();
		table::attendance()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		table::leaves()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		table::employee_faces()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		table::employee_salary()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		table::schedules()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		table::tasks()->where('branch_id', $branch_id)->where('reference', $id)->delete();
		return redirect('employees')->with('success', 'Employee information has been deleted!');
	}

	public function archive($id, Request $request)
	{
		if (permission::permitted('employees-archive') == 'fail') {
			return redirect()->route('denied');
		}
		//if($request->sh == 2){return redirect()->route('employees');}
		$branch_id = Auth::user()->branch_id;

		$id = $request->id;
		table::people()->where('branch_id', $branch_id)->where('id', $id)->update(['employmentstatus' => 'Archived', 'status' => '0']);
		// table::people()->where('reference', $id)->update(['status' => '0']);

		return redirect('employees')->with('success', 'Employee information has been archived!');
	}


	public function editPerson($id)
	{
		if (permission::permitted('employees-edit') == 'fail') {
			return redirect()->route('denied');
		}

		$branch_id = Auth::user()->branch_id;

		$person_details = table::people()->where('branch_id', $branch_id)->where('id', $id)->first();
		$the_job_title = table::jobtitle()->where('branch_id', $branch_id)->where('id', $person_details->job_title_id)->first();
		// dd($the_job_title, $person_details);
		$company = table::company()->where('branch_id', $branch_id)->first();
		$department = table::department()->where('branch_id', $branch_id)->get();
		$jobtitle = table::jobtitle()->where('branch_id', $branch_id)->get();
		// dd($jobtitle, $the_job_title, $person_details);
		$leavegroup = table::leavetypes()->where('branch_id', $branch_id)->get();
		$e_id = ($person_details->id == null) ? 0 : Crypt::encryptString($person_details->id);

		$leaves_this_year = DB::select("SELECT SUM(DATEDIFF(DATE_ADD(leaveto, INTERVAL 1 DAY), leavefrom)) AS diff  FROM leaves WHERE reference = $person_details->id AND YEAR(leavefrom) = YEAR(CURDATE()) AND YEAR(leaveto) = YEAR(CURDATE()) AND status = 'Approved';");
		// dd($leaves_this_year[0]->diff);

		$u = table::people()->where('branch_id', $branch_id)->where('id', $id)->first();
		$r = table::roles()->get();
		$e_id = ($u->id == null) ? 0 : Crypt::encryptString($u->id);

		return view('admin.edits.edit-personal-info', compact('the_job_title', 'person_details', 'company', 'leaves_this_year', 'department', 'jobtitle', 'leavegroup', 'e_id', 'u', 'r'));
	}

	public function updatePerson(Request $request)
	{
		if (permission::permitted('employees-edit') == 'fail') {
			return redirect()->route('denied');
		}
		//if($request->sh == 2){return redirect()->route('employees');}
		$branch_id = Auth::user()->branch_id;

		$v = $request->validate([
			'id' => 'required|max:200',
			'lastname' => 'required|alpha_dash_space|max:155',
			'firstname' => 'required|alpha_dash_space|max:155',
			'idno' => 'required|max:155',
			'password' => 'confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
			'employmentstatus' => 'alpha_dash_space|max:155',	
		]);
		 // dd($request->all());
		$id = Crypt::decryptString($request->id);
		$lastname = mb_strtoupper($request->lastname);
		$firstname = mb_strtoupper($request->firstname);
		$mi = mb_strtoupper($request->mi);
		$gender = mb_strtoupper($request->gender);
		$emailaddress =  mb_strtolower($request->emailaddress);
		$civilstatus = mb_strtoupper($request->civilstatus);

		$mobileno = $request->mobileno;
		$birthday = date("Y-m-d", strtotime($request->birthday));
		$nationalid = mb_strtoupper($request->nationalid);
		$birthplace = mb_strtoupper($request->birthplace);
		$homeaddress = mb_strtoupper($request->homeaddress);
		$company = mb_strtoupper($request->company);

		$existing_company = table::company()->where('branch_id', $branch_id)->where('company', $company)->first();

		$department = mb_strtoupper($request->department);

		$existing_department = table::department()->where('branch_id', $branch_id)->where('department', $department)->first();

		$jobposition = mb_strtoupper($request->jobposition);

		$existing_jobtitle = table::jobtitle()->where('branch_id', $branch_id)->where('jobtitle', $jobposition)->first();


		$companyemail = mb_strtolower($request->companyemail);
		$leaveprivilege = $request->leaveprivilege;
		$idno = mb_strtoupper($request->idno);
		$employmenttype = $request->employmenttype;
		$employmentstatus = $request->employmentstatus;
		$startdate = date("Y-m-d", strtotime($request->startdate));
		$dateregularized = date("Y-m-d", strtotime($request->dateregularized));


		$file = $request->file('image');
		if ($file != null) {
			$filename = $firstname.strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($file)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/images/faces/';
            $resize_image->save("$destinationPath" . "$filename");
			
		} else {
			$filename = table::people()->where('branch_id', $branch_id)->where('id', $id)->value('avatar');
		}

		$pass_image = $request->file('pass_image');
		if ($pass_image != null) {
			$pass_image_name = 'Pass'.$firstname.strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($pass_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$pass_image_name");
	
		} else {
			$pass_image_name = table::people()->where('branch_id', $branch_id)->where('id', $id)->value('pass_image');
		}

		$drive_image = $request->file('drive_image');
		if ($drive_image != null) {
			$drive_image_name = 'Drive'.$firstname.strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($drive_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$drive_image_name");
		
		} else {
			$drive_image_name = table::people()->where('branch_id', $branch_id)->where('id', $id)->value('drive_image');
		}

		$image_bpr = $request->file('image_bpr');
		if ($image_bpr != null) {
			$image_bpr_name = 'BRP'.$firstname.strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($image_bpr)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$image_bpr_name");
	
		} else {
			$image_bpr_name = table::people()->where('branch_id', $branch_id)->where('id', $id)->value('bpr_image');
		}

		$doc_image = $request->file('doc_image');
		if ($doc_image != null) {
			$doc_image_name = 'Doc'.$firstname.strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($doc_image)->encode('png', 65)->resize(300, 300);
            $destinationPath = public_path() . '/assets/employee_info_image/';
            $resize_image->save("$destinationPath" . "$doc_image_name");

		} else {
			$doc_image_name = table::people()->where('branch_id', $branch_id)->where('id', $id)->value('document_image');
		}

		table::people()->where('id', $id)->update([
			'branch_id' => $branch_id,
			'lastname' => $lastname,
			'firstname' => $firstname,
			'mi' => $mi,
			'gender' => $gender,
			'emailaddress' => $emailaddress,
			'civilstatus' => $civilstatus,
			'mobileno' => $mobileno,
			'birthday' => $birthday,
			'birthplace' => $birthplace,
			'nationalid' => $nationalid,
			'homeaddress' => $homeaddress,
			'employmenttype' => $employmenttype,
			'employmentstatus' => $employmentstatus,
			'avatar' => $filename,
			'company_id' => $existing_company->id,
			'department_id' => $existing_department->id,
			'job_title_id' => $existing_jobtitle->id,
			'companyemail' => $companyemail,
			'leaveprivilege' => $leaveprivilege,
			'idno' => $idno,
			'startdate' => $startdate,
			'dateregularized' => $dateregularized,
			'passport_number' => $request->passport_number,
			'passport_issue_date' => $request->passport_issue_date,
			'passport_ex_date' => $request->passport_ex_date,
			'pass_image' => $pass_image_name,
			'd_license_number' => $request->d_license_number,
			'drive_issue_date' => $request->drive_issue_date,
			'drive_ex_date' => $request->drive_ex_date,
			'drive_image' => $drive_image_name,
			'bpr_number' => $request->bpr_number,
			'bpr_issue_date' => $request->bpr_issue_date,
			'bpr_ex_date' => $request->bpr_ex_date,
			'bpr_issue_date' => $request->bpr_issue_date,
			'bpr_image' => $image_bpr_name,
			'checked' => $request->checked,
			'current_status' => $request->current_status,
			'document_name' => $request->document_name,
			'document_issue_date' => $request->document_issue_date,
			'document_ex_date' => $request->document_ex_date,
			'document_image' => $doc_image_name,
			'comment' => $request->comment,
			'updated_at' => Carbon::now()
		]);

		return redirect('profile/edit/' . $id)->with('success', 'Employee information has been updated!');
	}

	public function viewProfile(Request $request)
	{
		$id = \Auth::user()->id;
		$branch_id = Auth::user()->branch_id;

		$myuser = table::people()->where('branch_id', $branch_id)->where('id', $id)->first();
		$myrole = table::roles()->where('id', $myuser->role_id)->value('role_name');

		return view('admin.update-profile', compact('myuser', 'myrole'));
	}

	public function viewPassword()
	{
		return view('admin.update-password');
	}

	public function updateUser(Request $request)
	{
		//if($request->sh == 2){return redirect()->route('updateProfile');}

		$v = $request->validate([
			'name' => 'required|max:100',
			'email' => 'required|email|max:100',
		]);
		$branch_id = Auth::user()->branch_id;

		$id = \Auth::id();
		$name = mb_strtoupper($request->name);
		$email = mb_strtolower($request->email);

		if ($id == null) {
			return redirect('personal/update-user')->with('error', 'Whoops! Please fill the form completely.');
		}

		table::people()->where('branch_id', $branch_id)->where('id', $id)->update([
			'name' => $name,
			'email' => $email,
		]);

		return redirect('update-profile')->with('success', 'Updated!');
	}

	public function updatePassword(Request $request)
	{
		//if($request->sh == 2){return redirect()->route('updatePassword');}

		$v = $request->validate([
			'currentpassword' => 'required|max:100',
			'newpassword' => 'required|min:8|max:100',
			'confirmpassword' => 'required|min:8|max:100',
		]);
		$branch_id = Auth::user()->branch_id;

		$id = \Auth::id();
		$p = \Auth::user()->password;
		$c_password = $request->currentpassword;
		$n_password = $request->newpassword;
		$c_p_password = $request->confirmpassword;

		if ($id == null) {
			return redirect('personal/update-user')->with('error', 'Whoops! Please fill the form completely.');
		}

		if ($n_password != $c_p_password) {
			return redirect('update-password')->with('error', 'New password does not match!');
		}

		if (Hash::check($c_password, $p)) {
			table::people()->where('branch_id', $branch_id)->where('id', $id)->update([
				'password' => Hash::make($n_password),
			]);

			return redirect('update-password')->with('success', 'Updated!');
		} else {
			return redirect('update-password')->with('error', 'Oops! current password does not match.');
		}
	}
	
}
