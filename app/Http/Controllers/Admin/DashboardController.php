<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use App\User;
use App\Task;
use App\People;

use Datetime;
use Illuminate\Support\Facades\Auth;


class Leaves
{

  public $employee = '';
  public $leavefrom = '';



  public function __construct($employee, $leavefrom)
  {
    $this->employee = $employee;
    $this->leavefrom = $leavefrom;
  }
}

class Tasks
{

  public $id = '';
  public $assigned_to = '';
  public $original_deadline = '';
  public $deadline = '';

  public function __construct($id, $assigned_to, $deadline, $original_deadline)
  {
    $this->id = $id;
    $this->assigned_to = $assigned_to;
    $this->original_deadline = $original_deadline;
    $this->deadline = $deadline;
  }
}

class Activity
{

  public $employee = '';
  public $datetime = '';
  public $label = '';

  public function __construct($employee, $datetime, $label)
  {
    $this->employee = $employee;
    $this->datetime = $datetime;
    $this->label = $label;
  }
}



class DashboardController extends Controller
{
  // Admin Dashboard controller function.
  // Pulls all the data from database tables
  // and sends them to database view file which is only accessible by admin.
  public function index(Request $request)
  {
   if (permission::permitted('dashboard') == 'fail') {
     return redirect()->route('denied');
   }
    $title = "dashboard";
    $datenow = date('m/d/Y');
    $datetoday = date('Y-m-d');
    $branch_id = Auth::user()->branch_id;

    $emp_typeR = table::people()
      ->where('branch_id', $branch_id)
      ->where('employmenttype', 'Regular')
      ->where('employmentstatus', 'Active')
      ->where('acc_type', 1)
      ->count();

    $emp_typeT = table::people()
      ->where('branch_id', $branch_id)
      ->where('employmenttype', 'Trainee')
      ->where('employmentstatus', 'Active')
      ->where('acc_type', 1)
      ->count();

    $emp_allActive = table::people()
      ->where('branch_id', $branch_id)
      ->where('employmentstatus', 'Active')
      ->count();

    $a = table::attendance()
      ->where('branch_id', $branch_id)
      ->latest('timein')
      ->take(4)
      ->get();

    $all_company = table::company()->where('branch_id', $branch_id)->count();
    $all_department = table::department()->where('branch_id', $branch_id)->count();

    $emp_approved_leave = table::leaves()
      ->where('branch_id', $branch_id)
      ->where('status', 'Approved')
      ->orderBy('leavefrom', 'desc')
      ->take(8)
      ->get();

    $recent_leaves = collect([]);

    foreach ($emp_approved_leave as $l) {
      $the_emp = User::find($l->reference);
      $recent_leaves->push(new Leaves($the_emp->firstname . " " . $the_emp->lastname, $l->leavefrom));
    }

    $emp_leaves_approve = table::leaves()
      ->where('branch_id', $branch_id)
      ->where('status', 'Approved')
      ->count();

    $emp_leaves_pending = table::leaves()
      ->where('branch_id', $branch_id)
      ->where('status', 'Pending')
      ->count();

    $emp_leaves_all = table::leaves()
      ->where('branch_id', $branch_id)
      ->where('status', 'Approved')
      ->orWhere('status', 'Pending')
      ->count();

    $recent_breaks = table::daily_breaks()->where('branch_id', $branch_id)->latest('start_at')->take(6)->get();
    //Attendance Chart
    $chartData1_old = DB::select("select idno as name,count(date) as points from attendance where MONTH(CURDATE()) = MONTH(date) and branch_id = ". Auth::user()->branch_id ." group by idno order by points desc limit 8");
    $chart_1 = count($chartData1_old);
    
    $chartData1 = json_encode($chartData1_old);
    // Task Chart
    $chartData2 = DB::select("select tasks.reference,people.firstname,people.idno,sum(case when tasks.done_status = 1 then 1 else 0 end) as completed,sum(case when tasks.done_status = 0 then 1 else 0 end) as pending,sum(case when tasks.done_status = 0 AND STR_TO_DATE(tasks.deadline, '%Y-%m-%d') < CURDATE() then 1 else 0 end) as failed from tasks join people on tasks.reference = people.id where people.branch_id = $branch_id group by tasks.reference,people.firstname,people.idno");
    $task_count = count($chartData2);
    $chartData2_collection = collect($chartData2)->take(5);
    $sorted = $chartData2_collection->sortByDesc('completed');
    $chartData_task = json_encode($sorted->values()->all());
    // dd($chartData_task);

    // Newest Employees
    $newest_employee = DB::table('people')
      ->join('jobtitle', 'jobtitle.id', '=', 'people.job_title_id')
      ->select('people.firstname', 'people.lastname', 'jobtitle.jobtitle', 'people.startdate')
      ->where('people.branch_id', $branch_id)
      ->latest('people.startdate')->take(5)->get();

    $recentleaves = DB::table('people')
      ->join('leaves', 'leaves.reference', '=', 'people.id')
      ->select('people.firstname', 'people.lastname', 'leaves.leavefrom', 'leaves.leaveto')
      ->where('people.branch_id', $branch_id)
      ->latest('leaves.leavefrom')->take(5)->get();

    $pending_tasks = DB::table('people')
      ->join('tasks', 'tasks.reference', '=', 'people.id')
      ->select('people.firstname', 'people.mi', 'people.lastname', 'tasks.title', 'tasks.deadline', 'tasks.created_at')
      ->where('people.branch_id', $branch_id)
      ->latest('tasks.created_at')->take(5)->get();

    $running_month = date('m');
    //  dd($running_month);

    $recent_birthday = DB::select("SELECT firstname,mi,lastname,birthday,(366 + DAYOFYEAR(birthday) - DAYOFYEAR(NOW())) % 366 as left_days FROM people where birthday is not null and birthday != '' and branch_id = $branch_id  ORDER BY left_days LIMIT 5");
    //  dd( $recent_birthday);
    $activity_collection = collect([]);

    if ($a) {
      foreach ($a as $r_e) {
        $user = User::find($r_e->reference);
        if ($r_e->timein && $user) {
          $activity_collection->push(new Activity($user->firstname . " " . $user->lastname, $r_e->timein, 'Clock In'));
        }
        if ($r_e->timeout && $user) {
          $activity_collection->push(new Activity($user->firstname . " " . $user->lastname, $r_e->timeout, 'Clock Out'));
        }
      }
    }

    if ($recent_breaks) {
      foreach ($recent_breaks as $r_b) {
        $user = User::find($r_b->reference);
        if ($r_b->start_at && $user) {
          $activity_collection->push(new Activity($user->firstname . " " . $user->lastname, $r_b->start_at, 'Break In'));
        }
        if ($r_b->end_at && $user) {
          $activity_collection->push(new Activity($user->firstname . " " . $user->lastname, $r_b->end_at, 'Break Out'));
        }
      }
    }

    $sortedActivities = Arr::sort($activity_collection, function ($activity) {
      return $activity->datetime;
    });

    $tasks = Task::all();

    $task_collection = collect([]);

    if ($tasks) {
      foreach ($tasks as $task) {
        $extended_task = table::task_extension()->where('task_id', $task->id)->latest('new_deadline')->first();

        $user = User::find($task->reference);

        if ($extended_task && $user) {
          $task_collection->push(new Tasks($task->id, $user->firstname . " " . $user->lastname, $extended_task->new_deadline, $task->deadline));
        }
      }
    }

    $is_online_now = table::people()
      ->where('branch_id', $branch_id)
      ->where('acc_type', 1)
      ->count();

    $is_offline_now = table::attendance()
      ->where('branch_id', $branch_id)
      ->where('date', $datetoday)
      ->count();


    $today_present = table::attendance()->where('branch_id', $branch_id)->where('date', $datetoday)->orderBy('timein', 'desc')->take(10)->get();

    // BPR Expiration

    // $expire_res = People::where('branch_id', $branch_id)->whereDate('bpr_ex_date', '<', Carbon::now()->addMonth(1))->select('firstname','mi','lastname','idno','bpr_ex_date')->get();
    // $expire_passport = People::where('branch_id', $branch_id)->whereDate('passport_ex_date', '<', Carbon::now()->addMonth(1))->select('firstname','mi','lastname','idno','passport_ex_date')->get();
    // $expire_drive = People::where('branch_id', $branch_id)->whereDate('drive_ex_date', '<', Carbon::now()->addMonth(1))->select('firstname','mi','lastname','idno','drive_ex_date')->get();
    // $expire_document = People::where('branch_id', $branch_id)->whereDate('document_ex_date', '<', Carbon::now()->addMonth(1))->select('firstname','mi','lastname','idno','document_ex_date')->get();
    
    $expire_documents_employees = DB::select('select id, firstname, mi, lastname from people where (branch_id = '. Auth::user()->branch_id .') AND ((YEAR(passport_ex_date) = YEAR(CURRENT_DATE + INTERVAL 1 MONTH)) OR (YEAR(bpr_ex_date) = YEAR(CURRENT_DATE + INTERVAL 1 MONTH)) OR (YEAR(document_ex_date) = YEAR(CURRENT_DATE + INTERVAL 1 MONTH)) OR (YEAR(drive_ex_date) = YEAR(CURRENT_DATE + INTERVAL 1 MONTH)))');
    
    // dd($expire_documents_employees);

    return view('admin.dashboard', compact('emp_typeR','chartData1', 'task_count', 'chartData_task', 'recent_birthday', 'today_present', 'title', 'emp_typeT', 'emp_allActive', 'emp_leaves_pending', 'emp_leaves_approve', 'emp_leaves_all', 'emp_approved_leave', 'a', 'sortedActivities', 'task_collection', 'newest_employee', 'is_online_now', 'is_offline_now', 'recentleaves', 'pending_tasks', 'all_company', 'all_department', 'recent_leaves', 'chart_1', 'expire_documents_employees'));
  }

  // Attendance Details (Break history and Duration)
  // by the given attendace id.
  public function details(Request $request)
  {
   if (permission::permitted('dashboard') == 'fail') {
     return redirect()->route('denied');
   }
    $branch_id = Auth::user()->branch_id;
    $attendanceID = request()->attendanceID;
    $theAttendance = table::attendance()->where('branch_id', $branch_id)->where('id', $attendanceID)->first();
    $theDate = date("Y-m-d", strtotime($theAttendance->created_at));

    $all_breaks = table::daily_breaks()->where('branch_id', $branch_id)->where('reference_id', $theAttendance->reference)->whereDate('start_at', $theDate)->get();
    return response()->json(array($all_breaks));
  }

  public function Organization()
  {

    return view('admin.organizationChart');
  }
  public function back_to_super_admin()
  {
    $role = Auth::user()->role_id;
    $branch_id = Auth::user()->branch_id;
    $id = Auth::user()->id;

    if (($role = 3) && ($branch_id != null)) {
      $super_admin = People::where('id', $id)->where('role_id', $role)->update([
        'branch_id' => null,
        'acc_type' => 3
      ]);
    }
    Auth::logout();
    
    Auth::loginUsingId($id);
    return redirect(url('/branches'));
  }
}
