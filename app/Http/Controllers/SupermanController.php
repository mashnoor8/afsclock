<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Stripe;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class SupermanController extends Controller
{
    //



    public function dashboard()
    {

        $title = "superman";
        $super_admins = People::where('role_id', 3)->get();

        return view('superman.superman_index', compact('super_admins', 'title'));
    }
    public function packages()
    {
	
        $packages = DB::table('packages')->where('status','!=','3')->get();
        $title = "packages";
        return view('superman.packages', compact('packages', 'title'));
    }

    public function package_edit($id)
    {

        $package = DB::table('packages')->where('id', $id)->first();


        $split = explode("-", $package->limit);
        $start = (int)$split[0];
        $end = (int)$split[1];

        $interval = $package->interval;
        $currency = $package->currency;

        return view('superman.package_edit', compact('package', 'start', 'end', 'interval', 'currency'));
    }

    public function package_update(Request $request)
    {
        // dd($request->all());

        $package = DB::table('packages')->where('id', $request->id)->first();
        $price = floatval($request->price)*100;
        $l = $request->start . '-' . $request->end;
        $limit = $request->start . '-' . $request->end;

        $stripe = new \Stripe\StripeClient(
            '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'
        );
        if ($package->name != $request->name || $package->limit != $l) {
            $product_info = $stripe->products->update(
                $package->product_id,
                [
                    'name' => $request->name,
                    'description' => $limit

                ]
            );
            //   dd($product_info );

            DB::table('packages')->where('id', $request->id)->update([
                'name' => $product_info->name,
                'product_id' => $product_info->id,
                'limit' => $product_info->description,
                'updated_at' => Carbon::Now()
            ]);
            $package = DB::table('packages')->where('id', $request->id)->first();
        }
        // dd($package);
        
        if ($package->price != $request->price || $package->currency != $request->currency || $package->interval != $request->interval) {
            
            $plan_info = $stripe->prices->create([
                'currency' => strtolower($request->currency),
                'unit_amount' => $price,
                'recurring' => ['interval' => strtolower($request->interval)],
                'product' => $package->product_id
              ]);

              $up_price = $plan_info->unit_amount/100;
              DB::table('packages')->insert([
                    'name' => $package->name,
                    'plan_id' => $plan_info->id,
                    'product_id' => $plan_info->product,
                    'limit' => $package->limit,
                    'price' => $up_price,
                    'currency' => strtoupper($plan_info->currency),
                    'interval' => ucfirst($plan_info->recurring['interval']),
                    'status' => '1',
                    'created_at' => Carbon::Now()
            ]); 
            DB::table('packages')->where('id', $request->id)->update(['status' => '0','updated_at' => Carbon::Now()]);
        }

        return redirect('/superman/packages')->with('success', 'Package has Been Updated successfully!');
    }

    public function package_create(){
        return view('superman.package_create');
    }
    public function package_store(Request $request){
        // dd($request->all());
        $limit = $request->start . '-' . $request->end;
        $price = floatval($request->price)*100;

        $stripe = new \Stripe\StripeClient(
            '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'
          );
        $product = $stripe->products->create([
        'name' => $request->name,
        'description' => $limit
        ]);

        $plan_info = $stripe->prices->create([
            'currency' => strtolower($request->currency),
            'unit_amount' => $price,
            'recurring' => ['interval' => strtolower($request->interval)],
            'product' => $product->id
        ]);
        $up_price = $plan_info->unit_amount/100;

        DB::table('packages')->insert([
            'name' => $product->name,
            'plan_id' => $plan_info->id,
            'product_id' => $plan_info->product,
            'limit' =>  $product->description,
            'price' => $up_price,
            'currency' => strtoupper($plan_info->currency),
            'interval' => ucfirst($plan_info->recurring['interval']),
            'status' => '1',
            'created_at' => Carbon::Now()
    ]); 

    return redirect('/superman/packages')->with('success', 'Package has been created successfully!');
    }

    public function package_delete($id)
    {
        $package = DB::table('packages')->where('id', $id)->first();
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_51I0sYDC0vslXRlHEvuMUC7IgNAsvzov4QwIdNkzZpjEqMFngH3Geu0qZL8SekEwc9oNP0eSC6WLzXf2fKZuTZokH00BByqAlQz'
          );
        
        

          try {
            $res = $stripe->products->delete(
              'prod_IqBahsvxc0PrxL',
                []
              );
        } catch (Exception $e) {
            $api_error = $e->getMessage();
            
        }finally {
           
            return redirect('/superman/packages')->with('error', 'Oops! You can\'t delete this product it has one or more user-created prices.');
        }

        if($res){
            DB::table('packages')->where('id', $id)->delete();
            return redirect('/superman/packages')->with('success', 'Package has been deleted successfully!');
        }
        else{
            return redirect('/superman/packages')->with('error', 'Oops! You can\'t delete this product it has one or more user-created prices.');
        }
    }
}
