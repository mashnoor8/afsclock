<?php

namespace App\Http\Controllers;

use App\Training;
use App\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Redis;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($branch_uid)
    {
        // dd($branch_id);

        $user = Auth::user();
        $branch = Branch::where([['branch_uid', $branch_uid],['owner_id', $user->id]])->first();

        if ($branch) {
          $tainings = Training::where('branch_id', $branch->id)->get();

          $hasPendingTraining = Training::where([['branch_id', $branch->id],['status', False]])->exists();

          return view('super_admin.training', compact('tainings', 'branch', 'hasPendingTraining'));
        }else {

          return redirect()->back()->with('error', 'No branch exist');
        }
    }


    //
    // TRAINING REQUEST PROCESSING BY MASHNOOR BHAI
    //

    public function trainQueue($branch_uid)
    {
      $user = Auth::user();
      $branch = Branch::where([['branch_uid', $branch_uid],['owner_id', $user->id]])->first();
      $training = new Training();
      $training->branch_id = $branch->id;
      $training->status = 0;
      $training->save();

      // Add training request for the following branch to the Redis Queue

      $training_req = array(
          'taining_id' => $training->id,
          //'taining_id' => $training->id,
          'branch_id' => $branch->id,
          'branch_name' => $branch->branch_name,
          'branch_uid' => $branch->branch_uid,
          'user_email' => $user->companyemail
      );

      $redis = new Redis();
      $redis->connect('127.0.0.1', 6379);
      $redis->rpush('taininig_queue_merge', json_encode($training_req));


      $user = Auth::user();
        $branch = Branch::where([['branch_uid', $branch_uid],['owner_id', $user->id]])->first();

        if ($branch) {
          $tainings = Training::where('branch_id', $branch->id)->get();

          $hasPendingTraining = Training::where([['branch_id', $branch->id],['status', False]])->exists();

          // return view('super_admin.training', compact('tainings', 'branch', 'hasPendingTraining'));
          return redirect()->back()->with('success', 'Training request is being processed');
        }else {

          return redirect('/home');
        }


      //return view('train_success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function show(Training $training)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function edit(Training $training)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Training $training)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function destroy(Training $training)
    {
        //
    }
}
