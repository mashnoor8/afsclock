<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\Classes\table;
use App\Classes\permission;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Http\Request;

class WebcamController extends Controller
{
  // Webcam Data Feed.
  // Real time camera data stored in the database table called 'webcam_table'.
  // Following controller function queries the data from the table and
  // sends to the view file.
  public function realtime_webcam_data(Request $request)
  {
    if (permission::permitted('dashboard') == 'fail') {
      return redirect()->route('denied');
    }

    // $webcam_data =  DB::table('webcam_data')->select('webcam_data.id', 'webcam_data.last_seen', 'tbl_company_data.idno')->join('tbl_company_data', 'webcam_data.reference','=', 'tbl_company_data.reference')->get();
    $webcam_data = table::webcam_table()->get();
    return view('admin.webcam_data', compact('webcam_data'));
  }


  // Attendace using the Mobile App / CCTV Camera.
  // The remote camera devices are connected with this controller function
  // through an API. This function is the controller fuction of that mentioned API.
  public function webcam_attendance(Request $request)
  {
    // Post request received data
    // $type = $request->type;

    $idno = $request->idno;
    $branch_uid = $request->branch_uid;
    $email_preference = DB::table('people')->where('idno',$idno)->value('attendance_email_preference');
    $branch_id = DB::table('branches')->where('branch_uid',$branch_uid)->value('id');
    $timezone = DB::table('settings')->where('branch_id',$branch_id)->value('timezone');

    if ($idno) {
      $existing_user = User::where('branch_id', $branch_id)->where('idno', $idno)->first();
      if ($existing_user) {
        $reference = $existing_user->id;
      } else {
        return "User not found !";
      }
    } else {
      return "No IDNO detected !";
    }

    $ongoing_attendance_today = table::attendance()->where('branch_id', $branch_id)->where('reference', $reference)->whereNull('timeout')->first();
    // return $ongoing_attendance_today->id;
    if ($ongoing_attendance_today) {
      $type = 1;
    } else {
      $type = 0;
    }

    $lastseen = Carbon::now($timezone)->format('Y-m-d h:i:s A');
    // return $lastseen;
    

    // Necessary Date
    $current_date = Carbon::now($timezone)->format('Y-m-d');
    $lastseen_date = date("Y-m-d", strtotime($lastseen));

    // For entry camera
    if ($type == 0) {
      $user = User::find($reference);

      if (!$user) {
        return 'User not found with the given reference id';
      }

      // Finds existing attendance information from database.
      $existing_attendance = table::attendance()->where('branch_id', $branch_id)->where('reference', $user->id)->whereNotNull('timein')->whereNull('timeout')->orderBy('id', 'desc')->first();

      // If the person still infront of the entry camera before going out.
      if ($existing_attendance) {
        return "Your attendance information already exists";
      }

      // If the person come back from outside from break.
      $ongoing_closed_attendance = table::attendance()->where('branch_id', $branch_id)->where('reference', $reference)->whereNotNull('timeout')->latest('date')->first();

      // return $ongoing_closed_attendance->id;

      if ($ongoing_closed_attendance) {
        $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $ongoing_closed_attendance->timeout);
        $time2 = $lastseen;
        $th = $time1->diffInHours($time2);
      }
      // return $th;
      // There exist ongoing attendance and last timeout in less than 4 hours.
      if ($ongoing_closed_attendance && ($th <= 4)) {
        $start_at = Carbon::createFromFormat("Y-m-d h:i:s A", $ongoing_closed_attendance->timeout);
        $end_at = Carbon::createFromFormat("Y-m-d h:i:s A", $lastseen);

        $break = DB::table('daily_breaks')->insert(
          ['reference' => $reference, 'branch_id'=>$branch_id,'attendance_id' => $ongoing_closed_attendance->id, 'start_at' => $start_at, 'end_at' => $end_at]
        );

        // If the break is successfully created, timout become null again.
        if ($break) {
          $attendance = table::attendance()->where('branch_id', $branch_id)->where('id', $ongoing_closed_attendance->id)->update(
            array(
              'timeout' => NULL,
              'totalhours' => NULL,
              'status_timeout' => NULL,
            )
          );

          if ($attendance) {
            $message = "Hi " . $user->firstname . " " . $user->lastname . " " . '(' . $user->idno . ')' . ", Welcome back from Break !";
            return "Welcome back from break.";
            return $message;
          } else {
            return "Break is recorded but failed to modify attendance timeout.";
          }
        } else {
          return "Failed to record break.";
        }
      }

      // If there exist no ongoing attendance or timeout more than 4 hours ago.
      $fresh_attendance = table::attendance()->where('branch_id', $branch_id)->where('reference', $reference)->whereDate('timein', $lastseen_date)->exists();

      if (!$fresh_attendance) {
        $assigned_schedule_id = table::schedules()->where('branch_id', $branch_id)->where([['reference', $reference], ['active_status', 1]])->value('schedule_id');
        $schedule_template = table::sch_template()->where('branch_id', $branch_id)->where('id', $assigned_schedule_id)->first();

        $today = Carbon::now($timezone);
        $day = strtolower($today->isoFormat('dddd'));

        $day_today = $schedule_template->$day;
        if ($day_today) {
          $str_arr = explode("-", $day_today);
          $in_time = $str_arr[0];
          $out_time = $str_arr[1];
        } else {
          $in_time = NULL;
          $out_time = NULL;
        }


        $time = date('h:i:s A');

        if ($in_time == NULL) {
          $status_in = "Not Scheduled";
        } else {
          // $sched_clock_in_time_24h = date("H.i", strtotime($sched_in_time));
          $time_in_24h = date("H.i", strtotime($time));
          // return[$in_time,$time_in_24h];
          if ($time_in_24h <= $in_time) {
            $status_in = 'In Time';
          } else {
            $status_in = 'Late Arrival';
          }
        }

        $attendance = table::attendance()->insert([
          [
            'branch_id' => $branch_id,
            'idno' => $user->idno,
            'reference' => $reference,
            'date' => $current_date,
            'employee' => $user->firstname . " " . $user->lastname,
            'timein' => $lastseen,
            'status_timein' => $status_in,
            'comment' => "",
            'schedule_id' => $assigned_schedule_id,
          ],
        ]);
        $people = table::people()->where('branch_id', $branch_id)->where('idno', $user->idno)->first();
        if ($attendance) {
          $message = "Hi " . $user->firstname . " " . $user->lastname . " " . '(' . $user->idno . ')' . ", You are Successfully clocked in !\n Welcome to Office.";
          if($email_preference == 1){
         Mail::send('email.timein', ['name' => $user->firstname . " " . $user->lastname, 'date' =>  date("d M, Y", strtotime($current_date)), 'timein' => date("m-d-Y h:i:s A", strtotime($lastseen))], function ($m) use ($people) {
           $m->from('info@attendancekeeper.net', 'Attendance Keeper');

            $m->to($people->companyemail)->subject('Attendance Confirmation!');
       });
      }
          return $message;
        } else {
          return "Attendance failed to be recorded!";
        }
      }
    }

    // For Exit camera
    elseif ($type == 1) {
      // return 'Seems like you are already logged in and trying to timeout';
      // $existing_attendance = table::attendance()->where('reference', $reference)->whereDate('timein', $lastseen_date)->whereNull('timeout')->exists();
      $user = User::where('id', $reference)->where('branch_id', $branch_id)->first();
      // return $user->firstname;
      if ($user) {
        $existing_attendance = table::attendance()->where('branch_id', $branch_id)->where('reference', $reference)->whereNotNull('timein')->whereNull('timeout')->orderBy('id', 'desc')->first();
        // return $existing_attendance->timein;
      } else {
        return 'User not found with the given reference id';
      }
      if ($existing_attendance) {
        // return $existing_attendance->timein;
        $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $existing_attendance->timein);
        $time2 = $lastseen;
        $the = $time1->diffInHours($time2);
      }

      // return $the;

      // If there exist ongoing attendace
      if ($existing_attendance && ($the < 16)) {
        $assigned_schedule_id = table::schedules()->where('branch_id', $branch_id)->where([['reference', $reference], ['active_status', 1]])->value('schedule_id');
        $schedule_template = table::sch_template()->where('branch_id', $branch_id)->where('id', $assigned_schedule_id)->first();

        $today = Carbon::now($timezone);
        $day = strtolower($today->isoFormat('dddd'));

        $day_today = $schedule_template->$day;

        

        if ($day_today) {
          $str_arr = explode("-", $day_today);
          $in_time = $str_arr[0];
          $out_time = $str_arr[1];
          $time11 = explode(':', $in_time);
          $time22 = explode(':', $out_time);
          $hours1 = $time11[0];
          $hours2 = $time22[0];
          $mins1 = $time11[1];
          $mins2 = $time22[1];
          $hours = $hours2 - $hours1;
          $mins = 0;
          if ($hours < 0) {
            $hours = 24 + $hours;
          }
          if ($mins2 >= $mins1) {
            $mins =(int) $mins2 - (int)$mins1;
          } else {
            $mins = ( (int)$mins2 + 60) - (int)$mins1;
            $hours--;
          }
          if ($mins < 9) {
            $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);
          }
          if ($hours < 9) {
            $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);
          }
          $total_scheduled_time = ($hours * 60) + $mins;


          // return $out_time;
        } else {
     // if schedule not exist
        
          
          $time_in = date("H:i", strtotime($existing_attendance->timein));
          $time_out_24h = date("H:i", strtotime($lastseen));
        

          $time1 = explode(':', $time_in);
          $time2 = explode(':', $time_out_24h);
          $hours1 = $time1[0];
          $hours2 = $time2[0];
          $mins1 = $time1[1];
          $mins2 = $time2[1];
          $hours = $hours2 - $hours1;
          $mins = 0;
          if ($hours < 0) {
            $hours = 24 + $hours;
          }
          if ($mins2 >= $mins1) {
            $mins = (int)$mins2 - (int)$mins1;
          } else {
            $mins = ($mins2 + 60) - $mins1;
            $hours--;
          }
          if ($mins < 9) {
            $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);
          }
          if ($hours < 9) {
            $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);
          }
          $totalHours = ($hours * 60) + $mins;
        
         $attendance_no_schedule = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->update(
          array(
            'timeout' => $lastseen,
            'totalhours' => $totalHours,
            'overtime_mins' => Null,
            'status_timeout' => "Not Scheduled",
          )
        );
        
         if ($attendance_no_schedule) {
          $message = "Hi " . $user->firstname . " " . $user->lastname . " " . '(' .  $user->idno . ')' . ", You are Successfully Clocked Out ! Today you worked : " .$hours." Hours & ". $mins. " Minutes . See You Soon.";
          
        //  if($email_preference == 1){
        //  $attendance_existence = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->first();
        //   Mail::send('email.timeout', ['name' => $attendance_existence->employee, 'timein' => date("m-d-Y h:i:s A", strtotime($attendance_existence->timein)), 'timeout' =>  date("m-d-Y h:i:s A", strtotime($attendance_existence->timeout)), 'totalhours' => $attendance_existence->totalhours, 'date' => $attendance_existence->date], function ($m) use ($people) {
        //   $m->from('info@attendancekeeper.net', 'Attendance Keeper');

        //   $m->to($people->companyemail)->subject('Attendance Confirmation!');
        //  });
        //  }
          return $message;
        }
          
           
          
        }


        if ($out_time == NULL) {
          $status_out = "Not Scheduled";
        } else {
          $time_in_old = $existing_attendance->timein;
          $time_in = date("H:i", strtotime($time_in_old));
          // $time_in = $time_in_old->format("H:i");


          // $sched_clock_out_time_24h = date("H.i", strtotime($sched_out_time));
          $time_out_24h = date("H:i", strtotime($lastseen));
        

          $time1 = explode(':', $time_in);
          $time2 = explode(':', $time_out_24h);
          $hours1 = $time1[0];
          $hours2 = $time2[0];
          $mins1 = $time1[1];
          $mins2 = $time2[1];
          $hours = $hours2 - $hours1;
          $mins = 0;
          if ($hours < 0) {
            $hours = 24 + $hours;
          }
          if ($mins2 >= $mins1) {
            $mins = (int)$mins2 - (int)$mins1;
          } else {
            $mins = ($mins2 + 60) - $mins1;
            $hours--;
          }
          if ($mins < 9) {
            $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);
          }
          if ($hours < 9) {
            $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);
          }
          $total_worked_time = ($hours * 60) + $mins;



          if ($total_scheduled_time <= $total_worked_time) {
            $overtime = $total_worked_time - $total_scheduled_time;
            // return [$time_out_24h,$time_in_old,$time_in,$total_worked_time,$total_scheduled_time,$overtime];
            $status_out = 'On Time';
          } else {
            $status_out = 'Early Departure';
          }
        }
        $clockInDate = $existing_attendance->date;
        $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $existing_attendance->timein);
        $time2 = Carbon::createFromFormat("Y-m-d h:i:s A", $lastseen);
        $th = $time1->diffInHours($time2);
        $tm = floor(($time1->diffInMinutes($time2) - (60 * $th)));
        $totalhour = $th . "." . $tm;
        $attendance_existence = table::attendance()->where('id', $existing_attendance->id)->get();
        if ($total_scheduled_time <= $total_worked_time) {
        $attendance = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->update(
          array(
            'timeout' => $lastseen,
            'totalhours' => $totalhour,
            'overtime_mins' => $overtime,
            'status_timeout' => $status_out
          )
        );
      }
      else {
        $attendance = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->update(
          array(
            'timeout' => $lastseen,
            'totalhours' => $totalhour,
            'status_timeout' => $status_out
          )
        );
      }
        $attendance_existence = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->first();
        $people = table::people()->where('branch_id', $branch_id)->where('idno', $attendance_existence->idno)->first();
        if ($attendance) {
          $message = "Hi " . $user->firstname . " " . $user->lastname . " " . '(' .  $user->idno . ')' . ", You are Successfully Clocked Out ! Today you worked : " .$th." Hours & ". $tm. " Minutes . See You Soon.";
          // return "Clock out Successfully";
         if($email_preference == 1){
          Mail::send('email.timeout', ['name' => $attendance_existence->employee, 'timein' => date("m-d-Y h:i:s A", strtotime($attendance_existence->timein)), 'timeout' =>  date("m-d-Y h:i:s A", strtotime($attendance_existence->timeout)), 'totalhours' => $attendance_existence->totalhours, 'date' => $attendance_existence->date], function ($m) use ($people) {
          $m->from('info@attendancekeeper.net', 'Attendance Keeper');

          $m->to($people->companyemail)->subject('Attendance Confirmation!');
         });
         }
          return $message;
        }
      } elseif ($existing_attendance && ($the >= 16)) {
        $start_time = Carbon::createFromFormat("Y-m-d h:i:s A", $existing_attendance->timein);
        $end_time = date('Y-m-d h:i:s A', strtotime($start_time) + 57600);
        $total_hours = 16.0;

        $attendance = table::attendance()->where('branch_id', $branch_id)->where('id', $existing_attendance->id)->update(
          array(
            'timeout' => $end_time,
            'totalhours' => $total_hours,
            'status_timeout' => "Forgot"
          )
        );

        // If there exist no ongoing attendance or timeout more than 4 hours ago.
        $fresh_attendance = table::attendance()->where('branch_id', $branch_id)->where('reference', $reference)->whereDate('timein', $lastseen_date)->exists();

        if (!$fresh_attendance) {
          $assigned_schedule_id = table::schedules()->where('branch_id', $branch_id)->where([['reference', $reference], ['active_status', 1]])->value('schedule_id');
          $schedule_template = table::sch_template()->where('branch_id', $branch_id)->where('id', $assigned_schedule_id)->first();

          $today = Carbon::now($timezone);
          $day = strtolower($today->isoFormat('dddd'));

          $day_today = $schedule_template->$day;
          if ($day_today) {
            $str_arr = explode("-", $day_today);
            $in_time = $str_arr[0];
            $out_time = $str_arr[1];
          } else {
            $in_time = NULL;
            $out_time = NULL;
          }


          $time = date('h:i:s A');

          if ($in_time == NULL) {
            $status_in = "Not Scheduled";
          } else {
            // $sched_clock_in_time_24h = date("H.i", strtotime($sched_in_time));
            $time_in_24h = date("H.i", strtotime($time));

            if ($time_in_24h <= $in_time) {
              $status_in = 'In Time';
            } else {
              $status_in = 'Late Arrival';
            }
          }
          
          $attendance = table::attendance()->insert([
            [
              'branch_id' => $branch_id,
              'idno' => $user->idno,
              'reference' => $reference,
              'date' => $current_date,
              'employee' => $user->firstname . " " . $user->lastname,
              'timein' => $lastseen,
              'status_timein' => $status_in,
              'comment' => "",
              'schedule_id' => $assigned_schedule_id,
            ],
          ]);
          $people = table::people()->where('branch_id', $branch_id)->where('idno', $user->idno)->first();

          if ($attendance) {
            $message = "Hi " . $user->firstname . " " . $user->lastname . " " . '(' .  $user->idno . ')' . ",You Forgot To Clock Out Your Last Attendance. Your Today's Attendance Recoreded Successfully !\n Welcome to Office.";
           if($email_preference == 1){
           Mail::send('email.late_timeout', ['name' => $user->firstname . " " . $user->lastname, 'date' =>  date("d M, Y", strtotime($current_date)), 'timein' => date("m-d-Y h:i:s A", strtotime($lastseen))], function ($m) use ($people) {
             $m->from('info@attendancekeeper.net', 'Attendance Keeper');

             $m->to($people->companyemail)->subject('Attendance Confirmation!');
            });
            }
            return $message;
          } else {
            return "Attendance failed to be recorded!";
          }
        }
      } else {
        return "No attendace there";
      }
    }
  }
}

    // Attendace using the Mobile App / CCTV Camera.
    // The remote camera devices are connected with this controller function
    // through an API. This function is the controller fuction of that mentioned API.
    // public function webcam_attendance(Request $request)
    // {
    //   // Post request received data
    //   $type = $request->type;
    //   $reference = $request->reference;
    //   $lastseen = Carbon::now()->format('Y-m-d h:i:s A');

    //   // Necessary Date
    //   $current_date = Carbon::now()->format('Y-m-d');
    //   $lastseen_date = date("Y-m-d", strtotime($lastseen));

    //   // For entry camera
    //   if ($type == 0) {

    //     $user = User::where('id', $reference)->first();

    //     if (!$user) {
    //       return 'User not found with the given reference id';
    //     }

    //     // Finds existing attendance information from database.
    //     $existing_attendance = table::attendance()->where('reference', $user->id)->whereNotNull('timein')->whereNull('timeout')->orderBy('id', 'desc')->first();

    //     // If the person still infront of the entry camera before going out.
    //     if ($existing_attendance)
    //     {
    //       return "Your attendance information already exists";
    //     }

    //     // If the person come back from outside from break.
    //     $ongoing_closed_attendance = table::attendance()->where('reference', $reference)->whereNotNull('timeout')->orderBy('id', 'desc')->first();

    //     if ($ongoing_closed_attendance) {
    //       $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $ongoing_closed_attendance->timeout);
    //       $time2 = $lastseen;
    //       $th = $time1->diffInHours($time2);
    //     }

    //     // There exist ongoing attendance and last timeout in less than 4 hours.
    //     if ($ongoing_closed_attendance && ($th <= 4))
    //     {
    //       $start_at = Carbon::createFromFormat("Y-m-d h:i:s A", $ongoing_closed_attendance->timeout);
    //       $end_at = Carbon::createFromFormat("Y-m-d h:i:s A", $lastseen);

    //       $break = DB::table('daily_breaks')->insert(
    //           ['reference' => $reference, 'attendance_id' => $ongoing_closed_attendance->id , 'start_at' => $start_at, 'end_at' => $end_at]
    //       );

    //       // If the break is successfully created, timout become null again.
    //       if ($break) {
    //         $attendance = table::attendance()->where('id', $ongoing_closed_attendance->id)->update(array(
    //             'timeout' => NULL,
    //             'totalhours' => NULL,
    //             'status_timeout' => NULL,)
    //         );

    //         if ($attendance) {
    //           return "Welcome back from break.";
    //         }
    //         else{
    //           return "Break is recorded but failed to modify attendance timeout.";
    //         }
    //       }else {
    //         return "Failed to record break.";
    //       }

    //     }

    //     // If there exist no ongoing attendance or timeout more than 4 hours ago.
    //     $fresh_attendance = table::attendance()->where('reference', $reference)->whereDate('timein', $lastseen_date)->exists();

    //     if(!$fresh_attendance){
    //       $assigned_schedule_id = table::schedules()->where([['reference', $reference],['active_status', 1]] )->value('schedule_id');
    //       $schedule_template = table::sch_template()->where('id', $assigned_schedule_id)->first();

    //       $today = Carbon::now();
    //       $day = strtolower($today->isoFormat('dddd'));

    //       $day_today = $schedule_template->$day;
    //       if ($day_today) {
    //         $str_arr = explode ("-", $day_today);
    //         $in_time = $str_arr[0];
    //         $out_time = $str_arr[1];
    //       }else {
    //         $in_time = NULL;
    //         $out_time = NULL;
    //       }


    //       $time = date('h:i:s A');

    //       if($in_time == NULL)
    //       {
    //           $status_in = "Not Scheduled";
    //       } else {
    //           // $sched_clock_in_time_24h = date("H.i", strtotime($sched_in_time));
    //           $time_in_24h = date("H.i", strtotime($time));

    //           if ($time_in_24h <= $in_time)
    //           {
    //               $status_in = 'In Time';
    //           } else {
    //               $status_in = 'Late Arrival';
    //           }
    //       }

    //       $attendance = table::attendance()->insert([
    //           [
    //               'idno' => $user->idno,
    //               'reference' => $reference,
    //               'date' => $current_date,
    //               'employee' => $user->firstname." ".$user->lastname,
    //               'timein' => $lastseen,
    //               'status_timein' => $status_in,
    //               'comment' => "",
    //               'schedule_id' => $assigned_schedule_id,
    //           ],
    //       ]);

    //       if ($attendance) {
    //         return "Attendance recorded Successfully";
    //       }else {
    //         return "Attendance failed to be recorded!";
    //       }
    //     }

    //   }

    //   // For Exit camera
    //   elseif ($type == 1) {
    //     // $existing_attendance = table::attendance()->where('reference', $reference)->whereDate('timein', $lastseen_date)->whereNull('timeout')->exists();
    //     $user = User::where('id',$reference)->first();
    //     if ($user) {
    //       $existing_attendance = table::attendance()->where('reference', $reference)->whereNotNull('timein')->orderBy('id', 'desc')->first();
    //     }else {
    //       return 'User not found with the given reference id';
    //     }

    //     // If there exist ongoing attendace
    //     if ($existing_attendance)
    //     {
    //       $assigned_schedule_id = table::schedules()->where([['reference', $reference],['active_status', 1]] )->value('schedule_id');
    //       $schedule_template = table::sch_template()->where('id', $assigned_schedule_id)->first();

    //       $today = Carbon::now();
    //       $day = strtolower($today->isoFormat('dddd'));

    //       $day_today = $schedule_template->$day;
    //       if ($day_today) {
    //         $str_arr = explode ("-", $day_today);
    //         $in_time = $str_arr[0];
    //         $out_time = $str_arr[1];
    //       }else{
    //         $in_time = NULL;
    //         $out_time = NULL;
    //       }


    //       if($out_time == NULL)
    //       {
    //           $status_out = "Not Scheduled";
    //       } else {
    //           // $sched_clock_out_time_24h = date("H.i", strtotime($sched_out_time));
    //           $time_out_24h = date("H.i", strtotime($lastseen));

    //           if($time_out_24h >= $out_time)
    //           {
    //               $status_out = 'On Time';
    //           } else {
    //               $status_out = 'Early Departure';
    //           }
    //       }
    //       $clockInDate = $existing_attendance->date;
    //       $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $existing_attendance->timein);
    //       $time2 = Carbon::createFromFormat("Y-m-d h:i:s A", $lastseen);
    //       $th = $time1->diffInHours($time2);
    //       $tm = floor(($time1->diffInMinutes($time2) - (60 * $th)));
    //       $totalhour = $th.".".$tm;

    //       $attendance = table::attendance()->where('id', $existing_attendance->id)->update(array(
    //           'timeout' => $lastseen,
    //           'totalhours' => $totalhour,
    //           'status_timeout' => $status_out)
    //       );

    //       if ($attendance) {
    //         return "Clock out Successfully";
    //       }

    //     }else{
    //       return "No attendace there";
    //     }
    //   }

    // }
//}
