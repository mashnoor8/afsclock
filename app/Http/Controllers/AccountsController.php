<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\table;
use DB;
use Carbon\Carbon;
use app\User;
use Cartalyst\Sentinel\Users\EloquentUser;
use Sentinel;
use Reminder;
use App\Mail\forgot_password;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;



class AccountsController extends Controller
{
  public function validatePasswordRequest(Request $request){

    
            //You can add validation login here
            $user = table::people()->where('companyemail', '=', $request->email)->first();
            //Check if the user exists
            
                if (!$user) {
            return redirect()->back()->withErrors(['companyemail' => trans('User does not exist')]);
            }

            //Create Password Reset Token
            DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => str_random(60),
            'created_at' => Carbon::now()
            ]);

            //Get the token just created above
            $tokenData = DB::table('password_resets')->where('email', $request->email)->first();

            if (!$this->sendResetEmail($request->email, $tokenData->token)) {
                $notification=array(
                    'message' => 'A reset link has been sent to your email address!',
                    'alert-type' => 'success'
                );
              
            return redirect()->back()->with($notification);
            } else {
                $notification=array(
                    'message' => 'A Network Error occurred. Please try again!',
                    'alert-type' => 'error'
                );
            return redirect()->back()->with($notification);
            }

}

public function resetPassword(Request $request)
{
    //Validate input
    $validator = Validator::make($request->all(), [
        'email' => 'required|email|exists:people,companyemail',
        'password' => 'required|confirmed',
        'token' => 'required' ]);

    //check if payload is valid before moving on
    if ($validator->fails()) {
        $notification=array(
            'message' => 'Please complete the form!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    $password = $request->password;
// Validate the token
    $tokenData = DB::table('password_resets')
    ->where('token', $request->token)->first();
// Redirect the user back to the password reset request form if the token is invalid
    if (!$tokenData) return view('auth.passwords.email');

    $user_password = \Hash::make($password);
    // $user = table::people()->where('companyemail', '=', $request->email)->first();
    $user = table::people()->where('companyemail', '=', $request->email)->update(['password'=> $user_password ,'updated_at' => Carbon::now()]);
   
// Redirect the user back if the email is invalid
    if (!$user) 
    {
        $notification=array(
            'message' => 'Email not found!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
          
   
    }
    // $user->update(['password'=> $user_password ]); //or $user->save();
    else
    {
    $notification=array(
        'message' => 'Your password has been changed a few minutes ago. Please login to continue!',
        'alert-type' => 'Success'
    );

    return redirect()->route('login')->with($notification);
    }

    //Delete the token
    DB::table('password_resets')->where('email', $user->companyemail)
    ->delete();

    //Send Email Reset Success Email
    if ($this->sendSuccessEmail($tokenData->email)) {
        return view('index');
    } else {
        return redirect()->back()->withErrors(['email' => trans('A Network Error occurred. Please try again.')]);
    }

}


  private function sendResetEmail($email, $token)
{
//Retrieve the user from the database
$user = table::people()->where('companyemail', $email)->select('firstname', 'lastname','companyemail')->first();
//Generate, the password reset link. The token generated is embedded in the link
$link = url('/') . '/password/reset/' . $token . '?email=' . urlencode($user->companyemail);
// dd($link);

$data=['firstname'=>$user->firstname,'lastname'=>$user->lastname,'companyemail'=>$user->companyemail,'link'=>$link];


// Mail::send('email.forgot',['link'=>$link], function ($message) use ($data) {
//     $message->from('mashfiqurrr@gmail.com', 'News Information');
//     $message->to($data['companyemail']);
//     $message->subject($data['firstname']);
//     });

Mail::to($data['companyemail'])->send(new forgot_password($data));

// Mail::send(['text'=>'mail'],[$user->firstname,$link], function($message){
//     $message->to('mashfiqurrr@gmail.com','CLOCK')->subject('Test Mail');
//     $message->from('mashfiqurrr@gmail.com','web title');
//     // $message->subject("Appointment Scheduled");
   
// });
// Mail::send(
//     'email.forgot',
//     ['user'=>$user],
//     function($message) use ($user){

//         $message->to($user->companyemail);
//         $message->subject("$user->firstname,Reset Your Password!");
//     }


// );


    // try {
    // //Here send the link with CURL with an external email API 
    //     return true;
    // } catch (\Exception $e) {
    //     return false;
    // }
}

public function forgot(){

    return view('auth.passwords.reset_new');
}

public function forgot_email(Request $request){
//  dd($request->all());
    $user = table::people()->where('companyemail', '=', $request->email)->first();
      
            //Check if the user exists
            
    if (!$user) {
                return redirect()->back()->with(['error' => 'User does not exist']);
                }
     $user = Sentinel::findUserById($user->id);
     dd($user);  
     $reminder = Reminder::exists($user) ? : Reminder::create($user);
     $this->sendEmail($user,$reminder->code);
     return redirect()->back()->with(['success' => 'Email Sent!']);

}

public function sendEmail($user,$code){
    Mail::send(
        'email.forgot',
        ['user'=>$user,'code'=>$code],
        function($message) use ($user){

            $message->to($user->companyemail);
            $message->subject("$user->firstname,Reset Your Password!");
        }


    );
}
}
