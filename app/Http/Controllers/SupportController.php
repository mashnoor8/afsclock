<?php

namespace App\Http\Controllers;

use App\CaseResponse;
use App\SupportCase;
use App\Classes\table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;


use Intervention\Image\Facades\Image;



class SupportController extends Controller
{
    //admin site 
    public function index(){

        return view('admin.help');
    
    }



    public function support(){

        $branch_id = Auth::user()->branch_id;
        
        $data = SupportCase::where('branch_id',$branch_id)->get();

        // dd( $data[0]->responses);

        return view('admin.support_center' , compact('data'));
    
    }


    public function case_view($id)
    {
        $user_id = Auth::user()->id;
        $branch_id = Auth::user()->branch_id; 
       

        $case = table::case()->where('id', $id)->first();

        $case_responses = CaseResponse::where('case_id', $id)
                        // ->where('branch_id', $branch_id)
                        ->get();
           
        return view('admin.case_view' ,compact('case', 'case_responses', 'user_id'));
    }


    public function admin_case_view_comment(Request $request){
       
        $user_id =  $request->user_id ;
        // dd($request->all(),  $user_id);
        Auth::loginUsingId($user_id);
        $case_id = $request->case_id;
      

        $case_screenshot = $request->file('comment_screenshot');

        if ($case_screenshot != null) {

			$case_image_name = $request->case_id .strtoupper(substr(md5(time()), 0, 6)). '.jpg';
            $resize_image = Image::make($case_screenshot)->encode('jpg', 75)->resize(500, 500);
            $destinationPath = public_path() . '/assets/Case_Image/';
            $resize_image->save("$destinationPath" . "$case_image_name");
			
		} else {
			$case_image_name = '';
		}

        $case = table::case()->where('id', $case_id)->first();
        $response =  new CaseResponse();
        $response->case_id = $request->case_id;
        $response->response = $request->response;
        $response->branch_id = $case->branch_id;
        $response->screenshot = $case_image_name;
        $response->response_by = $request->user_id ;
        $response->save();

        return back();
    }

    public function solve($id)
    {
       

        $case = table::case()->where('id', $id)->update(['status' => false]);;

       
           
        return back();
    }

    public function response_image_download($id){

        
        $image_file = CaseResponse::where('id' , $id)->value('screenshot');
     
        // $filepath = public_path('assets/Case_Image/') . $image_file;

        $download_path = ( public_path() . '/assets/Case_Image/' .  $image_file );

        return( Response::download( $download_path ) );

        
    }


    // ............................................................

    // ............................................................

    // SUPPORT SITE

    // ............................................................

    // ............................................................


    public function support_index(){

        return view('support.support_index' );
    }

    public function cases(){

        $data = table::case()->where('status' , 1)->get();

        // dd( $data);
        return view('support.cases' , compact('data'));
    }




    public function support_case_view($id){

        $case = table::case()->where('id', $id)->first();
       
        $case_responses = CaseResponse::where('case_id', $id)->get();
     
        return view('support.support_case_view' ,compact('case', 'case_responses'));

    }


    public function support_case_view_comment( Request $request){
       
        $case_id = $request->case_id;
        $case = table::case()->where('id', $case_id)->first();
        $user_id = Auth::user()->id;


        $case_screenshot = $request->file('comment_screenshot');

        if ($case_screenshot != null) {

			$case_image_name = $request->case_id .strtoupper(substr(md5(time()), 0, 6)). '.jpg';
            $resize_image = Image::make($case_screenshot)->encode('jpg', 75)->resize(500, 500);
            $destinationPath = public_path() . '/assets/Case_Image/';
            $resize_image->save("$destinationPath" . "$case_image_name");
			
		} else {
			$case_image_name = '';
		}

        
        $response =  new CaseResponse();
        $response->case_id = $request->case_id;
        $response->response = $request->response;
        $response->branch_id = $case->branch_id;
        $response->screenshot = $case_image_name;
        $response->response_by = $user_id ;
        $response->save();

        return redirect('/support/cases/case_view/' . $case_id);
    }
    public function case_image_download($id){
       
        
        $image_file = SupportCase::where('id' , $id)->value('file');
     
        
        $filepath = public_path('assets/Case_Image/') . $image_file;

        $download_path = ( public_path() . '/assets/Case_Image/' .  $image_file );
        return( Response::download( $download_path ) );



    }


    
}
