<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\table;
use App\User;
use Carbon\Carbon;
use DateTime;
use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Salary{

    public $id = '';
    public $employee ='';
    public $salary_type = '';
    public $amount = '';
    public $currency = '';

    public function __construct($id, $employee, $salary_type, $amount, $currency)
    {
        $this->id = $id;
        $this->employee = $employee;
        $this->salary_type = $salary_type;
        $this->amount = $amount;
        $this->currency = $currency;
    }
}

class MonthlySalary{

    public $employee ='';
    public $idno = '';
    public $total_working_hour = '';
    public $total_working_days = '';
    public $calculated_salary = '';
    public $currency = '';
    public $gross_salary = '';
    public $late_arrival_count = '';
    public $early_departure_count = '';
    public $total_approved_leaves = '';
    public $total_day_off = '';

    public function __construct($employee, $idno, $total_working_hour, $total_working_days, $calculated_salary, $currency, $gross_salary, $late_arrival_count, $early_departure_count, $total_approved_leaves, $total_day_off)
    {
        $this->employee = $employee;
        $this->idno = $idno;
        $this->total_working_hour = $total_working_hour;
        $this->total_working_days = $total_working_days;
        $this->calculated_salary = $calculated_salary;
        $this->currency = $currency;
        $this->gross_salary = $gross_salary;
        $this->late_arrival_count = $late_arrival_count;
        $this->early_departure_count = $early_departure_count;
        $this->total_approved_leaves = $total_approved_leaves;
        $this->total_day_off = $total_day_off;
    }
}

class HourlySalary{

    public $employee ='';
    public $idno = '';
    public $total_working_hour = '';
    public $total_working_days = '';
    public $calculated_salary = '';
    public $currency = '';
    
    public $gross_salary = '';
    public $total_late_arrival = '';
    public $total_early_departure = '';

    public function __construct($employee, $idno, $total_working_hour, $total_working_days ,$calculated_salary, $currency, $gross_salary, $total_late_arrival, $total_early_departure)
    {

        $this->employee = $employee;
        $this->idno = $idno;
        $this->total_working_hour = $total_working_hour;
        $this->calculated_salary = $calculated_salary;
        $this->currency = $currency;
        $this->total_working_days = $total_working_days;
        $this->gross_salary = $gross_salary;
        $this->total_late_arrival = $total_late_arrival;
        $this->total_early_departure = $total_early_departure;
    }
}


class SalaryController extends Controller
{
    // Add new salary type.
    // Data comes from the form placed in Salary Types page.
    // Takes only salary type name.
    public function add_salary_types(Request $request)
    {
      $salary_type = $request->type_name;
      // dd($salary_type);
      $branch_id = Auth::user()->branch_id;

      table::salary_types()->insert(['branch_id'=>$branch_id,'type' => $salary_type]);
      return redirect(url('admin/salary_types'))->with('success', 'Salary type has been created!');
    }

    // Delete Salary Type
    public function delete_salary_type($id)
    {
      $branch_id = Auth::user()->branch_id;

      table::salary_types()->where('branch_id',$branch_id)->delete($id);
      return redirect(url('admin/salary_types'))->with('success', 'Salary type has been deleted!');
    }

    // Edit Salary Type
    public function edit_salary_types($id)
    {
      $branch_id = Auth::user()->branch_id;

      $salary_type = table::salary_types()->where('branch_id',$branch_id)->where('id',$id)->first();
      return view('admin/edit_salary_type', compact('salary_type'));
    }

    // Update Salary Type
    public function update_salary_types(Request $request)
    {
      $id =  $request->id;
      $type = $request->type_name;
      $branch_id = Auth::user()->branch_id;

      $salary_type = table::salary_types()->where('branch_id',$branch_id)->where('id',$id)->first();
      if ($salary_type) {
        table::salary_types()->where('id' , $id)->update(array('type' => $type));
      }
      return redirect(url('admin/salary_types'))->with('success', 'Salary type has been updated!');
    }

    // Salary Types page
    // Displays existing salary types and
    // Form to create new salary type.
    public function salary_types(Request $request)
    {
      $branch_id = Auth::user()->branch_id;

      $salary_types = table::salary_types()->where('branch_id',$branch_id)->get();
      return view('admin.salary_types', compact('salary_types'));
    }


    // Salary main page.
    // Contains all necessary functionalities related to salary.
    public function salary(Request $request)
    {
      $title = "salary";
      
      return view('admin.salary',compact('title'));
    }

    // Employee salary page.
    public function employee_salary(Request $request)
    {

      $branch_id = Auth::user()->branch_id;

      $employee = table::people()->where('branch_id',$branch_id)->get();
      $salary_types = table::salary_types()->get();
      $employee_salaries = table::employee_salary()->where('branch_id',$branch_id)->get();

      
      $salary_collection = collect([]);

      foreach($employee_salaries as $employee_salary)
      {
        if ($employee_salary) {
        
          $user = User::find($employee_salary->reference);
          $salary_type = table::salary_types()->where('id', $employee_salary->salary_type)->first();
          // dd($salary_type , $user , $employee_salary->salary_type);
          if($salary_type)
          {
          $salary_collection->push(new Salary($employee_salary->id, $user->firstname." ".$user->lastname, $salary_type->type, $employee_salary->gross_salary, $employee_salary->currency));
          }
        }
      }

      // dd($salary_collection);

      return view('admin.employee_salary', compact('salary_collection','employee','salary_types'));
    }

    // Add Employee Salary
    public function add_employee_salary(Request $request)
    {

      // dd( $request->all());


      $v = $request->validate([
        'reference' => 'required|unique:employee_salary|alpha_dash_space|max:10',
      ]);
    
      // dd( $salary_type);
      $salary_type = $request->salary_type;
      $salary_amount = $request->salary_amount;
      $employee_id = $request->reference;
      $currency = $request->currency;

      // dd($employee_id);
      $branch_id = Auth::user()->branch_id;

      table::employee_salary()->insert(['branch_id'=>$branch_id,'reference'=>$employee_id,'salary_type' => $salary_type,'gross_salary' => $salary_amount, 'currency' => $currency]);
      return redirect(url('admin/employee_salary'))->with('success', 'Employee salary has been added!');
    }


    public function delete_employee_salary($id)
    {
      $branch_id = Auth::user()->branch_id;

      table::employee_salary()->where('branch_id',$branch_id)->delete($id);
      return redirect(url('admin/employee_salary'))->with('success', 'Employee Salary has been deleted!');
    }

    // Holidays main page
    public function holidays(Request $request)
    {
      $branch_id = Auth::user()->branch_id;
      $title ="holidays";
      $holidays = table::holidays()->where('branch_id',$branch_id)->orderby('month')->get();
      return view('admin.holidays', compact('holidays', 'title'));
    }


    // Add Holidays
    public function add_holidays(Request $request)
    {
      $month = $request->month;
      $dates = $request->dates;
      $branch_id = Auth::user()->branch_id;

      $existing_holiday = table::holidays()->where('month', $month)->first();
      if ($existing_holiday) {
        return redirect(url('admin/holidays'))->with('error', 'The follwing month already exist!');
      }else
      {
        table::holidays()->insert(['branch_id'=>$branch_id,'month'=>$month,'dates' => $dates]);
        return redirect(url('admin/holidays'))->with('success', 'Holiday has been added!');
      }
    }

    // Delete Holidays
    public function delete_holidays($id)
    {
      $branch_id = Auth::user()->branch_id;

      table::holidays()->where('branch_id',$branch_id)->delete($id);
      return redirect(url('admin/holidays'))->with('success', 'Holidays have been deleted!');
    }

    // public function edit_holidays($id)
    // {
    //   $holiday = table::holidays()->where('id', $id)->first();
    //
    //   return view('admin.edit_holidays', compact('holiday'));
    // }
    //
    // public function update_holidays(Request $request)
    // {
    //
    //   $month = $request->month;
    //   $dates = $request->dates;
    //
    //   $holiday = table::holidays()->where('month', $month)->first();
    //   if ($holiday)
    //   {
    //     table::holidays()->where('month' , $month)->update(array('dates' => $dates));
    //     return redirect(url('admin/holidays'))->with('success', 'Holidays have been deleted!');
    //   }else{
    //     return redirect(url('admin/holidays'))->with('error', 'Holiday month does not exist!');
    //   }
    // }




    
    public function calculate_monthly_salary(Request $request)
    {
      // Month and Year form the salary form data via POST request.
      $month = (int)$request->month;
    
      $year = (int)$request->year;
      $branch_id = Auth::user()->branch_id;

      if(!table::attendance()->where('branch_id',$branch_id)->whereMonth('date', $month)->whereYear('date',$year)->exists()) 
      {
        return redirect('admin/salary')->with('error', 'No Attendance data available according to the following Month and Year.');
      }

      $all_employee = table::people()->where('branch_id',$branch_id)->where('acc_type', 1)->get();
      // dd( $all_employee);
      $salary_collection = collect([]);
      $all_employee_attendance = array();
      $sub_total = 0;
      $days_in_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);



      // $atn=2;

      // $schedule_info = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.reference', $atn)->get();

      // dd( $schedule_info );

      foreach($all_employee as $employee)
      {

        $total_working_days = 0;
        $total_working_minutes = 0;
        $total_expected_working_hours = 0.0;
        $total_day_off = 0;
        $total_approved_leaves = 0;
        $total_paid_holidays = 0;
        $total_paid_amount = 0;

        $total_earned_salary = 0.0;

        $total_late_arrival = 0;
        $total_early_departure = 0;
        

        // Finds the salary information of the following employee.
        $salary = table::employee_salary()->where('branch_id',$branch_id)->where([['reference', $employee->id], ['salary_type', 1]])->first();
        
        // Finds all attendances of the following month 
        // for the following employee.
        $attendances = table::attendance()->where('branch_id',$branch_id)->whereMonth('date',$month)->where('reference', $employee->id)->whereNotNull('timeout')->get();
        // dd($attendances, $salary);
       
        $total_working_days = count($attendances);
        // dd($total_working_days, $salary);

      
        if($salary)
        {
          // dd($salary ,$employee);
        
          // Finds the number of days in the following month
          
          $salary_per_day = $salary->gross_salary / $days_in_this_month;

          // dd($salary_per_day);
          $all_attendance = array();
                            
          // All attendances iteration in order to calculate work hours 
          // dd($attendances,$all_attendance);
          foreach($attendances as $attendance )
          {
            // dd($attendance);
            if($attendance)
            {
              // Parsing attendance date into carbon date object
              $attendance_date = Carbon::parse($attendance->date);

              if($attendance->status_timein == "Late Arrival")
              {
                $total_late_arrival += 1;
              }
              
              if($attendance->status_timeout == "Early Departure")
              {
                $total_early_departure += 1;
              }

              // Finds schedule information for the associated attendance.
              $schedule = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where('schedules.id', $attendance->schedule_id)->value(strtolower($attendance_date->format('l')));
              // dd($schedule,  $attendance->schedule_id);
              if(!$schedule){
              // dd($schedule );
              $schedule = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where([['schedules.reference', $attendance->reference],['active_status',True]])->value(strtolower($attendance_date->format('l')));
                  
              }
              $schedule_info = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where('schedules.id', $attendance->schedule_id)->first();
              if(!$schedule_info){
              $schedule_info = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where([['schedules.reference', $attendance->reference],['active_status',True]])->first();
                
              }
              // dd($schedule,$schedule_info);
              // $schedule = table::schedules()->where('id', 1)->first();
              // dd( $employee->idno, $employee->firstname, $schedule, count($attendances), $attendance->schedule_id);
              // If there exist schedule informations
              // dd($attendance, $schedule,$schedule_info ,$employee);
              if($schedule && $schedule_info)
              {

                $str_arr = explode ("-", $schedule);
                $start = $str_arr[0];
                $end = $str_arr[1];
                $scheduled_work_hour = gmdate("H:i", strtotime($end) - strtotime($start)); // Find time difference in 24 hours
                // dd($scheduled_work_hour);
                $total_expected_working_hours += floatval($scheduled_work_hour);
                $break = DB::select("select sec_to_time(sum(time_to_sec(x.diff))) as result from (select TIMEDIFF(TIME(end_at), TIME(start_at)) as diff from daily_breaks where attendance_id =" . $attendance->id ." ) as x;");
                $total_break = $break[0]->result;

                // Finds total work hour from attendance
                $total_work_hour_from_attendance = $attendance->totalhours;
                // dd($total_work_hour_from_attendance,$total_break);
                // If there exist break information
                if($total_break)
                {

                  // Total break taken in this folling attendence
                  $timesplit=explode(':',$total_break);
                  $total_break_minutes = ($timesplit[0]*60)+($timesplit[1])+($timesplit[2]>30?1:0);
                  $break_allowence = $schedule_info->break_allowence;

                  if($total_break_minutes > $break_allowence)
                  {
                    $extra_break_minutes = $total_break_minutes - $break_allowence;
                    $actual_work_minutes = (floatval($total_work_hour_from_attendance) * 60) - $extra_break_minutes;
                  }
                  else
                  {
                    // Amount of minutes that employee actually work.
                    $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                  }

                }
                else{
                  // Amount of minutes that employee actually work.
                  $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                  // dd($actual_work_minutes);
                }

                // Amount of minutes that employee had to work.
                $scheduled_work_minutes = floatval($scheduled_work_hour) * 60;
                // dd($actual_work_minutes,$scheduled_work_minutes );

                // If actual work minute is grater than scheduled work minutes.
                if($actual_work_minutes > $scheduled_work_minutes )
                {
                  $total_valid_work_minutes =  $scheduled_work_minutes;
                  
                }
                // If actual work minute is less than scheduled work minutes.
                else
                {
                  $total_valid_work_minutes =  $actual_work_minutes;
                }
                
                $salary_per_minute = $salary_per_day / $scheduled_work_minutes;
                // dd($salary_per_minute);
                $earned_salary_today = $salary_per_minute * $total_valid_work_minutes;
              
                $total_working_minutes += $total_valid_work_minutes;
                // dd($earned_salary_today ,$total_working_minutes);

                $all_attendance[$attendance->date] = $total_valid_work_minutes;
                // dd($all_attendance);
                $all_employee_attendance[$employee->firstname] = $all_attendance;
                // $total_working_days += 1;
                $total_earned_salary += $earned_salary_today; 
                // dd($total_earned_salary, $total_working_days, $total_working_minutes);

              }
            }
            // dd($total_working_minutes/60);
            // $all_attendance[]
          } // End of attendance loop
            // dd($all_attendance);
            // dd( $total_working_minutes);

            // If there does not exist any attendance information,
            // Checks if there exist any holidays or leave.
            
            $leaves = table::leaves()->where('branch_id',$branch_id)->where([['reference', $employee->id],['status', 'Approved']])->whereMonth('leavefrom', $month)->whereYear('leavefrom', $year)->get();
              
            foreach($leaves as $leave)
            {
              $start = Carbon::parse($leave->leavefrom);
              $end = Carbon::parse($leave->leaveto);
              $end = $end->addDays(1);
              $leave_duration = $start->diffInDays($end);
              $total_approved_leaves += $leave_duration;
              // dd($total_approved_leaves);
            }

            $total_earned_salary += $total_approved_leaves * $salary_per_day;
            // dd($total_earned_salary);

            // If there exist any holiday in the following month
            // The days will be counted as paid day.

            $holidays = table::holidays()->where('branch_id',$branch_id)->where('month', $month)->first();

            if ($holidays) 
            {
              $holidays = explode(",", $holidays->dates);
              $total_holidays = sizeof($holidays);
            }
            else 
            {
              $total_holidays = 0;
            }

            $total_earned_salary += $total_holidays * $salary_per_day;

            // dd($total_earned_salary);

            $hours = (int)($total_working_minutes / 60);
            $minutes = (int)($total_working_minutes % 60); 
            $total_working_hour = $hours . " Hours and ". $minutes . " Minutes "; 

            // dd($total_working_minutes,$total_working_hour);

            $days = array('saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday');

            $dayoff_days = array();

            foreach($days as $day)
            {

             
              // check whether the person has schedule info or not
            if($attendances->count() > 0){
                if($schedule_info)
                {
                    if(!$schedule_info->$day)
                    {
                      // dd($schedule_info);
                      array_push($dayoff_days, $day);
                      
                    } 
                }
                else{
                  // dd($employee,$schedule_info);
                  // dd($schedule_info);
                  // dd($attendance->schedule_id, $employee);
                }    
              }      
              
            }

            $startDay = Carbon::createFromDate($year, $month)->startOfMonth();
            $endDay = Carbon::createFromDate($year, $month)->endOfMonth();;
            // dd($startDay ,$endDay);

            // $dt = Carbon::createFromFormat('m', $month);

            // $dt->startOfMonth();
            // dd($dt);

            // $startDay = new Carbon();
            // $startDay->startOfMonth();

            // dd(   $startDay->startOfMonth());

            // $endDay = new Carbon();
            // $endDay->endOfMonth();

            $weekdays = week_days_this_month($startDay, $endDay);
            // dd($weekdays);

            foreach($dayoff_days as $day)
            {
              $total_day_off += $weekdays[$day];
              // dd($total_day_off);
            }

            $total_earned_salary += $total_day_off * $salary_per_day;
            $sub_total += $total_earned_salary ;

            $salary_collection->push(new MonthlySalary($employee->firstname." ".$employee->lastname, $employee->idno, $total_working_hour, $total_working_days ,$total_earned_salary, $salary->currency, $salary->gross_salary, $total_late_arrival, $total_early_departure, $total_approved_leaves, $total_day_off));      
            // dd( $salary_collection);
        }
      }
      // dd($all_employee_attendance);
      // dd($salary_collection);

      $dateObj   = DateTime::createFromFormat('!m', $month);
      $monthName = $dateObj->format('F');

      // $sub_total = (int) $sub_total;
      $sub_total = floatval( $sub_total);
      // dd( $sub_total );
      // dd($salary_collection);
      return view('admin.generated_salary', compact("salary_collection",'sub_total', "monthName", 'year'));
    }


    public function calculate_hourly_salary(Request $request)
    {
      // Month and Year form the salary form data via POST request.
      $start = $request->start;
      $end = $request->end;
      $branch_id = Auth::user()->branch_id;

      // dd($start, $end);

      $sub_total = 0;
      // dd($start, $end);

      if(!table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->exists())
      {
        return redirect('admin/salary')->with('error', 'No attendance data available within the date range.');
      }

      $all_employee = table::people()->where('branch_id',$branch_id)->where('acc_type', 1)->get();

      // dd($all_employee);

      $total_valid_work_minutes = 0;
      $salary_collection = collect([]);

      $employee_attendance_collection = array();
      

      foreach($all_employee as $employee)
      {

        $total_working_days = 0;
        $total_working_minutes = 0;
        $total_expected_working_hours = 0.0;
        $total_day_off = 0;
        $total_approved_leaves = 0;
        $total_paid_holidays = 0;
        $total_paid_amount = 0;

        $total_earned_salary = 0.0;
        $total_approved_leaves = 0;

        $total_late_arrival = 0;
        $total_early_departure = 0;


        // Finds the salary information of the following employee.
        $salary = table::employee_salary()->where('branch_id',$branch_id)->where([['reference', $employee->id], ['salary_type', 2]])->first();
        
        // Finds all attendances of the following month 
        // for the following employee.
        $attendances = table::attendance()->where('branch_id',$branch_id)->whereBetween('date', [$start, $end])->where('reference', $employee->id)->whereNotNull('timeout')->get();
        

        $total_working_days = count($attendances);
        

        // if(!count($attendances))
        // { 
        //   $all_attendances = table::attendance()->whereBet->where('reference', $employee->id)->get();
        //   dd($employee->id, count($all_attendances), $start, $end);

        //   $new_attendances = table::attendance()->whereBetween('date', [$start, $end])->where('reference', $employee->id)->get();
        //   dd(count($new_attendances));
        //  }

        // // IF THERE EXIST ATTENDANCE COUNT MORE THAN ZERO.
        // dd($attendances);
        if(count($attendances))
        {
          
          if($salary)
          {
            // Finds the number of days in the following month
            // $days_in_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $salary_per_hour = floatval($salary->gross_salary);
            // dd($salary_per_hour);

            // dd($employee->firstname, $salary_per_hour, $total_working_days);

            // All attendances iteration in order to calculate work hours 
            foreach($attendances as $attendance )
            {

              if($attendance)
              {
                // dd($attendance->id);
                // Parsing attendance date into carbon date object
                $attendance_date = Carbon::parse($attendance->date);

                if($attendance->status_timein == "Late Arrival")
                {
                  $total_late_arrival += 1;
                }
                
                if($attendance->status_timeout == "Early Departure")
                {
                  $total_early_departure += 1;
                }

                // Finds schedule information for the associated attendance.
               

                 // Finds schedule information for the associated attendance.
              $schedule = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where('schedules.id', $attendance->schedule_id)->value(strtolower($attendance_date->format('l')));
              if(!$schedule){
              $schedule = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where([['schedules.reference', $attendance->reference],['active_status',True]])->value(strtolower($attendance_date->format('l')));
                              
              }
              $schedule_info = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where('schedules.id', $attendance->schedule_id)->first();
              if(!$schedule_info){
              $schedule_info = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.branch_id',$branch_id)->where([['schedules.reference', $attendance->reference],['active_status',True]])->first();
                
              }


                // $schedule = table::schedules()->join('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.id','=',$attendance->schedule_id)->value(strtolower($attendance_date->format('l')));
                // dd($schedule);
                // $schedule_info = table::schedules()->rightjoin('schedule_template', 'schedules.schedule_id', '=', 'schedule_template.id')->where('schedules.id', $attendance->schedule_id)->first();
              // dd($schedule_info);
                // If there exist schedule informations
                if($schedule && $schedule_info)
                {

                  $str_arr = explode ("-", $schedule);
                  $start_at= $str_arr[0];
                  $end_at = $str_arr[1];

                  // $str_arr = explode ("-", $schedule);
                  // $start_at = "23:00";
                  // $end_at ="9:00";
                  // dd($start_at,$end_at);
                  $time1 = explode(':',$start_at);
                  $time2 = explode(':',$end_at);
                  $hours1 = $time1[0];
                  $hours2 = $time2[0];
                  $mins1 = $time1[1];
                  $mins2 = $time2[1];
                  $hours = $hours2 - $hours1;
                  $mins = 0;
                  if($hours < 0)
                  {
                    $hours = 24 + $hours;
                  }
                  if($mins2 >= $mins1) {
                        $mins = (int)$mins2 - (int)$mins1;
                    }
                    else {
                      $mins = ($mins2 + 60) - $mins1;
                      $hours--;
                    }
                    if($mins < 9)
                    {
                      $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);
                    }
                    if($hours < 9)
                    {
                      $hours =str_pad($hours, 2, '0', STR_PAD_LEFT);
                    }

                    $scheduled_work_hour = $hours.':'.$mins;

                  // $scheduled_work_hour = gmdate("H:i", strtotime($end_at) - strtotime($start_at)); // Find time difference in 24 hours
                  // if($employee->idno == 'WUSA0002'){
                  //   dd($scheduled_work_hour);
                  // }

                  $total_expected_working_hours += floatval($scheduled_work_hour);
                  $break = DB::select("select sec_to_time(sum(time_to_sec(x.diff))) as result from (select TIMEDIFF(TIME(end_at), TIME(start_at)) as diff from daily_breaks where branch_id =" . $branch_id ." and attendance_id =" . $attendance->id ." ) as x;");
                  $total_break = $break[0]->result;

                  // Finds total work hour from attendance
                  $total_work_hour_from_attendance = $attendance->totalhours;
                  //   if($employee->idno == 'WUSA0002'){
                  //   dd($total_work_hour_from_attendance);
                  // }
                  // If there exist break information
                  if($total_break)
                  {

                    // Total break taken in this folling attendence
                    $timesplit=explode(':',$total_break);
                    $total_break_minutes = ($timesplit[0]*60)+($timesplit[1])+($timesplit[2]>30?1:0);
                    $break_allowence = $schedule_info->break_allowence;


  //                   if($employee->idno == 'WUSA0002'){
  //                     dd($actual_work_minutes );

  // }

                    if($total_break_minutes > $break_allowence)
                    {
                      if (strpos($total_work_hour_from_attendance, '.') !== false) {
                        list($hour, $minutes) = explode('.',$total_work_hour_from_attendance);
                        $actual_work_minutes = ((int)$hour *60) + (int)$minutes ;
                       
                      }
                      else{
                        $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                      }

                      $extra_break_minutes = $total_break_minutes - $break_allowence;
                      $actual_work_minutes =  $actual_work_minutes - $extra_break_minutes;
                    }
                    else
                    {
                      // Amount of minutes that employee actually work.
                      if (strpos($total_work_hour_from_attendance, '.') !== false) {
                        list($hour, $minutes) = explode('.',$total_work_hour_from_attendance);
                        $actual_work_minutes = ((int)$hour *60) + (int)$minutes ;
                       
                      }
                      else{
                        $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                      }
                    }

                  }
                  else{
                    // Amount of minutes that employee actually work.
                    // $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                    if (strpos($total_work_hour_from_attendance, '.') !== false) {
                      list($hour, $minutes) = explode('.',$total_work_hour_from_attendance);
                      $actual_work_minutes = ((int)$hour *60) + (int)$minutes ;
                     
                    }
                    else{
                      $actual_work_minutes = floatval($total_work_hour_from_attendance) * 60;
                    }
                      // dd($total_work_hour_from_attendance);
                      // list($hour, $minutes) = explode('.',$total_work_hour_from_attendance);
                      // $actual_work_minutes = ((int)$hour *60) + (int)$minutes ;

                  }
                  
                  // Amount of minutes that employee had to work.
                  $scheduled_work_minutes = floatval($scheduled_work_hour) * 60;

                  // If actual work minute is grater than scheduled work minutes.
                  if($actual_work_minutes > $scheduled_work_minutes )
                  {
                    // dd($scheduled_work_minutes,$schedule_info);
                    $total_valid_work_minutes =  $scheduled_work_minutes;
                  }
                  // If actual work minute is less than scheduled work minutes.
                  else
                  {

    
                    $total_valid_work_minutes =  $actual_work_minutes;
                  }
                  // dd($total_valid_work_minutes);
                  // if($employee->idno == 'WUSA0002'){
                  //                     dd($total_valid_work_minutes);

                  // }
                  $salary_per_minute = $salary_per_hour / 60;
                  // dd($salary_per_minute);
                  $earned_salary_today = $salary_per_minute * $total_valid_work_minutes;
                  $total_working_minutes += $total_valid_work_minutes;
                  // $total_working_days += 1;
                  $total_earned_salary += $earned_salary_today; 
// dd($total_earned_salary);
                  // dd($total_earned_salary, $attendance->date, $total_working_minutes / 60 );
                  // dd($attendance, $total_working_minutes);

                }
              }
              // dd($total_working_minutes/60);

            } // End of attendance loop
              // dd($total_earned_salary, $attendance->date, $total_working_minutes / 60 );

              // If there does not exist any attendance information,
              // Checks if there exist any holidays or leave.

              $hours = (int)($total_working_minutes / 60);
              $minutes = floatval($total_working_minutes % 60); 
              $total_working_hour = $hours . " Hours and ". $minutes . " Minutes "; 
              $sub_total += floatval($total_earned_salary); 
              // dd($total_earned_salary);
              if($employee->idno == 'WUSA0002'){
                // dd($total_working_hour);

}

              $salary_collection->push(new HourlySalary($employee->lastname, $employee->idno, $total_working_hour, $total_working_days ,$total_earned_salary, $salary->currency, $salary->gross_salary, $total_late_arrival, $total_early_departure));
                // dd( $salary_collection);
          } // END OF SALARY IF STATEMENT

        }// ATTENDANCE COUNT IF STATEMENT ENDS HERE

      } // END OF EMPLOYEE ITERATION
      // dd( $salary_collection);


      return view('admin.generated_hourly_salary', compact("salary_collection", 'sub_total',"start", 'end'));
    }

















    // Salary Calculation
    public function calculate_salary_old(Request $request)
    {
      $datefrom = $request->datefrom;
      $dateto = $request->dateto;
      $parsed_datefrom = Carbon::parse($datefrom);
      $parsed_dateto = Carbon::parse($dateto);
      $parsed_dateto = $parsed_dateto->addDays(1);

      $expected_salary_days = $parsed_datefrom->diffInDays($parsed_dateto);

      // dd($expected_salary_days);

      // $month = Carbon::parse($datefrom)->format('m');
      // $year = Carbon::parse($dateto)->format('Y');
      // $days_in_this_month = cal_days_in_month(CAL_GREGORIAN,$month,$year);

      $days_in_this_month = 30;
      $all_employee = table::people()->where('acc_type', 1)->get();

      // $all_holidays = table::holidays()->where('month', $month)->first();
      //
      // if ($all_holidays) {
      //   $holidays = explode(",", $all_holidays->dates);
      //   dd($holidays[0]);
      //   $total_holidays = sizeof($holidays);
      // }else {
      //   $total_holidays = 0;
      // }

      $total_holidays = 0;

      $salary_collection = collect([]);
      $hourly_salary_collection = collect([]);

      foreach($all_employee as $employee)
      {

        $total_attendance = 0;
        $total_leaves = 0;

        $total_paid_office_days = 0;
        $total_paid_office_hours = 0.0;

        // Checks all attendance
        $all_attendance = table::attendance()->where('reference',$employee->id)->whereBetween('timein', [$datefrom, $dateto])->whereNotNull('timeout')->get();

        if ($all_attendance->count() > 0) 
        {
          $employee_salary = table::employee_salary()->where('reference', $employee->id)->first();
          if ($employee_salary) 
          {

          $total_attendance = count($all_attendance);

          // // Checks all leaves
          // $all_leaves = table::leaves()->where('reference', $employee->id)->where('status', 'Approved')->whereBetween('created_at', [$datefrom, $dateto])->get();
          // if ($all_leaves->count()) {
          //   dd($all_leaves);
          // }else{
          //
          // }
          //
          // if (sizeof($all_leaves) > 0) {
          //   $total_leaves = sizeof($all_leaves);
          // }
          // else {
          //   $total_leaves = 0;
          // }

          $total_leaves = 0;

          // $employee_salary = table::employee_salary()->where('reference', $employee->id)->first();
          $salary_amount = $employee_salary->gross_salary;
          $salary_currency = $employee_salary->currency;
          $salary_type = (int)$employee_salary->salary_type;

          // If salary type if monthly.
          if ($salary_type == 1)
          {

            
            $total_paid_office_days = $total_attendance + $total_leaves + $total_holidays;
            $daily_salary = $salary_amount / $days_in_this_month;
            $this_month_salary = $total_paid_office_days * $daily_salary;
            $absent_days = $days_in_this_month - $total_paid_office_days;
            $salary_collection->push(new MonthlySalary($employee_salary->id, $employee->lastname, $salary_type, (int)$this_month_salary, $employee_salary->currency, $total_paid_office_days, $salary_amount));
          }
          // If salary type is Hourly
          elseif($salary_type == 2){
            $total_schedule_id_found = 0;
            $truely_total_office_minutes = 0;

            foreach($all_attendance as $attendance)
            {
              $total_office_calculated_hours = 0;
              $total_office_calculated_minutes = 0;

              $total_office_minutes = 0;

              // If there exist schedule id along with attendance
              if ((int)$attendance->schedule_id) {
                $total_schedule_id_found++;

                $str_arr = explode (".", $attendance->totalhours);
                $hours = $str_arr[0];
                $minutes = $str_arr[1];

                // dd($hours, $minutes);
                $total_office_calculated_hours += $hours;
                $total_office_calculated_minutes += $minutes;

                // dd($total_office_calculated_minutes);

                // Break Taken
                $all_breaks = table::daily_breaks()->where([['reference', $employee->id],['attendance_id', $attendance->id]])->get();
                // dd($all_breaks);
                $total_break_duration = 0.0;
                $total_break_hour = 0.0;
                $total_break_minutes = 0.0;

                if (count($all_breaks) > 0) {
                  foreach($all_breaks as $break){
                    if ($break->start_at != NULL && $break->end_at != NULL) {
                      $start_at = date("Y-m-d h:i:s A", strtotime($break->start_at));
                      $end_at = date("Y-m-d h:i:s A", strtotime($break->end_at));
                      $time1 = Carbon::createFromFormat("Y-m-d h:i:s A", $start_at);
                      $time2 = Carbon::createFromFormat("Y-m-d h:i:s A", $end_at);
                      // $th = $time1->diffInHours($time2);
                      $tm = $time1->diffInMinutes($time2);

                      // $break_duration = floatval($th.".".$tm);

                      // dd($tm);
                      // $total_break_hour += $th;
                      $total_break_minutes += $tm;
                      // $total_break_duration += $break_duration;
                    }
                  }

                  // dd($total_break_minutes);
                  // $the_break_duration = 0;
                  // // Converts break minutes into hour
                  // if ($total_break_minutes >= 60) {
                  //   $the_break_duration = $total_break_minutes / 60;
                  // }
                  $convert_hour = $total_break_minutes / 60;
                  $total_break_hour += (int)$convert_hour;
                  $total_actual_break_minutes = $total_break_minutes - ((int)$convert_hour * 60);

                  $total_break_duration = $total_break_hour.".".$total_actual_break_minutes;

                  $converted_break_minutes = ($total_break_hour * 60) + $total_actual_break_minutes;
                  $converted_office_hour_minutes = ($total_office_calculated_hours * 60) + $total_office_calculated_minutes;


                  // If the break is less than 30 Minutes, it considers as 30 Minutes
                  if ($converted_break_minutes <= 30)
                  {
                    $converted_break_minutes = 30;
                    $total_office_minutes = $converted_office_hour_minutes - $converted_break_minutes;
                    if ($total_office_minutes < 0) {
                      $total_office_minutes = 0;
                    }
                  }else{
                    $total_office_minutes = $converted_office_hour_minutes - $converted_break_minutes;
                  }
                }
              }
              if ($total_office_minutes > 8) {
                $total_office_minutes = 8 * 60;
              }

              $truely_total_office_minutes += $total_office_minutes;
            }

            $converted_paid_office_hours = (int)($truely_total_office_minutes / 60);
            $converted_paid_office_minutes = $truely_total_office_minutes - (int)($converted_paid_office_hours * 60);
            $total_paid_office_hours = $converted_paid_office_hours .".".$converted_paid_office_minutes;

            $calculated_salary = ($converted_paid_office_hours * $salary_amount) + ($converted_paid_office_minutes * ($salary_amount/60));
            $absent_days = $days_in_this_month - $total_attendance;

            $hourly_salary_collection->push(new HourlySalary($employee_salary->id, $employee->lastname, $salary_type, $total_paid_office_hours ,(int)$calculated_salary, $employee_salary->currency, $total_attendance, $salary_amount));

          }

        }else{
          // If there is no attendance simply Pass
        }
      }else{
        // If there is no salary information Pass
      }

      }

      $dateObj = DateTime::createFromFormat('!m', $month);
      $monthName = $dateObj->format('F');

      return view('admin.generated_salary', compact('salary_collection','hourly_salary_collection','monthName', 'year' ));
    }
}
