<?php

namespace App\Http\Controllers;

use App\Notice;
use App\People;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "notice";
        $notices= Notice::where('branch_id', Auth::user()->branch_id)->get();
        $timezone = DB::table('settings')->where('branch_id', Auth::user()->branch_id)->value('timezone');


        // dd($notices[0]->noticeFor);
     
        return view('admin.notice', compact('notices','timezone'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "notice";

        $employees = People::where([['branch_id', Auth::user()->branch_id]])->get();
        return view('admin.create_notice', compact('employees', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $reference = $request->reference;
        $message = $request->message;
        $valid_till = $request->valid_till;
        $notice_by = Auth::user()->id;
        $branch_id = Auth::user()->branch_id;

        $new_notice = new Notice();
        if($reference){ $new_notice->reference = $reference; }
        $new_notice->message = $message;
        $new_notice->valid_till = $valid_till;
        $new_notice->notice_by = $notice_by;
        $new_notice->branch_id = $branch_id;
        $new_notice->save();

        // dd( $new_notice);
        if($new_notice)
        {
            return back()->with('success', 'Notice created successfully');
        }else{
            return back()->with('error', 'Failed to create notice.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $notice = Notice::find($id);
        if($notice){
            if($notice->notice_by == Auth::user()->id){
                $notice->delete();
                return back()->with('success', 'Notice deleted successfully.');
            }else{
                return back()->with('error', 'You are not allowed to delete the notice because you are not the owner.');
            }
        }else{
            return back()->with('error', 'Notice not found.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::find($id);
        if($notice){
            if($notice->notice_by == Auth::user()->id){  
                $title = "notice";
                $employees = People::where([['branch_id', Auth::user()->branch_id]])->get();
                return view('admin.notice_edit', compact( 'title', 'notice'));
            }else{
                return back()->with('error', 'You are not allowed to delete the notice because you are not the owner.');
            }
        }else{
            return back()->with('error', 'Notice not found.');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        //
        $id = $request->notice_id;
        $reference = $request->reference;
        $message = $request->message;
        $valid_till = $request->valid_till;
        $notice_by = Auth::user()->id;
        $branch_id = Auth::user()->branch_id;

        $update_notice = Notice::find($id);
        if($reference){ $update_notice->reference = $reference; }
        $update_notice->message = $message;
        $update_notice->valid_till = $valid_till;
        $update_notice->notice_by = $notice_by;
        $update_notice->branch_id = $branch_id;
        $update_notice->update();

        // dd( $new_notice);
        if($update_notice)
        {
            return back()->with('success', 'Notice updated successfully');
        }else{
            return back()->with('error', 'Failed to update notice.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        //
    }

    public function personal_notice_view()
    {
        $notices= Notice::where('branch_id', Auth::user()->branch_id)->where('reference',Auth::user()->id )->get();
        $notices_all= Notice::where('branch_id', Auth::user()->branch_id)->where('reference', null )->get();
        // dd(  $notice_all, Auth::user()->id);
        return view('personal.personal-notice-view', compact('notices', 'notices_all'));
    }

    public function document_notify($emp_id)
    {
        $employee = People::find($emp_id);

        $notice = new Notice();
        $notice->reference = $employee->id;
        $notice->message = "Hi, Some of your documents about to expire or already expired. Please contact administration with valid documents";
        $notice->valid_till = Carbon::now()->addMonth(1);
        $notice->notice_by = Auth::user()->id;
        $notice->branch_id = Auth::user()->branch_id;
        $notice->save();

        // dd($notice);

        if($notice){
            return back()->with('success', 'A notice is sent to the following employee');
        }
        else{
            return back()->with('error', 'Failed to notify the employee. Please use the notice menu to notify the employee');
        }
        

    }
}
