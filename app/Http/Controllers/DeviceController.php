<?php

namespace App\Http\Controllers;

use App\Device;
use App\Branch;
use App\Screenshot;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DeviceController extends Controller
{
    public function new_device($branch_id)
    {
      // dd($branch_id);
      $branch = Branch::where('id',(int)$branch_id)->first();
      return view('new_device', compact('branch'));
    }

    public function add_device(Request $request)
    {

      $branch = Branch::where('id',(int)$request->branch_id)->first();

      $device_name = $request->device_name;
      $device_type = $request->device_type;
      $device_code = $request->device_code;
      $branch_id = (int)$request->branch_id;

      $validatedData = $request->validate([
          'device_name' => ['required','max:50'],
          'device_type' => ['required'],
          'device_code' => ['required', 'unique:devices'],
      ]);

      $device = new Device();
      $device->device_name = $device_name;
      $device->device_type = $device_type;
      $device->device_code = $device_code;
      $device->branch_id = $branch_id;
      $device->status = false;
      $device->save();

      return redirect('/branch/'.$branch->branch_uid)->with('success', 'Device added successfully!');
    }

    public function activate_device($device_id)
    {
      // dd((int)$device_id);
      $id = (int)$device_id;
      $device_to_activate = Device::where('id',$id)->first();

      if ($device_to_activate) {
        $device_to_activate->status = true;
        $device_to_activate->save();

        return redirect()->back()->with('success', 'Device successfully activated!');
      }else{
        return redirect()->back()->with('error', 'Device not found!');
      }
    }


    public function deactivate_device($device_id)
    {
      $id = (int)$device_id;
      $device_to_deactivate = Device::where('id', $id)->first();
      if ($device_to_deactivate) {
        $device_to_deactivate->status = false;
        $device_to_deactivate->save();

        return redirect()->back()->with('success', 'Device successfully deactivated!');
      }else{
        return redirect()->back()->with('error', 'Device not found!');
      }
    }


    public function device_details($device_id)
    {
      return 'device details will show up here';
    }

    public function screenshots($device_id)
    {
      $device = Device::find($device_id);
      if($device)
      {
        $data = Screenshot::where('device_id', $device->id)->paginate(16);
        return view('super_admin.branches.screenshot', compact('data', 'device'));
      }else{
        return redirect()->back()->with('error', 'Device not found!');
      }
      
    }

    public function screenshots_search(Request $request){
     
      if($request->ajax())
      {
      $date = Carbon::now();
      $lastMonth =  $date->subMonth()->format('m');
      $lastWeek = Carbon::today()->subDays(7);
      $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
      $today = date("Y-m-d");
      $search_value = $request->get('search_value');
      $device_id = $request->get('device_id');
      $device = Device::find($device_id);


      if($search_value == 'all'){
        $data = Screenshot::where('device_id', $device_id)->paginate(16);
      }
      else if($search_value == 'last_week'){
        $data = Screenshot::whereBetween('created_at', array($lastWeek, $today))->where('device_id', $device_id)->paginate(16);
      }
      else if($search_value == 'last_month'){
        $data = Screenshot::whereMonth('created_at',$lastMonth)->where('device_id', $device_id)->paginate(16);
      }
      else if($search_value == 'today'){
        $data = Screenshot::whereDate('created_at', $today )->where('device_id', $device_id)->paginate(16);
      }
      else if($search_value == 'yesterday'){
        $data = Screenshot::whereDate('created_at', $yesterday )->where('device_id', $device_id)->paginate(16);
      }
      else{
        $data = Screenshot::where('device_id', $device_id)->paginate(16);
      }

      return view('super_admin.branches.device_screenshots_section', compact('data', 'device'))->render();

      }
    }

  public function advance_screenshots_search(Request $request){
    
    if($request->ajax())
    {
      $start = $request->get('start');
      $end_string = $request->get('end');
      $end = date('Y-m-d', strtotime( $end_string . " +1 days"));
      
      $device_id = $request->get('device_id');
      //dd($start,$end,$device_id);
      $device = Device::find($device_id);
      $today = date("Y-m-d", strtotime( '+1 days' ) );

      if($start == Null && $end == Null){
         $data = Screenshot::where('device_id', $device_id)->paginate(16);
      }
      else if($start == Null && $end != Null){
        $data = Screenshot::where('created_at','<=',$end)->where('device_id', $device_id)->paginate(16);
      }
      else if($start != Null && $end == Null){
        $data = Screenshot::whereBetween('created_at', array($start, $today))->where('device_id', $device_id)->paginate(16);
      }
      else if($start != Null && $end != Null){
        $data = Screenshot::whereBetween('created_at', array($start, $end))->where('device_id', $device_id)->paginate(16);
      }
      else{
        $data = Screenshot::where('device_id', $device_id)->paginate(16);
      }

      return view('super_admin.branches.device_screenshots_section', compact('data', 'device'))->render();
    }


  }

  public function delete_device($id){

    $device = Device::where('id',$id)->delete();
    return redirect()->back()->with('success', 'Device has been deleted successfully!');
    
  }



 public function apps_view(){

   $title = "applications";
  return view('super_admin.download_apps' , compact('title'));
}

}
