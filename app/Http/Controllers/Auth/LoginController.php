<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Branch;
use App\People;
use Illuminate\Support\Facades\Auth;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    protected function authenticated(Request $request)
    {
        $delete_status = Branch::where('id', Auth::user()->branch_id)->select('delete_status')->first();
        
        // dd( $delete_status );
        $type = Auth::user()->acc_type;
        $role_id = Auth::user()->role_id;
        if((($type == '1') || ($type == '2')) && ($delete_status->delete_status == '0')){
            $package_status = package_stage(Auth::user()->branch_id);
         if($package_status == 0){
            Auth::logout();
            return redirect('login')->with('warning', 'Access Denied! Please contact to your admin and try again later.');
         }
            
        }
        

        if (Auth::user()->verified == 0) {
            Auth::logout();
            return redirect('login')->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
          } 
       
        if (($type == '1') && ($delete_status->delete_status == '0'))
        {
            return redirect('personal/dashboard');
        }
        if(($type == '2') && ($delete_status->delete_status == '0'))
        {
            return redirect('dashboard');
        }
        if($type == '3')
        {
            return redirect('branches');
        }
        if($type == '4' && $role_id == '4')
        {
            return redirect('superman');
        }
        if($type == '5' )
        {
            return redirect('support/index');
        }
        if($type == null || $type == 0 || $delete_status->delete_status == '1') {
            Auth::logout();
            return redirect('login');
        }
    }

    /**
     * Include status as credential.
     *
    */
    protected function credentials(\Illuminate\Http\Request $request)
    {
         return ['companyemail' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Auth logout user.
     *
     */
    public function logout()
    {   
        $role =Auth::user()->role_id;
        
        $branch_id = Auth::user()->branch_id;
        if( ($role = 3 ) && ($branch_id != null )){
            $super_admin = People::where('id', Auth::user()->id)->where('role_id', $role )->update([
                'branch_id' => null,
                'acc_type' => 3 
            ]);

        }
        Auth::logout();
        return redirect('login');
    }

    



    
}
