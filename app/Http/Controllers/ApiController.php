<?php

namespace App\Http\Controllers;
use App\Device;
use App\Branch;
use App\Screenshot;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    function getDevice(Request $request)
    {
        $deviceCode = $request->device_code;

        $device = Device::where('device_code',$deviceCode)->first();
        
        if($device == NULL)
        {
            return "null";
        }
        else if($device->status == '0')
        {
            return "disabled";
        }
        else
        {
            $branch = Branch::where('id', $device->branch_id)->first();
            $info = ['branch_name'=> $branch->branch_name, 'branch_uid' => $branch->branch_uid];
            return json_encode($info);
        }

    }


    function storeScreenShot(Request $request)
    {
        $base64 = $request->screenshot;
        $device_code = $request->device_code;

        $device = Device::where('device_code', $device_code)->first();


        if($device && $device->branch->screenshot_preference == '1')
        {

            $length = 15;
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $base64 = str_replace('data:image/png;base64,', '', $base64);
			$base64 = str_replace(' ', '+', $base64);

            // Obtain the original content (usually binary data)
            $img_data = base64_decode($base64);

            // Load GD resource from binary data
            // $im = imageCreateFromString($img_data);

            // Make sure that the GD library was able to load the image
            // This is important, because you should not miss corrupted or unsupported images
            
            $resized_image = Image::make($img_data)->resize(800, 450)->stream('jpg', 90); // resize the image 
       

            $name = $randomString;
            // Specify the location where you want to save the image
            $destinationPath = public_path(). '/manager/screenshots/' . $name .'.jpg';

            // Save the GD resource as PNG in the best possible quality (no compression)
            // This will strip any metadata or invalid contents (including, the PHP backdoor)
            // To block any possible exploits, consider increasing the compression level
            // imagepng($im, $img_file, 0);
            // $destinationPath = public_path() . '/assets/faces/' . $image_name;
            file_put_contents($destinationPath, $resized_image);

            $screenshoot = new Screenshot();
            $screenshoot->branch_id = $device->branch_id;
            $screenshoot->device_id = $device->id;
            $screenshoot->image = $name;
            $screenshoot->save();

            if($screenshoot){
                return "True";
            }
            else
            {
                return "False";
            }
        }else{
            return "False";
        }
    }


}



