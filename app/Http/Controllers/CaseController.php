<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\table;
use App\SupportCase;
use Illuminate\Support\Facades\Auth;

use Image;



class CaseController extends Controller
{
    //
    public function create_case(Request $request){

        $branch_id = Auth::user()->branch_id;

        $case_image = $request->file('image');

        if ($case_image != null) {

			$case_image_name = $branch_id . strtoupper(substr(md5(time()), 0, 6)) . '.png';
            $resize_image = Image::make($case_image)->resize(500, 500);
            $destinationPath = public_path() . '/assets/Case_Image/';
            $resize_image->save("$destinationPath" . "$case_image_name");
			
		} else {
			$case_image_name = '';
		}
        
        $case = new SupportCase();
        $case->branch_id = $branch_id;
        $case->subject = $request->subject;
        $case->comment = $request->comment;
        $case->file = $case_image_name;
        $case->status = true;
        $case->created_by = Auth::user()->id;
        $case->save();

        return redirect()->back()->with('Success', 'Your Case Submitted Successfully' );

    }
}
