<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Branch;
use App\People;
use App\Company;
use App\Device;
use Illuminate\Support\Facades\DB;
use Datetime;
use Carbon\Carbon;

use App\Classes\table;

use Mail;



class BranchController extends Controller
{
    //
    public function index()
    {
        $title = "Branch";
        
        $branches = Branch::Join('people', 'branches.id', '=', 'people.branch_id')
            ->select('branches.id', 'branches.branch_name', 'branches.created_at', 'people.companyemail', 'branches.branch_uid','branches.screenshot_preference')
            ->where('branches.owner_id', Auth::user()->id)
            ->where('people.role_id', 2)
            ->where('branches.delete_status', 0)
            ->get();


            $data = DB::table('user_subscriptions')->join('packages', 'packages.id', '=', 'user_subscriptions.package_id')->where('user_subscriptions.user_id', Auth::user()->id)
            ->first();    

            if ($data) {
                $end_date = new Datetime($data->plan_period_end);
                $presentdate = new Datetime('now');

                if($presentdate < $end_date){
                $interval = $presentdate->diff($end_date);
                $days_left = $interval->days . " days " .$interval->h . " hours";
                $condition = 'running';
                }
                else{
                $interval = $presentdate->diff($end_date);
                $days_left = $interval->days . " days " .$interval->h . " hours";
                $condition = 'over';
                }
            
            }
            else{
            $days_left = "N/A";
             $condition = "N/A";
            }
        // dd(count($branches));
        // dd($created_date, $presentdate, $interval, $days_left);

        return view('super_admin.branches.index', compact('branches', 'title', 'condition','days_left'));
    }
    public function setup()
    {
        $title = "Branch";

        return view('super_admin.branches.setup', compact('title'));
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function create(Request $request)
    {
        //dd($request->all());


        $validatedData = $request->validate([

            'first_name' => ['required'],
            'last_name' => ['required'],
            'admin_email' => ['required', 'unique:people,companyemail'],
            // 'company_name' => ['required','unique:company,company'],
            'branch_name' => ['required', 'unique:branches,branch_name'],
            'password' => ['required', 'min:6','confirmed', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'],
            //  'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'

        ]);
        
        if($request->screenshot_preference){
           $Screenshot_Preference = 1;
        }
        else{
          $Screenshot_Preference = 0;
        }
        
        $email = $request->admin_email;

        $branch = new Branch();
        $branch->branch_name = $request->branch_name;
        $branch->delete_status = 0;
        $branch->branch_uid = $this->generateRandomString();
        $branch->owner_id = Auth::user()->id;
        $branch->screenshot_preference =  $Screenshot_Preference;
        $branch->status = 1;
        $branch->save();

        $company = new Company();
        $company->branch_id = $branch->id;
        $company->company = $request->company_name;
        $company->save();

        $admin = new People();
        $admin->branch_id = $branch->id;
        $admin->firstname = $request->first_name;
        $admin->mi = $request->middle_name;
        $admin->lastname = $request->last_name;
        
        $admin->company_id = $company->id;
        $admin->companyemail = $request->admin_email;
        $admin->employmentstatus = "Active";
        $admin->employmenttype = "Regular";
        $admin->password = Hash::make($request->password);
        $admin->role_id = "2";
        $admin->acc_type = "2";
        $admin->status = "1";
        $admin->verified = True;
        $admin->save();

        table::settings()->insert([
            'branch_id' => $branch->id,
            'country' => $request->country,
            'timezone' => $request->timezone,
            'clock_comment' => 0,
            'company_name' => $request->company_name

        ]);

	try{
        Mail::send('email.branch_created', ['firstname' => $request->first_name, 'lastname' => $request->last_name, 'company' => $request->company_name, 'branch_name' => $request->branch_name, 'companyemail' => $email], function ($m) use ($email) {
            $m->from('info@attendancekeeper.net', 'Attendance Keeper');
            $m->to($email)->subject('Branch Has Been Created Successfully');
        });
        return redirect(url('/branches'))->with('success', 'Branch Created successfully!');
        }
        catch(\Exception $e){
        return redirect(url('/branches'))->with('success', 'Branch has been created successfully but can not send email!');
        }
        

        
    }
    
    public function screenshot_preference_change(Request $request){
       
    	$Screenshot_Preference = Branch::where('id', $request->get('branch_id'))->value('screenshot_preference');
        
        if($Screenshot_Preference == 1){
            $result = Branch::where('id', $request->get('branch_id'))->update(['screenshot_preference' => 0]);
        }
        else{
            $result = Branch::where('id', $request->get('branch_id'))->update(['screenshot_preference' => 1]);

        }
        return $result;
    }
    
    public function branch()
    {
        $title = "Branch";

        return view('super_admin.branches.branch', compact('title'));
    }

    public function view_branch($branch_uid)
    {

        //   dd($branch_uid);
        $title = "Branch";

        $branch = Branch::where('branch_uid', $branch_uid)->first();

        $devices = Device::where('branch_id', $branch->id)->get();
        return view('super_admin.branches.branch', compact('branch', 'devices', 'title'));
    }

    public function admins()
    {
        $title = "Admin";

        $admins = DB::table('branches')
            ->Join('people', 'branches.id', '=', 'people.branch_id')
            ->Join('settings', 'settings.branch_id', '=', 'branches.id')
            ->where('branches.owner_id', Auth::user()->id)
            ->where('branches.delete_status', 0)
            ->where('people.role_id', 2)
            ->select('branches.branch_name', 'branches.created_at', 'people.id', 'people.companyemail', 'people.firstname', 'people.lastname', 'settings.company_name', 'settings.country', 'settings.timezone')
            ->paginate(10);

        // dd( $admins);
        return view('super_admin.branches.admins', compact('admins', 'title'));
    }
    public function delete_branch($id)
    {
        // dd($id);
        $branches = Branch::where('id', $id)->update(['delete_status' => 1]);
        $title = "Branch";
        $branches = Branch::where('owner_id', Auth::user()->id)
            ->Join('people', 'branches.id', '=', 'people.branch_id')
            ->where('people.role_id', 2)
            ->where('branches.delete_status', 0)
            ->select('branches.id', 'branches.branch_name', 'branches.created_at', 'people.companyemail',)
            ->get();
        // dd($branches );
        $remaining_days = People::where('id', Auth::user()->id)->value('created_at');
        $created_date = new Datetime($remaining_days);
        $presentdate = new Datetime();
        // dd($created_date, $presentdate);
        $interval = $presentdate->diff($created_date);
        $days_left = 60 - (int)$interval->format('%d');

        return redirect(url('/branches'))->with('success','Branch has been deleted successfully!');
    }
    public function edit_admin($id)
    {
        $title = "Admin";
        $admin = People::where('id', $id)->first();

        return view('super_admin.branches.edit_admin', compact('admin', 'title'));
    }
    
    public function admin_edit(Request $request){
        $title = "Admin";
        $admin = People::where('id', $request->id)->first();
        $admin->firstname = $request->firstName;
        // $admin->mi = $request->middle_name;
        $admin->lastname = $request->lastName;

        $admin->companyemail = $request->email;

        $admin->password = Hash::make($request->password);

        $admin->save();
        return view('super_admin.branches.edit_admin', 'title');
        
    }

    public function super_admin_to_branch($branch_id)
    {
        $package_status = package_stage($branch_id);
        if($package_status == 0){
           
            return back()->with('success', 'Your Subscription is over! Please subscribe or contact to AttendanceKeeper and try again later.');
        }
        $id = Auth::user()->id;
        $super_admin = People::where('id', $id)->update([
            'branch_id' => $branch_id,
            'acc_type' => 2
        ]);
        
        $exist_info = DB::table('branch_visited')->where('user_id', $id)->value('visited_branchId');

        if ($super_admin) {
            if ($exist_info == null) {
                DB::table('branch_visited')->insert([
                    'user_id' => $id,
                    'visited_branchId' => $branch_id,
                    'created_at' => Carbon::Now()
                ]);
            } else {
                DB::table('branch_visited')->update([
                    'visited_branchId' => $branch_id,
                    'updated_at' => Carbon::Now()
                ]);
            }
        }
        Auth::logout();
        Auth::loginUsingId($id);
        return redirect('dashboard');
    }
    public function super_admin_profile_view(){
        
        $id = Auth::user()->id;
        $p = table::people()->where('id', $id)->first();

		
		$profile_photo = table::people()->select('avatar')->where('id', $p->id)->value('avatar');


        return view('super_admin.super_admin_profile_page' , compact('p' , 'profile_photo' ));

    }
    
}
