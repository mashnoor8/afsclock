<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Stripe;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Datetime;
use Mail;

class SubscriptionController extends Controller
{

    public function index()
    {

        $country = DB::table('countries')->where('id',Auth::user()->country_id)->value('name');
        $country_europe = ['Andorra','Austria','Belarus','Belgium','Bosnia and Herzegovina','Bulgaria','Croatia','Cyprus','Czech Republic','Denmark','Estonia','Finland','France','Germany','Greece','Hungary','Iceland','Ireland','Italy','Latvia','Liechtenstein','Lithuania','Luxembourg','Malta','Moldova','Monaco','Montenegro','Netherlands','North Macedonia','Norway','Poland','Portugal','Romania','San Marino','Serbia','Slovakia','Slovenia','Spain','Sweden','Switzerland','Turkey','Ukraine'] ;
        if($country == 'United Kingdom'){
              $currency = "GBP";
          }
        elseif(in_array($country, $country_europe)){
              $currency = "EUR";
          }
        else{
               $currency = "USD";
          }
        
        $title = "Subscription";
        $packages = DB::table('packages')->where('currency',$currency)->where('status', 1)->get();

        return view('super_admin.branches.subscription', compact('title', 'packages'));
    }
    public function subscription_checkout($package)
    {
        
        $package = DB::table('packages')->where('id', $package)->first();
        $title = "Subscription";
        return view('super_admin.branches.subscription_checkout', compact('title', 'package'));
    }


    public function checkout_payment(Request $request)
    {
      
        // Retrieve stripe token and user info from the submitted form data 

        $planName = $request->package;

        $package = DB::table('packages')->where('id', $planName)->first();
        
        $stripe = new \Stripe\StripeClient(
            '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'
        );

        
        try {
        $response = $stripe->tokens->create([
              'card' => [
                'number' => $request->card_no,
                'exp_month' => $request->exp_month,
                'exp_year' => $request->exp_year,
                'cvc' => $request->cvc
            ],
        ]);
         } catch(\Stripe\Exception\CardException $e) {
  	
        return back()->with('error', $e->getMessage());
         }

       
        
        $token = $response->id;

        $email = $request->email;

        $planPrice = floatval($package->price);

        $planInterval = $package->interval;
        $priceCents = round($planPrice * 100);

        Stripe\Stripe::setApiKey('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');
        $customer_existence = DB::table('user_subscriptions')->where('package_id', '!=', '1')->where('stripe_customer_id', '!=', 'trial')->where('payer_email', $email)->first();
        
        if ($customer_existence == null) {
            // Add customer to stripe 
            try {
                $customer =  \Stripe\Customer::create(array(
                    'email' => $email,
                    'source'  => $token
                ));
            } catch (Exception $e) {
                $api_error = $e->getMessage();
            }
            $c_id =  $customer->id;
        } else {
            $c_id = $customer_existence->stripe_customer_id;
        }


        if (empty($api_error)) {
            // Creates a new subscription 
            try {
                $subscription = \Stripe\Subscription::create(array(
                    "customer" =>  $c_id,
                    "items" => array(
                        array(
                            "plan" => $package->plan_id,
                        ),
                    ),
                ));
            } catch (Exception $e) {
                $api_error = $e->getMessage();
            }
        }

        if (empty($api_error) && $subscription) {
            // Retrieve subscription data 
            $subsData = $subscription->jsonSerialize();

            if ($subsData['status'] == 'active') {
                // Subscription info 
                $subscrID = $subsData['id'];
                $custID = $subsData['customer'];
                $planIntervalCount = $subsData['plan']['interval_count'];
                $created = date("Y-m-d H:i:s", $subsData['created']);
                $current_period_start = date("Y-m-d H:i:s", $subsData['current_period_start']);
                $current_period_end = date("Y-m-d H:i:s", $subsData['current_period_end']);
                $status = $subsData['status'];
                $s = date("Y-m-d", $subsData['current_period_start']);
                $e =  date("Y-m-d", $subsData['current_period_end']);

                DB::table('user_subscriptions')->insert([
                    'user_id' => Auth::user()->id,
                    'package_id' => $package->id,
                    'stripe_subscription_id' => $subscrID,
                    'stripe_customer_id' => $custID,
                    'plan_interval_count' => $planIntervalCount,
                    'payer_email' => $email,
                    'status' =>  $status,
                    'plan_period_start' =>  $current_period_start,
                    'plan_period_end' => $current_period_end,
                    'created_at' => $created
                ]);
                DB::table('user_subscriptions')->where('user_id', Auth::user()->id)->where('package_id', '!=', '1')->where('stripe_subscription_id', '!=', 'trial')->where('status', 'active')->update([
                    'status' => 'migrated',
                    'plan_period_end' => $created,
                    'updated_at'=> Carbon::now()
                ]);

            }
            elseif($subsData['status'] == 'incomplete'){
             return back()->with('error', 'Your payment status has been declined! Please contact to your bank and try again. ');
            }

            
        }
        $user = Auth::user();
        Mail::send('email.subscription_create', ['firstname' => $user->first_name, 'lastname' => $user->last_name,'plan'=> $planName,'email' => $email,'start'=> $s, 'end'=> $e], function ($m) use ($email) {
            $m->from('info@attendancekeeper.net', 'Attendance Keeper');
            $m->to($email)->subject('Confirmation of your Subscription');
        });

        return redirect(url('/my_subscription'))->with('success', 'Your Subscription Has Been Created successfully!');
    
    }

    public function my_subscription()
    {


        $title = "my_subscription";
        $data = DB::table('user_subscriptions')->join('packages', 'packages.id', '=', 'user_subscriptions.package_id')
              ->where('user_subscriptions.user_id', Auth::user()->id)->where('user_subscriptions.status','active')
              ->select('packages.id', 'packages.name', 'packages.plan_id', 'packages.limit', 'packages.price', 'packages.currency', 'packages.interval', 'user_subscriptions.payment_method', 'user_subscriptions.stripe_subscription_id', 'user_subscriptions.stripe_customer_id', 'user_subscriptions.payer_email', 'user_subscriptions.plan_period_start', 'user_subscriptions.plan_period_end', 'user_subscriptions.status','user_subscriptions.created_at')
              ->orderby('user_subscriptions.created_at','DESC')->first();


        if ($data) {
            $end_date = new Datetime($data->plan_period_end);
            $presentdate = new Datetime('now');

            $interval = $presentdate->diff($end_date);

            $days_left = $interval->days . " days " .$interval->h . " hours";
            
        } else {
            $days_left = "N/A";
        }
        
        $country = DB::table('countries')->where('id',Auth::user()->country_id)->value('name');
        if($country != null){
        $country_europe = ['Andorra','Austria','Belarus','Belgium','Bosnia and Herzegovina','Bulgaria','Croatia','Cyprus','Czech Republic','Denmark','Estonia','Finland','France','Germany','Greece','Hungary','Iceland','Ireland','Italy','Latvia','Liechtenstein','Lithuania','Luxembourg','Malta','Moldova','Monaco','Montenegro','Netherlands','North Macedonia','Norway','Poland','Portugal','Romania','San Marino','Serbia','Slovakia','Slovenia','Spain','Sweden','Switzerland','Turkey','Ukraine'] ;
        if($country == 'United Kingdom'){
              $currency = "GBP";
          }
        elseif(in_array($country, $country_europe)){
              $currency = "EUR";
          }
        else{
               $currency = "USD";
          }
          }
          else{
          $currency = "";
          }
        
        return view('super_admin.branches.my_subscription', compact('data', 'days_left','currency', 'title'));
    }

    public function my_subscription_cancel()
    {
        $user = Auth::user();
        $exist = DB::table('user_subscriptions')->where('user_id', $user->id)->where('package_id', '!=', '1')->where('stripe_subscription_id', '!=', 'trial')->where('status', 'active')->first();
        $package = DB::table('packages')->where('id',$exist->package_id)->first();
        if ($exist == null) {
            return back()->with('success', 'Oops! You have no subscription to cancel because you are currently subscribed to Trial package.');
        } else {
            $stripe = new \Stripe\StripeClient(
                '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'
            );
            $response = $stripe->subscriptions->cancel(
                $exist->stripe_subscription_id,
                []
            );
            if($response->id == $exist->stripe_subscription_id && $response->status =='canceled' && $response->customer == $exist->stripe_customer_id){
                DB::table('user_subscriptions')->where('user_id', $user->id)->where('package_id', '!=', '1')->where('stripe_subscription_id', '!=', 'trial')->where('status', 'active')->update([
                    'status' => 'canceled',
                    'plan_period_end' => date("Y-m-d H:i:s", $response->canceled_at),
                    'updated_at'=> Carbon::now()
                ]);
                
            }

            $s = date("Y-m-d h:i:s A", $response->start_date);
            $e = date("Y-m-d h:i:s A", $response->canceled_at);
            $email = $exist->payer_email;
            Mail::send('email.subscription_cancel', ['firstname' => $user->first_name, 'lastname' => $user->last_name,'plan'=> $package->name,'email' => $exist->payer_email,'start'=> $s, 'end'=> $e], function ($m) use ($email) {
                $m->from('info@attendancekeeper.net', 'Attendance Keeper');
                $m->to($email)->subject('Your Subscription has been cancelled');
            });
    
            return redirect(url('/my_subscription'))->with('success', 'Your Subscription Has Been Cancelled successfully!');


        }
    }
}
