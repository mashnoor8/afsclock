<?php

namespace App\Http\Controllers\personal;

use App\Http\Controllers\Controller;
use App\People;
use App\Task;
use App\Classes\table;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\User;
use Mail;
use DateTime;



class TasksHistory{

    public $id = '';
    public $datetime ='';
    public $reason = '';
    public $new_deadline = '';


    public function __construct($id, $datetime, $reason, $new_deadline)
    {
        $this->id = $id;
        $this->datetime = $datetime;
        $this->reason = $reason;
        $this->new_deadline = $new_deadline;
    }

}

class Tasks{

    public $id = '';
    public $assigned_by ='';
    public $title = '';
    public $deadline = '';
    public $finish_date = '';
    public $status = '';
    public $comment = '';

    public function __construct($id, $assigned_by, $title, $deadline, $finish_date, $status, $comment)
    {
        $this->id = $id;
        $this->assigned_by = $assigned_by;
        $this->title = $title;
        $this->deadline = $deadline;
        $this->finish_date = $finish_date;
        $this->status = $status;
        $this->comment = $comment;
    }

}

class PersonalTasksController extends Controller
{
    public function index()
    {
		$branch_id = Auth::user()->branch_id;

        $tasks = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->orderby('created_at','DESC')->get();
        $title="my_tasks";
        return view('personal.personal-tasks-view', compact('title','tasks'));
    }

    public function edit($id)
    {

        $task = Task::find($id);
        $e_id = Crypt::encryptString($id);

        return view('personal.edits.personal-task-edit', compact('task', 'e_id'));

    }
    public function view($id)
    {

        $task = Task::find($id);

        return view('personal.edits.personal-task-view', compact('task'));

    }



    public function myTasks()
    {
		$branch_id = Auth::user()->branch_id;

        $data = Task::where('reference', Auth::user()->id)->where('branch_id',$branch_id )->orderby('created_at','DESC')->paginate(12);
        


        $title="my_tasks";
        return view('personal.personal-my-tasks', compact('title','data'));

    }

    public function task_search(Request $request)
	{
		$branch_id = Auth::user()->branch_id;

        $query = $request->get('query');
        $situation = $request->get('situation');
    //    dd($query,$situation);
            if($query != Null)
                {
                    $people_id = table::people()->where('firstname', 'like', '%'.$query.'%')
                    ->where('branch_id',$branch_id)
                    ->orWhere('mi', 'like', '%'.$query.'%')
                    ->orWhere('lastname', 'like', '%'.$query.'%')->value('id');
                }

        if($query == "" && $situation == "all")
        {
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id)
            ->orderby('created_at','DESC')->paginate(12);

        } 
        elseif($query == "" && $situation == "completed")
        {
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->where('done_status','=',1)->orderby('created_at','DESC')->paginate(12);
        	
        } 
        elseif($query == "" && $situation == "pending")
        {
           
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->where('done_status','=',0)->orderby('created_at','DESC')->paginate(12);
         	
        } 
        elseif($query != "" && $situation == "all")
        {
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->where('assigned_by', $people_id)->orderby('created_at','DESC')->paginate(12);

        } 
        elseif($query != "" && $situation == "completed")
        {
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->where('assigned_by', $people_id)->where('done_status','=',1)->orderby('created_at','DESC')->paginate(12);	
        } 
        elseif($query != "" && $situation == "pending")
        {
           
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->where('done_status','=',0)->where('assigned_by', $people_id)->orderby('created_at','DESC')->paginate(12);	
        } 
        else
        {
            $data = Task::where('reference', Auth::user()->id)
            ->where('branch_id',$branch_id )
            ->orderby('created_at','DESC')->paginate(12);   
        }
        return view('personal.personal-my-tasks-table', compact('data'))->render();

    }

    public function add(Request $request)
    {
		$branch_id = Auth::user()->branch_id;

        //if (permission::permitted('schedules-add')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('schedule');}

        $v = $request->validate([
            'employee' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date',
        ]);

        $id = $request->employee;
        $assigned_by = Auth::id();
        $title = $request->title;
        $descriptopn = $request->description;
        $deadline = $request->deadline;
        $deadline_mail_datetime = new DateTime($deadline);
        $deadline_mail = $deadline_mail_datetime->format('Y-m-d');

        $task = new Task();
        $task->branch_id = $branch_id;
        $task->reference = $id;
        $task->assigned_by = $assigned_by;
        $task->title = $title;
        $task->description = $descriptopn;
        $task->deadline = $deadline;
        $task->done_status = 0;

        $task->save();

        $assign_to_info = table::people()->where('id', '=', $id)->where('branch_id',$branch_id )->first();
        $assign_by_info = table::people()->where('id', '=', $assigned_by)->where('branch_id',$branch_id )->first();
        
        $assign_to = $assign_to_info->firstname." ".$assign_to_info->lastname;
        $assign_by = $assign_by_info->firstname." ".$assign_by_info->lastname;

        Mail::send('email.task', ['assigned_to' => $assign_to,'assigned_by' => $assign_by, 'title' => $title ,'deadline' => $deadline], function ($m) use ($assign_to_info) {
            $m->from('info@attendancekeeper.net', 'Attendance Keeper');

            $m->to($assign_to_info->companyemail, $assign_to_info->firstname)->subject('Task Reminder!');
        });


        return redirect('personal/tasks/assignatask')->with('success', 'Task assigned successfully!');
    }

    public function update_assignATask(Request $request)
    {
        //if (permission::permitted('leaves-edit')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('leave');}
		$branch_id = Auth::user()->branch_id;

        $v = $request->validate([
            'id' => 'required|max:200',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date'
        ]);

        $id = Crypt::decryptString($request->id);


        $task = Task::find($id);



        $task->title = $request->title;
        $task->description = $request->description;
        $task->deadline = $request->deadline;
        $task->comment = $request->comment;

        $task->save();
        
        return redirect('personal/tasks/assignatask')->with('success','Task has been updated!');



    }

    public function delete($id)
    {
		$branch_id = Auth::user()->branch_id;


        Task::where('id', $id)->where('branch_id',$branch_id )->delete();
        table::task_extension()->where('branch_id', $branch_id)->where('task_id',$id)->delete();
        

        return redirect('personal/tasks/mytasks')->with('success', 'Deleted!');
    }

    public function delete_assignATask($id)
    {
		$branch_id = Auth::user()->branch_id;


        Task::where('id', $id)->where('branch_id',$branch_id )->delete();
        table::task_extension()->where('branch_id', $branch_id)->where('task_id',$id)->delete();
        return redirect('personal/tasks/assignatask')->with('success', 'Deleted!');
    }


    public function edit_assignATask($id)
    {
        $task = Task::find($id);

        $e_id = Crypt::encryptString($id);

        $title="my_tasks";
        return view('personal.edits.personal-task-assign-a-task-edit', compact('title','task', 'e_id'));

    }

    public function assignATask()
    {
		$branch_id = Auth::user()->branch_id;

        $data = Task::where('assigned_by', Auth::id())->where('branch_id',$branch_id )->paginate(12);

        $employee = People::where('branch_id',$branch_id )->where('role_id','!=','3')->where('id', '!=', Auth::user()->id)->get();
        $title="my_tasks";
        return view('personal.personal-tasks-assign-a-task', compact('title','data', 'employee'));

    }

    public function assigned_task_search(Request $request)
	{
		$branch_id = Auth::user()->branch_id;

        $query = $request->get('query');
        $situation = $request->get('situation');
       
            if($query != Null)
                {
                    $people_id = table::people()->where('branch_id',$branch_id )->where('firstname', 'like', '%'.$query.'%')
                    ->orWhere('mi', 'like', '%'.$query.'%')
                    ->orWhere('lastname', 'like', '%'.$query.'%')->value('id');
                }

        if($query == "" && $situation == "all")
        {
            $data = Task::where('assigned_by', Auth::id())->where('branch_id',$branch_id )->paginate(12);

        } 
        elseif($query == "" && $situation == "completed")
        {
            $data = Task::where('assigned_by', Auth::id())->where('branch_id',$branch_id )->where('done_status','=',1)->paginate(12);
        	
        } 
        elseif($query == "" && $situation == "pending")
        {
           
            $data = Task::where('assigned_by', Auth::id())->where('branch_id',$branch_id )->where('done_status','=',0)->paginate(12);
         	
        } 
        elseif($query != "" && $situation == "all")
        {
            $data = Task::where('reference', $people_id)->where('branch_id',$branch_id )->where('assigned_by', Auth::id())->paginate(12);

        } 
        elseif($query != "" && $situation == "completed")
        {
            $data = Task::where('reference', $people_id)->where('branch_id',$branch_id )->where('assigned_by', Auth::id())->where('done_status','=',1)->paginate(12);	
        } 
        elseif($query != "" && $situation == "pending")
        {
           
            $data = Task::where('reference', $people_id)->where('branch_id',$branch_id )->where('assigned_by', Auth::id())->where('done_status','=',0)->paginate(12);	
        } 
        else
        {
            $data = Task::where('assigned_by', Auth::id())->where('branch_id',$branch_id )->paginate(12);
  
        }
        return view('personal.personal-tasks-assign-a-task-table', compact('data'))->render();

    }

    public function update(Request $request)
    {
        //if (permission::permitted('leaves-edit')=='fail'){ return redirect()->route('denied'); }
        //if($request->sh == 2){return redirect()->route('leave');}
		$branch_id = Auth::user()->branch_id;

        $v = $request->validate([

            'comment' => 'required'
        ]);

       
        $id = $request->id;

        $task = Task::find($id);
        

        $task->comment = $request->comment;
        $task->finishdate = date('Y-m-d');
        $task->done_status = 1;
        $task->save();

        $assign_to_info = table::people()->where('id', '=', Auth::user()->id)->where('branch_id',$branch_id )->first();
        $assign_by_info = table::people()->where('id', '=', $task->assigned_by)->where('branch_id',$branch_id )->first();
        // dd($assign_to_info,$assign_by_info,$task->assigned_by,Auth::user()->id);
        
        $assign_to = $assign_to_info->firstname." ".$assign_to_info->lastname;
        $assign_by = $assign_by_info->firstname." ".$assign_by_info->lastname;

        //Mail::send('email.task_done', ['assigned_to' => $assign_to,'assigned_by' => $assign_by,'title' =>$task->title], function ($m) use ($assign_by_info) {
          //  $m->from('info@attendancekeeper.net', 'Attendance Keeper');

           // $m->to($assign_by_info->companyemail, $assign_by_info->firstname)->subject('Task Status!');
       // });

        return redirect('personal/tasks/mytasks')->with('success','Hurrah! Your task has been done successfully.');

    }


    public function extend_deadline($id){
      $task = Task::find($id);
      return view('personal.personal-tasks-extend-deadline', compact('task'));
    }


    public function update_deadline(Request $request){

        
        $branch_id = Auth::user()->branch_id;


      $v = $request->validate([
          'reason' => 'required',
          'new_deadline' => 'required',
      ]);

      $task_id = $request->task_id;
      $new_deadline = $request->new_deadline;
      $reason = $request->reason;
      
      $exist = table::task_extension()->where('task_id',$task_id)->first();
      if($exist != Null){
      table::task_extension()->where('branch_id', $branch_id)->where('task_id',$task_id)->update(
          ['new_deadline' => $new_deadline, 'reason' => $reason, 'updated_at' => Carbon::now()]
      );
      }else{

      table::task_extension()->insert(
          ['branch_id' => $branch_id,'task_id' => $task_id, 'new_deadline' => $new_deadline, 'reason' => $reason, 'created_at' => Carbon::now()]
      );
      }

      return redirect('personal/tasks/mytasks')->with('success','Task has been updated!');
    }


    public function task_details($id){
        $branch_id = Auth::user()->branch_id;


      $task = Task::find($id);

      $task_history = collect([]);


        $extended_deadlines = table::task_extension()->where('branch_id',$branch_id )->where('task_id', $task->id)->get();

        $assigned_to = User::find($task->reference);
        $assigned_by = User::find($task->assigned_by);

        // dd($extended_deadlines);

        if ($extended_deadlines) {
          foreach ($extended_deadlines as $data) {
            $task_history->push(new TasksHistory($task->id, $data->created_at, $data->reason, $data->new_deadline));
          }
        }

      return view('personal.task_details', compact('task', 'task_history', 'assigned_to', 'assigned_by'));
    }

    function task_eligible($id){

        $task = Task::where('reference' , $id)->where('done_status', false)->count();
        $deadline = Task::where('reference' , $id)->where('done_status', false)->orderBy('deadline', 'DESC')->first();
        $date = date('d-M-Y' , strtotime($deadline->deadline )); 
        if($task >= 6){
            $response = [
                'status' => 1,
                'deadline' => $date,
                'task' => $task 
            ];
            
            return response()->json($response);   
        }
        else{
            // return true;
            return response()->json('0');
        }       
    }

}
