<?php

namespace App\Http\Controllers\personal;

use DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;



class PersonalLeavesController extends Controller
{
  public function index()
  {
    $branch_id = Auth::user()->branch_id;

    $i = Auth::user()->id;
    $user = Auth::user();
    $cur_year = date('Y');

    $available_leaves = remainingLeaves($i);

    $l = table::leaves()->where('branch_id', $branch_id)->where('reference', $i)->paginate(10);

    $lp = table::people()->where('branch_id', $branch_id)->where('id', $i)->value('leaveprivilege');
    $r = table::leavegroup()->where('branch_id', $branch_id)->where('id', $lp)->value('leaveprivileges');
    $rights = explode(",", $r);
    $lt = table::leavetypes()->where('branch_id', $branch_id)->where('id', $user->leaveprivilege)->first();
    $tl =  table::leaves()->where('branch_id', $branch_id)->where([['reference', $i], ['status', 'Approved']])->count();
    if ($lt) {
      $al = $lt->limit;
    }
    // $al = $lt->limit;
    else {
      $al = 0;
    }
    $rl = $al - $tl;


    $lg = table::leavegroup()->where('branch_id', $branch_id)->get();
    $title = "my_leaves";
    return view('personal.personal-leaves-view', compact('title','available_leaves', 'rl', 'l', 'lt', 'lg', 'lp', 'rights'));
  }

  public function requestL(Request $request)
  {
    //if($request->sh == 2){return redirect()->route('viewPersonalLeave');}
    $branch_id = Auth::user()->branch_id;

    $cur_year = date('Y');

    $v = $request->validate([
      'leavefrom' => 'required|date|max:15',
      'leaveto' => 'required|date|max:15',
      'returndate' => 'required|date|max:15',
      'reason' => 'required|max:255',
    ]);

    if ($request->leavefrom > $request->leaveto) {
      return redirect('personal/leaves/view')->with('error', 'Oops! Sorry, you have entered wrong interval of leave duration.');
    }
    // $datetime1 = new DateTime($request->leavefrom);
    // $datetime2 = new DateTime($request->leaveto);
    // $interval = $datetime1->diff($datetime2);
    // $days = $interval->format('%a') + 1;
    $auth_schedule = table::schedules()->where('branch_id', $branch_id)->where('reference', Auth::user()->id)->first();
    $auth_schedule_template = table::sch_template()->where('branch_id', $branch_id)->where('id', $auth_schedule->schedule_id)->first();
    // dd($auth_schedule_template);

    $start = new Carbon($request->leavefrom);
    $end = (new Carbon($request->leaveto))->addDays(1);
    $days = 0;

    for ($i = $start; $i < $end; $i->addDays(1)) {
      if (($i->format('Y')) == $cur_year) {

        $cur_month_holidays_dates = table::holidays()->where('branch_id', $branch_id)->where('month', $i->format('m'))->select('dates')->get();
        // dd(count($cur_month_holidays_dates),$i->format('m'));
        if (count($cur_month_holidays_dates) != 0) {
          $cur_month_holidays = explode(",", $cur_month_holidays_dates[0]->dates);
          // dd($i->format('d'),$cur_month_holidays);
          if (!in_array($i->format('d'), $cur_month_holidays)) {
            $day = strtolower($i->format('l'));
            // dd($auth_schedule_template->$day);
            if ($auth_schedule_template->$day != Null) {
              $days += 1;
            }
          }
        } else {
          $day = strtolower($i->format('l'));
          // dd($auth_schedule_template->$day);
          if ($auth_schedule_template->$day != Null) {
            $days += 1;
          }
        }
      }
    }

    $available = (int)$request->available;


    $id = Auth::user()->id;
    $user = Auth::user();

    $lt = table::leavetypes()->where('branch_id', $branch_id)->where('id', $user->leaveprivilege)->first();
    $typeid = $user->leaveprivilege;
    if ($lt) {
      $type = mb_strtoupper($lt->leavetype);
    } else {
      $type = mb_strtoupper("N/A");
    }
    $reason = mb_strtoupper($request->reason);
    $leavefrom = date("Y-m-d", strtotime($request->leavefrom));
    $leaveto = date("Y-m-d", strtotime($request->leaveto));
    $returndate = date("Y-m-d", strtotime($request->returndate));

    $id = Auth::user()->id;

    if ($available >= $days) {

      table::leaves()->insert([
        'branch_id' =>  $branch_id,
        'reference' => $id,
        'type' => $type,
        'typeid' => $typeid,
        'leavefrom' => $leavefrom,
        'leaveto' => $leaveto,
        'returndate' => $returndate,
        'reason' => $reason,
        'status' => 'Pending',
      ]);

      return redirect('personal/leaves/view')->with('success', 'Leave request sent!');
    } else {
      return redirect('personal/leaves/view')->with('error', 'Oops! Sorry, you have exceeded your available leave limit.');
    }
  }



  public function  leave_search(Request $request)
  {
    $branch_id = Auth::user()->branch_id;

    $i = Auth::user()->id;
    $start = $request->get('start');
    $end = $request->get('end');
    $cur_date = date('Y-m-d');
    //   dd($start,$end,$cur_date);

    // return response()->json([$start, $end]);

    if ($start == '' && $end == '') {
      $l = table::leaves()->where('branch_id', $branch_id)->where('reference', $i)->paginate(10);
    } elseif ($start != '' && $end != '') {
      $l = table::leaves()
        ->where('branch_id', $branch_id)
        ->where('reference', $i)
        ->whereDate('leavefrom', '<=', $end)
        ->whereDate('leavefrom', '>=', $start)
        ->paginate(10);
    } elseif ($start != '' && $end == '') {
      $l = table::leaves()->where('branch_id', $branch_id)->where('reference', $i)->whereBetween('leavefrom', [$start, $cur_date])->paginate(10);
    } elseif ($start == '' && $end != '') {
      $l = table::leaves()->where('branch_id', $branch_id)->where('reference', $i)->where('leavefrom', '<=', $end)->paginate(10);
    } else {
      $l = table::leaves()->where('branch_id', $branch_id)->where('reference', $i)->paginate(10);
    }

    // return $data;
    return view('personal.personal-leaves-view-table', compact('l'))->render();
  }










  public function getPL(Request $request)
  {
    $branch_id = Auth::user()->branch_id;

    $id = Auth::user()->reference;
    $datefrom = date("Y-m-d", strtotime($request->datefrom));
    $dateto = date("Y-m-d", strtotime($request->dateto));

    if ($datefrom == null || $dateto == null) {
      $data = table::leaves()->where('branch_id', $branch_id)->where('reference', $id)->get();

      return response()->json($data);
    }

    if ($datefrom !== null and $dateto !== null) {
      $data = table::leaves()->where('branch_id', $branch_id)
        ->where('reference', $id)
        ->whereDate('leavefrom', '<=', $dateto)
        ->whereDate('leavefrom', '>=', $datefrom)
        ->get();

      return response()->json($data);
    }
  }

  public function viewPL(Request $request)
  {
    $branch_id = Auth::user()->branch_id;

    $id = $request->id;
    $view = table::leaves()->where('id', $id)->where('branch_id', $branch_id)->first();
    $view->leavefrom = date('M d, Y', strtotime($view->leavefrom));
    $view->leaveto = date('M d, Y', strtotime($view->leaveto));
    $view->returndate = date('M d, Y', strtotime($view->returndate));

    return response()->json($view);
  }

  public function edit($id, Request $request)
  {
    $branch_id = Auth::user()->branch_id;

    $l = table::leaves()->where('id', $id)->where('branch_id', $branch_id)->first();

    $lt = table::leavetypes()->where('branch_id', $branch_id)->get();
    $type = $l->type;

    $e_id = ($l->id == null) ? 0 : Crypt::encryptString($l->id);

    return view('personal.edits.personal-leaves-edit', compact('l', 'lt', 'type', 'e_id'));
  }

  public function update(Request $request)
  {


    $v = $request->validate([
      'id' => 'required|max:200',
      'type' => 'required|max:100',
      'leavefrom' => 'required|date|max:15',
      'leaveto' => 'required|date|max:15',
      'returndate' => 'required|date|max:15',
      'reason' => 'required|max:255',
    ]);
    $branch_id = Auth::user()->branch_id;


    $id = Crypt::decryptString($request->id);
    $type = mb_strtoupper($request->type);
    $leavefrom = $request->leavefrom;
    $leaveto = $request->leaveto;
    $returndate = $request->returndate;
    $reason = mb_strtoupper($request->reason);

    //if($request->sh == 2){return redirect()->route('viewPersonalLeave');}

    table::leaves()
      ->where('branch_id', $branch_id)
      ->where('id', $id)
      ->update([
        'type' => $type,
        'leavefrom' => $leavefrom,
        'leaveto' => $leaveto,
        'reason' => $reason
      ]);

    return redirect('personal/leaves/view')->with('success', 'Leave has been updated!');
  }

  public function delete($id, Request $request)
  {
    $branch_id = Auth::user()->branch_id;

    //if($request->sh == 2){return redirect()->route('viewPersonalLeave');}

    table::leaves()->where('id', $id)->where('branch_id', $branch_id)->delete();

    return redirect('personal/leaves/view')->with('success', 'Leave has been deleted!');
  }
}
