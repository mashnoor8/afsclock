<?php

namespace App\Http\Controllers\personal;
use DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class PersonalAttendanceController extends Controller
{
    public function index()
    {
        $i = Auth::user()->idno;
        $branch_id = Auth::user()->branch_id;
       
        $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->orderBy('date', 'desc')->paginate(10);
        $email_preference = Auth::user()->attendance_email_preference;
        

        $employee_reference_id = Auth::user()->reference;
        // $all_entries = table::daily_entries()->where('reference', $employee_reference_id)->get();
         $title="my_attendances";
        // $all_breaks = table::daily_breaks()->where('reference', $employee_reference_id)->get();
        return view('personal.personal-attendance-view', compact('title','email_preference','data'));
    }

  public function email_preference_change(){
  $email_preference = Auth::user()->attendance_email_preference;
  if($email_preference == 1){
  
    $job = DB::table('people')->where('id',Auth::user()->id)->update(['attendance_email_preference'  => 0]);
   }
   elseif($email_preference == 0){
   $job =  DB::table('people')->where('id',Auth::user()->id)->update(['attendance_email_preference'=> 1]);
   }
   if($job){
   
  return 'OK';}
  }
    public function details(Request $request){

        $attendanceID = request()->attendanceID;
        $branch_id = Auth::user()->branch_id;

        // return $attendanceID;

        if ($attendanceID) {
          
          $all_breaks = table::daily_breaks()->where('branch_id',$branch_id )->where('attendance_id', $attendanceID)->get();

          // return count($all_breaks);

          if (count($all_breaks) > 0) {



            $total_break_minute = 0;

            foreach ($all_breaks as $break) {
              $time1 = Carbon::parse($break->start_at);
              $time2 = Carbon::parse($break->end_at);
              $tm = ($time1->diffInMinutes($time2));
              $total_break_minute += $tm;
            }

            if ($total_break_minute >= 60 ) {

              $converted_hour_from_minute = (int)($total_break_minute / 60);
              $remaining_minutes = $total_break_minute - ($converted_hour_from_minute * 60);
              $total_break = "Total ".$converted_hour_from_minute." Hour and ".$remaining_minutes." Minutes";
              return response()->json([$all_breaks, $total_break]);
            }else {
              $total_break = "Total ".$total_break_minute." Minutes";
              return response()->json([$all_breaks, $total_break]);
            }


            return $converted_hour_from_minute;

            return response()->json([$all_breaks, $total_working_hour]);
          }else {
            return response()->json(NULL);
          }

        }else {
          return response()->json(NULL);
        }
    }
   
    public function  attendance_search(Request $request)
    {
      $branch_id = Auth::user()->branch_id;

      $i = Auth::user()->idno;
      $start = $request->get('start');
      $end = $request->get('end');
      $cur_date = date('Y-m-d');

      // return response()->json([$start, $end]);

    if($start == '' && $end == '' )
        {
          $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->orderBy('date', 'desc')->paginate(10);
      
    } 
    elseif ($start != '' && $end != '') 
    {
      $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->whereBetween('date', [$start, $end])->orderBy('date', 'desc')->paginate(10);
           
        }

    elseif ($start != '' && $end == '') 
    {
      $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->whereBetween('date', [$start, $cur_date])->orderBy('date', 'desc')->paginate(10);
          
        }
    elseif ($start == '' && $end != '') 
    {
      $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->where('date','<=', $end)->orderBy('date', 'desc')->paginate(10);
         
        }
    else
    {
      $data = table::attendance()->where('branch_id',$branch_id )->where('idno', $i)->orderBy('date', 'desc')->paginate(10);
     
        }
    
        // return $data;
        return view('personal.personal-attendance-view-table', compact('data'))->render();
	}

    public function getPA(Request $request)
    {
    $branch_id = Auth::user()->branch_id;
    
		$id = Auth::user()->id;
		$datefrom = $request->start;
    $dateto = $request->end;
    
    return response()->json([$datefrom, $dateto]);

        if($datefrom == '' && $dateto == '' )
        {
             $data = table::attendance()
             ->where('branch_id',$branch_id )
             ->select('date', 'timein', 'timeout', 'totalhours', 'status_timein', 'status_timeout')
             ->where('reference', $id)
             ->get();


//             $data = table::attendance()->select('created_at', 'totalhours', 'status_timein', 'status_timeout')->where('idno', $id)->get();


			return response()->json($data);

		} elseif ($datefrom != '' AND $dateto != '') {
            $data = table::attendance()
            ->where('branch_id',$branch_id )
            ->select('date', 'timein', 'timeout', 'totalhours', 'status_timein', 'status_timeout')
            ->where('idno', $id)
            ->whereBetween('date', [$datefrom, $dateto])
            ->get();

			return response()->json($data);
        }
  }
  
      public function apply_missing_attendance(){
        $title="my_attendances";
        return view('personal.edits.personal-apply-missing-attendance', compact('title'));

      }
      public function apply_missing_attendance_post(Request $request){
        // dd($request->all());
      $branch_id = Auth::user()->branch_id;

      $id = Auth::user()->id;
      $user = table::people()->where('branch_id',$branch_id )->where('id', $id)->first();
      $schedule = table::schedules()->where('branch_id',$branch_id )->where('reference',$id)->value('schedule_id');
      $date = $request->date;
      $time_in_old = $request->time_in;
      if($request->time_out != Null){
        $time_out_old = $request->time_out;
        $time_out_date = date('Y-m-d', strtotime($time_out_old));
        $time_out_time = date('g:i:s A', strtotime($time_out_old));
        $time_out = $time_out_date.' '.$time_out_time;
      }
     else{
       $time_out = "N\A";
     }
      $time_in_date = date('Y-m-d', strtotime($time_in_old));
      $time_in_time = date('g:i:s A', strtotime($time_in_old));
     
      $curtime = Carbon::now();
      $time_in = $time_in_date.' '.$time_in_time;
      
      $employee_name = $user->firstname.' '.$user->lastname;
      $reason = $request->reason;
      table::applications()->insert([
        'branch_id' => $branch_id,
        'reference' => $id,
        'date' => $date,
        'idno' => $user->idno,
        'employee' => $employee_name,
        'timein' => $time_in,
        'timeout' => $time_out,
        'schedule_id' => $schedule,
        'reason' => $reason,
        'AdminApproved' => 'Applied',
        'created_at' => $curtime,
        
    ]);
    return redirect('personal/attendance/view')->with('success', 'Application of your missing attendance has been sent for Admin Approval!');
        
      }

  public function applications_for_missing_attendances()
     
  {
      $branch_id = Auth::user()->branch_id;

      $id = Auth::user()->id;
      $applications = table::applications()->where('branch_id',$branch_id )->where('reference',$id)->orderBy('created_at', 'desc')->get();
      $title = "my_attendances";
      
      return view('personal.applications-missing-attendance',compact('applications','title'));
    
  }

}
