<?php

namespace App\Http\Controllers\personal;
use DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;



class PersonalSchedulesController extends Controller
{
    public function index()
    {
		$branch_id = Auth::user()->branch_id;

        $i = Auth::user()->id;
        $s = table::schedules()->where('branch_id',$branch_id )->where([['reference', $i],['active_status', 1]])->first();
        if($s)
        {
        $schedule_template = table::sch_template()->where('branch_id',$branch_id )->where('id', $s->schedule_id)->first();
        }
        else $schedule_template = "Null";
       
        $title="my_schedules";
        return view('personal.personal-schedules-view', compact('title','schedule_template'));
    }

    public function getPS(Request $request)
    {
		$branch_id = Auth::user()->branch_id;

        $id = Auth::user()->idno;
        $datefrom = $request->datefrom;
		$dateto = $request->dateto;

        if($datefrom == null || $dateto == null )
        {
            $data = table::schedules()
            ->where('branch_id',$branch_id )
            ->select('intime', 'outime', 'datefrom', 'dateto', 'hours', 'restday', 'archive')
            ->where('idno', $id)
            ->get();
            return response()->json($data);

		} elseif ($datefrom !== null AND $dateto !== null) {
            $data = table::schedules()
            ->where('branch_id',$branch_id)
            ->select('intime', 'outime', 'datefrom', 'dateto', 'hours', 'restday', 'archive')
            ->where('idno', $id)
            ->whereBetween('datefrom', [$datefrom, $dateto])
            ->get();
            return response()->json($data);
        }
    }
}
