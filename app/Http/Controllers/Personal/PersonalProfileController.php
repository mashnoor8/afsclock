<?php

namespace App\Http\Controllers\personal;
use DB;
use App\Classes\table;
use App\Classes\permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class PersonalProfileController extends Controller
{
	public function index() 
	{
		$branch_id = Auth::user()->branch_id;

        $user = Auth::user();
		// $profile = table::people()->where('id', $id)->first();
		$company_data = table::company()->where('branch_id',$branch_id )->where('id', $user->company_id)->first();
		$profile_photo = table::people()->where('branch_id',$branch_id )->select('avatar')->where('id', $user->id)->value('avatar');
		$department = table::department()->where('branch_id',$branch_id )->where('id',$user->department_id)->first();
		$job_title = table::jobtitle()->where('branch_id',$branch_id )->where('id', $user->job_title_id)->first();
		$leavetype = table::leavetypes()->where('branch_id',$branch_id )->where('id', $user->leaveprivilege)->first();
		
		$leavegroup = table::leavegroup()->where('branch_id',$branch_id )->get();

        return view('personal.personal-profile-view', compact('user','job_title', 'department','company_data', 'profile_photo', 'leavetype', 'leavegroup'));
    }
	   
	public function profileEdit() 
	{
		$branch_id = Auth::user()->branch_id;

		$id = Auth::user()->id;
		$person_details = table::people()->where('branch_id',$branch_id )->where('id', $id)->first();
        // dd($person_details);
		return view('personal.edits.personal-profile-edit', compact('person_details'));
	}

	public function profileUpdate(Request $request) 
	{
		//if($request->sh == 2){return redirect()->route('myProfile');}
		$branch_id = Auth::user()->branch_id;

		$v = $request->validate([
			'lastname' => 'required|alpha_dash_space|max:155',
			'firstname' => 'required|alpha_dash_space|max:155',
			'mi' => 'required|alpha_dash_space|max:155',
			'gender' => 'required|alpha|max:155',
			'emailaddress' => 'required|email|max:155',
			'civilstatus' => 'required|alpha|max:155',
			'mobileno' => 'required|max:155',
			'birthday' => 'required|date|max:155',
			// 'nationalid' => 'required|max:155',
			'birthplace' => 'required|max:255',
			'homeaddress' => 'required|max:255',
		]);

		$id = Auth::user()->id;

		$lastname = mb_strtoupper($request->lastname);
		$firstname = mb_strtoupper($request->firstname);
		$mi = mb_strtoupper($request->mi);
		$gender = mb_strtoupper($request->gender);
		$emailaddress = mb_strtolower($request->emailaddress);
		$civilstatus = mb_strtoupper($request->civilstatus);
		$mobileno = $request->mobileno;
		$birthday = date("Y-m-d", strtotime($request->birthday));
		$birthplace = mb_strtoupper($request->birthplace);
		$homeaddress = mb_strtoupper($request->homeaddress);

		table::people()->where('branch_id',$branch_id )->where('id', $id)->update([
			'lastname' => $lastname,
			'firstname' => $firstname,
			'mi' => $mi,
			'gender' => $gender,
			'emailaddress' => $emailaddress,
			'civilstatus' => $civilstatus,
			'mobileno' => $mobileno,
			'birthday' => $birthday,
			'birthplace' => $birthplace,
			'homeaddress' => $homeaddress,
		]);

    	return redirect('personal/profile/view')->with('success','Updated!');
	}

}

