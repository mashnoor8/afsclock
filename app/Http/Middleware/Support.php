<?php

namespace App\Http\Middleware;

use Closure;

class Support
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $t = \Auth::user()->acc_type;
        
        // return $t;
        
        if ($t == '5') {
     
            // nothing
        }
         else {
            \Auth::logout();
            // return redirect()->route('denied');
        }
        return $next($request);
    }
}
