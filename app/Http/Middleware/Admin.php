<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\Branch;
use Illuminate\Support\Facades\DB;
use App\People;
use Carbon\Carbon;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::user()->id;
        if(Auth::user()->acc_type == '3' && Auth::user()->role_id == '3'){
            $exist_info = DB::table('branch_visited')->where('user_id', $id)->value('visited_branchId');

            $super_admin = People::where('id',$id)->where('role_id','3')->where('acc_type','3')->update(['branch_id'=> $exist_info, 'acc_type' => '2']);
            if ($super_admin) {
                if ($exist_info == null) {
                    DB::table('branch_visited')->insert([
                        'user_id' => Auth::user()->id,
                        'visited_branchId' => (int)$exist_info,
                        'created_at' => Carbon::Now()
                    ]);
                } else {
                    DB::table('branch_visited')->update([
                        'visited_branchId' => (int)$exist_info,
                        'updated_at' => Carbon::Now()
                    ]);
                }
            }
            Auth::logout();
            Auth::loginUsingId($id);
        }
        

        

        $delete_status = Branch::where('id', Auth::user()->branch_id)->select('delete_status')->first();

        $t = Auth::user()->acc_type;
        
        if (($t == '2') &&  ($delete_status->delete_status == '0')) {
     
            // nothing
        } else {
            Auth::logout();
            // return redirect()->route('denied');
        }

        return $next($request);
    }
}
