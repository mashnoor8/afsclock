<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\People;
use Closure;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::user()->id;
        if(Auth::user()->acc_type == '2' && Auth::user()->role_id == '3'){
            $super_admin = People::where('id',$id)->where('role_id','3')->where('acc_type','2')->update(['branch_id'=> null, 'acc_type' => '3']);
            Auth::logout();
            Auth::loginUsingId($id);
        }
        
        $t = Auth::user()->acc_type;
        if ($t == '3') {
            // nothing
        } else {
            Auth::logout();
            return redirect()->route('denied');
        }

        return $next($request);
    }
}
