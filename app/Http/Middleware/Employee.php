<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Branch;


class Employee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $t = \Auth::user()->acc_type;
        $delete_status = Branch::where('id', Auth::user()->branch_id)->select('delete_status')->first();

        
        if (($t == '1' || $t == '2') && ($delete_status->delete_status == '0')) 
        {
            // nothing
        } else {
            Auth::logout(); 
            return redirect()->route('denied');
        }

        return $next($request);
    }
}
