<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    protected $table = 'devices';

    public function branch(){
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
}
