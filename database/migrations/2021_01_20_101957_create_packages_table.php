<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('plan_id');
            $table->string('product_id');
            $table->string('limit');
            $table->float('price', 8, 2);	
            $table->string('currency');
            $table->string('interval');
            $table->string('status')->default('1');
            $table->timestamps();

        });

        DB::table('packages')->insert(
			array(
				[
					'name' => 'Trial Package',
                    'plan_id' => 'trial',
                    'product_id' => 'trial',
                    'limit' => '1-1000',
                    'price' => '0',
                    'currency' => 'GBP',
                    'interval' => 'Month',
                    'status' => '3'
                ]
				// ],
				// [
				// 	'name' => 'Package-2',
                //     'plan_id' => 'price_1IBGXFC0vslXRlHEW95dx7jk',
                //     'product_id' => 'prod_ImqDca59Forvmw',
                //     'limit' => '7-20',
                //     'price' => '14.99',
                //     'currency' => 'GBP',
                //     'interval' => 'Month'

				// ],

				// [
				// 	'name' => 'Package-3',
                //     'plan_id' => 'price_1IArOxC0vslXRlHEzqtgQCCY',
                //     'product_id' => 'prod_ImQF3uPrCZRpKl',
                //     'limit' => '21-50',
                //     'price' => '19.99',
                //     'currency' => 'GBP',
                //     'interval' => 'Month'

				// ],
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
