<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('salary_types')) {
        Schema::create('salary_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->integer('branch_id');
            $table->string('type');
            $table->timestamps();
        });

        DB::table('salary_types')->insert([
        ['id' => 1,
        'type' => 'Monthly'],
        ['id' => 2,
        'type' => 'Hourly']
        ]);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_types');
    }
}
