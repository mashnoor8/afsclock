<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('notices')) {
            Schema::create('notices', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('reference')->nullable();
                $table->string('message');
                $table->string('valid_till');
                $table->integer('notice_by');
                $table->integer('branch_id');

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
