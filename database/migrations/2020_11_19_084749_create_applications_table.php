<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('applications')) {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('branch_id');
            $table->integer('reference')->nullable();
            $table->string('idno', 11)->nullable()->default('');
            $table->date('date')->nullable();
            $table->string('employee')->nullable()->default('');
            $table->string('timein')->nullable();
            $table->string('timeout')->nullable();
            $table->string('totalhours')->nullable()->default('');
            $table->string('status_timein')->nullable()->default('');
            $table->string('status_timeout')->nullable()->default('');
            $table->string('reason')->nullable()->default('');
            $table->string('comment')->nullable();
            $table->string('schedule_id');
            $table->string('AdminApproved');
            $table->string('application_time')->nullable();
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
