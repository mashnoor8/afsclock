<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::redirect('/home', '/');

Route::get('test_clock', 'ClockController@test_clock');
Route::post('attendance/add', 'ClockController@add');

Route::get('/', 'WelcomeController@index');
Route::get('/registration', 'WelcomeController@register')->name('registration');
Route::post('registration/form', 'WelcomeController@super_admin_registration');

Route::get('/features', 'WelcomeController@features');
Route::get('/pricing', 'WelcomeController@pricing');
Route::get('/packages/search', 'WelcomeController@packages_search');
Route::get('/solution', 'WelcomeController@solution');
Route::get('/trial', 'WelcomeController@trial');
Route::get('/faqs', 'WelcomeController@faqs');
Route::get('/user-manual', 'WelcomeController@manual');
Route::get('/remote-staff', 'WelcomeController@remoteStaff');
Route::get('/call-center', 'WelcomeController@callCenter');
Route::get('/contact', 'WelcomeController@contat');
Route::get('/warehouse-staff', 'WelcomeController@warehouseStaff');
Route::get('/user/verify/{token}', 'WelcomeController@verifyUser');
//contact email
Route::post('/contact/send', 'WelcomeController@send');

Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');

Route::post('reset_password_without_token', 'AccountsController@validatePasswordRequest');
Route::post('reset_password_with_token', 'AccountsController@resetPassword');



Route::group(['middleware' => 'auth'], function () {
	Route::get('clock', 'ClockController@clock');
	Route::group(['middleware' => 'checkstatus'], function () {
		/*
		|--------------------------------------------------------------------------
		| Universal SmartClock
		|--------------------------------------------------------------------------
		*/
		Route::group(['middleware' => 'superman'], function () {

			// Route::post('/superman_login', 'SupermanController@login');
			Route::get('/superman', 'SupermanController@dashboard');
			Route::get('/superman/packages', 'SupermanController@packages');
			Route::get('/superman/package/create', 'SupermanController@package_create');
			Route::post('/superman/package/create_new', 'SupermanController@package_store');
			Route::get('/superman/packages/edit/{id}', 'SupermanController@package_edit');
			Route::post('/superman/package/update', 'SupermanController@package_update');
			Route::get('/superman/packages/delete/{id}', 'SupermanController@package_delete');
		});

		/*
		|--------------------------------------------------------------------------
		| Universal SmartClock
		|--------------------------------------------------------------------------
		*/
		Route::group(['middleware' => 'support'], function () {

			
			Route::get('/support/index', 'SupportController@support_index');
			Route::get('/support/cases', 'SupportController@cases');
			Route::get('/support/cases/case_view/{id}', 'SupportController@support_case_view');
			Route::post('support/support_case_view/comment', 'SupportController@support_case_view_comment');
			Route::post('admin/case_view/comment', 'SupportController@admin_case_view_comment');
			Route::get('/case_image/download/{id}', 'SupportController@case_image_download');
			Route::get('/response_image/download/{id}', 'SupportController@response_image_download');
		});



		Route::group(['middleware' => 'superadmin'], function () {

			// branch 
			Route::get('/branches', 'BranchController@index')->name('branches');
			Route::get('/branches/setup', 'BranchController@setup');
			Route::post('branch/create', 'BranchController@create');
			Route::get('/branch', 'BranchController@branch')->name('branch');
			Route::get('/branch/{id}', 'BranchController@view_branch');
			Route::get('/branch/delete/{id}', 'BranchController@delete_branch');
			Route::get('/branch/super_admin_to_branch/{branch_id}', 'BranchController@super_admin_to_branch');
            Route::get('/branch/superadmin/screenshot_preference_change', 'BranchController@screenshot_preference_change');
                       
                       
			// device
			Route::post('/add_device', 'DeviceController@add_device');
			Route::get('/device/activate/{id}', 'DeviceController@activate_device');
			Route::get('/device/deactivate/{id}', 'DeviceController@deactivate_device');
			Route::get('/screenshots/{id}', 'DeviceController@screenshots');
			Route::get('/screenshot/search', 'DeviceController@screenshots_search');
            Route::get('/advance/screenshot/search', 'DeviceController@advance_screenshots_search');
            Route::get('/device/delete/{id}', 'DeviceController@delete_device');
                       
			// admin
			Route::get('/admins', 'BranchController@admins')->name('admins');
			Route::get('/admins/edit/{id}', 'BranchController@edit_admin');
			Route::post('/admins/edit', 'BranchController@admin_edit');



			// training
			Route::get('/training/{branch_uid}', 'TrainingController@index');
			Route::get('/train/{branch_uid}', 'TrainingController@trainQueue');

			Route::get('super_admin/update-password', 'WelcomeController@viewPassword')->name('updatePassword');
			Route::post('super_admin/password/update-password', 'WelcomeController@updatePassword');

			// subscriotion
			Route::get('/subscription', 'SubscriptionController@index');
			Route::get('/subscription_checkout/{package}', 'SubscriptionController@subscription_checkout');
			Route::post('/subscrption/checkout', 'SubscriptionController@checkout_payment');
			Route::get('/my_subscription', 'SubscriptionController@my_subscription');
			Route::get('/my_subscription/cancel', 'SubscriptionController@my_subscription_cancel');

			//Super Admin Profile
			Route::get('/super_admin/profile', 'BranchController@super_admin_profile_view');


			//desktop app
			Route::get('/desktop_apps', 'DeviceController@apps_view');

		});


		Route::group(['middleware' => 'admin'], function () {
			/*
			|--------------------------------------------------------------------------
			| Dashboard
			|--------------------------------------------------------------------------
			*/
			Route::get('/index', 'Admin\DashboardController@index');
			Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');
			Route::get('admin/Organization', 'Admin\DashboardController@Organization');
			Route::get('/back_to_super_admin', 'Admin\DashboardController@back_to_super_admin');


			/*
			|--------------------------------------------------------------------------
			| WebCam Realtime Data
			|--------------------------------------------------------------------------
			*/
			Route::get('webcam_feed', 'WebcamController@realtime_webcam_data')->name('webcam_feed');

			/*
			|--------------------------------------------------------------------------
			| Salary
			|--------------------------------------------------------------------------
			*/

			Route::get('admin/salary', 'SalaryController@salary');

			Route::get('admin/salary_types', 'SalaryController@salary_types');
			Route::post('admin/add_salary_types', 'SalaryController@add_salary_types');
			Route::get('admin/edit_salary_types/{id}', 'SalaryController@edit_salary_types');
			Route::post('admin/update_salary_types', 'SalaryController@update_salary_types');
			Route::get('admin/delete_salary_type/{id}', 'SalaryController@delete_salary_type');


			Route::get('admin/employee_salary', 'SalaryController@employee_salary');
			Route::post('admin/add_employee_salary', 'SalaryController@add_employee_salary');
			Route::get('admin/edit_employee_salary/{id}', 'SalaryController@edit_employee_salary');
			Route::post('admin/update_employee_salary/{id}', 'SalaryController@update_employee_salary');
			Route::get('admin/delete_employee_salary/{id}', 'SalaryController@delete_employee_salary');


			Route::get('admin/holidays', 'SalaryController@holidays');
			Route::post('admin/add_holidays', 'SalaryController@add_holidays');
			Route::get('admin/edit_holidays/{id}', 'SalaryController@edit_holidays');
			Route::post('admin/update_holidays/', 'SalaryController@update_holidays');
			Route::get('admin/delete_holidays/{id}', 'SalaryController@delete_holidays');
			Route::post('/admin/calculate_monthly_salary', 'SalaryController@calculate_monthly_salary');
			Route::post('/admin/calculate_hourly_salary', 'SalaryController@calculate_hourly_salary');

			/*
			|--------------------------------------------------------------------------
			| Support
			|--------------------------------------------------------------------------
			*/
			Route::get('admin/help', 'SupportController@index');
			Route::get('admin/support', 'SupportController@support');
			Route::post('support/create_case', 'CaseController@create_case');
			Route::get('case/case_view/{case_id}', 'SupportController@case_view');
			Route::get('case/solve/{case_id}', 'SupportController@solve');
			Route::get('/response_image/download/{id}', 'SupportController@response_image_download');




			/*
			|--------------------------------------------------------------------------
			| Employees
			|--------------------------------------------------------------------------
			*/
			Route::get('employees', 'Admin\EmployeesController@index')->name('employees');
			Route::get('employees/new', 'Admin\EmployeesController@new');
			Route::post('employee/add',  'Admin\EmployeesController@add');
			Route::get('employee/faceregistration/{id}', 'Admin\EmployeesController@faceregistration');
			Route::post('employee/registerface/{id}', 'Admin\EmployeesController@registerface');
			Route::get('/employee/delete_image/{id}', 'Admin\EmployeesController@deleteImage');
			Route::get('/employee/get_images/{id}', 'Admin\EmployeesController@employeeImages');
			Route::get('/employee/search', 'Admin\EmployeesController@employee_search');


			/*
			|--------------------------------------------------------------------------
			| Employee Profile
			|--------------------------------------------------------------------------
			*/
			Route::get('profile/view/{id}', 'Admin\ProfileController@view');
			Route::get('profile/delete/{id}', 'Admin\ProfileController@delete');
			Route::post('profile/delete/employee', 'Admin\ProfileController@clear');
			Route::get('profile/archive/{id}', 'Admin\ProfileController@archive');


			// Profile Info
			Route::get('profile/edit/{id}', 'Admin\ProfileController@editPerson');
			Route::post('profile/update', 'Admin\ProfileController@updatePerson');



			/*
			|--------------------------------------------------------------------------
			| Employee Attendance
			|--------------------------------------------------------------------------
			*/

			Route::get('attendance', 'Admin\AttendanceController@index')->name('attendance');
			Route::get('attendance/edit/{id}', 'Admin\AttendanceController@edit');
			Route::get('attendance/delete/{id}', 'Admin\AttendanceController@delete');
			Route::post('attendance/update', 'Admin\AttendanceController@update');
			Route::get('attendance/view/details', 'Admin\AttendanceController@details');
			Route::get('applications/missing_attendances', 'Admin\AttendanceController@applications_for_missing_attendances');
			Route::get('/applications/{id}', 'Admin\AttendanceController@application_details');
			Route::post('admin/approval/missing_attendance', 'Admin\AttendanceController@approval_missing_attendance_post');
			Route::post('admin/applications/deny/reason', 'Admin\AttendanceController@deny_missing_attendance_reason');







			/*
			|--------------------------------------------------------------------------
			| Employee Schedules
			|--------------------------------------------------------------------------
			*/
			Route::get('schedules', 'Admin\SchedulesController@index')->name('schedule');
			Route::get('schedules/templates', 'Admin\SchedulesController@templates');
			Route::get('schedules/templates/create', 'Admin\SchedulesController@create_templates');
			Route::post('schedules/templates/add', 'Admin\SchedulesController@add_templates');
			Route::post('schedules/template/assign', 'Admin\SchedulesController@assign_template');
			Route::post('schedules/add', 'Admin\SchedulesController@add');
			Route::get('schedules/edit/{id}', 'Admin\SchedulesController@edit');
			Route::post('schedules/update', 'Admin\SchedulesController@update');
			Route::get('schedules/delete/{id}', 'Admin\SchedulesController@delete');
			Route::get('schedules/archive/{id}', 'Admin\SchedulesController@archive');
			Route::get('schedules/details/{id}', 'Admin\SchedulesController@details');
			/*
            |--------------------------------------------------------------------------
            | Employee Task Manager
            |--------------------------------------------------------------------------
            */
			Route::get('taskmanager', 'Admin\TasksController@index')->name('taskmanager');
			Route::get('admin/mytasks', 'Admin\TasksController@myTasks');
			Route::post('task/add', 'Admin\TasksController@add');
			Route::get('task/edit/{id}', 'Admin\TasksController@edit');
			Route::get('task/task_eligible/{id}', 'Admin\TasksController@task_eligible');
			Route::get('task/view/{id}', 'Admin\TasksController@view');
			Route::get('task/delete/{id}', 'Admin\TasksController@delete');
			Route::post('admin/tasks/update', 'Admin\TasksController@markdone');
			Route::post('task/update', 'Admin\TasksController@update');
			Route::get('task/details/{id}', 'Admin\TasksController@task_details');
			Route::get('/tasks/search', 'Admin\TasksController@task_search');
			Route::get('task/extend-deadline/{id}', 'Admin\TasksController@extend_deadline');
			Route::post('admin/update-deadline', 'Admin\TasksController@update_deadline');
			Route::get('/admin/mytasks/search', 'Admin\TasksController@mytasks_search');


			/*
			|--------------------------------------------------------------------------
			| Employee Notice
			|--------------------------------------------------------------------------
			*/

			Route::get('notices', 'NoticeController@index')->name('notices');
			Route::get('notices/create', 'NoticeController@create')->name('create_notice');
			Route::post('notices/store', 'NoticeController@store')->name('store_notice');
			Route::get('notices/delete/{id}', 'NoticeController@delete')->name('delete_notice');
			Route::get('notices/edit/{id}', 'NoticeController@edit')->name('edit_notice');
			Route::post('notices/update', 'NoticeController@update')->name('update_notice');
			Route::get('notices/document_expiry_notice/{emp_id}', 'NoticeController@document_notify');


			/*
			|--------------------------------------------------------------------------
			| Employee Leaves
			|--------------------------------------------------------------------------
			*/
			Route::get('leaves', 'Admin\LeavesController@index')->name('leave');
			Route::get('leaves/edit/{id}', 'Admin\LeavesController@edit');
			Route::get('leaves/delete/{id}', 'Admin\LeavesController@delete');
			Route::post('leaves/update', 'Admin\LeavesController@update');
			Route::get('/leave/search', 'Admin\LeavesController@leave_search');


			/*
			|--------------------------------------------------------------------------
			| Users
			|--------------------------------------------------------------------------
			*/
			Route::get('users', 'Admin\UsersController@index')->name('users');
			Route::get('users/enable/{id}', 'Admin\UsersController@enable');
			Route::get('users/disable/{id}', 'Admin\UsersController@disable');
			Route::get('users/edit/{id}', 'Admin\UsersController@edit');
			Route::get('users/delete/{id}', 'Admin\UsersController@delete');
			Route::post('users/register', 'Admin\UsersController@register');
			Route::post('users/update/user', 'Admin\UsersController@update');


			/*
			|--------------------------------------------------------------------------
			| Employee Users & Roles
			|--------------------------------------------------------------------------
			*/
			/*Route::get('users/roles', 'Admin\RolesController@index')->name('roles');
			Route::post('users/roles/add', 'Admin\RolesController@add');
			Route::get('user/roles/get', 'Admin\RolesController@get');
			Route::post('users/roles/update', 'Admin\RolesController@update');
			Route::get('users/roles/delete/{id}', 'Admin\RolesController@delete');
			Route::get('users/roles/permissions/edit/{id}', 'Admin\RolesController@editperm');
			Route::post('users/roles/permissions/update', 'Admin\RolesController@updateperm');
*/

			/*
			|--------------------------------------------------------------------------
			| Update User
			|--------------------------------------------------------------------------
			*/
			Route::get('update-profile', 'Admin\ProfileController@viewProfile')->name('updateProfile');
			Route::get('update-password', 'Admin\ProfileController@viewPassword')->name('updatePassword');
			Route::post('user/update-profile', 'Admin\ProfileController@updateUser');
			Route::post('user/update-password', 'Admin\ProfileController@updatePassword');


			/*
			|--------------------------------------------------------------------------
			| Reports
			|--------------------------------------------------------------------------
			*/
			Route::get('reports', 'Admin\ReportsController@index')->name('reports');
			Route::get('reports/employee-list', 'Admin\ReportsController@empList');
			Route::get('reports/employee-attendance', 'Admin\ReportsController@empAtten');
			Route::get('reports/individual-attendance', 'Admin\ReportsController@indiAtten');
			Route::get('reports/employee-leaves', 'Admin\ReportsController@empLeaves');
			Route::get('reports/individual-leaves', 'Admin\ReportsController@indiLeaves');
			Route::get('reports/employee-schedule', 'Admin\ReportsController@empSched');
			Route::get('reports/organization-profile', 'Admin\ReportsController@orgProfile');
			Route::get('reports/employee-birthdays', 'Admin\ReportsController@empBday');
			Route::get('reports/user-accounts', 'Admin\ReportsController@userAccs');
			Route::get('get/employee-attendance', 'Admin\ReportsController@getEmpAtten');
			Route::get('get/employee-leaves', 'Admin\ReportsController@getEmpLeav');
			Route::get('get/employee-schedules', 'Admin\ReportsController@getEmpSched');
			Route::get('get/employee-report/search', 'Admin\ReportsController@employeeReportSearch');
			Route::get('/reports/get_employee_attendance', 'Admin\ReportsController@attendance_search');
			Route::get('/reports/get_employee_name', 'Admin\ReportsController@attendance_search_by_name');
			Route::get('/get/personal/attendance', 'Admin\PersonalAttendanceController@attendance_search');
			Route::get('/get/admin/attendance', 'Admin\AttendanceController@attendance_search');


			/*
			|--------------------------------------------------------------------------
			| Settings
			|--------------------------------------------------------------------------
			*/
			Route::get('settings', 'Admin\SettingsController@index')->name('settings');
			Route::post('settings/update', 'Admin\SettingsController@update');
			Route::post('settings/reverse/activation', 'Admin\SettingsController@reverse');
			Route::get('settings/get/app/info', 'Admin\SettingsController@appInfo');


			/*
			|--------------------------------------------------------------------------
			| Application Shortcuts
			|--------------------------------------------------------------------------
			*/
			// Company
			Route::get('fields/company/', 'Admin\FieldsController@company')->name('company');
			Route::post('fields/company/add', 'Admin\FieldsController@addCompany');
			Route::get('fields/company/delete/{id}', 'Admin\FieldsController@deleteCompany');
			Route::post('fields/company/delete', 'Admin\FieldsController@clearCompany');
			Route::get('fields/company/edit/{id}', 'Admin\FieldsController@editCompany');
			Route::post('fields/company/update', 'Admin\FieldsController@updateCompany');

			// Department
			Route::get('fields/department', 'Admin\FieldsController@department')->name('department');
			Route::post('fields/department/add', 'Admin\FieldsController@addDepartment');
			Route::get('fields/department/delete/{id}', 'Admin\FieldsController@deleteDepartment');
			Route::post('fields/department/delete', 'Admin\FieldsController@clearDepartment');
			Route::get('fields/department/edit/{id}', 'Admin\FieldsController@editDepartment');
			Route::post('fields/department/update', 'Admin\FieldsController@updateDepartment');

			// Job Title
			Route::get('fields/jobtitle', 'Admin\FieldsController@jobtitle')->name('jobtitle');
			Route::post('fields/jobtitle/add', 'Admin\FieldsController@addJobtitle');
			Route::get('fields/jobtitle/delete/{id}', 'Admin\FieldsController@deleteJobtitle');
			Route::post('fields/jobtitle/delete', 'Admin\FieldsController@clearJobtitle');
			Route::get('fields/jobtitle/edit/{id}', 'Admin\FieldsController@editJobtitle');
			Route::post('fields/jobtitle/update', 'Admin\FieldsController@updateJobtitle');

			// Leave Types
			Route::get('fields/leavetype', 'Admin\FieldsController@leavetype')->name('leavetype');
			Route::post('fields/leavetype/add', 'Admin\FieldsController@addLeavetype');
			Route::get('fields/leavetype/delete/{id}', 'Admin\FieldsController@deleteLeavetype');
			Route::post('fields/leavetype/delete', 'Admin\FieldsController@clearLeavetype');
			Route::get('fields/leavetype/edit/{id}', 'Admin\FieldsController@editLeavetype');
			Route::post('fields/leavetype/update', 'Admin\FieldsController@updateLeavetype');

			// Leave Groups
			Route::get('fields/leavetype/leave-groups',  'Admin\FieldsController@leaveGroups')->name('leavegroup');
			Route::post('fields/leavetype/leave-groups/add',  'Admin\FieldsController@addLeaveGroups');
			Route::get('fields/leavetype/leave-groups/edit/{id}',  'Admin\FieldsController@editLeaveGroups');
			Route::post('fields/leavetype/leave-groups/update',  'Admin\FieldsController@updateLeaveGroups');
			Route::get('fields/leavetype/leave-groups/delete/{id}',  'Admin\FieldsController@deleteLeaveGroups');


			// Common Taks
			Route::get('fields/common_task', 'Admin\FieldsController@common_task')->name('common_task');
			Route::post('fields/common_task/add', 'Admin\FieldsController@addCommonTask');

			/*
			|--------------------------------------------------------------------------
			| Exports : Employee data
			|--------------------------------------------------------------------------
			*/
			// export
			Route::get('export/fields/company', 'Admin\ExportsController@company');
			Route::get('export/fields/department', 'Admin\ExportsController@department');
			Route::get('export/fields/jobtitle', 'Admin\ExportsController@jobtitle');
			Route::get('export/fields/leavetypes', 'Admin\ExportsController@leavetypes');

			// import
			Route::post('import/fields/company', 'Admin\ImportsController@importCompany');
			Route::post('import/fields/department', 'Admin\ImportsController@importDepartment');
			Route::post('import/fields/jobtitle', 'Admin\ImportsController@importJobtitle');
			Route::post('import/fields/leavetypes', 'Admin\ImportsController@importLeavetypes');

			// import options
			Route::post('import/options', 'Admin\ImportsController@opt');

			// reports export
			Route::get('export/report/employees', 'Admin\ExportsController@employeeList');
			Route::post('export/report/attendance', 'Admin\ExportsController@attendanceReport');
			Route::post('export/report/leaves', 'Admin\ExportsController@leavesReport');
			Route::get('export/report/birthdays', 'Admin\ExportsController@birthdaysReport');
			Route::get('export/report/accounts', 'Admin\ExportsController@accountReport');
			Route::get('export/report/schedule', 'Admin\ExportsController@scheduleReport');
		});

		Route::group(['middleware' => 'employee'], function () {
			/*
			|--------------------------------------------------------------------------
			| Employee Frontend : Dashboard, Profile, Attendance, Schedules, Leaves, Settings
			|--------------------------------------------------------------------------
			*/
			// dashboard
			Route::get('personal/dashboard', 'Personal\PersonalDashboardController@index');

			Route::get('personal/dashboard/task_reminder', 'Personal\PersonalDashboardController@task_reminder');

			// profile
			Route::get('personal/profile/view', 'Personal\PersonalProfileController@index')->name('myProfile');
			Route::get('personal/profile/edit/', 'Personal\PersonalProfileController@profileEdit');
			Route::post('personal/profile/update', 'Personal\PersonalProfileController@profileUpdate');

			// attendance  
			Route::get('personal/attendance/view', 'Personal\PersonalAttendanceController@index');
			Route::get('/get/personal/attendance', 'Personal\PersonalAttendanceController@attendance_search');
			Route::get('personal/attendance/details', 'Personal\PersonalAttendanceController@details');
			Route::get('personal/apply/missing-attendance', 'Personal\PersonalAttendanceController@apply_missing_attendance');
			Route::post('personal/apply/missing_attendance', 'Personal\PersonalAttendanceController@apply_missing_attendance_post');
			Route::get('personal/applications/missing_attendances', 'Personal\PersonalAttendanceController@applications_for_missing_attendances');
                       Route::get('/personal/attendance/email_preference', 'Personal\PersonalAttendanceController@email_preference_change');




			// schedules
			Route::get('personal/schedules/view', 'Personal\PersonalSchedulesController@index');
			Route::get('get/personal/schedules', 'Personal\PersonalSchedulesController@getPS');

			// tasks
			Route::get('personal/tassk/details/{id}', 'Personal\PersonalTasksController@task_details');
			Route::get('personal/tasks/manager', 'Personal\PersonalTasksController@index');
			Route::get('personal/tasks/mytasks', 'Personal\PersonalTasksController@myTasks');
			Route::get('personal/tasks/assignatask', 'Personal\PersonalTasksController@assignATask');
			Route::get('personal/tasks/edit/{id}', 'Personal\PersonalTasksController@edit');
			Route::get('personal/tasks/view/{id}', 'Personal\PersonalTasksController@view');
			Route::get('personal/tasks/delete/{id}', 'Personal\PersonalTasksController@edit');
			Route::post('personal/tasks/update', 'Personal\PersonalTasksController@update');
			Route::post('personal/tasks/add', 'Personal\PersonalTasksController@add');
			Route::get('personal/tasks/assignatask/edit/{id}', 'Personal\PersonalTasksController@edit_assignATask');
			Route::get('personal/tasks/assignatask/delete/{id}', 'Personal\PersonalTasksController@delete_assignATask');
			Route::post('personal/tasks/assignatask/update', 'Personal\PersonalTasksController@update_assignATask');
			Route::get('personal/extend-deadline/{id}', 'Personal\PersonalTasksController@extend_deadline');
			Route::post('personal/update-deadline', 'Personal\PersonalTasksController@update_deadline');
			Route::get('/personal/tasks/search', 'Personal\PersonalTasksController@task_search');
			Route::get('/personal/assigned/tasks/search', 'Personal\PersonalTasksController@assigned_task_search');
			Route::get('personal/task/task_eligible/{id}', 'Personal\PersonalTasksController@task_eligible');



			// leaves
			Route::get('personal/leaves/view', 'Personal\PersonalLeavesController@index')->name('viewPersonalLeave');
			Route::get('personal/leaves/edit/{id}', 'Personal\PersonalLeavesController@edit');
			Route::post('personal/leaves/update', 'Personal\PersonalLeavesController@update');
			Route::post('personal/leaves/request', 'Personal\PersonalLeavesController@requestL');
			Route::get('personal/leaves/delete/{id}', 'Personal\PersonalLeavesController@delete');
			Route::get('get/personal/leaves', 'Personal\PersonalLeavesController@getPL');
			Route::get('/get/personal/leave', 'Personal\PersonalLeavesController@leave_search');
			Route::get('view/personal/leave', 'Personal\PersonalLeavesController@viewPL');

			// settings
			Route::get('personal/settings', 'Personal\PersonalSettingsController@index');

			// user
			Route::get('personal/update-user', 'Personal\PersonalAccountController@viewUser')->name('changeUser');
			Route::get('personal/update-password', 'Personal\PersonalAccountController@viewPassword')->name('changePass');
			Route::post('personal/update/user', 'Personal\PersonalAccountController@updateUser');
			Route::post('personal/update/password', 'Personal\PersonalAccountController@updatePassword');

			//notice 
			Route::get('personal/notice/view', 'NoticeController@personal_notice_view')->name('notices');
			

		});
	});
});


Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::view('permission-denied', 'errors.permission-denied')->name('denied');
Route::view('account-disabled', 'errors.account-disabled')->name('disabled');
Route::view('account-not-found', 'errors.account-not-found')->name('notfound');

//Routes for webcam
Route::view('camlogin', 'auth/camlogin');
