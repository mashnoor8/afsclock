-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2020 at 08:13 AM
-- Server version: 8.0.22-0ubuntu0.20.04.2
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timesheet_westace_bd`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int UNSIGNED NOT NULL,
  `reference` int DEFAULT NULL,
  `idno` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `date` date DEFAULT NULL,
  `employee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `timein` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `break_in` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `break_out` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalhours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status_timein` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status_timeout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `reference`, `idno`, `date`, `employee`, `timein`, `timeout`, `break_in`, `break_out`, `totalhours`, `status_timein`, `status_timeout`, `reason`, `comment`, `schedule_id`) VALUES
(4, 4, 'WES002', '2020-09-07', 'RAHMAN, MASHFIQUR ', '2020-09-07 10:00:00 AM', '2020-09-07 06:25:58 PM', NULL, NULL, '7.43', 'In Time', 'Early Departure', '', NULL, '3'),
(6, 6, 'WES004', '2020-09-07', 'KHANDAKAR, SIYAM ', '2020-09-07 12:34:50 PM', '2020-09-07 06:53:08 PM', NULL, NULL, '6.18', 'Late Arrival', 'Early Departure', '', NULL, '3'),
(7, 8, 'WES006', '2020-09-07', 'SAKIB, SEYED NAZMUS', '2020-09-07 12:41:34 PM', '2020-09-07 09:29:57 PM', NULL, NULL, '8.48', 'Late Arrival', 'On Time', '', NULL, '4'),
(8, 9, 'WES007', '2020-09-07', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-07 01:05:32 PM', '2020-09-07 05:58:39 PM', NULL, NULL, '4.53', 'Late Arrival', 'Early Departure', '', NULL, '5'),
(9, 3, 'WES001', '2020-09-07', 'RUMII, MASUD ', '2020-09-07 01:11:48 PM', '2020-09-08 01:01:07 AM', NULL, NULL, '11.49', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(10, 7, 'WES005', '2020-09-07', 'SHAHA, AVIJIT ', '2020-09-07 04:21:11 PM', '2020-09-08 01:01:23 AM', NULL, NULL, '8.40', 'In Time', 'On Time', '', NULL, '2'),
(11, 9, 'WES007', '2020-09-08', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-08 08:55:10 AM', '2020-09-08 05:13:51 PM', NULL, NULL, '8.18', 'In Time', 'Early Departure', '', NULL, '5'),
(12, 4, 'WES002', '2020-09-08', 'RAHMAN, MASHFIQUR ', '2020-09-08 09:52:58 AM', '2020-09-08 05:55:33 PM', NULL, NULL, '8.2', 'In Time', 'Early Departure', '', NULL, '3'),
(13, 6, 'WES004', '2020-09-08', 'KHANDAKAR, SIYAM ', '2020-09-08 10:25:20 AM', '2020-09-08 06:33:14 PM', NULL, NULL, '8.7', 'In Time', 'Early Departure', '', NULL, '3'),
(14, 3, 'WES001', '2020-09-08', 'RUMII, MASUD ', '2020-09-08 12:37:19 PM', '2020-09-08 10:56:46 PM', NULL, NULL, '10.19', 'In Time', 'On Time', '', NULL, '1'),
(15, 8, 'WES006', '2020-09-08', 'SAKIB, SEYED NAZMUS', '2020-09-08 12:37:27 PM', '2020-09-09 12:30:24 AM', NULL, NULL, '11.52', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(16, 7, 'WES005', '2020-09-08', 'SHAHA, AVIJIT ', '2020-09-08 01:25:44 PM', '2020-09-08 11:43:23 PM', NULL, NULL, '10.17', 'In Time', 'On Time', '', NULL, '2'),
(18, 9, 'WES007', '2020-09-09', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-09 10:02:13 AM', '2020-09-09 06:15:05 PM', NULL, NULL, '8.12', 'Late Arrival', 'On Time', '', NULL, '5'),
(19, 4, 'WES002', '2020-09-09', 'RAHMAN, MASHFIQUR ', '2020-09-09 10:03:34 AM', '2020-09-09 06:05:46 PM', NULL, NULL, '8.2', 'In Time', 'Early Departure', '', NULL, '3'),
(20, 6, 'WES004', '2020-09-09', 'KHANDAKAR, SIYAM ', '2020-09-09 10:07:48 AM', '2020-09-09 06:11:27 PM', NULL, NULL, '8.3', 'In Time', 'Early Departure', '', NULL, '3'),
(22, 3, 'WES001', '2020-09-09', 'RUMII, MASUD ', '2020-09-09 12:30:09 PM', '2020-09-09 09:25:42 PM', NULL, NULL, '8.55', 'In Time', 'On Time', '', NULL, '1'),
(23, 8, 'WES006', '2020-09-09', 'SAKIB, SEYED NAZMUS', '2020-09-09 12:40:05 PM', '2020-09-09 08:41:04 PM', NULL, NULL, '8.0', 'Late Arrival', 'On Time', '', NULL, '4'),
(24, 7, 'WES005', '2020-09-09', 'SHAHA, AVIJIT ', '2020-09-09 04:09:04 PM', '2020-09-10 01:18:19 AM', NULL, NULL, '9.9', 'In Time', 'On Time', '', NULL, '2'),
(25, 4, 'WES002', '2020-09-10', 'RAHMAN, MASHFIQUR ', '2020-09-10 09:59:44 AM', '2020-09-10 06:11:55 PM', NULL, NULL, '8.12', 'In Time', 'Early Departure', '', NULL, '3'),
(26, 6, 'WES004', '2020-09-10', 'KHANDAKAR, SIYAM ', '2020-09-10 10:28:04 AM', '2020-09-10 06:35:19 PM', NULL, NULL, '8.7', 'In Time', 'Early Departure', '', NULL, '3'),
(27, 9, 'WES007', '2020-09-10', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-10 10:39:49 AM', '2020-09-10 07:35:17 PM', NULL, NULL, '8.55', 'Late Arrival', 'On Time', '', NULL, '5'),
(28, 8, 'WES006', '2020-09-10', 'SAKIB, SEYED NAZMUS', '2020-09-10 01:01:28 PM', '2020-09-10 09:17:58 PM', NULL, NULL, '8.16', 'Late Arrival', 'On Time', '', NULL, '4'),
(30, 3, 'WES001', '2020-09-10', 'RUMII, MASUD ', '2020-09-10 01:41:14 PM', '2020-09-10 10:29:00 PM', NULL, NULL, '8.47', 'Late Arrival', 'On Time', '', NULL, '1'),
(31, 7, 'WES005', '2020-09-10', 'SHAHA, AVIJIT ', '2020-09-10 06:15:25 PM', '2020-09-11 02:51:33 AM', NULL, NULL, '8.36', 'Late Arrival', 'On Time', '', NULL, '2'),
(32, 8, 'WES006', '2020-09-11', 'SAKIB, SEYED NAZMUS', '2020-09-11 01:26:08 PM', '2020-09-11 10:01:18 PM', NULL, NULL, '8.35', 'Late Arrival', 'On Time', '', NULL, '4'),
(33, 3, 'WES001', '2020-09-11', 'RUMII, MASUD ', '2020-09-11 01:57:17 PM', '2020-09-12 02:20:19 AM', NULL, NULL, '12.23', 'Late Arrival', 'Not Scheduled', '', NULL, '1'),
(35, 7, 'WES005', '2020-09-11', 'SHAHA, AVIJIT ', '2020-09-11 06:01:35 PM', '2020-09-12 03:04:59 AM', NULL, NULL, '9.3', 'Late Arrival', 'Not Scheduled', '', NULL, '2'),
(36, 9, 'WES007', '2020-09-12', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-12 09:40:00 AM', '2020-09-12 06:51:05 PM', NULL, NULL, '9.11', 'In Time', 'On Time', '', NULL, '5'),
(37, 6, 'WES004', '2020-09-12', 'KHANDAKAR, SIYAM ', '2020-09-12 09:44:09 AM', '2020-09-12 06:18:29 PM', NULL, NULL, '8.34', 'In Time', 'Early Departure', '', NULL, '3'),
(38, 4, 'WES002', '2020-09-12', 'RAHMAN, MASHFIQUR ', '2020-09-12 09:56:25 AM', '2020-09-12 06:01:50 PM', NULL, NULL, '8.5', 'In Time', 'Early Departure', '', NULL, '3'),
(40, 3, 'WES001', '2020-09-12', 'RUMII, MASUD ', '2020-09-12 01:20:54 PM', '2020-09-12 10:32:34 PM', NULL, NULL, '9.11', 'Not Scheduled', 'Not Scheduled', '', NULL, '1'),
(41, 9, 'WES007', '2020-09-13', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-13 09:49:25 AM', '2020-09-13 05:57:54 PM', NULL, NULL, '8.8', 'In Time', 'Early Departure', '', NULL, '5'),
(42, 4, 'WES002', '2020-09-13', 'RAHMAN, MASHFIQUR ', '2020-09-13 10:00:04 AM', '2020-09-13 06:01:35 PM', NULL, NULL, '8.1', 'In Time', 'Early Departure', '', NULL, '3'),
(43, 6, 'WES004', '2020-09-13', 'KHANDAKAR, SIYAM ', '2020-09-13 10:06:11 AM', '2020-09-13 06:43:34 PM', NULL, NULL, '8.37', 'In Time', 'Early Departure', '', NULL, '3'),
(44, 3, 'WES001', '2020-09-13', 'RUMII, MASUD ', '2020-09-13 01:30:29 PM', '2020-09-19 01:39:41 PM', NULL, NULL, '16.0', 'Late Arrival', 'Not Scheduled', '', NULL, '1'),
(45, 8, 'WES006', '2020-09-13', 'SAKIB, SEYED NAZMUS', '2020-09-13 01:40:52 PM', '2020-09-14 02:08:23 AM', NULL, NULL, '12.27', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(46, 10, 'WES008', '2020-09-13', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-13 02:27:09 PM', '2020-09-14 02:14:47 AM', NULL, NULL, '11.47', 'In Time', 'Early Departure', '', NULL, '6'),
(47, 7, 'WES005', '2020-09-13', 'SHAHA, AVIJIT ', '2020-09-13 05:08:50 PM', '2020-09-14 03:33:31 AM', NULL, NULL, '10.24', 'Late Arrival', 'On Time', '', NULL, '2'),
(48, 8, 'WES006', '2020-09-16', 'SAKIB, SEYED NAZMUS', '2020-09-16 03:35:37 PM', '2020-09-17 01:26:20 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(49, 7, 'WES005', '2020-09-16', 'SHAHA, AVIJIT ', '2020-09-16 04:46:43 PM', '2020-09-17 07:19:59 PM', NULL, NULL, '16.0', 'In Time', 'On Time', '', NULL, '2'),
(50, 9, 'WES007', '2020-09-16', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-16 10:25:26 PM', '2020-09-16 10:25:45 PM', NULL, NULL, '0.0', 'Late Arrival', 'On Time', '', NULL, '5'),
(51, 6, 'WES004', '2020-09-17', 'KHANDAKAR, SIYAM ', '2020-09-17 10:01:45 AM', '2020-09-17 06:37:09 PM', NULL, NULL, '8.35', 'In Time', 'Early Departure', '', NULL, '3'),
(52, 4, 'WES002', '2020-09-17', 'RAHMAN, MASHFIQUR ', '2020-09-17 10:31:33 AM', '2020-09-17 08:09:33 PM', NULL, NULL, '9.38', 'In Time', 'On Time', '', NULL, '3'),
(53, 9, 'WES007', '2020-09-17', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-17 12:47:26 PM', '2020-09-17 07:19:35 PM', NULL, NULL, '6.32', 'Late Arrival', 'On Time', '', NULL, '5'),
(55, 8, 'WES006', '2020-09-17', 'SAKIB, SEYED NAZMUS', '2020-09-17 01:26:32 PM', '2020-09-18 01:55:03 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(56, 10, 'WES008', '2020-09-17', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-17 02:52:00 PM', '2020-09-18 03:55:32 AM', NULL, NULL, '13.3', 'In Time', 'Early Departure', '', NULL, '6'),
(57, 7, 'WES005', '2020-09-17', 'SHAHA, AVIJIT ', '2020-09-17 07:20:08 PM', '2020-09-18 06:18:34 AM', NULL, NULL, '10.58', 'Late Arrival', 'On Time', '', NULL, '2'),
(58, 9, 'WES007', '2020-09-18', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-18 10:51:41 AM', '2020-09-18 05:48:33 PM', NULL, NULL, '6.56', 'Not Scheduled', 'Not Scheduled', '', NULL, '5'),
(59, 8, 'WES006', '2020-09-18', 'SAKIB, SEYED NAZMUS', '2020-09-18 01:55:15 PM', '2020-09-20 01:41:37 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(60, 10, 'WES008', '2020-09-18', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-18 03:12:00 PM', '2020-09-19 04:19:34 AM', NULL, NULL, '13.7', 'Late Arrival', 'Not Scheduled', '', NULL, '6'),
(61, 7, 'WES005', '2020-09-18', 'SHAHA, AVIJIT ', '2020-09-18 06:13:11 PM', '2020-09-19 03:03:01 AM', NULL, NULL, '8.49', 'Late Arrival', 'Not Scheduled', '', NULL, '2'),
(62, 6, 'WES004', '2020-09-19', 'KHANDAKAR, SIYAM ', '2020-09-19 09:49:14 AM', '2020-09-19 06:06:59 PM', NULL, NULL, '8.17', 'In Time', 'Early Departure', '', NULL, '3'),
(63, 4, 'WES002', '2020-09-19', 'RAHMAN, MASHFIQUR ', '2020-09-19 10:03:14 AM', '2020-09-19 06:13:44 PM', NULL, NULL, '8.10', 'In Time', 'Early Departure', '', NULL, '3'),
(64, 7, 'WES005', '2020-09-19', 'SHAHA, AVIJIT ', '2020-09-19 10:17:42 AM', '2020-09-19 10:17:57 AM', NULL, NULL, '0.0', 'Not Scheduled', 'Not Scheduled', '', NULL, '2'),
(65, 3, 'WES001', '2020-09-19', 'RUMII, MASUD ', '2020-09-19 01:39:55 PM', '2020-09-19 10:04:57 PM', NULL, NULL, '8.25', 'Not Scheduled', 'Not Scheduled', '', NULL, '1'),
(66, 6, 'WES004', '2020-09-20', 'KHANDAKAR, SIYAM ', '2020-09-20 09:50:02 AM', '2020-09-20 06:37:42 PM', NULL, NULL, '8.47', 'In Time', 'Early Departure', '', NULL, '3'),
(67, 4, 'WES002', '2020-09-20', 'RAHMAN, MASHFIQUR ', '2020-09-20 09:57:49 AM', '2020-09-20 06:12:06 PM', NULL, NULL, '8.14', 'In Time', 'Early Departure', '', NULL, '3'),
(68, 9, 'WES007', '2020-09-20', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-20 10:31:37 AM', '2020-09-20 07:07:23 PM', NULL, NULL, '8.35', 'Late Arrival', 'On Time', '', NULL, '5'),
(69, 3, 'WES001', '2020-09-20', 'RUMII, MASUD ', '2020-09-20 01:16:06 PM', '2020-09-20 10:39:14 PM', NULL, NULL, '9.23', 'Late Arrival', 'On Time', '', NULL, '1'),
(71, 8, 'WES006', '2020-09-20', 'SAKIB, SEYED NAZMUS', '2020-09-20 01:42:43 PM', '2020-09-21 01:07:12 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(72, 10, 'WES008', '2020-09-20', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-20 03:27:30 PM', '2020-09-21 05:26:41 AM', NULL, NULL, '13.59', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(73, 7, 'WES005', '2020-09-20', 'SHAHA, AVIJIT ', '2020-09-20 06:17:10 PM', '2020-09-21 09:47:43 PM', NULL, NULL, '16.0', 'Late Arrival', 'On Time', '', NULL, '2'),
(74, 9, 'WES007', '2020-09-21', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-21 09:25:40 AM', '2020-09-21 05:34:25 PM', NULL, NULL, '8.8', 'In Time', 'Early Departure', '', NULL, '5'),
(75, 4, 'WES002', '2020-09-21', 'RAHMAN, MASHFIQUR ', '2020-09-21 10:14:40 AM', '2020-09-22 12:13:50 AM', NULL, NULL, '13.59', 'In Time', 'Early Departure', '', NULL, '3'),
(76, 6, 'WES004', '2020-09-21', 'KHANDAKAR, SIYAM ', '2020-09-21 10:31:32 AM', '2020-09-21 06:12:53 PM', NULL, NULL, '7.41', 'In Time', 'Early Departure', '', NULL, '3'),
(77, 8, 'WES006', '2020-09-21', 'SAKIB, SEYED NAZMUS', '2020-09-21 01:07:22 PM', '2020-09-22 12:28:51 AM', NULL, NULL, '11.21', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(79, 3, 'WES001', '2020-09-21', 'RUMII, MASUD ', '2020-09-21 01:27:02 PM', '2020-09-21 11:21:58 PM', NULL, NULL, '9.54', 'Late Arrival', 'On Time', '', NULL, '1'),
(80, 7, 'WES005', '2020-09-21', 'SHAHA, AVIJIT ', '2020-09-21 09:48:52 PM', '2020-09-22 02:10:06 AM', NULL, NULL, '4.21', 'Late Arrival', 'On Time', '', NULL, '2'),
(81, 9, 'WES007', '2020-09-22', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-22 09:42:26 AM', '2020-09-22 06:42:29 PM', NULL, NULL, '9.0', 'In Time', 'On Time', '', NULL, '5'),
(82, 4, 'WES002', '2020-09-22', 'RAHMAN, MASHFIQUR ', '2020-09-22 11:03:20 AM', '2020-09-22 09:19:14 PM', NULL, NULL, '10.15', 'Late Arrival', 'On Time', '', NULL, '3'),
(83, 6, 'WES004', '2020-09-22', 'KHANDAKAR, SIYAM ', '2020-09-22 11:25:20 AM', '2020-09-22 06:12:11 PM', NULL, NULL, '6.46', 'Late Arrival', 'Early Departure', '', NULL, '3'),
(85, 3, 'WES001', '2020-09-22', 'RUMII, MASUD ', '2020-09-22 01:17:24 PM', '2020-09-23 05:34:39 AM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(86, 8, 'WES006', '2020-09-22', 'SAKIB, SEYED NAZMUS', '2020-09-22 01:35:33 PM', '2020-09-23 04:10:46 AM', NULL, NULL, '14.35', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(87, 10, 'WES008', '2020-09-22', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-22 02:59:41 PM', '2020-09-23 03:53:40 AM', NULL, NULL, '12.53', 'In Time', 'Early Departure', '', NULL, '6'),
(88, 9, 'WES007', '2020-09-23', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-23 09:21:21 AM', '2020-09-23 05:24:37 PM', NULL, NULL, '8.3', 'In Time', 'Early Departure', '', NULL, '5'),
(89, 4, 'WES002', '2020-09-23', 'RAHMAN, MASHFIQUR ', '2020-09-23 10:18:44 AM', '2020-09-23 06:11:58 PM', NULL, NULL, '7.53', 'In Time', 'Early Departure', '', NULL, '3'),
(90, 6, 'WES004', '2020-09-23', 'KHANDAKAR, SIYAM ', '2020-09-23 10:29:24 AM', '2020-09-23 06:47:22 PM', NULL, NULL, '8.17', 'In Time', 'Early Departure', '', NULL, '3'),
(91, 8, 'WES006', '2020-09-23', 'SAKIB, SEYED NAZMUS', '2020-09-23 01:45:12 PM', '2020-09-24 02:00:01 AM', NULL, NULL, '12.14', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(93, 10, 'WES008', '2020-09-23', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-23 02:54:59 PM', '2020-09-24 03:08:19 AM', NULL, NULL, '12.13', 'In Time', 'Early Departure', '', NULL, '6'),
(94, 3, 'WES001', '2020-09-23', 'RUMII, MASUD ', '2020-09-23 04:27:59 PM', '2020-09-23 10:14:50 PM', NULL, NULL, '5.46', 'Late Arrival', 'On Time', '', NULL, '1'),
(95, 7, 'WES005', '2020-09-23', 'SHAHA, AVIJIT ', '2020-09-23 04:28:29 PM', '2020-09-23 05:58:28 PM', NULL, NULL, '1.29', 'In Time', 'On Time', '', NULL, '2'),
(96, 9, 'WES007', '2020-09-24', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-24 08:57:38 AM', '2020-09-25 10:46:13 AM', NULL, NULL, '16.0', 'In Time', 'Not Scheduled', '', NULL, '5'),
(97, 4, 'WES002', '2020-09-24', 'RAHMAN, MASHFIQUR ', '2020-09-24 09:58:30 AM', '2020-09-24 06:13:14 PM', NULL, NULL, '8.14', 'In Time', 'Early Departure', '', NULL, '3'),
(98, 6, 'WES004', '2020-09-24', 'KHANDAKAR, SIYAM ', '2020-09-24 10:10:44 AM', '2020-09-24 06:05:50 PM', NULL, NULL, '7.55', 'In Time', 'Early Departure', '', NULL, '3'),
(99, 8, 'WES006', '2020-09-24', 'SAKIB, SEYED NAZMUS', '2020-09-24 12:11:40 PM', '2020-09-25 03:32:37 AM', NULL, NULL, '15.20', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(100, 3, 'WES001', '2020-09-24', 'RUMII, MASUD ', '2020-09-24 02:41:59 PM', '2020-09-24 10:33:36 PM', NULL, NULL, '7.51', 'Late Arrival', 'On Time', '', NULL, '1'),
(102, 7, 'WES005', '2020-09-24', 'SHAHA, AVIJIT ', '2020-09-24 06:06:55 PM', '2020-09-25 04:23:09 AM', NULL, NULL, '10.16', 'Late Arrival', 'On Time', '', NULL, '2'),
(103, 6, 'WES004', '2020-09-25', 'KHANDAKAR, SIYAM ', '2020-09-25 10:29:12 AM', '2020-09-25 06:34:35 PM', NULL, NULL, '8.5', 'Not Scheduled', 'Not Scheduled', '', NULL, '3'),
(104, 9, 'WES007', '2020-09-25', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-25 10:46:23 AM', '2020-09-25 07:09:05 PM', NULL, NULL, '8.22', 'Not Scheduled', 'Not Scheduled', '', NULL, '5'),
(105, 8, 'WES006', '2020-09-25', 'SAKIB, SEYED NAZMUS', '2020-09-25 01:24:40 PM', '2020-09-25 08:37:17 PM', NULL, NULL, '7.12', 'Late Arrival', 'On Time', '', NULL, '4'),
(107, 10, 'WES008', '2020-09-25', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-25 03:05:47 PM', '2020-09-26 03:39:13 AM', NULL, NULL, '12.33', 'Late Arrival', 'Not Scheduled', '', NULL, '6'),
(108, 3, 'WES001', '2020-09-25', 'RUMII, MASUD ', '2020-09-25 03:06:19 PM', '2020-09-25 06:35:36 PM', NULL, NULL, '3.29', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(109, 7, 'WES005', '2020-09-25', 'SHAHA, AVIJIT ', '2020-09-25 06:12:51 PM', '2020-09-26 05:55:56 AM', NULL, NULL, '11.43', 'Late Arrival', 'Not Scheduled', '', NULL, '2'),
(110, 4, 'WES002', '2020-09-26', 'RAHMAN, MASHFIQUR ', '2020-09-26 09:56:47 AM', '2020-09-26 06:00:19 PM', NULL, NULL, '8.3', 'In Time', 'Early Departure', '', NULL, '3'),
(111, 4, 'WES002', '2020-09-27', 'RAHMAN, MASHFIQUR ', '2020-09-27 09:31:09 AM', '2020-09-27 05:39:41 PM', NULL, NULL, '8.8', 'In Time', 'Early Departure', '', NULL, '3'),
(112, 6, 'WES004', '2020-09-27', 'KHANDAKAR, SIYAM ', '2020-09-27 10:04:35 AM', '2020-09-27 06:30:15 PM', NULL, NULL, '8.25', 'In Time', 'Early Departure', '', NULL, '3'),
(113, 9, 'WES007', '2020-09-27', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-27 01:08:59 PM', '2020-09-27 07:35:15 PM', NULL, NULL, '6.26', 'Late Arrival', 'On Time', '', NULL, '5'),
(115, 8, 'WES006', '2020-09-27', 'SAKIB, SEYED NAZMUS', '2020-09-27 01:17:37 PM', '2020-09-27 09:05:18 PM', NULL, NULL, '7.47', 'Late Arrival', 'On Time', '', NULL, '4'),
(116, 10, 'WES008', '2020-09-27', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-27 04:48:07 PM', '2020-09-28 01:11:47 AM', NULL, NULL, '8.23', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(117, 4, 'WES002', '2020-09-28', 'RAHMAN, MASHFIQUR ', '2020-09-28 10:01:36 AM', '2020-09-28 08:14:12 PM', NULL, NULL, '10.12', 'In Time', 'On Time', '', NULL, '3'),
(118, 9, 'WES007', '2020-09-28', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-28 10:05:56 AM', '2020-09-28 09:10:15 PM', NULL, NULL, '11.4', 'Late Arrival', 'On Time', '', NULL, '5'),
(119, 6, 'WES004', '2020-09-28', 'KHANDAKAR, SIYAM ', '2020-09-28 10:53:16 AM', '2020-09-28 07:38:14 PM', NULL, NULL, '8.44', 'In Time', 'On Time', '', NULL, '3'),
(121, 8, 'WES006', '2020-09-28', 'SAKIB, SEYED NAZMUS', '2020-09-28 12:04:05 PM', '2020-09-28 08:32:19 PM', NULL, NULL, '8.28', 'Late Arrival', 'On Time', '', NULL, '4'),
(122, 3, 'WES001', '2020-09-28', 'RUMII, MASUD ', '2020-09-28 01:11:00 PM', '2020-09-28 08:40:11 PM', NULL, NULL, '7.29', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(123, 10, 'WES008', '2020-09-28', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-28 02:35:58 PM', '2020-09-29 03:06:38 AM', NULL, NULL, '12.30', 'In Time', 'Early Departure', '', NULL, '6'),
(124, 7, 'WES005', '2020-09-28', 'SHAHA, AVIJIT ', '2020-09-28 05:54:02 PM', '2020-09-29 03:35:26 AM', NULL, NULL, '9.41', 'Late Arrival', 'On Time', '', NULL, '2'),
(125, 4, 'WES002', '2020-09-29', 'RAHMAN, MASHFIQUR ', '2020-09-29 09:51:50 AM', '2020-09-29 06:33:57 PM', NULL, NULL, '8.42', 'In Time', 'Early Departure', '', NULL, '3'),
(126, 6, 'WES004', '2020-09-29', 'KHANDAKAR, SIYAM ', '2020-09-29 10:34:02 AM', '2020-09-29 07:20:01 PM', NULL, NULL, '8.45', 'In Time', 'On Time', '', NULL, '3'),
(128, 3, 'WES001', '2020-09-29', 'RUMII, MASUD ', '2020-09-29 01:21:14 PM', '2020-09-29 08:59:37 PM', NULL, NULL, '7.38', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(129, 8, 'WES006', '2020-09-29', 'SAKIB, SEYED NAZMUS', '2020-09-29 01:22:38 PM', '2020-09-29 08:05:28 PM', NULL, NULL, '6.42', 'Late Arrival', 'On Time', '', NULL, '4'),
(130, 7, 'WES005', '2020-09-29', 'SHAHA, AVIJIT ', '2020-09-29 05:47:31 PM', '2020-09-30 03:54:50 AM', NULL, NULL, '10.7', 'Late Arrival', 'On Time', '', NULL, '2'),
(131, 10, 'WES008', '2020-09-29', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-29 05:47:42 PM', '2020-09-30 04:20:06 AM', NULL, NULL, '10.32', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(132, 4, 'WES002', '2020-09-30', 'RAHMAN, MASHFIQUR ', '2020-09-30 10:02:15 AM', '2020-09-30 06:24:01 PM', NULL, NULL, '8.21', 'In Time', 'Early Departure', '', NULL, '3'),
(133, 6, 'WES004', '2020-09-30', 'KHANDAKAR, SIYAM ', '2020-09-30 10:49:13 AM', '2020-09-30 07:21:10 PM', NULL, NULL, '8.31', 'In Time', 'On Time', '', NULL, '3'),
(134, 9, 'WES007', '2020-09-30', 'KHAN, BAJLUL HUDA NURULLAH', '2020-09-30 10:54:03 AM', '2020-09-30 08:36:26 PM', NULL, NULL, '9.42', 'Late Arrival', 'On Time', '', NULL, '5'),
(136, 8, 'WES006', '2020-09-30', 'SAKIB, SEYED NAZMUS', '2020-09-30 12:47:33 PM', '2020-10-01 12:49:22 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '4'),
(137, 3, 'WES001', '2020-09-30', 'RUMII, MASUD ', '2020-09-30 02:44:11 PM', '2020-09-30 10:12:30 PM', NULL, NULL, '7.28', 'Late Arrival', 'On Time', '', NULL, '1'),
(138, 10, 'WES008', '2020-09-30', 'SHIHAB, SHAHARIAR RAHMAN', '2020-09-30 03:22:15 PM', '2020-10-01 12:03:01 AM', NULL, NULL, '8.40', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(139, 7, 'WES005', '2020-09-30', 'SHAHA, AVIJIT ', '2020-09-30 05:51:08 PM', '2020-10-01 12:02:53 AM', NULL, NULL, '6.11', 'Late Arrival', 'Early Departure', '', NULL, '2'),
(140, 9, 'WES007', '2020-10-01', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-01 08:59:06 AM', '2020-10-01 05:26:09 PM', NULL, NULL, '8.27', 'In Time', 'Early Departure', '', NULL, '5'),
(141, 4, 'WES002', '2020-10-01', 'RAHMAN, MASHFIQUR ', '2020-10-01 09:33:15 AM', '2020-10-03 09:59:34 AM', NULL, NULL, '16.0', 'In Time', 'Early Departure', '', NULL, '3'),
(142, 6, 'WES004', '2020-10-01', 'KHANDAKAR, SIYAM ', '2020-10-01 10:51:05 AM', '2020-10-01 07:21:50 PM', NULL, NULL, '8.30', 'In Time', 'On Time', '', NULL, '3'),
(143, 7, 'WES005', '2020-10-01', 'SHAHA, AVIJIT ', '2020-10-01 12:26:24 PM', '2020-10-02 12:04:48 AM', NULL, NULL, '11.38', 'In Time', 'Early Departure', '', NULL, '2'),
(144, 8, 'WES006', '2020-10-01', 'SAKIB, SEYED NAZMUS', '2020-10-01 12:49:55 PM', '2020-10-01 10:52:08 PM', NULL, NULL, '10.2', 'Late Arrival', 'On Time', '', NULL, '4'),
(146, 3, 'WES001', '2020-10-01', 'RUMII, MASUD ', '2020-10-01 01:11:48 PM', '2020-10-01 10:37:25 PM', NULL, NULL, '9.25', 'Late Arrival', 'On Time', '', NULL, '1'),
(147, 10, 'WES008', '2020-10-01', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-01 02:36:29 PM', '2020-10-02 01:27:49 AM', NULL, NULL, '10.51', 'In Time', 'Early Departure', '', NULL, '6'),
(148, 6, 'WES004', '2020-10-02', 'KHANDAKAR, SIYAM ', '2020-10-02 11:24:37 AM', '2020-10-02 07:54:20 PM', NULL, NULL, '8.29', 'Not Scheduled', 'Not Scheduled', '', NULL, '3'),
(149, 7, 'WES005', '2020-10-02', 'SHAHA, AVIJIT ', '2020-10-02 12:26:10 PM', '2020-10-02 10:00:23 PM', NULL, NULL, '9.34', 'In Time', 'On Time', '', NULL, '2'),
(150, 8, 'WES006', '2020-10-02', 'SAKIB, SEYED NAZMUS', '2020-10-02 01:05:04 PM', '2020-10-02 09:22:50 PM', NULL, NULL, '8.17', 'Late Arrival', 'On Time', '', NULL, '4'),
(152, 3, 'WES001', '2020-10-02', 'RUMII, MASUD ', '2020-10-02 02:02:58 PM', '2020-10-02 10:00:31 PM', NULL, NULL, '7.57', 'Late Arrival', 'On Time', '', NULL, '1'),
(153, 10, 'WES008', '2020-10-02', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-02 03:06:41 PM', '2020-10-03 01:25:31 AM', NULL, NULL, '10.18', 'Late Arrival', 'Not Scheduled', '', NULL, '6'),
(154, 9, 'WES007', '2020-10-03', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-03 09:50:18 AM', '2020-10-03 02:48:47 PM', NULL, NULL, '4.58', 'In Time', 'Early Departure', '', NULL, '5'),
(155, 4, 'WES002', '2020-10-03', 'RAHMAN, MASHFIQUR ', '2020-10-03 10:00:01 AM', '2020-10-03 06:27:17 PM', NULL, NULL, '8.27', 'In Time', 'Early Departure', '', NULL, '3'),
(156, 3, 'WES001', '2020-10-03', 'RUMII, MASUD ', '2020-10-03 12:23:15 PM', '2020-10-04 01:42:53 PM', NULL, NULL, '16.0', 'Not Scheduled', 'Early Departure', '', NULL, '1'),
(157, 9, 'WES007', '2020-10-04', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-04 09:46:50 AM', '2020-10-04 09:28:01 PM', NULL, NULL, '11.41', 'In Time', 'On Time', '', NULL, '5'),
(158, 4, 'WES002', '2020-10-04', 'RAHMAN, MASHFIQUR ', '2020-10-04 09:50:39 AM', '2020-10-04 07:14:59 PM', NULL, NULL, '9.24', 'In Time', 'On Time', '', NULL, '3'),
(159, 6, 'WES004', '2020-10-04', 'KHANDAKAR, SIYAM ', '2020-10-04 10:23:10 AM', '2020-10-04 08:00:19 PM', NULL, NULL, '9.37', 'In Time', 'On Time', '', NULL, '3'),
(160, 8, 'WES006', '2020-10-04', 'SAKIB, SEYED NAZMUS', '2020-10-04 10:36:20 AM', '2020-10-04 07:18:46 PM', NULL, NULL, '8.42', 'In Time', 'Early Departure', '', NULL, '4'),
(161, 7, 'WES005', '2020-10-04', 'SHAHA, AVIJIT ', '2020-10-04 12:55:41 PM', '2020-10-05 12:29:41 AM', NULL, NULL, '11.34', 'In Time', 'Early Departure', '', NULL, '2'),
(162, 3, 'WES001', '2020-10-04', 'RUMII, MASUD ', '2020-10-04 01:43:02 PM', '2020-10-04 09:42:18 PM', NULL, NULL, '7.59', 'Late Arrival', 'On Time', '', NULL, '1'),
(164, 10, 'WES008', '2020-10-04', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-04 03:17:00 PM', '2020-10-05 03:43:37 AM', NULL, NULL, '12.26', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(165, 9, 'WES007', '2020-10-05', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-05 09:24:09 AM', '2020-10-05 06:19:07 PM', NULL, NULL, '8.54', 'In Time', 'On Time', '', NULL, '5'),
(166, 4, 'WES002', '2020-10-05', 'RAHMAN, MASHFIQUR ', '2020-10-05 10:01:08 AM', '2020-10-05 06:17:38 PM', NULL, NULL, '8.16', 'In Time', 'Early Departure', '', NULL, '3'),
(167, 6, 'WES004', '2020-10-05', 'KHANDAKAR, SIYAM ', '2020-10-05 11:33:06 AM', '2020-10-05 07:12:24 PM', NULL, NULL, '7.39', 'Late Arrival', 'On Time', '', NULL, '3'),
(168, 10, 'WES008', '2020-10-05', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-05 12:19:54 PM', '2020-10-05 10:34:17 PM', NULL, NULL, '10.14', 'In Time', 'Early Departure', '', NULL, '6'),
(169, 7, 'WES005', '2020-10-05', 'SHAHA, AVIJIT ', '2020-10-05 12:26:47 PM', '2020-10-05 09:29:21 PM', NULL, NULL, '9.2', 'In Time', 'On Time', '', NULL, '2'),
(171, 3, 'WES001', '2020-10-05', 'RUMII, MASUD ', '2020-10-05 12:34:17 PM', '2020-10-05 10:36:31 PM', NULL, NULL, '10.2', 'In Time', 'On Time', '', NULL, '1'),
(172, 8, 'WES006', '2020-10-05', 'SAKIB, SEYED NAZMUS', '2020-10-05 01:07:54 PM', '2020-10-05 08:33:47 PM', NULL, NULL, '7.25', 'Late Arrival', 'On Time', '', NULL, '4'),
(173, 9, 'WES007', '2020-10-06', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-06 09:11:42 AM', '2020-10-06 06:58:16 PM', NULL, NULL, '9.46', 'In Time', 'On Time', '', NULL, '5'),
(174, 4, 'WES002', '2020-10-06', 'RAHMAN, MASHFIQUR ', '2020-10-06 12:31:06 PM', '2020-10-06 07:21:07 PM', NULL, NULL, '6.50', 'Late Arrival', 'On Time', '', NULL, '3'),
(175, 6, 'WES004', '2020-10-06', 'KHANDAKAR, SIYAM ', '2020-10-06 12:38:13 PM', '2020-10-06 07:12:20 PM', NULL, NULL, '6.34', 'Late Arrival', 'On Time', '', NULL, '3'),
(177, 3, 'WES001', '2020-10-06', 'RUMII, MASUD ', '2020-10-06 01:23:01 PM', '2020-10-06 10:11:34 PM', NULL, NULL, '8.48', 'Late Arrival', 'On Time', '', NULL, '1'),
(178, 8, 'WES006', '2020-10-06', 'SAKIB, SEYED NAZMUS', '2020-10-06 02:06:26 PM', '2020-10-06 08:38:42 PM', NULL, NULL, '6.32', 'Late Arrival', 'On Time', '', NULL, '4'),
(179, 7, 'WES005', '2020-10-06', 'SHAHA, AVIJIT ', '2020-10-06 02:12:20 PM', '2020-10-06 11:36:32 PM', NULL, NULL, '9.24', 'In Time', 'On Time', '', NULL, '2'),
(180, 10, 'WES008', '2020-10-06', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-06 03:17:33 PM', '2020-10-07 12:14:32 AM', NULL, NULL, '8.56', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(181, 9, 'WES007', '2020-10-07', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-07 09:24:13 AM', '2020-10-07 08:18:13 PM', NULL, NULL, '10.54', 'In Time', 'On Time', '', NULL, '5'),
(182, 4, 'WES002', '2020-10-07', 'RAHMAN, MASHFIQUR ', '2020-10-07 09:56:08 AM', '2020-10-07 06:19:32 PM', NULL, NULL, '8.23', 'In Time', 'Early Departure', '', NULL, '3'),
(183, 6, 'WES004', '2020-10-07', 'KHANDAKAR, SIYAM ', '2020-10-07 11:04:41 AM', '2020-10-07 07:14:34 PM', NULL, NULL, '8.9', 'Late Arrival', 'On Time', '', NULL, '3'),
(184, 7, 'WES005', '2020-10-07', 'SHAHA, AVIJIT ', '2020-10-07 12:32:00 PM', '2020-10-08 12:21:35 AM', NULL, NULL, '11.49', 'In Time', 'Early Departure', '', NULL, '2'),
(185, 8, 'WES006', '2020-10-07', 'SAKIB, SEYED NAZMUS', '2020-10-07 12:54:20 PM', '2020-10-07 09:12:38 PM', NULL, NULL, '8.18', 'Late Arrival', 'On Time', '', NULL, '4'),
(187, 3, 'WES001', '2020-10-07', 'RUMII, MASUD ', '2020-10-07 01:14:21 PM', '2020-10-08 06:07:41 AM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(188, 10, 'WES008', '2020-10-07', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-07 03:56:36 PM', '2020-10-08 02:20:23 AM', NULL, NULL, '10.23', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(189, 4, 'WES002', '2020-10-08', 'RAHMAN, MASHFIQUR ', '2020-10-08 10:02:08 AM', '2020-10-08 06:15:32 PM', NULL, NULL, '8.13', 'In Time', 'Early Departure', '', NULL, '3'),
(190, 9, 'WES007', '2020-10-08', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-08 10:15:58 AM', '2020-10-08 06:27:28 PM', NULL, NULL, '8.11', 'Late Arrival', 'On Time', '', NULL, '5'),
(191, 6, 'WES004', '2020-10-08', 'KHANDAKAR, SIYAM ', '2020-10-08 10:44:19 AM', '2020-10-08 07:27:03 PM', NULL, NULL, '8.42', 'In Time', 'On Time', '', NULL, '3'),
(192, 8, 'WES006', '2020-10-08', 'SAKIB, SEYED NAZMUS', '2020-10-08 11:59:32 AM', '2020-10-21 01:27:40 PM', NULL, NULL, '16.0', 'In Time', 'Early Departure', '', NULL, '4'),
(194, 3, 'WES001', '2020-10-08', 'RUMII, MASUD ', '2020-10-08 12:59:05 PM', '2020-10-08 10:42:36 PM', NULL, NULL, '9.43', 'In Time', 'On Time', '', NULL, '1'),
(195, 7, 'WES005', '2020-10-08', 'SHAHA, AVIJIT ', '2020-10-08 02:38:46 PM', '2020-10-09 05:16:27 AM', NULL, NULL, '14.37', 'In Time', 'On Time', '', NULL, '2'),
(196, 9, 'WES007', '2020-10-09', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-09 11:47:00 AM', '2020-10-09 06:34:25 PM', NULL, NULL, '6.47', 'Not Scheduled', 'Not Scheduled', '', NULL, '5'),
(197, 6, 'WES004', '2020-10-09', 'KHANDAKAR, SIYAM ', '2020-10-09 11:48:45 AM', '2020-10-09 07:26:57 PM', NULL, NULL, '7.38', 'Not Scheduled', 'Not Scheduled', '', NULL, '3'),
(198, 7, 'WES005', '2020-10-09', 'SHAHA, AVIJIT ', '2020-10-09 12:23:16 PM', '2020-10-10 08:38:25 AM', NULL, NULL, '16.0', 'In Time', 'Not Scheduled', '', NULL, '2'),
(200, 3, 'WES001', '2020-10-09', 'RUMII, MASUD ', '2020-10-09 02:05:10 PM', '2020-10-09 10:55:25 PM', NULL, NULL, '8.50', 'Late Arrival', 'On Time', '', NULL, '1'),
(201, 10, 'WES008', '2020-10-09', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-09 03:21:06 PM', '2020-10-10 02:54:56 AM', NULL, NULL, '11.33', 'Late Arrival', 'Not Scheduled', '', NULL, '6'),
(202, 4, 'WES002', '2020-10-10', 'RAHMAN, MASHFIQUR ', '2020-10-10 09:48:44 AM', '2020-10-10 07:14:47 PM', NULL, NULL, '9.26', 'In Time', 'On Time', '', NULL, '3'),
(203, 9, 'WES007', '2020-10-11', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-11 08:54:23 AM', '2020-10-11 05:30:21 PM', NULL, NULL, '8.35', 'In Time', 'Early Departure', '', NULL, '5'),
(204, 4, 'WES002', '2020-10-11', 'RAHMAN, MASHFIQUR ', '2020-10-11 09:52:51 AM', '2020-10-11 06:39:02 PM', NULL, NULL, '8.46', 'In Time', 'Early Departure', '', NULL, '3'),
(205, 6, 'WES004', '2020-10-11', 'KHANDAKAR, SIYAM ', '2020-10-11 10:44:58 AM', '2020-10-11 06:38:21 PM', NULL, NULL, '7.53', 'In Time', 'Early Departure', '', NULL, '3'),
(207, 3, 'WES001', '2020-10-11', 'RUMII, MASUD ', '2020-10-11 12:09:16 PM', '2020-10-11 11:11:55 PM', NULL, NULL, '11.2', 'In Time', 'On Time', '', NULL, '1'),
(208, 7, 'WES005', '2020-10-11', 'SHAHA, AVIJIT ', '2020-10-11 02:20:59 PM', '2020-10-11 10:15:05 PM', NULL, NULL, '7.54', 'In Time', 'On Time', '', NULL, '2'),
(209, 10, 'WES008', '2020-10-11', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-11 05:15:35 PM', '2020-10-12 02:03:23 AM', NULL, NULL, '8.47', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(210, 4, 'WES002', '2020-10-12', 'RAHMAN, MASHFIQUR ', '2020-10-12 10:27:00 AM', '2020-10-12 05:26:00 PM', NULL, NULL, '6.59', 'Late Arrival', 'Early Departure', '', 'wercfvg', '3'),
(211, 9, 'WES007', '2020-10-12', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-12 10:27:47 AM', '2020-10-12 06:32:23 PM', NULL, NULL, '8.4', 'Late Arrival', 'On Time', '', NULL, '5'),
(212, 7, 'WES005', '2020-10-12', 'SHAHA, AVIJIT ', '2020-10-12 12:35:15 PM', '2020-10-12 09:33:13 PM', NULL, NULL, '8.57', 'In Time', 'On Time', '', NULL, '2'),
(214, 3, 'WES001', '2020-10-12', 'RUMII, MASUD ', '2020-10-12 01:50:05 PM', '2020-10-12 02:38:02 PM', NULL, NULL, '0.47', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(215, 10, 'WES008', '2020-10-12', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-12 06:46:43 PM', '2020-10-12 11:45:46 PM', NULL, NULL, '4.59', 'Late Arrival', 'On Time', '', NULL, '6'),
(216, 6, 'WES004', '2020-10-12', 'KHANDAKAR, SIYAM ', '2020-10-12 07:06:01 PM', '2020-10-12 07:06:14 PM', NULL, NULL, '0.0', 'Late Arrival', 'On Time', '', NULL, '3'),
(217, 4, 'WES002', '2020-10-13', 'RAHMAN, MASHFIQUR ', '2020-10-13 09:47:37 AM', '2020-10-14 10:09:14 AM', NULL, NULL, '16.0', 'In Time', 'Early Departure', '', NULL, '3'),
(218, 3, 'WES001', '2020-10-13', 'RUMII, MASUD ', '2020-10-13 10:50:58 AM', '2020-10-13 08:17:16 PM', NULL, NULL, '9.26', 'In Time', 'Early Departure', '', NULL, '1'),
(219, 6, 'WES004', '2020-10-13', 'KHANDAKAR, SIYAM ', '2020-10-13 10:59:09 AM', '2020-10-13 07:05:35 PM', NULL, NULL, '8.6', 'In Time', 'On Time', '', NULL, '3'),
(220, 9, 'WES007', '2020-10-13', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-13 11:01:02 AM', '2020-10-13 05:10:08 PM', NULL, NULL, '6.9', 'Late Arrival', 'Early Departure', '', NULL, '5'),
(221, 7, 'WES005', '2020-10-13', 'SHAHA, AVIJIT ', '2020-10-13 12:41:39 PM', '2020-10-14 12:32:33 AM', NULL, NULL, '11.50', 'In Time', 'Early Departure', '', NULL, '2'),
(222, 10, 'WES008', '2020-10-13', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-13 03:01:08 PM', '2020-10-14 02:21:03 AM', NULL, NULL, '11.19', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(223, 4, 'WES002', '2020-10-14', 'RAHMAN, MASHFIQUR ', '2020-10-14 10:09:26 AM', '2020-10-14 06:00:45 PM', NULL, NULL, '7.51', 'In Time', 'Early Departure', '', NULL, '3'),
(224, 6, 'WES004', '2020-10-14', 'KHANDAKAR, SIYAM ', '2020-10-14 10:55:12 AM', '2020-10-14 07:29:33 PM', NULL, NULL, '8.34', 'In Time', 'On Time', '', NULL, '3'),
(225, 9, 'WES007', '2020-10-14', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-14 11:27:05 AM', '2020-10-14 07:38:42 PM', NULL, NULL, '8.11', 'Late Arrival', 'On Time', '', NULL, '5'),
(226, 3, 'WES001', '2020-10-14', 'RUMII, MASUD ', '2020-10-14 11:27:43 AM', '2020-10-14 08:24:53 PM', NULL, NULL, '8.57', 'In Time', 'Early Departure', '', NULL, '1'),
(228, 7, 'WES005', '2020-10-14', 'SHAHA, AVIJIT ', '2020-10-14 12:43:49 PM', '2020-10-15 01:59:12 AM', NULL, NULL, '13.15', 'In Time', 'On Time', '', NULL, '2'),
(229, 10, 'WES008', '2020-10-14', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-14 02:02:35 PM', '2020-10-15 02:01:30 AM', NULL, NULL, '11.58', 'In Time', 'Early Departure', '', NULL, '6'),
(230, 6, 'WES004', '2020-10-15', 'KHANDAKAR, SIYAM ', '2020-10-15 11:32:44 AM', '2020-10-16 12:46:55 AM', NULL, NULL, '13.14', 'Late Arrival', 'Not Scheduled', '', NULL, '3'),
(231, 9, 'WES007', '2020-10-15', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-15 11:34:27 AM', '2020-10-15 06:45:23 PM', NULL, NULL, '7.10', 'Late Arrival', 'On Time', '', NULL, '5'),
(232, 4, 'WES002', '2020-10-15', 'RAHMAN, MASHFIQUR ', '2020-10-15 11:35:19 AM', '2020-10-15 11:42:35 PM', NULL, NULL, '12.7', 'Late Arrival', 'On Time', '', NULL, '3'),
(234, 7, 'WES005', '2020-10-15', 'SHAHA, AVIJIT ', '2020-10-15 01:08:12 PM', '2020-10-16 01:09:46 PM', NULL, NULL, '16.0', 'In Time', 'On Time', '', NULL, '2'),
(235, 3, 'WES001', '2020-10-15', 'MASUD RUMII', '2020-10-15 01:47:00 PM', '2020-10-18 02:06:18 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', '', '1'),
(236, 10, 'WES008', '2020-10-15', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-15 02:28:34 PM', '2020-10-16 04:02:32 AM', NULL, NULL, '13.33', 'In Time', 'Early Departure', '', NULL, '6'),
(237, 6, 'WES004', '2020-10-16', 'KHANDAKAR, SIYAM ', '2020-10-16 11:31:57 AM', '2020-10-16 06:04:02 PM', NULL, NULL, '6.32', 'Not Scheduled', 'Not Scheduled', '', NULL, '3'),
(238, 7, 'WES005', '2020-10-16', 'SHAHA, AVIJIT ', '2020-10-16 01:09:52 PM', '2020-10-16 07:05:50 PM', NULL, NULL, '5.55', 'In Time', 'On Time', '', NULL, '2'),
(240, 10, 'WES008', '2020-10-16', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-16 02:09:22 PM', '2020-10-17 02:19:33 AM', NULL, NULL, '12.10', 'In Time', 'Not Scheduled', '', NULL, '6'),
(241, 4, 'WES002', '2020-10-17', 'RAHMAN, MASHFIQUR ', '2020-10-17 09:47:43 AM', '2020-10-17 05:48:17 PM', NULL, NULL, '8.0', 'In Time', 'Early Departure', '', NULL, '3'),
(242, 9, 'WES007', '2020-10-18', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-18 09:58:17 AM', '2020-10-18 08:42:32 PM', NULL, NULL, '10.44', 'In Time', 'On Time', '', NULL, '5'),
(243, 4, 'WES002', '2020-10-18', 'RAHMAN, MASHFIQUR ', '2020-10-18 09:58:25 AM', '2020-10-18 06:08:21 PM', NULL, NULL, '8.9', 'In Time', 'Early Departure', '', NULL, '3'),
(244, 6, 'WES004', '2020-10-18', 'KHANDAKAR, SIYAM ', '2020-10-18 11:21:08 AM', '2020-10-18 07:15:07 PM', NULL, NULL, '7.53', 'Late Arrival', 'On Time', '', NULL, '3'),
(245, 3, 'WES001', '2020-10-18', 'RUMII, MASUD ', '2020-10-18 02:06:30 PM', '2020-10-19 01:50:45 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(247, 10, 'WES008', '2020-10-18', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-18 02:10:38 PM', '2020-10-19 02:48:12 AM', NULL, NULL, '12.37', 'In Time', 'Early Departure', '', NULL, '6'),
(248, 9, 'WES007', '2020-10-19', 'KHAN, BAJLUL HUDA NURULLAH', '2020-10-19 10:50:59 AM', '2020-10-29 11:20:15 AM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '5'),
(249, 4, 'WES002', '2020-10-19', 'RAHMAN, MASHFIQUR ', '2020-10-19 10:52:56 AM', '2020-10-19 02:03:56 PM', NULL, NULL, '3.11', 'In Time', 'Early Departure', '', NULL, '3'),
(250, 6, 'WES004', '2020-10-19', 'KHANDAKAR, SIYAM ', '2020-10-19 11:07:12 AM', '2020-10-19 07:42:36 PM', NULL, NULL, '8.35', 'Late Arrival', 'On Time', '', NULL, '3'),
(252, 3, 'WES001', '2020-10-19', 'RUMII, MASUD ', '2020-10-19 01:51:59 PM', '2020-10-19 08:44:53 PM', NULL, NULL, '6.52', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(253, 10, 'WES008', '2020-10-19', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-19 03:35:45 PM', '2020-10-20 12:19:52 AM', NULL, NULL, '8.44', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(254, 6, 'WES004', '2020-10-20', 'KHANDAKAR, SIYAM ', '2020-10-20 10:58:17 AM', '2020-10-20 05:42:58 PM', NULL, NULL, '6.44', 'In Time', 'Early Departure', '', NULL, '3'),
(255, 4, 'WES002', '2020-10-20', 'RAHMAN, MASHFIQUR ', '2020-10-20 11:01:56 AM', '2020-10-25 01:31:31 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '3'),
(257, 3, 'WES001', '2020-10-20', 'RUMII, MASUD ', '2020-10-20 02:27:58 PM', '2020-10-21 05:00:13 PM', NULL, NULL, '26.32', 'Late Arrival', 'Early Departure', '', NULL, '1'),
(258, 10, 'WES008', '2020-10-20', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-20 02:32:03 PM', '2020-10-21 01:38:24 AM', NULL, NULL, '11.6', 'In Time', 'Early Departure', '', NULL, '6'),
(259, 6, 'WES004', '2020-10-21', 'KHANDAKAR, SIYAM ', '2020-10-21 11:05:53 AM', '2020-10-21 07:02:30 PM', NULL, NULL, '7.56', 'Late Arrival', 'On Time', '', NULL, '3'),
(262, 3, 'WES001', '2020-10-22', 'MASUD RUMII', '2020-10-22 12:00:37 PM', '2020-10-25 01:30:23 PM', NULL, NULL, '16.0', 'In Time', 'Early Departure', '', '', '1'),
(264, 13, 'WES011', '2020-10-22', 'MD RAHMAN', '2020-10-21 06:18:07 PM', '2020-10-22 08:41:53 PM', NULL, NULL, '26.23', 'Late Arrival', 'On Time', '', '', '9'),
(265, 6, 'WES004', '2020-10-23', 'KHANDAKAR, SIYAM ', '2020-10-23 02:52:10 PM', '2020-10-23 09:45:34 PM', NULL, NULL, '6.53', 'Not Scheduled', 'Not Scheduled', '', NULL, '3'),
(266, 8, 'WES006', '2020-10-23', 'SEYED SAKIB', '2020-10-23 06:20:00 PM', '2020-10-25 06:44:00 PM', NULL, NULL, '48.24', 'Late Arrival', 'Early Departure', '', 'test', '4'),
(270, 4, 'WES002', '2020-10-25', 'RAHMAN, MASHFIQUR ', '2020-10-25 04:20:36 PM', '2020-10-28 12:53:11 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '3'),
(271, 10, 'WES008', '2020-10-25', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-25 06:24:25 PM', '2020-10-26 12:09:16 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(272, 6, 'WES004', '2020-10-25', 'KHANDAKAR, SIYAM ', '2020-10-25 06:56:33 PM', '2020-10-25 06:56:46 PM', NULL, NULL, '0.0', 'Late Arrival', 'Early Departure', '', NULL, '3'),
(273, 6, 'WES004', '2020-10-26', 'KHANDAKAR, SIYAM ', '2020-10-26 11:39:00 AM', '2020-10-05 06:41:00 PM', NULL, NULL, '496.58', 'Late Arrival', 'On Time', '', 'erg457', '3'),
(274, 10, 'WES008', '2020-10-26', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-26 12:09:45 PM', '2020-10-27 02:40:31 AM', NULL, NULL, '14.30', 'In Time', 'Early Departure', '', NULL, '6'),
(275, 3, 'WES001', '2020-10-26', 'RUMII, MASUD ', '2020-10-26 01:44:00 PM', '2020-10-27 07:06:00 PM', NULL, NULL, '29.22', 'Late Arrival', 'Early Departure', '', 'test', '1'),
(276, 6, 'WES004', '2020-10-27', 'KHANDAKAR, SIYAM ', '2020-10-27 11:40:00 AM', '2020-10-27 07:05:00 PM', NULL, NULL, '7.25', 'Late Arrival', 'On Time', '', 'test', '3'),
(277, 10, 'WES008', '2020-10-27', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-27 03:20:00 PM', '2020-10-28 05:51:00 AM', NULL, NULL, '14.31', 'Late Arrival', 'Early Departure', '', 'cghngh', '6'),
(278, 8, 'WES006', '2020-10-27', 'SEYED SAKIB', '2020-10-27 04:00:55 PM', '2020-10-28 02:14:47 PM', NULL, NULL, '22.13', 'Late Arrival', 'Early Departure', '', '', '4'),
(279, 6, 'WES004', '2020-10-28', 'KHANDAKAR, SIYAM ', '2020-10-28 11:17:20 AM', '2020-10-28 07:04:31 PM', NULL, NULL, '7.47', 'Late Arrival', 'On Time', '', NULL, '3'),
(284, 10, 'WES008', '2020-10-28', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-28 03:57:12 PM', '2020-10-29 02:49:55 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', NULL, '6'),
(285, 8, 'WES006', '2020-10-28', 'SEYED SAKIB', '2020-10-28 10:15:53 PM', '2020-10-29 02:23:20 PM', NULL, NULL, '16.0', 'Late Arrival', 'Early Departure', '', '', '4'),
(288, 8, 'WES006', '2020-10-29', 'SAKIB, SEYED NAZMUS', '2020-10-29 02:23:36 PM', '2020-10-29 09:55:56 PM', NULL, NULL, '7.32', 'Late Arrival', 'On Time', '', NULL, '4'),
(289, 10, 'WES008', '2020-10-29', 'SHIHAB, SHAHARIAR RAHMAN', '2020-10-29 02:50:11 PM', '2020-10-30 12:09:08 AM', NULL, NULL, '9.18', 'In Time', 'Early Departure', '', NULL, '6'),
(290, 8, 'WES006', '2020-10-30', 'SEYED SAKIB', '2020-10-30 01:57:00 PM', '2020-10-31 12:40:00 PM', NULL, NULL, '22.43', 'Ok', 'Ok', '', 'test', '4'),
(292, 7, 'WES005', '2020-11-01', 'AVIJIT SHAHA', '2020-11-01 12:21:00 PM', '2020-11-01 11:00:00 PM', NULL, NULL, '10.39', 'Late Arrival', 'On Time', '', 'technical problem', '1'),
(293, 4, 'WES002', '2020-11-01', 'MASHFIQUR RAHMAN', '2020-11-01 10:00:00 AM', '2020-11-01 06:28:00 PM', NULL, NULL, '8.28', 'In Time', 'On Time', '', 'Technical problem occurred  as desktop app was not working.', '3'),
(294, 12, 'WES010', '2020-11-01', 'ZINIA SHOSHI', '2020-11-01 09:30:00 AM', '2020-11-01 06:27:00 PM', NULL, NULL, '8.57', 'In Time', 'On Time', '', 'Technical problem occurred  as desktop app was not working.', '8'),
(295, 8, 'WES006', '2020-11-01', 'SEYED SAKIB', '2020-11-01 02:12:26 PM', '2020-11-01 09:55:12 PM', NULL, NULL, '7.42', 'Late Arrival', 'On Time', '', '', '4'),
(296, 3, 'WES0012', '2020-11-01', 'MASUD RUMII', '2020-11-01 02:19:58 PM', '2020-11-01 10:23:09 PM', NULL, NULL, '8.3', 'Late Arrival', 'On Time', '', '', '1'),
(297, 10, 'WES008', '2020-11-01', 'SHAHARIAR SHIHAB', '2020-11-01 02:26:35 PM', '2020-11-02 03:07:05 AM', NULL, NULL, '12.40', 'In Time', 'Early Departure', '', '', '6'),
(298, 9, 'WES003', '2020-11-01', 'KHAN, BAJLUL HUDA NURULLAH', '2020-11-01 10:20:00 AM', '2020-11-01 06:29:00 PM', NULL, NULL, '8.9', 'Late Arrival', 'On Time', '', 'Technical issue becouse of desktop software was not working.', '5'),
(299, 6, 'WES008', '2020-11-01', 'SIYAM KHANDAKAR', '2020-11-01 10:00:00 AM', '2020-11-01 07:07:00 PM', NULL, NULL, '9.7', 'In Time', 'On Time', '', 'technical problem', '3'),
(300, 4, 'WES004', '2020-11-02', 'MASHFIQUR RAHMAN', '2020-11-02 10:00:00 AM', '2020-11-02 06:07:43 PM', NULL, NULL, '8.7', 'In Time', 'Early Departure', '', 'technical issue', '3'),
(301, 9, 'WES003', '2020-11-02', 'BAJLUL HUDA KHAN', '2020-11-02 09:00:00 AM', '2020-11-02 05:07:00 PM', NULL, NULL, '8.7', 'In Time', 'On Time', '', 'Technical problem', '15'),
(302, 6, 'WES008', '2020-11-02', 'SIYAM KHANDAKAR', '2020-11-02 11:46:19 AM', '2020-11-02 07:14:23 PM', NULL, NULL, '7.28', 'Late Arrival', 'On Time', '', '', '3'),
(303, 3, 'WES007', '2020-11-02', 'MASUD RUMII', '2020-11-02 01:30:00 PM', '2020-11-02 11:05:00 PM', NULL, NULL, '9.35', 'Late Arrival', 'On Time', '', 'technical issue', '1'),
(304, 16, 'WE009', '2020-11-02', 'MOHAMMAD MIRAZ', '2020-11-02 01:30:00 PM', '2020-11-02 11:05:00 PM', NULL, NULL, '9.35', 'Late Arrival', 'On Time', '', 'technical issue', '1'),
(305, 8, 'WES006', '2020-11-02', 'SEYED SAKIB', '2020-11-02 01:38:04 PM', '2020-11-02 09:55:46 PM', NULL, NULL, '8.17', 'Late Arrival', 'On Time', '', '', '4'),
(306, 10, 'WES002', '2020-11-02', 'SHAHARIAR SHIHAB', '2020-11-02 02:35:38 PM', '2020-11-03 12:21:14 AM', NULL, NULL, '9.45', 'In Time', 'Early Departure', '', '', '16'),
(307, 13, 'WES011', '2020-11-02', 'MD RAHMAN', '2020-11-02 03:02:56 PM', '2020-11-02 09:03:13 PM', NULL, NULL, '6.0', 'In Time', 'Early Departure', '', '', '17'),
(308, 17, 'WES001', '2020-11-02', 'ANISUR RAHMAN', '2020-11-02 03:15:00 PM', '2020-11-02 09:19:00 PM', NULL, NULL, '6.4', 'Late Arrival', 'On Time', '', 'foe face registration delay', '17'),
(309, 9, 'WES003', '2020-11-03', 'BAJLUL HUDA KHAN', '2020-11-03 08:47:28 AM', '2020-11-03 05:03:14 PM', NULL, NULL, '8.15', 'In Time', 'Early Departure', '', '', '15'),
(310, 12, 'WES010', '2020-11-03', 'ZINIA SHOSHI', '2020-11-03 09:28:38 AM', '2020-11-03 05:31:37 PM', NULL, NULL, '8.2', 'In Time', 'Early Departure', '', '', '13'),
(311, 4, 'WES004', '2020-11-03', 'MASHFIQUR RAHMAN', '2020-11-03 09:36:53 AM', '2020-11-03 06:19:01 PM', NULL, NULL, '8.42', 'In Time', 'Early Departure', '', '', '3'),
(312, 6, 'WES008', '2020-11-03', 'SIYAM KHANDAKAR', '2020-11-03 10:41:35 AM', '2020-11-03 07:11:10 PM', NULL, NULL, '8.29', 'In Time', 'On Time', '', '', '3'),
(313, 3, 'WES007', '2020-11-03', 'MASUD RUMII', '2020-11-03 01:27:36 PM', '2020-11-03 10:53:20 PM', NULL, NULL, '9.25', 'Late Arrival', 'On Time', '', '', '1'),
(314, 16, 'WE009', '2020-11-03', 'MOHAMMAD MIRAZ', '2020-11-03 01:28:00 PM', '2020-11-03 10:53:00 PM', NULL, NULL, '9.25', 'Late Arrival', 'On Time', '', 'technical', '1'),
(315, 7, 'WES005', '2020-11-03', 'AVIJIT SHAHA', '2020-11-03 01:38:00 PM', '2020-11-03 10:53:00 PM', NULL, NULL, '9.15', 'Late Arrival', 'On Time', '', 'technical', '1'),
(316, 10, 'WES002', '2020-11-03', 'SHAHARIAR SHIHAB', '2020-11-03 02:19:18 PM', '2020-11-04 01:05:39 AM', NULL, NULL, '10.46', 'In Time', 'Early Departure', '', '', '16'),
(317, 17, 'WES001', '2020-11-03', 'ANISUR RAHMAN', '2020-11-03 02:57:05 PM', '2020-11-03 09:03:27 PM', NULL, NULL, '6.6', 'In Time', 'Early Departure', '', '', '17'),
(318, 13, 'WES011', '2020-11-03', 'MD RAHMAN', '2020-11-03 02:58:12 PM', '2020-11-03 08:59:40 PM', NULL, NULL, '6.1', 'In Time', 'Early Departure', '', '', '17'),
(321, 7, 'WES005', '2020-11-02', 'AVIJIT SHAHA', '2020-11-02 01:38:00 PM', '2020-11-04 12:48:00 AM', NULL, NULL, '35.10', 'Late Arrival', 'Early Departure', '', 'technical', '1'),
(322, 9, 'WES003', '2020-11-04', 'BAJLUL HUDA KHAN', '2020-11-04 08:48:39 AM', '2020-11-04 05:07:15 PM', NULL, NULL, '8.18', 'In Time', 'Early Departure', '', '', '15'),
(323, 12, 'WES010', '2020-11-04', 'ZINIA SHOSHI', '2020-11-04 09:26:39 AM', '2020-11-04 05:27:11 PM', NULL, NULL, '8.0', 'In Time', 'Early Departure', '', '', '13'),
(324, 4, 'WES004', '2020-11-04', 'MASHFIQUR RAHMAN', '2020-11-04 09:56:00 AM', '2020-11-04 06:10:00 pM', NULL, NULL, '8.14', 'In Time', 'On Time', '', 'test', '3'),
(325, 6, 'WES008', '2020-11-04', 'SIYAM KHANDAKAR', '2020-11-04 11:27:00 AM', '2020-11-04 07:05:00 PM', NULL, NULL, '7.38', 'Late Arrival', 'On Time', '', 'technical problem', '3'),
(326, 4, 'WES004', '2020-11-01', 'MASHFIQUR RAHMAN', '2020-11-01 09:56:58 AM', '2020-11-02 01:56:58 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', NULL, '3'),
(327, 3, 'WES007', '2020-11-04', 'MASUD RUMII', '2020-11-04 01:02:36 PM', '2020-11-04 10:57:46 PM', NULL, NULL, '9.55', 'Late Arrival', 'On Time', '', '', '1'),
(328, 7, 'WES005', '2020-11-04', 'AVIJIT SHAHA', '2020-11-04 01:06:12 PM', '2020-11-05 12:43:43 AM', NULL, NULL, '11.37', 'Late Arrival', 'On Time', '', '', '1'),
(329, 8, 'WES006', '2020-11-04', 'SEYED SAKIB', '2020-11-04 01:56:46 PM', '2020-11-04 09:15:13 PM', NULL, NULL, '7.18', 'Late Arrival', 'Early Departure', '', '', '4'),
(330, 10, 'WES002', '2020-11-04', 'SHAHARIAR SHIHAB', '2020-11-04 02:31:51 PM', '2020-11-05 12:40:25 AM', NULL, NULL, '10.8', 'In Time', 'On Time', '', '', '16'),
(331, 13, 'WES011', '2020-11-04', 'MD RAHMAN', '2020-11-04 02:45:56 PM', '2020-11-04 08:48:38 PM', NULL, NULL, '6.2', 'In Time', 'On Time', '', '', '17'),
(332, 17, 'WES001', '2020-11-04', 'ANISUR RAHMAN', '2020-11-04 03:44:28 PM', '2020-11-04 09:10:40 PM', NULL, NULL, '5.26', 'In Time', 'Early Departure', '', '', '17'),
(333, 9, 'WES003', '2020-11-05', 'BAJLUL HUDA KHAN', '2020-11-05 08:55:08 AM', '2020-11-05 09:10:58 PM', NULL, NULL, '12.15', 'In Time', 'On Time', '', '', '15'),
(334, 12, 'WES010', '2020-11-05', 'ZINIA SHOSHI', '2020-11-05 09:25:07 AM', '2020-11-05 05:28:00 PM', NULL, NULL, '8.2', 'In Time', 'On Time', '', '', '13'),
(335, 4, 'WES004', '2020-11-05', 'MASHFIQUR RAHMAN', '2020-11-05 09:53:05 AM', '2020-11-05 08:23:05 PM', NULL, NULL, '10.30', 'In Time', 'On Time', '', '', '3'),
(336, 6, 'WES008', '2020-11-05', 'SIYAM KHANDAKAR', '2020-11-05 10:49:06 AM', '2020-11-06 02:49:06 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '3'),
(337, 3, 'WES007', '2020-11-05', 'MASUD RUMII', '2020-11-05 11:36:23 AM', '2020-11-06 03:36:23 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(338, 7, 'WES005', '2020-11-05', 'AVIJIT SHAHA', '2020-11-05 12:35:09 PM', '2020-11-06 04:35:09 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(339, 8, 'WES006', '2020-11-05', 'SEYED SAKIB', '2020-11-05 01:43:16 PM', '2020-11-06 05:43:16 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '4'),
(340, 17, 'WES001', '2020-11-05', 'ANISUR RAHMAN', '2020-11-05 02:20:14 PM', '2020-11-05 09:09:21 PM', NULL, NULL, '6.49', 'In Time', 'On Time', '', '', '17'),
(341, 10, 'WES002', '2020-11-05', 'SHAHARIAR SHIHAB', '2020-11-05 02:46:20 PM', '2020-11-06 02:25:49 AM', NULL, NULL, '11.39', 'In Time', 'On Time', '', '', '16'),
(342, 13, 'WES011', '2020-11-05', 'MD RAHMAN', '2020-11-05 02:52:26 PM', '2020-11-05 08:55:44 PM', NULL, NULL, '6.3', 'In Time', 'On Time', '', '', '17'),
(343, 6, 'WES008', '2020-11-06', 'SIYAM KHANDAKAR', '2020-11-06 11:39:16 AM', '2020-11-07 03:39:16 AM', NULL, NULL, '16', 'Not Scheduled', 'Forgot', '', '', '3');
INSERT INTO `attendance` (`id`, `reference`, `idno`, `date`, `employee`, `timein`, `timeout`, `break_in`, `break_out`, `totalhours`, `status_timein`, `status_timeout`, `reason`, `comment`, `schedule_id`) VALUES
(344, 7, 'WES005', '2020-11-06', 'AVIJIT SHAHA', '2020-11-06 01:24:03 PM', '2020-11-07 02:01:07 AM', NULL, NULL, '12.37', 'Late Arrival', 'Not Scheduled', '', '', '1'),
(345, 3, 'WES007', '2020-11-06', 'MASUD RUMII', '2020-11-06 01:59:11 PM', '2020-11-07 05:59:11 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '1'),
(346, 8, 'WES006', '2020-11-06', 'SEYED SAKIB', '2020-11-06 01:59:29 PM', '2020-11-06 10:25:19 PM', NULL, NULL, '8.25', 'Late Arrival', 'On Time', '', '', '4'),
(347, 10, 'WES002', '2020-11-06', 'SHAHARIAR SHIHAB', '2020-11-06 02:54:24 PM', '2020-11-07 01:26:53 AM', NULL, NULL, '10.32', 'In Time', 'Not Scheduled', '', '', '16'),
(348, 13, 'WES011', '2020-11-06', 'MD RAHMAN', '2020-11-06 03:02:54 PM', '2020-11-06 09:02:48 PM', NULL, NULL, '5.59', 'In Time', 'On Time', '', '', '17'),
(349, 17, 'WES001', '2020-11-06', 'ANISUR RAHMAN', '2020-11-06 03:59:27 PM', '2020-11-06 10:03:25 PM', NULL, NULL, '6.3', 'In Time', 'On Time', '', '', '17'),
(350, 12, 'WES010', '2020-11-07', 'ZINIA SHOSHI', '2020-11-07 09:28:58 AM', '2020-11-07 05:29:16 PM', NULL, NULL, '8.0', 'In Time', 'On Time', '', '', '13'),
(351, 4, 'WES004', '2020-11-07', 'MASHFIQUR RAHMAN', '2020-11-07 09:57:33 AM', '2020-11-07 06:50:07 PM', NULL, NULL, '8.52', 'In Time', 'On Time', '', '', '3'),
(352, 13, 'WES011', '2020-11-07', 'MD RAHMAN', '2020-11-07 03:05:13 PM', '2020-11-07 09:00:13 PM', NULL, NULL, '5.55', 'In Time', 'Early Departure', '', '', '17'),
(353, 17, 'WES001', '2020-11-07', 'ANISUR RAHMAN', '2020-11-07 03:28:54 PM', '2020-11-07 09:01:36 PM', NULL, NULL, '5.32', 'In Time', 'Early Departure', '', '', '17'),
(354, 12, 'WES010', '2020-11-08', 'ZINIA SHOSHI', '2020-11-08 09:40:02 AM', '2020-11-08 05:30:57 PM', NULL, NULL, '7.50', 'In Time', 'Early Departure', '', '', '13'),
(355, 9, 'WES003', '2020-11-08', 'BAJLUL HUDA KHAN', '2020-11-08 09:54:19 AM', '2020-11-08 06:24:11 PM', NULL, NULL, '8.29', 'In Time', 'On Time', '', '', '15'),
(356, 6, 'WES008', '2020-11-08', 'SIYAM KHANDAKAR', '2020-11-08 10:19:02 AM', '2020-11-08 06:23:40 PM', NULL, NULL, '8.4', 'In Time', 'On Time', '', '', '3'),
(357, 4, 'WES004', '2020-11-08', 'MASHFIQUR RAHMAN', '2020-11-08 11:23:25 AM', '2020-11-08 06:25:58 PM', NULL, NULL, '7.2', 'Late Arrival', 'Early Departure', '', '', '3'),
(358, 3, 'WES007', '2020-11-08', 'MASUD RUMII', '2020-11-08 11:28:59 AM', '2020-11-08 10:30:23 PM', NULL, NULL, '11.1', 'In Time', 'On Time', '', '', '1'),
(359, 7, 'WES005', '2020-11-08', 'AVIJIT SHAHA', '2020-11-08 12:24:37 PM', '2020-11-08 10:30:00 PM', NULL, NULL, '10.5', 'In Time', 'On Time', '', '', '1'),
(360, 8, 'WES006', '2020-11-08', 'SEYED SAKIB', '2020-11-08 02:13:58 PM', '2020-11-08 09:46:34 PM', NULL, NULL, '7.32', 'Late Arrival', 'Early Departure', '', '', '4'),
(361, 10, 'WES002', '2020-11-08', 'SHAHARIAR SHIHAB', '2020-11-08 04:18:18 PM', '2020-11-09 03:21:10 AM', NULL, NULL, '11.2', 'Late Arrival', 'On Time', '', '', '16'),
(362, 9, 'WES003', '2020-11-09', 'BAJLUL HUDA KHAN', '2020-11-09 08:48:37 AM', '2020-11-09 05:18:24 PM', NULL, NULL, '8.29', 'In Time', 'On Time', '', '', '15'),
(363, 12, 'WES010', '2020-11-09', 'ZINIA SHOSHI', '2020-11-09 09:17:24 AM', '2020-11-09 05:29:43 PM', NULL, NULL, '8.12', 'In Time', 'On Time', '', '', '13'),
(364, 4, 'WES004', '2020-11-09', 'MASHFIQUR RAHMAN', '2020-11-09 09:53:02 AM', '2020-11-09 06:24:41 PM', NULL, NULL, '8.31', 'In Time', 'On Time', '', '', '3'),
(365, 6, 'WES008', '2020-11-09', 'SIYAM KHANDAKAR', '2020-11-09 11:07:54 AM', '2020-11-10 03:07:54 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '3'),
(366, 3, 'WES007', '2020-11-09', 'MASUD RUMII', '2020-11-09 01:03:19 PM', '2020-11-10 05:03:19 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '1'),
(367, 7, 'WES005', '2020-11-09', 'AVIJIT SHAHA', '2020-11-09 01:06:37 PM', '2020-11-09 09:57:10 PM', NULL, NULL, '8.50', 'Late Arrival', 'On Time', '', '', '1'),
(368, 8, 'WES006', '2020-11-09', 'SEYED SAKIB', '2020-11-09 02:14:01 PM', '2020-11-09 10:10:26 PM', NULL, NULL, '7.56', 'Late Arrival', 'Early Departure', '', '', '4'),
(369, 10, 'WES002', '2020-11-09', 'SHAHARIAR SHIHAB', '2020-11-09 03:01:54 PM', '2020-11-10 02:41:24 AM', NULL, NULL, '11.39', 'Late Arrival', 'On Time', '', '', '16'),
(370, 17, 'WES001', '2020-11-09', 'ANISUR RAHMAN', '2020-11-09 03:03:36 PM', '2020-11-09 09:03:40 PM', NULL, NULL, '6.0', 'In Time', 'On Time', '', '', '17'),
(371, 13, 'WES011', '2020-11-09', 'MD RAHMAN', '2020-11-09 03:04:17 PM', '2020-11-09 09:03:27 PM', NULL, NULL, '5.59', 'In Time', 'Early Departure', '', '', '17'),
(372, 9, 'WES003', '2020-11-10', 'BAJLUL HUDA KHAN', '2020-11-10 09:00:09 AM', '2020-11-10 05:35:34 PM', NULL, NULL, '8.35', 'In Time', 'On Time', '', '', '15'),
(373, 12, 'WES010', '2020-11-10', 'ZINIA SHOSHI', '2020-11-10 09:23:23 AM', '2020-11-10 05:29:04 PM', NULL, NULL, '8.5', 'In Time', 'On Time', '', '', '13'),
(374, 4, 'WES004', '2020-11-10', 'MASHFIQUR RAHMAN', '2020-11-10 10:12:42 AM', '2020-11-10 06:18:15 PM', NULL, NULL, '8.5', 'In Time', 'On Time', '', '', '3'),
(375, 6, 'WES008', '2020-11-10', 'SIYAM KHANDAKAR', '2020-11-10 10:36:14 AM', '2020-11-10 07:16:57 PM', NULL, NULL, '8.40', 'In Time', 'On Time', '', '', '3'),
(376, 3, 'WES007', '2020-11-10', 'MASUD RUMII', '2020-11-10 11:07:36 AM', '2020-11-11 03:07:36 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(377, 7, 'WES005', '2020-11-10', 'AVIJIT SHAHA', '2020-11-10 12:37:33 PM', '2020-11-10 09:16:11 PM', NULL, NULL, '8.38', 'In Time', 'On Time', '', '', '1'),
(378, 8, 'WES006', '2020-11-10', 'SEYED SAKIB', '2020-11-10 01:30:15 PM', '2020-11-10 10:05:16 PM', NULL, NULL, '8.35', 'Late Arrival', 'On Time', '', '', '4'),
(379, 10, 'WES002', '2020-11-10', 'SHAHARIAR SHIHAB', '2020-11-10 02:31:00 PM', '2020-11-11 02:40:56 AM', NULL, NULL, '12.9', 'In Time', 'On Time', '', '', '16'),
(380, 17, 'WES001', '2020-11-10', 'ANISUR RAHMAN', '2020-11-10 03:00:10 PM', '2020-11-10 09:14:13 PM', NULL, NULL, '6.14', 'In Time', 'On Time', '', '', '17'),
(381, 13, 'WES011', '2020-11-10', 'MD RAHMAN', '2020-11-10 03:00:57 PM', '2020-11-10 09:13:45 PM', NULL, NULL, '6.12', 'In Time', 'On Time', '', '', '17'),
(382, 16, 'WE009', '2020-11-10', 'MOHAMMAD MIRAZ', '2020-11-10 10:31:49 AM', '2020-11-10 06:35:32 PM', NULL, NULL, '8.4', 'In Time', 'On Time', '', 'technical', '1'),
(383, 16, 'WES009', '2020-11-01', 'MOHAMMAD MIRAZ', '2020-11-01 01:30:00 PM', '2020-11-01 09:31:00 PM', NULL, NULL, '8.1', 'Late Arrival', 'On Time', '', NULL, '1'),
(384, 16, 'WES009', '2020-11-04', 'MOHAMMAD MIRAZ', '2020-11-04 12:30:00 PM', '2020-11-04 10:00:00 PM', NULL, NULL, '9.30', 'Late Arrival', 'On Time', '', NULL, '1'),
(385, 16, 'WES009', '2020-11-09', 'MOHAMMAD MIRAZ', '2020-11-09 12:30:00 PM', '2020-11-09 09:45:00 PM', NULL, NULL, '9.15', 'Late Arrival', 'On Time', '', NULL, '1'),
(388, 12, 'WES010', '2020-11-11', 'ZINIA SHOSHI', '2020-11-11 09:38:30 AM', '2020-11-11 05:32:41 PM', NULL, NULL, '7.54', 'In Time', 'Early Departure', '', '', '13'),
(389, 4, 'WES004', '2020-11-11', 'MASHFIQUR RAHMAN', '2020-11-11 10:22:35 AM', '2020-11-11 06:26:24 PM', NULL, NULL, '8.3', 'In Time', 'On Time', '', '', '3'),
(390, 6, 'WES008', '2020-11-11', 'SIYAM KHANDAKAR', '2020-11-11 10:58:00 AM', '2020-11-12 02:58:00 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '3'),
(391, 9, 'WES003', '2020-11-11', 'BAJLUL HUDA KHAN', '2020-11-11 11:43:40 AM', '2020-11-11 09:08:59 PM', NULL, NULL, '9.25', 'Late Arrival', 'On Time', '', '', '15'),
(392, 3, 'WES007', '2020-11-11', 'MASUD RUMII', '2020-11-11 12:23:45 PM', '2020-11-12 04:23:45 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(393, 16, 'WE009', '2020-11-11', 'MOHAMMAD MIRAZ', '2020-11-11 12:23:58 PM', '2020-11-11 06:29:45 PM', NULL, NULL, '6.5', 'In Time', 'Early Departure', '', '', '1'),
(394, 7, 'WES005', '2020-11-11', 'AVIJIT SHAHA', '2020-11-11 12:45:39 PM', '2020-11-11 10:11:46 PM', NULL, NULL, '9.26', 'In Time', 'On Time', '', '', '1'),
(395, 8, 'WES006', '2020-11-11', 'SEYED SAKIB', '2020-11-11 12:45:57 PM', '2020-11-11 09:02:49 PM', NULL, NULL, '8.16', 'Late Arrival', 'On Time', '', '', '4'),
(396, 10, 'WES002', '2020-11-11', 'SHAHARIAR SHIHAB', '2020-11-11 02:45:14 PM', '2020-11-12 02:21:00 AM', NULL, NULL, '11.35', 'In Time', 'On Time', '', '', '16'),
(397, 17, 'WES001', '2020-11-11', 'ANISUR RAHMAN', '2020-11-11 03:04:35 PM', '2020-11-11 09:01:50 PM', NULL, NULL, '5.57', 'In Time', 'Early Departure', '', '', '17'),
(398, 13, 'WES011', '2020-11-11', 'MD RAHMAN', '2020-11-11 03:05:49 PM', '2020-11-11 09:06:01 PM', NULL, NULL, '6.0', 'In Time', 'On Time', '', '', '17'),
(399, 9, 'WES003', '2020-11-12', 'BAJLUL HUDA KHAN', '2020-11-12 09:29:57 AM', '2020-11-12 06:13:28 PM', NULL, NULL, '8.43', 'In Time', 'On Time', '', '', '15'),
(400, 6, 'WES008', '2020-11-12', 'SIYAM KHANDAKAR', '2020-11-12 10:34:39 AM', '2020-11-12 07:14:50 PM', NULL, NULL, '8.40', 'In Time', 'On Time', '', '', '3'),
(401, 7, 'WES005', '2020-11-12', 'AVIJIT SHAHA', '2020-11-12 12:48:04 PM', '2020-11-12 09:05:07 PM', NULL, NULL, '8.17', 'In Time', 'On Time', '', '', '1'),
(402, 8, 'WES006', '2020-11-12', 'SEYED SAKIB', '2020-11-12 01:42:37 PM', '2020-11-12 09:04:21 PM', NULL, NULL, '7.21', 'Late Arrival', 'Early Departure', '', '', '4'),
(403, 17, 'WES001', '2020-11-12', 'ANISUR RAHMAN', '2020-11-12 03:07:26 PM', '2020-11-12 09:04:02 PM', NULL, NULL, '5.56', 'In Time', 'Early Departure', '', '', '17'),
(404, 13, 'WES011', '2020-11-12', 'MD RAHMAN', '2020-11-12 03:09:13 PM', '2020-11-12 09:10:57 PM', NULL, NULL, '6.1', 'In Time', 'On Time', '', '', '17'),
(405, 10, 'WES002', '2020-11-12', 'SHAHARIAR SHIHAB', '2020-11-12 03:16:00 PM', '2020-11-13 01:40:15 AM', NULL, NULL, '10.24', 'Late Arrival', 'On Time', '', '', '16'),
(406, 6, 'WES008', '2020-11-13', 'SIYAM KHANDAKAR', '2020-11-13 11:09:47 AM', '2020-11-14 03:09:47 AM', NULL, NULL, '16', 'Not Scheduled', 'Forgot', '', '', '3'),
(407, 3, 'WES007', '2020-11-13', 'MASUD RUMII', '2020-11-13 12:53:44 PM', '2020-11-14 04:53:44 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(408, 16, 'WE009', '2020-11-13', 'MOHAMMAD MIRAZ', '2020-11-13 12:54:08 PM', '2020-11-14 04:54:08 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(409, 7, 'WES005', '2020-11-13', 'AVIJIT SHAHA', '2020-11-13 12:57:54 PM', '2020-11-13 10:48:43 PM', NULL, NULL, '9.50', 'In Time', 'On Time', '', '', '1'),
(410, 8, 'WES006', '2020-11-13', 'SEYED SAKIB', '2020-11-13 01:32:02 PM', '2020-11-13 10:09:54 PM', NULL, NULL, '8.37', 'Late Arrival', 'On Time', '', '', '4'),
(411, 10, 'WES002', '2020-11-13', 'SHAHARIAR SHIHAB', '2020-11-13 02:45:01 PM', '2020-11-14 02:26:59 AM', NULL, NULL, '11.41', 'In Time', 'Not Scheduled', '', '', '16'),
(412, 17, 'WES001', '2020-11-13', 'ANISUR RAHMAN', '2020-11-13 03:18:02 PM', '2020-11-13 09:13:56 PM', NULL, NULL, '5.55', 'In Time', 'Early Departure', '', '', '17'),
(413, 13, 'WES011', '2020-11-13', 'MD RAHMAN', '2020-11-13 03:20:08 PM', '2020-11-13 09:13:17 PM', NULL, NULL, '5.53', 'In Time', 'Early Departure', '', '', '17'),
(414, 12, 'WES010', '2020-11-14', 'ZINIA SHOSHI', '2020-11-14 09:27:46 AM', '2020-11-14 05:28:43 PM', NULL, NULL, '8.0', 'In Time', 'On Time', '', '', '13'),
(415, 4, 'WES004', '2020-11-14', 'MASHFIQUR RAHMAN', '2020-11-14 09:53:37 AM', '2020-11-15 01:53:37 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '3'),
(416, 17, 'WES001', '2020-11-14', 'ANISUR RAHMAN', '2020-11-14 03:15:30 PM', '2020-11-14 09:03:31 PM', NULL, NULL, '5.48', 'In Time', 'Early Departure', '', '', '17'),
(417, 13, 'WES011', '2020-11-14', 'MD RAHMAN', '2020-11-14 03:16:21 PM', '2020-11-14 09:03:41 PM', NULL, NULL, '5.47', 'In Time', 'Early Departure', '', '', '17'),
(418, 9, 'WES003', '2020-11-15', 'BAJLUL HUDA KHAN', '2020-11-15 09:13:26 AM', '2020-11-15 05:13:46 PM', NULL, NULL, '8.0', 'In Time', 'On Time', '', '', '15'),
(419, 12, 'WES010', '2020-11-15', 'ZINIA SHOSHI', '2020-11-15 09:34:34 AM', '2020-11-15 05:29:55 PM', NULL, NULL, '7.55', 'In Time', 'Early Departure', '', '', '13'),
(420, 4, 'WES004', '2020-11-15', 'MASHFIQUR RAHMAN', '2020-11-15 10:00:46 AM', '2020-11-15 06:14:24 PM', NULL, NULL, '8.13', 'In Time', 'On Time', '', '', '3'),
(421, 6, 'WES008', '2020-11-15', 'SIYAM KHANDAKAR', '2020-11-15 10:50:35 AM', '2020-11-15 07:34:53 PM', NULL, NULL, '8.44', 'In Time', 'On Time', '', '', '3'),
(422, 7, 'WES005', '2020-11-15', 'AVIJIT SHAHA', '2020-11-15 12:45:26 PM', '2020-11-16 04:45:26 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '1'),
(423, 8, 'WES006', '2020-11-15', 'SEYED SAKIB', '2020-11-15 01:22:30 PM', '2020-11-15 07:58:28 PM', NULL, NULL, '6.35', 'Late Arrival', 'Early Departure', '', '', '4'),
(424, 16, 'WE009', '2020-11-15', 'MOHAMMAD MIRAZ', '2020-11-15 01:37:31 PM', '2020-11-16 05:37:31 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '1'),
(425, 3, 'WES007', '2020-11-15', 'MASUD RUMII', '2020-11-15 01:56:01 PM', '2020-11-16 05:56:01 AM', NULL, NULL, '16', 'Late Arrival', 'Forgot', '', '', '1'),
(426, 10, 'WES002', '2020-11-15', 'SHAHARIAR SHIHAB', '2020-11-15 02:48:13 PM', '2020-11-16 06:48:13 AM', NULL, NULL, '16', 'In Time', 'Forgot', '', '', '16'),
(427, 9, 'WES003', '2020-11-16', 'BAJLUL HUDA KHAN', '2020-11-16 09:10:02 AM', '2020-11-16 05:56:20 PM', NULL, NULL, '8.46', 'In Time', 'On Time', '', '', '15'),
(428, 12, 'WES010', '2020-11-16', 'ZINIA SHOSHI', '2020-11-16 09:33:17 AM', '2020-11-16 05:31:03 PM', NULL, NULL, '7.57', 'In Time', 'Early Departure', '', '', '13'),
(429, 4, 'WES004', '2020-11-16', 'MASHFIQUR RAHMAN', '2020-11-16 10:15:38 AM', '2020-11-16 05:54:39 PM', NULL, NULL, '7.39', 'In Time', 'Early Departure', '', '', '3'),
(430, 6, 'WES008', '2020-11-16', 'SIYAM KHANDAKAR', '2020-11-16 11:04:44 AM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '3'),
(431, 3, 'WES007', '2020-11-16', 'MASUD RUMII', '2020-11-16 11:53:49 AM', '2020-11-16 04:57:57 PM', NULL, NULL, '5.4', 'In Time', 'Early Departure', '', '', '1'),
(432, 16, 'WE009', '2020-11-16', 'MOHAMMAD MIRAZ', '2020-11-16 12:04:57 PM', '2020-11-16 04:58:12 PM', NULL, NULL, '4.53', 'In Time', 'Early Departure', '', '', '1'),
(433, 7, 'WES005', '2020-11-16', 'AVIJIT SHAHA', '2020-11-16 02:24:49 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '1'),
(434, 10, 'WES002', '2020-11-16', 'SHAHARIAR SHIHAB', '2020-11-16 02:52:02 PM', NULL, NULL, NULL, NULL, 'In Time', NULL, '', '', '16'),
(435, 13, 'WES011', '2020-11-16', 'MD RAHMAN', '2020-11-16 03:07:26 PM', '2020-11-16 09:03:50 PM', NULL, NULL, '5.56', 'In Time', 'Early Departure', '', '', '17'),
(436, 17, 'WES001', '2020-11-16', 'ANISUR RAHMAN', '2020-11-16 03:09:31 PM', '2020-11-16 09:02:53 PM', NULL, NULL, '5.53', 'In Time', 'Early Departure', '', '', '17'),
(437, 3, 'WES007', '2020-11-17', 'MASUD RUMII', '2020-11-17 01:47:45 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '1'),
(438, 8, 'WES006', '2020-11-17', 'SEYED SAKIB', '2020-11-17 01:48:40 PM', NULL, NULL, NULL, '', 'In Time', '', '', '', '14'),
(439, 9, 'WES003', '2020-11-17', 'BAJLUL HUDA KHAN', '2020-11-17 01:49:09 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '15'),
(440, 16, 'WE009', '2020-11-17', 'MOHAMMAD MIRAZ', '2020-11-17 01:49:29 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '1'),
(441, 12, 'WES010', '2020-11-17', 'ZINIA SHOSHI', '2020-11-17 01:49:46 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '13'),
(442, 4, 'WES004', '2020-11-17', 'MASHFIQUR RAHMAN', '2020-11-17 02:12:58 PM', NULL, NULL, NULL, '', 'Late Arrival', '', '', '', '3');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int UNSIGNED NOT NULL,
  `company` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company`) VALUES
(1, 'WESTACE'),
(2, 'EVIDENT'),
(3, 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `daily_breaks`
--

CREATE TABLE `daily_breaks` (
  `id` bigint UNSIGNED NOT NULL,
  `attendance_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daily_breaks`
--

INSERT INTO `daily_breaks` (`id`, `attendance_id`, `reference`, `start_at`, `end_at`) VALUES
(1, '7', '8', '2020-09-07 07:26:25', '2020-09-07 07:40:53'),
(2, '9', '3', '2020-09-07 07:26:52', '2020-09-07 07:40:03'),
(3, '6', '6', '2020-09-07 08:59:14', '2020-09-07 09:09:52'),
(4, '7', '8', '2020-09-07 08:59:58', '2020-09-07 09:29:19'),
(5, '9', '3', '2020-09-07 09:40:07', '2020-09-07 09:45:13'),
(6, '5', '5', '2020-09-07 11:33:03', '2020-09-07 11:58:33'),
(7, '9', '3', '2020-09-07 11:33:20', '2020-09-07 11:59:12'),
(8, '5', '5', '2020-09-07 11:58:45', NULL),
(9, '15', '8', '2020-09-08 07:13:15', '2020-09-08 07:24:59'),
(10, '14', '3', '2020-09-08 07:24:20', '2020-09-08 08:18:40'),
(11, '14', '3', '2020-09-08 08:18:59', '2020-09-08 09:34:26'),
(12, '13', '6', '2020-09-08 08:52:24', '2020-09-08 09:12:24'),
(13, '15', '8', '2020-09-08 08:53:49', '2020-09-08 09:08:39'),
(14, '17', '5', '2020-09-08 09:34:37', '2020-09-08 10:06:19'),
(15, '13', '6', '2020-09-08 09:36:29', '2020-09-08 09:49:48'),
(16, '15', '8', '2020-09-08 09:46:27', '2020-09-08 09:57:08'),
(17, '14', '3', '2020-09-08 10:05:39', NULL),
(18, '16', '7', '2020-09-08 10:05:51', '2020-09-08 10:05:51'),
(19, '16', '7', '2020-09-08 10:06:54', '2020-09-08 10:52:06'),
(20, '23', '8', '2020-09-09 08:42:54', '2020-09-09 09:05:21'),
(21, '22', '3', '2020-09-09 08:55:59', '2020-09-09 10:47:27'),
(22, '20', '6', '2020-09-09 09:28:16', '2020-09-09 09:50:11'),
(23, '24', '7', '2020-09-09 10:47:34', '2020-09-09 11:16:00'),
(24, '21', '5', '2020-09-09 10:47:41', '2020-09-09 11:06:23'),
(25, '23', '8', '2020-09-09 10:48:39', '2020-09-09 11:08:15'),
(26, '22', '3', '2020-09-09 11:15:59', NULL),
(27, '26', '6', '2020-09-10 08:39:37', '2020-09-10 08:48:25'),
(28, '28', '8', '2020-09-10 08:40:04', '2020-09-10 08:48:58'),
(29, '29', '5', '2020-09-10 09:48:03', '2020-09-10 10:09:23'),
(30, '30', '3', '2020-09-10 09:48:15', NULL),
(31, '28', '8', '2020-09-10 10:17:55', '2020-09-10 10:18:22'),
(32, '28', '8', '2020-09-10 10:19:49', '2020-09-10 10:34:37'),
(33, '26', '6', '2020-09-10 11:06:42', '2020-09-10 11:08:28'),
(34, '26', '6', '2020-09-10 11:28:57', '2020-09-10 11:38:35'),
(35, '40', '3', '2020-09-12 09:49:44', '2020-09-12 12:02:48'),
(36, '45', '8', '2020-09-13 08:55:53', '2020-09-13 09:30:48'),
(37, '46', '10', '2020-09-13 08:56:01', '2020-09-13 09:31:02'),
(38, '44', '3', '2020-09-13 08:56:54', '2020-09-13 10:05:43'),
(39, '43', '6', '2020-09-13 09:16:50', '2020-09-13 09:27:57'),
(40, '49', '7', '2020-09-16 12:51:48', NULL),
(41, '55', '8', '2020-09-17 08:38:11', '2020-09-17 10:22:44'),
(42, '55', '8', '2020-09-17 10:36:38', NULL),
(43, '60', '10', '2020-09-18 15:22:26', '2020-09-18 16:04:57'),
(44, '61', '7', '2020-09-18 15:23:05', '2020-09-18 16:04:55'),
(45, '71', '8', '2020-09-20 09:11:40', '2020-09-20 09:23:59'),
(46, '77', '8', '2020-09-21 10:24:41', '2020-09-21 10:36:48'),
(47, '83', '6', '2020-09-22 09:59:00', '2020-09-22 10:13:53'),
(48, '83', '6', '2020-09-22 11:19:09', '2020-09-22 11:35:19'),
(49, '86', '8', '2020-09-22 11:41:50', NULL),
(50, '91', '8', '2020-09-23 09:15:47', '2020-09-23 09:56:37'),
(51, '93', '10', '2020-09-23 10:31:25', '2020-09-23 11:26:42'),
(52, '98', '6', '2020-09-24 09:43:03', '2020-09-24 09:54:58'),
(53, '107', '10', '2020-09-25 09:07:40', '2020-09-25 09:14:39'),
(54, '107', '10', '2020-09-25 09:45:49', NULL),
(55, '103', '6', '2020-09-25 09:54:03', '2020-09-25 10:12:19'),
(56, '122', '3', '2020-09-28 07:45:14', '2020-09-28 07:58:53'),
(57, '121', '8', '2020-09-28 07:45:29', NULL),
(58, '122', '3', '2020-09-28 07:58:53', NULL),
(59, '119', '6', '2020-09-28 11:09:06', '2020-09-28 11:28:33'),
(60, '129', '8', '2020-09-29 10:08:29', NULL),
(61, '130', '7', '2020-09-29 14:59:53', '2020-09-29 17:16:43'),
(62, '142', '6', '2020-10-01 08:39:58', '2020-10-01 08:41:23'),
(63, '142', '6', '2020-10-01 09:10:25', '2020-10-01 09:31:26'),
(64, '164', '10', '2020-10-04 14:17:13', NULL),
(65, '169', '7', '2020-10-05 09:23:35', '2020-10-05 10:06:52'),
(66, '167', '6', '2020-10-05 09:24:57', '2020-10-05 09:24:58'),
(67, '167', '6', '2020-10-05 09:24:58', '2020-10-05 09:24:58'),
(68, '167', '6', '2020-10-05 09:24:58', '2020-10-05 09:24:58'),
(69, '167', '6', '2020-10-05 09:24:58', '2020-10-05 09:24:59'),
(70, '167', '6', '2020-10-05 09:24:59', '2020-10-05 09:25:00'),
(71, '167', '6', '2020-10-05 09:25:00', '2020-10-05 09:42:52'),
(72, '171', '3', '2020-10-05 14:38:55', '2020-10-05 16:16:06'),
(73, '195', '7', '2020-10-08 14:22:52', '2020-10-08 15:26:12'),
(74, '226', '3', '2020-10-14 14:24:41', '2020-10-14 14:24:48'),
(75, '226', '3', '2020-10-14 14:24:50', '2020-10-14 14:24:51'),
(76, '235', '3', '2020-10-16 08:15:04', '2020-10-16 08:15:25'),
(77, '251', '5', '2020-10-19 13:34:42', '2020-10-20 06:55:17'),
(78, '252', '3', '2020-10-19 14:05:01', '2020-10-19 14:44:54'),
(79, '257', '3', '2020-10-21 10:35:46', '2020-10-21 10:36:16'),
(80, '257', '3', '2020-10-21 10:39:22', '2020-10-21 10:40:19'),
(81, '257', '3', '2020-10-21 10:41:18', '2020-10-21 10:41:35'),
(82, '257', '3', '2020-10-21 10:58:58', '2020-10-21 10:59:24'),
(83, '257', '3', '2020-10-21 10:59:45', '2020-10-21 10:59:57'),
(84, '262', '3', '2020-10-22 06:03:13', '2020-10-22 06:03:28'),
(85, '262', '3', '2020-10-22 06:13:56', '2020-10-22 06:29:01'),
(86, '262', '3', '2020-10-22 07:08:30', '2020-10-22 10:33:55'),
(87, '262', '3', '2020-10-22 10:34:08', '2020-10-22 10:34:15'),
(88, '262', '3', '2020-10-22 12:10:55', '2020-10-22 12:11:03'),
(89, '261', '6', '2020-10-22 12:13:52', '2020-10-22 12:14:00'),
(90, '261', '6', '2020-10-22 12:14:25', '2020-10-22 12:14:37'),
(91, '261', '6', '2020-10-22 12:16:46', '2020-10-22 12:16:51'),
(92, '261', '6', '2020-10-22 12:16:56', '2020-10-22 12:17:03'),
(93, '261', '6', '2020-10-22 12:17:08', '2020-10-22 12:17:14'),
(94, '261', '6', '2020-10-22 12:17:24', '2020-10-22 12:17:29'),
(95, '261', '6', '2020-10-22 12:17:34', '2020-10-22 12:17:40'),
(96, '261', '6', '2020-10-22 12:17:47', '2020-10-22 12:17:53'),
(97, '261', '6', '2020-10-22 12:18:00', '2020-10-22 12:18:06'),
(98, '261', '6', '2020-10-22 12:18:12', '2020-10-22 12:18:17'),
(99, '261', '6', '2020-10-22 12:18:23', '2020-10-22 12:18:27'),
(100, '264', '13', '2020-10-22 12:18:21', '2020-10-22 12:18:32'),
(101, '261', '6', '2020-10-22 12:18:32', '2020-10-22 12:18:47'),
(102, '261', '6', '2020-10-22 12:19:11', '2020-10-22 12:19:48'),
(103, '261', '6', '2020-10-22 12:20:01', '2020-10-22 12:20:08'),
(104, '261', '6', '2020-10-22 12:21:23', '2020-10-22 12:21:31'),
(105, '261', '6', '2020-10-22 12:21:38', '2020-10-22 12:21:43'),
(106, '261', '6', '2020-10-22 12:21:47', '2020-10-22 12:25:45'),
(107, '261', '6', '2020-10-22 12:25:49', '2020-10-22 12:25:54'),
(108, '261', '6', '2020-10-22 12:25:58', '2020-10-22 12:26:03'),
(109, '261', '6', '2020-10-22 12:26:07', '2020-10-22 12:26:12'),
(110, '261', '6', '2020-10-22 12:26:17', '2020-10-22 12:26:24'),
(111, '261', '6', '2020-10-22 12:26:32', '2020-10-22 12:26:37'),
(112, '261', '6', '2020-10-22 12:26:41', '2020-10-22 12:26:46'),
(113, '261', '6', '2020-10-22 12:26:50', '2020-10-22 12:26:55'),
(114, '261', '6', '2020-10-22 12:27:00', '2020-10-22 12:27:04'),
(115, '261', '6', '2020-10-22 12:27:12', '2020-10-22 12:27:17'),
(116, '261', '6', '2020-10-22 12:27:22', '2020-10-22 12:27:27'),
(117, '261', '6', '2020-10-22 12:27:31', '2020-10-22 12:27:35'),
(118, '261', '6', '2020-10-22 12:27:40', '2020-10-22 12:27:45'),
(119, '261', '6', '2020-10-22 12:27:50', '2020-10-22 12:27:55'),
(120, '261', '6', '2020-10-22 12:28:11', '2020-10-22 12:28:39'),
(121, '261', '6', '2020-10-22 12:28:44', '2020-10-22 12:28:49'),
(122, '261', '6', '2020-10-22 12:28:54', '2020-10-22 12:29:57'),
(123, '261', '6', '2020-10-22 12:30:01', '2020-10-22 12:30:13'),
(124, '261', '6', '2020-10-22 12:33:13', '2020-10-22 12:33:18'),
(125, '261', '6', '2020-10-22 12:33:23', '2020-10-22 12:33:27'),
(126, '266', '8', '2020-10-23 12:21:00', '2020-10-23 12:21:12'),
(127, '264', '13', '2020-10-23 12:17:31', '2020-10-23 12:22:24'),
(128, '264', '13', '2020-10-23 12:22:36', '2020-10-23 12:22:49'),
(129, '264', '13', '2020-10-23 12:23:32', '2020-10-23 12:23:39'),
(130, '264', '13', '2020-10-23 12:23:54', '2020-10-23 12:24:34'),
(131, '264', '13', '2020-10-23 12:24:41', '2020-10-23 12:26:06'),
(132, '264', '13', '2020-10-23 12:37:36', '2020-10-23 12:37:41'),
(133, '265', '6', '2020-10-23 12:42:35', '2020-10-23 12:42:39'),
(134, '265', '6', '2020-10-23 12:42:44', '2020-10-23 12:42:56'),
(135, '265', '6', '2020-10-23 12:43:01', '2020-10-23 12:43:05'),
(136, '265', '6', '2020-10-23 12:43:10', '2020-10-23 12:43:14'),
(137, '265', '6', '2020-10-23 12:43:19', '2020-10-23 12:43:23'),
(138, '265', '6', '2020-10-23 12:43:27', '2020-10-23 12:43:31'),
(139, '265', '6', '2020-10-23 12:43:37', '2020-10-23 12:43:46'),
(140, '265', '6', '2020-10-23 12:43:51', '2020-10-23 12:43:55'),
(141, '265', '6', '2020-10-23 12:43:59', '2020-10-23 12:44:03'),
(142, '265', '6', '2020-10-23 12:44:07', '2020-10-23 12:44:11'),
(143, '265', '6', '2020-10-23 12:44:16', '2020-10-23 12:44:22'),
(144, '265', '6', '2020-10-23 12:44:28', '2020-10-23 12:44:32'),
(145, '265', '6', '2020-10-23 12:44:36', '2020-10-23 12:44:40'),
(146, '265', '6', '2020-10-23 12:44:44', '2020-10-23 12:44:48'),
(147, '265', '6', '2020-10-23 12:44:52', '2020-10-23 12:44:55'),
(148, '265', '6', '2020-10-23 12:44:59', '2020-10-23 12:45:02'),
(149, '265', '6', '2020-10-23 12:45:06', '2020-10-23 12:45:10'),
(150, '265', '6', '2020-10-23 12:45:15', '2020-10-23 12:45:18'),
(151, '265', '6', '2020-10-23 12:45:22', '2020-10-23 12:45:25'),
(152, '265', '6', '2020-10-23 12:45:29', '2020-10-23 12:45:32'),
(153, '265', '6', '2020-10-23 12:45:36', '2020-10-23 12:45:39'),
(154, '264', '13', '2020-10-23 12:45:16', '2020-10-23 14:18:49'),
(155, '264', '13', '2020-10-23 14:18:54', '2020-10-23 14:19:00'),
(156, '264', '13', '2020-10-23 14:19:05', '2020-10-23 14:20:44'),
(157, '264', '13', '2020-10-23 14:20:49', '2020-10-23 14:20:54'),
(158, '264', '13', '2020-10-23 14:20:58', '2020-10-23 14:40:13'),
(159, '264', '13', '2020-10-23 14:40:37', '2020-10-23 14:41:01'),
(160, '266', '8', '2020-10-25 07:44:22', '2020-10-25 07:44:33'),
(161, '285', '8', '2020-10-29 08:17:12', '2020-10-29 08:17:31'),
(162, '285', '8', '2020-10-29 08:21:17', '2020-10-29 08:21:27'),
(163, '285', '8', '2020-10-29 08:21:40', '2020-10-29 08:21:49'),
(164, '288', '8', '2020-10-29 08:37:52', '2020-10-29 08:37:59'),
(165, '281', '4', '2020-11-01 04:23:54', '2020-11-01 04:24:00'),
(166, '263', '12', '2020-11-01 04:26:41', '2020-11-01 04:26:55'),
(167, '287', '9', '2020-11-01 04:32:51', '2020-11-01 04:33:05'),
(168, '291', '6', '2020-11-01 04:59:42', '2020-11-01 05:00:00'),
(169, '291', '6', '2020-11-01 05:00:07', '2020-11-01 05:00:15'),
(170, '291', '6', '2020-11-01 05:00:25', '2020-11-01 05:01:06'),
(171, '291', '6', '2020-11-01 06:21:47', '2020-11-01 06:21:55'),
(172, '282', '3', '2020-11-01 07:18:39', '2020-11-01 07:18:47'),
(173, '291', '6', '2020-11-01 07:27:55', '2020-11-01 07:28:04'),
(174, '291', '6', '2020-11-01 08:07:16', '2020-11-01 08:07:29'),
(175, '291', '6', '2020-11-01 08:11:08', '2020-11-01 08:11:39'),
(176, '291', '6', '2020-11-01 08:11:59', '2020-11-01 08:12:14'),
(177, '299', '6', '2020-11-01 10:55:56', '2020-11-01 10:56:08'),
(178, '297', '10', '2020-11-01 10:57:31', '2020-11-01 10:57:43'),
(179, '296', '3', '2020-11-01 11:15:53', '2020-11-01 11:16:12'),
(180, '296', '3', '2020-11-01 11:19:36', '2020-11-01 11:19:48'),
(181, '296', '3', '2020-11-01 15:10:25', '2020-11-01 15:21:04'),
(182, '296', '3', '2020-11-01 15:21:11', '2020-11-01 16:22:59'),
(183, '304', '16', '2020-11-03 07:21:39', '2020-11-03 07:21:54'),
(184, '321', '7', '2020-11-04 06:29:57', '2020-11-04 06:30:43'),
(185, '321', '7', '2020-11-04 06:30:53', '2020-11-04 06:31:01'),
(186, '324', '4', '2020-11-04 14:18:22', '2020-11-04 15:04:00'),
(187, '324', '4', '2020-11-04 15:04:41', '2020-11-04 15:04:51'),
(188, '324', '4', '2020-11-04 15:04:53', '2020-11-04 15:05:53'),
(190, '346', '8', '2020-11-06 07:59:43', '2020-11-06 07:59:51'),
(191, '358', '3', '2020-11-08 08:00:21', '2020-11-08 08:17:10'),
(192, '357', '4', '2020-11-08 08:00:34', '2020-11-08 08:17:19'),
(193, '358', '3', '2020-11-08 11:34:42', '2020-11-08 11:34:53'),
(194, '364', '4', '2020-11-09 05:46:45', '2020-11-09 05:46:52'),
(195, '364', '4', '2020-11-09 12:24:19', '2020-11-09 12:24:33'),
(196, '389', '4', '2020-11-11 12:25:37', '2020-11-11 12:26:18'),
(197, '426', '10', '2020-11-15 09:09:31', '2020-11-15 13:26:25'),
(198, '434', '10', '2020-11-16 08:52:10', '2020-11-16 08:52:24'),
(199, '431', '3', '2020-11-16 09:28:38', '2020-11-16 09:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int UNSIGNED NOT NULL,
  `department` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department`) VALUES
(1, 'IT'),
(2, 'SALES & MARKETING'),
(3, 'ACCOUNTS'),
(4, 'CUSTOMER SUPPORT'),
(5, 'ADMINISTRATION');

-- --------------------------------------------------------

--
-- Table structure for table `employee_faces`
--

CREATE TABLE `employee_faces` (
  `id` int UNSIGNED NOT NULL,
  `reference` int NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_faces`
--

INSERT INTO `employee_faces` (`id`, `reference`, `image_name`, `created_at`, `updated_at`) VALUES
(1, 4, 'eyFMv1nmao.jpg', '2020-09-01 10:30:49', '2020-09-01 10:30:49'),
(2, 4, 'JyLbG3D8Wd.jpg', '2020-09-01 10:30:53', '2020-09-01 10:30:53'),
(3, 4, 'Qhfpr8L8mq.jpg', '2020-09-01 10:30:55', '2020-09-01 10:30:55'),
(4, 4, 'EV809LUyaZ.jpg', '2020-09-01 10:30:58', '2020-09-01 10:30:58'),
(5, 4, 'JXxyBfYzMu.jpg', '2020-09-01 10:31:01', '2020-09-01 10:31:01'),
(6, 4, '7Rj7Q9H8D7.jpg', '2020-09-01 10:31:03', '2020-09-01 10:31:03'),
(7, 4, '8yKqc7Sitr.jpg', '2020-09-01 10:31:05', '2020-09-01 10:31:05'),
(8, 4, 'YVoBKqeEa5.jpg', '2020-09-01 10:31:08', '2020-09-01 10:31:08'),
(9, 4, 'BkYuR841Wt.jpg', '2020-09-01 10:31:10', '2020-09-01 10:31:10'),
(10, 4, 'F41j9MLRXr.jpg', '2020-09-01 10:31:12', '2020-09-01 10:31:12'),
(11, 4, 'XA2UiLzE03.jpg', '2020-09-01 10:31:20', '2020-09-01 10:31:20'),
(12, 4, 'jJwEPdW9nY.jpg', '2020-09-01 10:31:22', '2020-09-01 10:31:22'),
(13, 4, 'yjdqiut0We.jpg', '2020-09-01 10:31:23', '2020-09-01 10:31:23'),
(30, 6, 'KKjIi9PaOJ.jpg', '2020-09-06 11:36:45', '2020-09-06 11:36:45'),
(31, 6, 'fTEsCtBv7f.jpg', '2020-09-06 11:36:47', '2020-09-06 11:36:47'),
(32, 6, 'WlvFw9BXCO.jpg', '2020-09-06 11:36:48', '2020-09-06 11:36:48'),
(33, 6, 'pY4zl5kMwW.jpg', '2020-09-06 11:36:48', '2020-09-06 11:36:48'),
(34, 6, 'pnjxFuRz5M.jpg', '2020-09-06 11:36:48', '2020-09-06 11:36:48'),
(35, 6, 'oapgPuBMAt.jpg', '2020-09-06 11:36:49', '2020-09-06 11:36:49'),
(36, 6, 'JoUDgYJJsQ.jpg', '2020-09-06 11:36:49', '2020-09-06 11:36:49'),
(37, 6, '55g5zkrtTd.jpg', '2020-09-06 11:36:49', '2020-09-06 11:36:49'),
(38, 6, 'ClAOwh8RIU.jpg', '2020-09-06 11:36:49', '2020-09-06 11:36:49'),
(39, 6, 'A6GfRMyMy9.jpg', '2020-09-06 11:36:49', '2020-09-06 11:36:49'),
(40, 6, '2DOIwvZykz.jpg', '2020-09-06 11:36:50', '2020-09-06 11:36:50'),
(41, 6, 'snK48QH5Mh.jpg', '2020-09-06 11:36:50', '2020-09-06 11:36:50'),
(42, 6, 'x7Iif0XD5r.jpg', '2020-09-06 11:36:50', '2020-09-06 11:36:50'),
(43, 6, 'gzIwdP31SE.jpg', '2020-09-06 11:36:50', '2020-09-06 11:36:50'),
(44, 6, 'WhhoMPrY0w.jpg', '2020-09-06 11:36:50', '2020-09-06 11:36:50'),
(45, 6, '3tKvJkLuZK.jpg', '2020-09-06 11:36:51', '2020-09-06 11:36:51'),
(46, 6, 'Ez084UkckY.jpg', '2020-09-06 11:36:51', '2020-09-06 11:36:51'),
(47, 6, 'UT1KvwXmL3.jpg', '2020-09-06 11:36:51', '2020-09-06 11:36:51'),
(48, 6, 'nh3zgUrTCM.jpg', '2020-09-06 11:36:51', '2020-09-06 11:36:51'),
(49, 6, 'BjvnMep2tC.jpg', '2020-09-06 11:36:51', '2020-09-06 11:36:51'),
(50, 6, 'YqnSuPcbJq.jpg', '2020-09-06 11:36:52', '2020-09-06 11:36:52'),
(51, 6, 'Ka5jqpiIa8.jpg', '2020-09-06 11:36:52', '2020-09-06 11:36:52'),
(52, 6, 'QFMp3TXhWS.jpg', '2020-09-06 11:36:52', '2020-09-06 11:36:52'),
(53, 6, 'T09JdBylon.jpg', '2020-09-06 11:36:52', '2020-09-06 11:36:52'),
(54, 6, 'ssCCAQlSvz.jpg', '2020-09-06 11:36:52', '2020-09-06 11:36:52'),
(55, 6, 'eWEYY55sV4.jpg', '2020-09-06 11:36:53', '2020-09-06 11:36:53'),
(56, 6, 'Rb4adfrVoj.jpg', '2020-09-06 11:36:53', '2020-09-06 11:36:53'),
(57, 6, 'WWQiCu6k3P.jpg', '2020-09-06 11:36:53', '2020-09-06 11:36:53'),
(58, 6, '9wBgKnhkC4.jpg', '2020-09-06 11:36:53', '2020-09-06 11:36:53'),
(59, 6, 'fqDoxdmcUK.jpg', '2020-09-06 11:36:53', '2020-09-06 11:36:53'),
(60, 6, 'ppWbVukxfd.jpg', '2020-09-06 11:36:54', '2020-09-06 11:36:54'),
(61, 6, 'wK49wiHlhA.jpg', '2020-09-06 11:36:54', '2020-09-06 11:36:54'),
(62, 6, 'Vm6cbQyJbW.jpg', '2020-09-06 11:36:54', '2020-09-06 11:36:54'),
(63, 6, '7s6Q42T6rM.jpg', '2020-09-06 11:36:55', '2020-09-06 11:36:55'),
(64, 6, '488SWLPYzR.jpg', '2020-09-06 11:36:55', '2020-09-06 11:36:55'),
(65, 6, 'DIYyAiwQyl.jpg', '2020-09-06 11:36:55', '2020-09-06 11:36:55'),
(66, 6, 'eP5pqlc2bB.jpg', '2020-09-06 11:36:55', '2020-09-06 11:36:55'),
(67, 6, 'FIlo822kTA.jpg', '2020-09-06 11:36:55', '2020-09-06 11:36:55'),
(68, 6, '5JSbO8yyJb.jpg', '2020-09-06 11:36:56', '2020-09-06 11:36:56'),
(69, 6, 'razr3liYcx.jpg', '2020-09-06 11:36:56', '2020-09-06 11:36:56'),
(70, 6, 'S7ZNwa7zvG.jpg', '2020-09-06 11:36:56', '2020-09-06 11:36:56'),
(71, 6, 'MfapGZjI8b.jpg', '2020-09-06 11:36:56', '2020-09-06 11:36:56'),
(72, 6, 'AeasGxlcCj.jpg', '2020-09-06 11:36:56', '2020-09-06 11:36:56'),
(73, 6, 'FhHzKCltul.jpg', '2020-09-06 11:36:57', '2020-09-06 11:36:57'),
(74, 6, '4JZ4TLNH7X.jpg', '2020-09-06 11:36:57', '2020-09-06 11:36:57'),
(75, 6, 'wUpxIt8T2D.jpg', '2020-09-06 11:36:57', '2020-09-06 11:36:57'),
(76, 6, 'UQyYELVohH.jpg', '2020-09-06 11:36:57', '2020-09-06 11:36:57'),
(77, 6, 'ebc1mIGYjZ.jpg', '2020-09-06 11:36:57', '2020-09-06 11:36:57'),
(78, 6, 'g2s3tu8Gun.jpg', '2020-09-06 11:36:58', '2020-09-06 11:36:58'),
(79, 6, 'ZtTuv4Co1I.jpg', '2020-09-06 11:36:58', '2020-09-06 11:36:58'),
(80, 6, 'u0ajLrqJ9m.jpg', '2020-09-06 11:36:58', '2020-09-06 11:36:58'),
(81, 6, 'rklygmdSJl.jpg', '2020-09-06 11:36:58', '2020-09-06 11:36:58'),
(82, 6, 'fnWkhpR4np.jpg', '2020-09-06 11:36:58', '2020-09-06 11:36:58'),
(83, 6, 'zVjHhodzhl.jpg', '2020-09-06 11:36:59', '2020-09-06 11:36:59'),
(84, 6, 'ms00Lkf7JD.jpg', '2020-09-06 11:36:59', '2020-09-06 11:36:59'),
(85, 6, 'kYLAvWn6DK.jpg', '2020-09-06 11:36:59', '2020-09-06 11:36:59'),
(86, 6, '9PQveZp3o0.jpg', '2020-09-06 11:37:00', '2020-09-06 11:37:00'),
(87, 6, 'mUcQUYDeAv.jpg', '2020-09-06 11:37:00', '2020-09-06 11:37:00'),
(88, 6, 'VuXE5mIRlK.jpg', '2020-09-06 11:37:00', '2020-09-06 11:37:00'),
(89, 6, 'UubC94Juye.jpg', '2020-09-06 11:37:00', '2020-09-06 11:37:00'),
(90, 6, 'jWzvkZsvDD.jpg', '2020-09-06 11:37:00', '2020-09-06 11:37:00'),
(91, 6, 'OJdCqtLgpc.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(92, 6, 'RwJKjTrGEd.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(93, 6, 'cYMF0PS5nn.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(94, 6, 'thAKDY10IT.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(95, 6, '9yBuLNLtYY.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(96, 6, 'iiY65UaWHJ.jpg', '2020-09-06 11:37:01', '2020-09-06 11:37:01'),
(97, 6, 'sLkJuDjk8E.jpg', '2020-09-06 11:37:02', '2020-09-06 11:37:02'),
(98, 6, 'HwrgPSH7Ty.jpg', '2020-09-06 11:37:02', '2020-09-06 11:37:02'),
(99, 6, 'HBVJM3nIP1.jpg', '2020-09-06 11:37:02', '2020-09-06 11:37:02'),
(100, 6, 'vec1f71ZWn.jpg', '2020-09-06 11:37:02', '2020-09-06 11:37:02'),
(101, 6, '1pJDz1uSqe.jpg', '2020-09-06 11:37:02', '2020-09-06 11:37:02'),
(102, 6, 'H4UFwfcoT8.jpg', '2020-09-06 11:37:03', '2020-09-06 11:37:03'),
(103, 7, 'S8ZZtqgRWS.jpg', '2020-09-06 11:38:26', '2020-09-06 11:38:26'),
(104, 7, '9pznxw82cY.jpg', '2020-09-06 11:38:28', '2020-09-06 11:38:28'),
(105, 7, 'W2bPuZePdf.jpg', '2020-09-06 11:38:28', '2020-09-06 11:38:28'),
(106, 7, 'SAd45kWApJ.jpg', '2020-09-06 11:38:28', '2020-09-06 11:38:28'),
(107, 7, 'Itf6ZlMMRy.jpg', '2020-09-06 11:38:28', '2020-09-06 11:38:28'),
(108, 7, 'GKg0mnyE4A.jpg', '2020-09-06 11:38:29', '2020-09-06 11:38:29'),
(109, 7, 'gFHkIbMhBu.jpg', '2020-09-06 11:38:29', '2020-09-06 11:38:29'),
(110, 7, 'LM7j2a4Ma5.jpg', '2020-09-06 11:38:29', '2020-09-06 11:38:29'),
(111, 7, 'WmJ2KYWzHx.jpg', '2020-09-06 11:38:29', '2020-09-06 11:38:29'),
(112, 7, 'ayqoeX4whm.jpg', '2020-09-06 11:38:30', '2020-09-06 11:38:30'),
(113, 7, 'ef9uYTw198.jpg', '2020-09-06 11:38:30', '2020-09-06 11:38:30'),
(114, 7, 'WaLEmPjH1r.jpg', '2020-09-06 11:38:30', '2020-09-06 11:38:30'),
(115, 7, 'W3maBZMpQ8.jpg', '2020-09-06 11:38:30', '2020-09-06 11:38:30'),
(116, 7, 'k7VhXJvAo6.jpg', '2020-09-06 11:38:30', '2020-09-06 11:38:30'),
(117, 7, 'M9iA1scuZk.jpg', '2020-09-06 11:38:31', '2020-09-06 11:38:31'),
(118, 7, 'JnpaYjCGbf.jpg', '2020-09-06 11:38:31', '2020-09-06 11:38:31'),
(119, 7, 'MdJd4wwdVV.jpg', '2020-09-06 11:38:31', '2020-09-06 11:38:31'),
(120, 7, '0MEjC9soD6.jpg', '2020-09-06 11:38:31', '2020-09-06 11:38:31'),
(121, 7, 'EyBqC6vmyX.jpg', '2020-09-06 11:38:31', '2020-09-06 11:38:31'),
(122, 7, 'OJpuIUrDJB.jpg', '2020-09-06 11:38:32', '2020-09-06 11:38:32'),
(123, 7, 'VTMrx98Vs5.jpg', '2020-09-06 11:38:32', '2020-09-06 11:38:32'),
(124, 7, 'RU6sSJsZND.jpg', '2020-09-06 11:38:32', '2020-09-06 11:38:32'),
(125, 7, 'A7xgy7MKwV.jpg', '2020-09-06 11:38:32', '2020-09-06 11:38:32'),
(126, 7, 'BgCRESzh1G.jpg', '2020-09-06 11:38:33', '2020-09-06 11:38:33'),
(127, 7, 'v3S8Frob8F.jpg', '2020-09-06 11:38:33', '2020-09-06 11:38:33'),
(128, 7, 'wykKcjmxSV.jpg', '2020-09-06 11:38:33', '2020-09-06 11:38:33'),
(129, 7, 'GrY1Xdroa2.jpg', '2020-09-06 11:38:33', '2020-09-06 11:38:33'),
(130, 7, '1gDGMEX0rq.jpg', '2020-09-06 11:38:34', '2020-09-06 11:38:34'),
(131, 7, 'swYZkKRW5U.jpg', '2020-09-06 11:38:34', '2020-09-06 11:38:34'),
(132, 7, 'Zdhl7A9ESK.jpg', '2020-09-06 11:38:34', '2020-09-06 11:38:34'),
(133, 7, 'Ymzt6vLwy1.jpg', '2020-09-06 11:38:34', '2020-09-06 11:38:34'),
(134, 7, 'EtF8cVv2DV.jpg', '2020-09-06 11:38:34', '2020-09-06 11:38:34'),
(135, 7, 'CunNlhJkDP.jpg', '2020-09-06 11:38:35', '2020-09-06 11:38:35'),
(136, 7, 'W1QZsUx6cm.jpg', '2020-09-06 11:38:35', '2020-09-06 11:38:35'),
(137, 7, 'bvuSMS2aVY.jpg', '2020-09-06 11:38:36', '2020-09-06 11:38:36'),
(138, 7, 'YYA3f8zjIV.jpg', '2020-09-06 11:38:36', '2020-09-06 11:38:36'),
(139, 7, 'XR7eoJOfjM.jpg', '2020-09-06 11:38:36', '2020-09-06 11:38:36'),
(140, 7, 'aMvSoh6S2j.jpg', '2020-09-06 11:38:37', '2020-09-06 11:38:37'),
(141, 7, 'UeN0xseTn2.jpg', '2020-09-06 11:38:37', '2020-09-06 11:38:37'),
(142, 7, 'a4Wz1f32NL.jpg', '2020-09-06 11:38:37', '2020-09-06 11:38:37'),
(143, 7, 'oS6l9X8pfd.jpg', '2020-09-06 11:38:37', '2020-09-06 11:38:37'),
(144, 7, 'XLcmvVnZbY.jpg', '2020-09-06 11:38:37', '2020-09-06 11:38:37'),
(145, 7, 'rRXQTTVNor.jpg', '2020-09-06 11:38:38', '2020-09-06 11:38:38'),
(146, 7, 'YBujiCMYIe.jpg', '2020-09-06 11:38:38', '2020-09-06 11:38:38'),
(147, 7, '5ytkZtUPkl.jpg', '2020-09-06 11:38:38', '2020-09-06 11:38:38'),
(148, 7, '3VUE6oPHog.jpg', '2020-09-06 11:38:38', '2020-09-06 11:38:38'),
(149, 7, '9RU32J3gW7.jpg', '2020-09-06 11:38:38', '2020-09-06 11:38:38'),
(150, 7, '8IbAJHKQVD.jpg', '2020-09-06 11:38:39', '2020-09-06 11:38:39'),
(151, 7, 'TtqRSK07WE.jpg', '2020-09-06 11:38:39', '2020-09-06 11:38:39'),
(152, 7, 'bIwNJIxU1t.jpg', '2020-09-06 11:38:39', '2020-09-06 11:38:39'),
(153, 7, '8uJY1AhsW7.jpg', '2020-09-06 11:38:39', '2020-09-06 11:38:39'),
(154, 7, 'ue2GkXD1or.jpg', '2020-09-06 11:38:39', '2020-09-06 11:38:39'),
(155, 7, 'SQ4dMSiGoZ.jpg', '2020-09-06 11:38:40', '2020-09-06 11:38:40'),
(156, 7, '5y9vcqaRwm.jpg', '2020-09-06 11:38:40', '2020-09-06 11:38:40'),
(157, 7, 'SSPLeUD9Gp.jpg', '2020-09-06 11:38:40', '2020-09-06 11:38:40'),
(158, 7, '1bG2mZj7Tz.jpg', '2020-09-06 11:38:41', '2020-09-06 11:38:41'),
(159, 7, 'qph6gHribV.jpg', '2020-09-06 11:38:41', '2020-09-06 11:38:41'),
(160, 7, 'MfTTTza6se.jpg', '2020-09-06 11:38:42', '2020-09-06 11:38:42'),
(161, 7, 'H9OfA3SJ9K.jpg', '2020-09-06 11:38:42', '2020-09-06 11:38:42'),
(162, 7, '4auWkRQ7Ml.jpg', '2020-09-06 11:38:42', '2020-09-06 11:38:42'),
(163, 7, 'MBAlj8RmYi.jpg', '2020-09-06 11:38:42', '2020-09-06 11:38:42'),
(164, 7, '1KaBm8gOkm.jpg', '2020-09-06 11:38:43', '2020-09-06 11:38:43'),
(165, 7, '2QCnuld71C.jpg', '2020-09-06 11:38:43', '2020-09-06 11:38:43'),
(166, 7, 'gqjxrTjdWv.jpg', '2020-09-06 11:38:43', '2020-09-06 11:38:43'),
(167, 7, 'WKsFPlwUeR.jpg', '2020-09-06 11:38:43', '2020-09-06 11:38:43'),
(168, 7, '5ELn7BbmV2.jpg', '2020-09-06 11:38:43', '2020-09-06 11:38:43'),
(169, 7, 'F5puwPAtHK.jpg', '2020-09-06 11:38:44', '2020-09-06 11:38:44'),
(170, 7, 'k4rI6iQpPP.jpg', '2020-09-06 11:38:44', '2020-09-06 11:38:44'),
(171, 7, 'MgAyorSqpm.jpg', '2020-09-06 11:38:44', '2020-09-06 11:38:44'),
(172, 7, 'HzD8GoiLn7.jpg', '2020-09-06 11:38:44', '2020-09-06 11:38:44'),
(173, 7, '9uHZBT8A3C.jpg', '2020-09-06 11:38:45', '2020-09-06 11:38:45'),
(174, 7, 'wOrmML8CWx.jpg', '2020-09-06 11:38:45', '2020-09-06 11:38:45'),
(175, 7, 'tp70x7tvOg.jpg', '2020-09-06 11:38:45', '2020-09-06 11:38:45'),
(176, 7, 'Xvk1KMJXRZ.jpg', '2020-09-06 11:38:45', '2020-09-06 11:38:45'),
(177, 7, 'XX0ZqsUMKL.jpg', '2020-09-06 11:38:45', '2020-09-06 11:38:45'),
(178, 7, '9F0XjBXnkP.jpg', '2020-09-06 11:38:46', '2020-09-06 11:38:46'),
(179, 7, 'JBVzePJwJq.jpg', '2020-09-06 11:38:46', '2020-09-06 11:38:46'),
(180, 7, 'Uumqhffylw.jpg', '2020-09-06 11:38:46', '2020-09-06 11:38:46'),
(181, 7, 'FCKANrdYq1.jpg', '2020-09-06 11:38:46', '2020-09-06 11:38:46'),
(182, 7, 'gmphMV998N.jpg', '2020-09-06 11:38:46', '2020-09-06 11:38:46'),
(183, 7, '3cXzrTV5tx.jpg', '2020-09-06 11:38:47', '2020-09-06 11:38:47'),
(184, 7, 'BSWa727VKF.jpg', '2020-09-06 11:38:47', '2020-09-06 11:38:47'),
(185, 7, 'UP8mQpZiAO.jpg', '2020-09-06 11:38:47', '2020-09-06 11:38:47'),
(186, 7, 'fIOVKkidNK.jpg', '2020-09-06 11:38:47', '2020-09-06 11:38:47'),
(187, 7, 'PyaU60WG90.jpg', '2020-09-06 11:38:48', '2020-09-06 11:38:48'),
(188, 7, '7VkpNCl6OT.jpg', '2020-09-06 11:38:48', '2020-09-06 11:38:48'),
(189, 7, 'S4p0RPknkg.jpg', '2020-09-06 11:38:48', '2020-09-06 11:38:48'),
(190, 7, 'T1c1P3IV4X.jpg', '2020-09-06 11:38:48', '2020-09-06 11:38:48'),
(191, 7, '8ciihweVCY.jpg', '2020-09-06 11:38:49', '2020-09-06 11:38:49'),
(192, 7, 'BsMediYSR9.jpg', '2020-09-06 11:38:49', '2020-09-06 11:38:49'),
(193, 7, 'rMTwTvdf50.jpg', '2020-09-06 11:38:49', '2020-09-06 11:38:49'),
(194, 7, 'Re4JpQohJh.jpg', '2020-09-06 11:38:49', '2020-09-06 11:38:49'),
(195, 7, 'hudyXzuZjg.jpg', '2020-09-06 11:38:49', '2020-09-06 11:38:49'),
(196, 7, '7x6lKPfI61.jpg', '2020-09-06 11:38:50', '2020-09-06 11:38:50'),
(197, 7, 'tGAbgqJMUt.jpg', '2020-09-06 11:38:50', '2020-09-06 11:38:50'),
(198, 7, 'CZCYiuPDSf.jpg', '2020-09-06 11:38:50', '2020-09-06 11:38:50'),
(199, 7, 'gMelw2XaXs.jpg', '2020-09-06 11:38:50', '2020-09-06 11:38:50'),
(200, 7, '2YHRSqPWS1.jpg', '2020-09-06 11:38:50', '2020-09-06 11:38:50'),
(201, 7, 'SEbqQEdt29.jpg', '2020-09-06 11:38:51', '2020-09-06 11:38:51'),
(202, 7, 'q1yPTmJtWq.jpg', '2020-09-06 11:38:51', '2020-09-06 11:38:51'),
(203, 7, 'ZFWF9D8W0H.jpg', '2020-09-06 11:38:51', '2020-09-06 11:38:51'),
(204, 7, '2Pk8xsprXS.jpg', '2020-09-06 11:38:51', '2020-09-06 11:38:51'),
(205, 7, 'u16Ss33WiN.jpg', '2020-09-06 11:38:52', '2020-09-06 11:38:52'),
(206, 7, 'AZKcdbUrAc.jpg', '2020-09-06 11:38:52', '2020-09-06 11:38:52'),
(207, 7, 'LEpoFjTZOh.jpg', '2020-09-06 11:38:52', '2020-09-06 11:38:52'),
(208, 7, 'ctbjigKoOM.jpg', '2020-09-06 11:38:52', '2020-09-06 11:38:52'),
(209, 7, 'Ov79p4ZB1G.jpg', '2020-09-06 11:38:52', '2020-09-06 11:38:52'),
(210, 7, 'DvAC26NKSC.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(211, 7, '6uC1Y8pRtv.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(212, 7, 'MS9XiS5ddF.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(213, 7, 'TKTVhOV6LE.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(214, 7, 'Vsetg32cfm.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(215, 7, 'zD17i7AetN.jpg', '2020-09-06 11:38:53', '2020-09-06 11:38:53'),
(216, 7, 'Jygn52TS2Y.jpg', '2020-09-06 11:38:54', '2020-09-06 11:38:54'),
(217, 7, 'QRC91bQDn4.jpg', '2020-09-06 11:38:54', '2020-09-06 11:38:54'),
(218, 8, 'dXMKQ0iTOu.jpg', '2020-09-06 16:04:29', '2020-09-06 16:04:29'),
(219, 8, 'tW9FNzGC4h.jpg', '2020-09-06 16:04:30', '2020-09-06 16:04:30'),
(220, 8, 'lyIuhdk4u2.jpg', '2020-09-06 16:04:31', '2020-09-06 16:04:31'),
(221, 8, 'yNxaccM5Ut.jpg', '2020-09-06 16:04:32', '2020-09-06 16:04:32'),
(222, 8, 'NbvWJxSwOZ.jpg', '2020-09-06 16:04:32', '2020-09-06 16:04:32'),
(223, 8, '2wJUMJoKfA.jpg', '2020-09-06 16:04:33', '2020-09-06 16:04:33'),
(224, 8, 'ezAc0zMbfz.jpg', '2020-09-06 16:04:33', '2020-09-06 16:04:33'),
(225, 8, 'vfyPVprdHf.jpg', '2020-09-06 16:04:33', '2020-09-06 16:04:33'),
(226, 8, '96C7Twhgm1.jpg', '2020-09-06 16:04:33', '2020-09-06 16:04:33'),
(227, 8, 'ESRftvbKCp.jpg', '2020-09-06 16:04:33', '2020-09-06 16:04:33'),
(228, 8, 'mJmyokypmu.jpg', '2020-09-06 16:04:34', '2020-09-06 16:04:34'),
(229, 8, 'JBL9wjiKCO.jpg', '2020-09-06 16:04:34', '2020-09-06 16:04:34'),
(230, 8, 'SRLJX8Co4d.jpg', '2020-09-06 16:04:34', '2020-09-06 16:04:34'),
(231, 8, 'kz7tgKZC5T.jpg', '2020-09-06 16:04:34', '2020-09-06 16:04:34'),
(232, 8, '5OS6bLGw2I.jpg', '2020-09-06 16:04:35', '2020-09-06 16:04:35'),
(233, 8, 'ux1KlnUIkk.jpg', '2020-09-06 16:04:35', '2020-09-06 16:04:35'),
(234, 8, '2vzpzuZJYc.jpg', '2020-09-06 16:04:35', '2020-09-06 16:04:35'),
(235, 8, 'Q4GkrQGfs9.jpg', '2020-09-06 16:04:35', '2020-09-06 16:04:35'),
(236, 8, 'NtIG8olui1.jpg', '2020-09-06 16:04:36', '2020-09-06 16:04:36'),
(237, 8, 'ZiCNS1hF53.jpg', '2020-09-06 16:04:36', '2020-09-06 16:04:36'),
(238, 8, 'QvaLWOHQLA.jpg', '2020-09-06 16:04:36', '2020-09-06 16:04:36'),
(239, 8, 'bVdPMjYeEK.jpg', '2020-09-06 16:04:36', '2020-09-06 16:04:36'),
(240, 8, 'vMnkuXtpsi.jpg', '2020-09-06 16:04:36', '2020-09-06 16:04:36'),
(241, 8, 'OVCM9XOqoY.jpg', '2020-09-06 16:04:37', '2020-09-06 16:04:37'),
(242, 8, '3v8OpQMs0c.jpg', '2020-09-06 16:04:37', '2020-09-06 16:04:37'),
(243, 8, 'KfweFqanvj.jpg', '2020-09-06 16:04:37', '2020-09-06 16:04:37'),
(244, 8, 'rCyhLZfL6C.jpg', '2020-09-06 16:04:37', '2020-09-06 16:04:37'),
(245, 8, 'Tz70IWxWty.jpg', '2020-09-06 16:04:38', '2020-09-06 16:04:38'),
(246, 8, 'IikEK0m79v.jpg', '2020-09-06 16:04:38', '2020-09-06 16:04:38'),
(247, 8, '3zCQ8ethVY.jpg', '2020-09-06 16:04:38', '2020-09-06 16:04:38'),
(248, 8, 'dxba4KMZCw.jpg', '2020-09-06 16:04:38', '2020-09-06 16:04:38'),
(249, 8, 'auP27oUTMj.jpg', '2020-09-06 16:04:38', '2020-09-06 16:04:38'),
(250, 8, '0o1zzwZZZw.jpg', '2020-09-06 16:04:39', '2020-09-06 16:04:39'),
(251, 8, 'BVYIL1vrRD.jpg', '2020-09-06 16:04:39', '2020-09-06 16:04:39'),
(252, 8, 'WNyhK8zt5B.jpg', '2020-09-06 16:04:39', '2020-09-06 16:04:39'),
(253, 8, 'LoeJZzvj4J.jpg', '2020-09-06 16:04:39', '2020-09-06 16:04:39'),
(254, 8, 'crXCzX7tRs.jpg', '2020-09-06 16:04:39', '2020-09-06 16:04:39'),
(255, 8, 'V7DG7290Fk.jpg', '2020-09-06 16:04:40', '2020-09-06 16:04:40'),
(256, 8, '7ztPqm1LNP.jpg', '2020-09-06 16:04:40', '2020-09-06 16:04:40'),
(257, 8, 'esvs7etvAY.jpg', '2020-09-06 16:04:40', '2020-09-06 16:04:40'),
(258, 8, 'LU1w14B7w3.jpg', '2020-09-06 16:04:40', '2020-09-06 16:04:40'),
(259, 8, 'zs1PquprrT.jpg', '2020-09-06 16:04:40', '2020-09-06 16:04:40'),
(260, 8, 'YGf5UbL8Wb.jpg', '2020-09-06 16:04:41', '2020-09-06 16:04:41'),
(261, 8, 'qgdUhVb10x.jpg', '2020-09-06 16:04:41', '2020-09-06 16:04:41'),
(262, 8, 'ji9XE88jZQ.jpg', '2020-09-06 16:04:41', '2020-09-06 16:04:41'),
(263, 8, 'SUDbIjDVHf.jpg', '2020-09-06 16:04:41', '2020-09-06 16:04:41'),
(264, 8, 'Tt1KDIAhM9.jpg', '2020-09-06 16:04:41', '2020-09-06 16:04:41'),
(265, 8, 'FwV0aPMxoe.jpg', '2020-09-06 16:04:42', '2020-09-06 16:04:42'),
(266, 8, 'TmoyHjL5Wg.jpg', '2020-09-06 16:04:42', '2020-09-06 16:04:42'),
(267, 8, 'RJ9mdm0nB7.jpg', '2020-09-06 16:04:42', '2020-09-06 16:04:42'),
(268, 8, 'P5JrIuz7e6.jpg', '2020-09-06 16:04:42', '2020-09-06 16:04:42'),
(269, 8, 'K0DzR3Xn5W.jpg', '2020-09-06 16:04:42', '2020-09-06 16:04:42'),
(270, 8, 'KACjTPv6RJ.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(271, 8, '9KIA11ea8l.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(272, 8, '7cBjOZAms8.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(273, 8, 'pXKuOyoc9Q.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(274, 8, 'wnppYMuxsB.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(275, 8, 'jgON6OtbHl.jpg', '2020-09-06 16:04:43', '2020-09-06 16:04:43'),
(276, 8, 'ukTUXUVZ4u.jpg', '2020-09-06 16:04:44', '2020-09-06 16:04:44'),
(277, 8, 'DZfB3fNhJq.jpg', '2020-09-06 16:04:44', '2020-09-06 16:04:44'),
(278, 8, 'Ovb7FvtHN2.jpg', '2020-09-06 16:04:44', '2020-09-06 16:04:44'),
(279, 8, 'IjnpOGb7gQ.jpg', '2020-09-06 16:04:44', '2020-09-06 16:04:44'),
(280, 8, '0Y1tB2Q1fk.jpg', '2020-09-06 16:04:45', '2020-09-06 16:04:45'),
(281, 8, '93pIMHYffA.jpg', '2020-09-06 16:04:45', '2020-09-06 16:04:45'),
(282, 8, 'Kq68qMJnOD.jpg', '2020-09-06 16:04:45', '2020-09-06 16:04:45'),
(283, 8, 'QLZWxLArNz.jpg', '2020-09-06 16:04:45', '2020-09-06 16:04:45'),
(284, 8, 'qiUGbliTmv.jpg', '2020-09-06 16:04:45', '2020-09-06 16:04:45'),
(285, 8, '8yAXO4jP0F.jpg', '2020-09-06 16:04:46', '2020-09-06 16:04:46'),
(286, 8, 'g1nctTY1uZ.jpg', '2020-09-06 16:04:46', '2020-09-06 16:04:46'),
(287, 8, 'gAIErKW8AD.jpg', '2020-09-06 16:04:46', '2020-09-06 16:04:46'),
(288, 8, '6UWF904VaA.jpg', '2020-09-06 16:04:46', '2020-09-06 16:04:46'),
(289, 8, 'vwGVHSj2Ca.jpg', '2020-09-06 16:04:46', '2020-09-06 16:04:46'),
(290, 8, 'CtpkChP6p1.jpg', '2020-09-06 16:04:47', '2020-09-06 16:04:47'),
(291, 8, 'G0wWaSzAvM.jpg', '2020-09-06 16:04:47', '2020-09-06 16:04:47'),
(292, 8, 'Qu7ry3ZEGQ.jpg', '2020-09-06 16:04:47', '2020-09-06 16:04:47'),
(293, 8, '8UGeEu16rT.jpg', '2020-09-06 16:04:47', '2020-09-06 16:04:47'),
(294, 8, 'lw37t0meAc.jpg', '2020-09-06 16:04:47', '2020-09-06 16:04:47'),
(295, 8, 'LhbfqPSCTv.jpg', '2020-09-06 16:04:48', '2020-09-06 16:04:48'),
(296, 9, 'gDwtM4VGO3.jpg', '2020-09-07 06:57:49', '2020-09-07 06:57:49'),
(297, 9, 'qDawm160P3.jpg', '2020-09-07 06:57:52', '2020-09-07 06:57:52'),
(298, 9, 'e4mUdeGIyW.jpg', '2020-09-07 06:57:55', '2020-09-07 06:57:55'),
(299, 9, 'UUfBb67vfE.jpg', '2020-09-07 06:57:57', '2020-09-07 06:57:57'),
(300, 9, 'hOU9kbomTy.jpg', '2020-09-07 06:57:58', '2020-09-07 06:57:58'),
(301, 9, 'dx9mCZUEPv.jpg', '2020-09-07 06:58:12', '2020-09-07 06:58:12'),
(302, 9, '1S8RYBwbUB.jpg', '2020-09-07 06:58:13', '2020-09-07 06:58:13'),
(303, 9, 'lNFXdUYXkI.jpg', '2020-09-07 06:58:14', '2020-09-07 06:58:14'),
(304, 9, 'TQvuh9LY4a.jpg', '2020-09-07 06:58:15', '2020-09-07 06:58:15'),
(305, 9, 'mNSjBfSE2x.jpg', '2020-09-07 06:58:16', '2020-09-07 06:58:16'),
(306, 9, 'Whs8rkRII2.jpg', '2020-09-07 06:58:18', '2020-09-07 06:58:18'),
(307, 9, 'hhkFMoT7TW.jpg', '2020-09-07 06:58:19', '2020-09-07 06:58:19'),
(308, 9, 'lT1YSfgcIN.jpg', '2020-09-07 06:58:25', '2020-09-07 06:58:25'),
(309, 9, 'WLLSmWMk2F.jpg', '2020-09-07 06:58:26', '2020-09-07 06:58:26'),
(310, 9, 'TNVLex79rT.jpg', '2020-09-07 06:58:27', '2020-09-07 06:58:27'),
(311, 9, 'WB8PYPoZ6E.jpg', '2020-09-07 06:58:28', '2020-09-07 06:58:28'),
(312, 9, 'Q806vFqoKA.jpg', '2020-09-07 06:58:29', '2020-09-07 06:58:29'),
(313, 9, 'rWVn9xBQMu.jpg', '2020-09-07 06:58:31', '2020-09-07 06:58:31'),
(314, 9, 'H8OHqTCDX9.jpg', '2020-09-07 06:58:32', '2020-09-07 06:58:32'),
(315, 3, 'cINlFToaw2.jpg', '2020-09-07 07:07:33', '2020-09-07 07:07:33'),
(316, 3, 'zIsnD8TtR5.jpg', '2020-09-07 07:07:40', '2020-09-07 07:07:40'),
(317, 3, 't14nneBT2o.jpg', '2020-09-07 07:07:41', '2020-09-07 07:07:41'),
(318, 3, 'vIbY5EqK6G.jpg', '2020-09-07 07:07:43', '2020-09-07 07:07:43'),
(319, 3, 'copwy826nl.jpg', '2020-09-07 07:07:44', '2020-09-07 07:07:44'),
(320, 3, 'n9ZvzNPdVl.jpg', '2020-09-07 07:07:46', '2020-09-07 07:07:46'),
(321, 3, 'KljAZJTtOr.jpg', '2020-09-07 07:07:48', '2020-09-07 07:07:48'),
(322, 3, 'XuNurkNsKh.jpg', '2020-09-07 07:07:49', '2020-09-07 07:07:49'),
(323, 3, 'yb3jnIQfXZ.jpg', '2020-09-07 07:07:50', '2020-09-07 07:07:50'),
(324, 3, 'wLRQ1CbyIu.jpg', '2020-09-07 07:07:51', '2020-09-07 07:07:51'),
(325, 3, 't0kndQ0Mq8.jpg', '2020-09-07 07:07:52', '2020-09-07 07:07:52'),
(326, 10, 'sxAj97trJQ.jpg', '2020-09-13 08:03:20', '2020-09-13 08:03:20'),
(327, 10, 'O1EU74v7s1.jpg', '2020-09-13 08:03:27', '2020-09-13 08:03:27'),
(328, 10, '4bOj0BlOCG.jpg', '2020-09-13 08:03:32', '2020-09-13 08:03:32'),
(329, 10, 'nsewKryy91.jpg', '2020-09-13 08:03:35', '2020-09-13 08:03:35'),
(330, 10, 'zSWGF8fGpf.jpg', '2020-09-13 08:03:39', '2020-09-13 08:03:39'),
(331, 10, 'qrBlFH7T9w.jpg', '2020-09-13 08:03:42', '2020-09-13 08:03:42'),
(332, 10, 'cH2bmZbLki.jpg', '2020-09-13 08:03:47', '2020-09-13 08:03:47'),
(333, 10, 'G5TpD64Gdp.jpg', '2020-09-13 08:03:54', '2020-09-13 08:03:54'),
(334, 12, 'Mp9sTsloWF.jpg', '2020-10-22 11:28:26', '2020-10-22 11:28:26'),
(335, 12, 'FE9Zz45Z6C.jpg', '2020-10-22 11:28:29', '2020-10-22 11:28:29'),
(336, 12, 'xnIdyUfC4m.jpg', '2020-10-22 11:28:29', '2020-10-22 11:28:29'),
(337, 12, 'x5c1z2OlJQ.jpg', '2020-10-22 11:28:29', '2020-10-22 11:28:29'),
(338, 12, 'uP6yUf56Vn.jpg', '2020-10-22 11:28:29', '2020-10-22 11:28:29'),
(339, 12, 'KiLcLgxuDt.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(340, 12, '3i3zGAMP7D.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(341, 12, 'Jl4v8YV38j.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(342, 12, '3Ueh4KDpTo.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(343, 12, 'xHnJDWkHXM.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(344, 12, 'TUYSnw8FfF.jpg', '2020-10-22 11:28:30', '2020-10-22 11:28:30'),
(345, 12, 'MTuX4ugVqP.jpg', '2020-10-22 11:28:31', '2020-10-22 11:28:31'),
(346, 12, '5iTv1gohwC.jpg', '2020-10-22 11:28:31', '2020-10-22 11:28:31'),
(347, 12, 'pDcwvUk3AY.jpg', '2020-10-22 11:28:31', '2020-10-22 11:28:31'),
(348, 12, '8ref6Pw9Cy.jpg', '2020-10-22 11:28:31', '2020-10-22 11:28:31'),
(349, 12, 'cTBvTXq1Cn.jpg', '2020-10-22 11:28:36', '2020-10-22 11:28:36'),
(350, 12, 'B1mkZQmIqQ.jpg', '2020-10-22 11:28:36', '2020-10-22 11:28:36'),
(351, 12, 'xmX001WK0Z.jpg', '2020-10-22 11:28:36', '2020-10-22 11:28:36'),
(352, 12, 'px7HITbymW.jpg', '2020-10-22 11:28:36', '2020-10-22 11:28:36'),
(353, 12, 'TBXkwKV1eR.jpg', '2020-10-22 11:28:37', '2020-10-22 11:28:37'),
(354, 12, 'ranYRCRVYW.jpg', '2020-10-22 11:28:37', '2020-10-22 11:28:37'),
(355, 12, 'RBMP8I6pfN.jpg', '2020-10-22 11:28:37', '2020-10-22 11:28:37'),
(356, 12, 'CIOWi30ath.jpg', '2020-10-22 11:28:37', '2020-10-22 11:28:37'),
(357, 13, 'JURlhO1IEk.jpg', '2020-10-22 12:07:51', '2020-10-22 12:07:51'),
(358, 13, 'RyyTRn7dKB.jpg', '2020-10-22 12:07:54', '2020-10-22 12:07:54'),
(359, 13, 'tSqlN5cp6l.jpg', '2020-10-22 12:08:03', '2020-10-22 12:08:03'),
(360, 13, 'UjTEvSwwnF.jpg', '2020-10-22 12:08:03', '2020-10-22 12:08:03'),
(361, 13, '66KRK9J0yv.jpg', '2020-10-22 12:08:03', '2020-10-22 12:08:03'),
(362, 13, 'tdLMNhfCki.jpg', '2020-10-22 12:08:07', '2020-10-22 12:08:07'),
(363, 13, 'hhJvSYSQOm.jpg', '2020-10-22 12:08:07', '2020-10-22 12:08:07'),
(364, 13, 'dKptHWNoqM.jpg', '2020-10-22 12:08:08', '2020-10-22 12:08:08'),
(365, 13, 'Dfd85Y57zA.jpg', '2020-10-22 12:08:08', '2020-10-22 12:08:08'),
(366, 13, 'bsWoqqe1TL.jpg', '2020-10-22 12:08:08', '2020-10-22 12:08:08'),
(367, 13, 'AMWNkCHivz.jpg', '2020-10-22 12:08:08', '2020-10-22 12:08:08'),
(368, 13, 'bZBUeYgdRX.jpg', '2020-10-22 12:08:09', '2020-10-22 12:08:09'),
(369, 13, '7pljUYnljd.jpg', '2020-10-22 12:08:09', '2020-10-22 12:08:09'),
(370, 13, 'DF1am1AmRz.jpg', '2020-10-22 12:08:09', '2020-10-22 12:08:09'),
(371, 13, 'WbMIRCVyi2.jpg', '2020-10-22 12:08:10', '2020-10-22 12:08:10'),
(372, 13, 'A0ek9kR2el.jpg', '2020-10-22 12:08:10', '2020-10-22 12:08:10'),
(373, 13, 'IPCdJJfJox.jpg', '2020-10-22 12:08:10', '2020-10-22 12:08:10'),
(374, 13, 'qJ34orbpmu.jpg', '2020-10-22 12:08:10', '2020-10-22 12:08:10'),
(375, 13, 'yppB9V05r1.jpg', '2020-10-22 12:08:10', '2020-10-22 12:08:10'),
(376, 13, 'Nz1u8iGNrj.jpg', '2020-10-22 12:08:11', '2020-10-22 12:08:11'),
(377, 13, 'xIRVlNvlu7.jpg', '2020-10-22 12:08:11', '2020-10-22 12:08:11'),
(378, 13, 'ARpARTQLWR.jpg', '2020-10-22 12:08:11', '2020-10-22 12:08:11'),
(379, 13, '7lbvxsY0EE.jpg', '2020-10-22 12:08:11', '2020-10-22 12:08:11'),
(380, 13, 'Ah7p1RS7kh.jpg', '2020-10-22 12:08:11', '2020-10-22 12:08:11'),
(381, 13, 'TL4M77zCAT.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(382, 13, 'M8EATPDlDP.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(383, 13, 'ZlGvTx7TvT.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(384, 13, 'sEfoeqTVxY.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(385, 13, 'YjOCPhN6IR.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(386, 13, 'u0OQ6jKHMK.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(387, 13, 'cF9000DtaP.jpg', '2020-10-22 12:08:12', '2020-10-22 12:08:12'),
(388, 13, '7IviipiR5d.jpg', '2020-10-22 12:08:13', '2020-10-22 12:08:13'),
(389, 13, 'wywF8X39s0.jpg', '2020-10-22 12:08:13', '2020-10-22 12:08:13'),
(390, 13, 'QW8oLV0rCc.jpg', '2020-10-22 12:08:14', '2020-10-22 12:08:14'),
(391, 13, 'I8NNyffDi0.jpg', '2020-10-22 12:08:14', '2020-10-22 12:08:14'),
(392, 13, 'Wa5TWKfuz3.jpg', '2020-10-22 12:08:15', '2020-10-22 12:08:15'),
(393, 13, '8XiXt09rcm.jpg', '2020-10-22 12:08:15', '2020-10-22 12:08:15'),
(394, 13, 'xrjtPoP5rm.jpg', '2020-10-22 12:08:16', '2020-10-22 12:08:16'),
(395, 13, 'dI4MrKbyRL.jpg', '2020-10-22 12:08:16', '2020-10-22 12:08:16'),
(396, 13, 'bxpDy3ULo0.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(397, 13, 'dtaUQXsMfX.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(398, 13, 'pTeVfe5VPO.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(399, 13, 'eRqdUZCS0x.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(400, 13, '8FXqego6i9.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(401, 13, '9CLBO7iGzB.jpg', '2020-10-22 12:08:17', '2020-10-22 12:08:17'),
(402, 13, 'hmQTOJpPuc.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(403, 13, '0cEukFuzyQ.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(404, 13, 'dbg5roqNK4.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(405, 13, 'WgPrG9O6mV.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(406, 13, 'KVuIdWdu79.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(407, 13, 'A0vj1VndZo.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(408, 13, 'HHVYmcLQCW.jpg', '2020-10-22 12:08:18', '2020-10-22 12:08:18'),
(409, 13, 'vZNlRTyrjY.jpg', '2020-10-22 12:08:19', '2020-10-22 12:08:19'),
(410, 13, 'fTxatyGzMF.jpg', '2020-10-22 12:08:19', '2020-10-22 12:08:19'),
(412, 16, 'gIaIgXqCze.jpg', '2020-11-01 12:43:18', '2020-11-01 12:43:18'),
(413, 16, 'P8cfdih7vt.jpg', '2020-11-01 12:43:22', '2020-11-01 12:43:22'),
(414, 16, 'uTflZabrTi.jpg', '2020-11-01 12:43:23', '2020-11-01 12:43:23'),
(415, 16, 'SfDJT3uf3l.jpg', '2020-11-01 12:43:25', '2020-11-01 12:43:25'),
(416, 16, 'dEJfETFROs.jpg', '2020-11-01 12:43:27', '2020-11-01 12:43:27'),
(417, 16, '73t8oX0DqA.jpg', '2020-11-01 12:43:29', '2020-11-01 12:43:29'),
(418, 16, 'EvOgIUljEt.jpg', '2020-11-01 12:43:34', '2020-11-01 12:43:34'),
(419, 16, 'w5tGNNGb3o.jpg', '2020-11-01 12:43:34', '2020-11-01 12:43:34'),
(420, 16, 'Nzg6tYx7jH.jpg', '2020-11-01 12:43:35', '2020-11-01 12:43:35'),
(421, 16, 'Zqg7j8OgWO.jpg', '2020-11-01 12:43:37', '2020-11-01 12:43:37'),
(422, 16, 'SvUTsa3ROz.jpg', '2020-11-01 12:43:38', '2020-11-01 12:43:38'),
(425, 16, 'oJ55FciLC3.jpg', '2020-11-01 12:43:41', '2020-11-01 12:43:41'),
(426, 16, '3b4SUy4nak.jpg', '2020-11-01 12:43:42', '2020-11-01 12:43:42'),
(427, 16, 'J3egGEXIER.jpg', '2020-11-01 12:43:43', '2020-11-01 12:43:43'),
(428, 16, 'vXXg3ddMmD.jpg', '2020-11-01 12:43:44', '2020-11-01 12:43:44'),
(429, 16, 'LaGrPayLiX.jpg', '2020-11-01 12:43:45', '2020-11-01 12:43:45'),
(430, 16, '27tBORNSKH.jpg', '2020-11-01 12:43:46', '2020-11-01 12:43:46'),
(431, 16, 'llDmwlFWqy.jpg', '2020-11-01 12:43:53', '2020-11-01 12:43:53'),
(432, 16, 'EIpXdRF0rf.jpg', '2020-11-01 12:43:54', '2020-11-01 12:43:54'),
(433, 16, 'wgu6HGKTHJ.jpg', '2020-11-01 12:43:55', '2020-11-01 12:43:55'),
(434, 16, 'hXKL17GgAP.jpg', '2020-11-01 12:43:58', '2020-11-01 12:43:58'),
(435, 16, 'LjORx2zaZX.jpg', '2020-11-01 12:44:00', '2020-11-01 12:44:00'),
(436, 16, '7MivXiAXEy.jpg', '2020-11-01 12:44:01', '2020-11-01 12:44:01'),
(437, 16, '6qUPXmCgSw.jpg', '2020-11-01 12:44:02', '2020-11-01 12:44:02'),
(438, 16, '0yDBwV22lB.jpg', '2020-11-01 12:44:03', '2020-11-01 12:44:03'),
(439, 16, 'hwZsqjaD8g.jpg', '2020-11-01 12:44:04', '2020-11-01 12:44:04'),
(440, 16, 'x6oiQSMboN.jpg', '2020-11-01 12:44:09', '2020-11-01 12:44:09'),
(441, 16, '6b2TddIv13.jpg', '2020-11-01 12:44:11', '2020-11-01 12:44:11'),
(442, 17, 'u2pFq9xwHE.jpg', '2020-11-02 09:43:04', '2020-11-02 09:43:04'),
(443, 17, 'gixr7EHUsQ.jpg', '2020-11-02 09:43:08', '2020-11-02 09:43:08'),
(444, 17, 'xBJUkIXI3E.jpg', '2020-11-02 09:43:10', '2020-11-02 09:43:10'),
(445, 17, 'EVacdrBzOl.jpg', '2020-11-02 09:43:12', '2020-11-02 09:43:12'),
(446, 17, 'gPPFv4aRsK.jpg', '2020-11-02 09:43:14', '2020-11-02 09:43:14'),
(447, 17, 'jUs3ZbycNr.jpg', '2020-11-02 09:43:17', '2020-11-02 09:43:17'),
(448, 17, 'xaJMZkRYVG.jpg', '2020-11-02 09:43:20', '2020-11-02 09:43:20'),
(449, 17, 'DUOySwRuaR.jpg', '2020-11-02 09:43:21', '2020-11-02 09:43:21'),
(450, 17, 'zqQJRk8vLy.jpg', '2020-11-02 09:43:21', '2020-11-02 09:43:21'),
(451, 17, 'ZoPEHj2sYP.jpg', '2020-11-02 09:43:23', '2020-11-02 09:43:23'),
(452, 17, 'g9RCKCB5cq.jpg', '2020-11-02 09:43:24', '2020-11-02 09:43:24'),
(453, 17, '1XIkXGraXS.jpg', '2020-11-02 09:43:28', '2020-11-02 09:43:28'),
(454, 17, 'cFs8GSbr5e.jpg', '2020-11-02 09:43:32', '2020-11-02 09:43:32'),
(455, 17, 'ChLFdA5lvd.jpg', '2020-11-02 09:43:34', '2020-11-02 09:43:34'),
(456, 17, 'gD4ArHCfPP.jpg', '2020-11-02 09:43:35', '2020-11-02 09:43:35'),
(457, 17, 'HpnI5U2bN8.jpg', '2020-11-02 09:43:36', '2020-11-02 09:43:36'),
(458, 17, 'IsrupqxRND.jpg', '2020-11-02 09:43:37', '2020-11-02 09:43:37'),
(459, 17, 'Gdu2i3BnGy.jpg', '2020-11-02 09:43:38', '2020-11-02 09:43:38'),
(460, 17, 'XnirBs8S3i.jpg', '2020-11-02 09:43:38', '2020-11-02 09:43:38'),
(461, 17, 'de6HPxNfwE.jpg', '2020-11-02 09:43:39', '2020-11-02 09:43:39'),
(462, 17, 'rY1Keopgn0.jpg', '2020-11-02 09:43:39', '2020-11-02 09:43:39'),
(463, 17, 'zKWx0iMvzo.jpg', '2020-11-02 09:43:40', '2020-11-02 09:43:40'),
(464, 17, 'ta0Hen3bV0.jpg', '2020-11-02 09:43:40', '2020-11-02 09:43:40'),
(465, 17, 'OK4U4Ayedd.jpg', '2020-11-02 09:43:40', '2020-11-02 09:43:40'),
(466, 17, 'Y4LatUvOTn.jpg', '2020-11-02 09:43:40', '2020-11-02 09:43:40'),
(467, 17, 'Qf6B5t8TcW.jpg', '2020-11-02 09:43:40', '2020-11-02 09:43:40'),
(468, 17, 'rE2R5IDJ2w.jpg', '2020-11-02 09:43:41', '2020-11-02 09:43:41'),
(469, 17, 'trlHW4u1ub.jpg', '2020-11-02 09:43:41', '2020-11-02 09:43:41'),
(470, 17, 'JN0T1FGUPF.jpg', '2020-11-02 09:43:52', '2020-11-02 09:43:52'),
(471, 17, 'dmiuC9etbB.jpg', '2020-11-02 09:43:53', '2020-11-02 09:43:53'),
(472, 17, 'bcBwBWbpN7.jpg', '2020-11-02 09:43:54', '2020-11-02 09:43:54'),
(473, 17, 'wehw4EgFSh.jpg', '2020-11-02 09:44:00', '2020-11-02 09:44:00'),
(474, 17, 'XOIpkuTg5v.jpg', '2020-11-02 09:44:02', '2020-11-02 09:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary`
--

CREATE TABLE `employee_salary` (
  `id` bigint UNSIGNED NOT NULL,
  `reference` int NOT NULL,
  `salary_type` int NOT NULL DEFAULT '1',
  `gross_salary` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_salary`
--

INSERT INTO `employee_salary` (`id`, `reference`, `salary_type`, `gross_salary`, `currency`, `created_at`, `updated_at`) VALUES
(9, 4, 1, '10000', 'bdt', NULL, NULL),
(15, 9, 1, '500', 'usd', NULL, NULL),
(18, 13, 2, '1', 'usd', NULL, NULL),
(20, 7, 2, '100', 'bdt', NULL, NULL),
(23, 10, 2, '50', 'bdt', NULL, NULL),
(24, 6, 2, '260.5', 'bdt', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint UNSIGNED NOT NULL,
  `month` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dates` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobtitle`
--

CREATE TABLE `jobtitle` (
  `id` int UNSIGNED NOT NULL,
  `jobtitle` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `dept_code` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobtitle`
--

INSERT INTO `jobtitle` (`id`, `jobtitle`, `dept_code`) VALUES
(1, 'SOFTWARE DEVELOPER', 1),
(2, 'JR. SOFTWARE DEVELOPER', 1),
(3, 'INVENTORY HEAD & CS', 2),
(4, 'EXECUTIVE ACCOUNTS', 3),
(5, 'UK ACCOUNTS MAINTAINER', 4),
(6, 'OPERATION MANAGER', 5),
(7, 'INTERNATIONAL CS TEAM LEADER', 4),
(8, 'DIGITAL MARKETING EXECUTIVE', 2),
(9, 'USA ACCOUNT MANAGMANT', 4),
(10, 'HJ  HHJ', 1),
(11, 'HEAD OF INTERNATIONAL CUSTOMER SUPPORT', 4),
(12, 'CEO', 5);

-- --------------------------------------------------------

--
-- Table structure for table `leavegroup`
--

CREATE TABLE `leavegroup` (
  `id` int UNSIGNED NOT NULL,
  `leavegroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leaveprivileges` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int UNSIGNED NOT NULL,
  `reference` int DEFAULT NULL,
  `typeid` int DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `leavefrom` timestamp NULL DEFAULT NULL,
  `leaveto` timestamp NULL DEFAULT NULL,
  `returndate` timestamp NULL DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `reference`, `typeid`, `type`, `leavefrom`, `leaveto`, `returndate`, `reason`, `status`, `comment`, `archived`, `created_at`, `updated_at`) VALUES
(3, 3, 12, 'RUMII', '2020-11-17 18:00:00', '2020-11-24 18:00:00', '2020-11-25 18:00:00', 'SFSFFSF', 'Pending', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leavetype`
--

CREATE TABLE `leavetype` (
  `id` int UNSIGNED NOT NULL,
  `leavetype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percalendar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leavetype`
--

INSERT INTO `leavetype` (`id`, `leavetype`, `limit`, `percalendar`) VALUES
(1, 'GENERAL', '28', 'Monthly'),
(3, 'SICK', '7', 'Yearly'),
(4, 'TOUR', '5', 'Yearly'),
(5, 'ANISUR RAHMAN', '2', 'Yearly'),
(6, 'SHIHAB', '14', 'Yearly'),
(7, 'ABDUR RAHMAN', '5', 'Yearly'),
(8, 'DIDAR KHAN', '14', 'Yearly'),
(9, 'SAKIB', '5', 'Yearly'),
(10, 'SIYAM', '10', 'Yearly'),
(11, 'AVIJIT', '10', 'Yearly'),
(12, 'RUMII', '14', 'Yearly');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_02_25_023020_create_attendance_table', 1),
(2, '2020_02_25_023020_create_company_table', 1),
(3, '2020_02_25_023020_create_department_table', 1),
(4, '2020_02_25_023020_create_employee_faces_table', 1),
(5, '2020_02_25_023020_create_jobtitle_table', 1),
(6, '2020_02_25_023020_create_leavegroup_table', 1),
(7, '2020_02_25_023020_create_leaves_table', 1),
(8, '2020_02_25_023020_create_leavetype_table', 1),
(9, '2020_02_25_023020_create_password_resets_table', 1),
(10, '2020_02_25_023020_create_report_views_table', 1),
(11, '2020_02_25_023020_create_settings_table', 1),
(12, '2020_02_25_023020_create_tasks_table', 1),
(13, '2020_02_25_023020_create_users_permissions_table', 1),
(14, '2020_02_25_023020_create_users_roles_table', 1),
(15, '2020_03_31_134425_create_daily_breaks_table', 1),
(16, '2020_05_11_184042_create_webcam_data_table', 1),
(17, '2020_05_17_150702_create_schedule_template_table', 1),
(18, '2020_05_17_150832_create_schedules_table', 1),
(19, '2020_05_26_153405_create_task_extension_table', 1),
(20, '2020_05_30_194317_create_salary_types_table', 1),
(21, '2020_05_30_194354_create_employee_salary_table', 1),
(22, '2020_05_30_194413_create_holidays_table', 1),
(23, '2020_06_14_084822_create_people_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`) VALUES
(1, 'mm.rumii@yahoo.com', 'p0s355cWsUnTsmLPrteA8HpMFREBh1HbEKf9jv84mzucvUC1FyV7Asy6dqzS', '2020-10-21 08:05:53'),
(2, 'mm.rumii@yahoo.com', 'QX2my8Chekn6F1SkGF7iqFSfSEZdzjZ7usvhg8UjCaEJRBw3RRx5SLLSxB2p', '2020-10-21 08:09:40'),
(3, 'mm.rumii@yahoo.com', 'OOICy8shYtmPEA10bJfSFofGzcU88o9zjdacCsQQZhtcdetwAyf3Czh5Z1Bt', '2020-10-21 08:22:38'),
(4, 'md.miraz.4000@gmail.com', 'nnSmNlBXWi8qGLz2frsnJGRZ8omaUWQfQ7Fkrr6VIJJSgts88iqGPJj7Zcu2', '2020-10-30 07:24:30'),
(5, 'avijit@afsfashion.co.uk', 'A3GCQ9XWA8ODNoNSogsa1fcYqPu4aXXj0KQaaMEhReacmYkFply2RJu9oeA1', '2020-11-05 17:50:35'),
(6, 'avijit@afsfashion.co.uk', 'YwwDgABOFO1oYYiHpSRr53AkHU3ZsZUQ04JPgUtTKk4JQMn6323YSKDm1bD6', '2020-11-06 09:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` bigint UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `emailaddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `civilstatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `mobileno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nationalid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthplace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `homeaddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `employmentstatus` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `employmenttype` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `acc_type` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int DEFAULT NULL,
  `department_id` int DEFAULT NULL,
  `job_title_id` int DEFAULT NULL,
  `companyemail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `idno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `startdate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `dateregularized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `reason` varchar(455) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `leaveprivilege` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `firstname`, `mi`, `lastname`, `gender`, `emailaddress`, `civilstatus`, `mobileno`, `birthday`, `nationalid`, `birthplace`, `homeaddress`, `employmentstatus`, `employmenttype`, `avatar`, `role_id`, `acc_type`, `status`, `password`, `remember_token`, `company_id`, `department_id`, `job_title_id`, `companyemail`, `idno`, `startdate`, `dateregularized`, `reason`, `leaveprivilege`, `created_at`, `updated_at`) VALUES
(1, 'Demo', '', 'Manager', 'Male', '', '', '', NULL, NULL, '', '', 'Active', '', NULL, 2, 2, 1, '$2y$10$tFXK3tF7jm0LWjCA6k8Z4OP4lDTfQx8GLeVSTV5tqXPNGZvvlGDiK', NULL, NULL, NULL, NULL, 'manager@example.com', '', '', '', '', NULL, NULL, NULL),
(2, 'Masud', NULL, 'Rumii', '', '', '', '', NULL, NULL, '', '', 'Active', '', NULL, 2, 2, 1, '$2y$10$tFXK3tF7jm0LWjCA6k8Z4OP4lDTfQx8GLeVSTV5tqXPNGZvvlGDiK', 'CVr0cpRvgkdtxP4Q3RTRd3IYbnO2dnbgZWklW7lYH3oM1KWM288Q9ImTFJkT', NULL, NULL, NULL, 'mm.rumii@yahoo.com', '', '', '', '', NULL, NULL, '2020-10-21 08:24:30'),
(3, 'MASUD', '', 'RUMII', 'MALE', 'mm.rumii@gmail.com', 'MARRIED', '01722669787', '1993-08-14', '19935013940000144', 'HOUSE: 8/J, ROAD: 1, BLOCK: D, BASHUNDHARA R/A', 'SONAY KUNDI, ALLARDARGA, DAULATPUR, KUSHTIA', 'Active', 'Regular', 'office.jpg', 2, 1, 1, '$2y$10$ad2lfXg4LqUU4A7rypOGbuA13RjBDnBCF7BfCePesSsyOz5Yh6FwW', 'Ypk0zYVPVemOA3im2suJ30VDR58evmTEtZYxPeOZyjT3lP0tFzHuxgncbNeQ', 1, 1, 1, 'rumii@evidentbd.com', 'WES007', '2020-03-01', '2022-08-01', '', 12, NULL, '2020-11-15 11:57:04'),
(4, 'MASHFIQUR', '', 'RAHMAN', 'MALE', 'mashfiqurrr@gmail.com', 'SINGLE', '01521455637', '1995-09-19', '8650936431', '29/1, 2, 3 JUSTICE LAL MOHAN, DAS LANE, SUTRAPUR, DHAKA - 1100', 'SAME', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$ad2lfXg4LqUU4A7rypOGbuA13RjBDnBCF7BfCePesSsyOz5Yh6FwW', 'HLnSH3OHVvTJO5SSFRm4Y4geQQfjQFOyCDcRWPA5rrXsK1I6J77NJwZQv4av', 1, 1, 2, 'mashfiq@evidentbd.com', 'WES004', '2020-09-01', '2022-09-01', '', 3, NULL, '2020-11-01 10:52:46'),
(6, 'SIYAM', '', 'KHANDAKAR', 'MALE', 'siyeam1122@gmail.com', 'SINGLE', '01775141633', '1998-10-15', '2856498437', '10/B, PALONKO, NIRJHOR ,DHAKA CANTONMENT', 'MEHENDI GONJ, BARISHAL', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$j0WzZiaabwO686xSU/y6NePbiOXWoxG1qV/2zDtm9j1pGpqJxh6AO', NULL, 1, 2, 3, 'siam@westacebd.com', 'WES008', '2020-01-01', '2022-01-01', '', 10, NULL, '2020-11-15 11:58:15'),
(7, 'AVIJIT', '', 'SHAHA', 'MALE', 'avijit.acca95@gmail.com', 'SINGLE', '01624528256', '1995-03-01', '6874770610', 'OFFICE ADDRESS', 'HOLDING: 214, VILLAGE: KUNDOSHI, POST: LOHAGARA-7511, THANA', 'Active', 'Regular', '', 1, 1, 1, '$2y$10$tJCsYoRP9hBobvM6p5zE2.frGFsmHK3h2Z4w0pQdQRRbFHpFbg4e.', NULL, 1, 3, 4, 'avijit@afsfashion.co.uk', 'WES005', '2020-03-01', '2020-03-02', '', 11, NULL, '2020-11-15 11:56:10'),
(8, 'SEYED', 'NAZMUS', 'SAKIB', 'MALE', 'seyedsakib2016@gmail.com', 'SINGLE', '01407070308', '1996-05-25', '', 'OFFICE ADDRESS', 'MUSA MOSHJID, AMTOLA MORE, MODDHOKATIA, SATKHIRA SADAR, SATKHIRA, KHULNA, BANGLADESH', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$JNodmkbC.tdPnBaimUM4i.Szb0FccGQbkUKVtXZIYgaPU0b1nAsJi', NULL, 1, 4, 5, 'n.sakib@westacebd.com', 'WES006', '2019-10-22', '2025-10-22', '', NULL, NULL, NULL),
(9, 'BAJLUL HUDA', 'NURULLAH', 'KHAN', 'MALE', 'didar@afsfashion.co.uk', 'MARRIED', '01407070303', '1988-11-01', '1481357968', 'MOLLAR TEK, UTTARA (MUNSHI MARKET)', 'MUKTAR PARA, NETROKONA', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$Voidv8EQLnrHz/y5kC3kbef9LlcNIjNW8Uyr1c2qzkHAKPTbqnG/u', NULL, 1, 5, 6, 'didar@afsfashion.co.uk', 'WES003', '2019-09-01', '2025-09-30', '', 8, NULL, '2020-11-15 11:56:37'),
(10, 'SHAHARIAR', 'RAHMAN', 'SHIHAB', 'MALE', 'shahriar.rahman26639@gmail.com', 'SINGLE', '01731191548', '2000-05-01', '20000695118000038', 'OFFICE ADDRESS', 'BARISHAL', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$xglVOpVPmUkDZHr4c/l7e.iyF/Y6s4cBsm5yFU3UN3z7GS1dexi86', 'fqq1mrX6AeOUlcAVs1bUz89Ya61DJgT684ZKgWS2wptB9hWFe4Q2wshzPvs4', 1, 4, 7, 'shihab@afsfashion.co.uk', 'WES002', '2017-10-27', '1970-01-01', '', 6, NULL, '2020-11-15 11:57:59'),
(11, 'OPERATION', '', 'MANAGER', 'MALE', 'operation@westacebd.com', 'MARRIED', '001407070303', '1980-05-07', 'NOT PROVIDED', 'NOT PROVIDED', 'NOT PROVIDED', 'Active', 'Regular', '', 1, 2, 1, '$2y$10$JIed7dawcOz1H5uz8AofKe91hUcoeXUwOf0YqJXv2qWx/JmqzAxQG', NULL, 1, 5, 6, 'operation_manager@westacebd.com', 'WES009', '2017-08-16', '1970-01-01', '', NULL, NULL, NULL),
(12, 'ZINIA', 'SHABNAM', 'SHOSHI', 'FEMALE', 'shoshishabnam10@gmail.com', 'SINGLE', '01407070306', '1970-01-01', '2365383500', 'SOUTH CHATAR, BOF, GAZIPUR CITY CORPORATION, GAZIPUR', 'SOUTH CHATAR, BOF, GAZIPUR CITY CORPORATION, GAZIPUR', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$WI9hfG1tpq.2RSY6Vp1cE./DQ2QjYc50yWxPYkH1qbQ.Cv.tr7W9y', 'hVmp3KANgjb3tvCF5X4kSaTPg7HvRgfHPrqbloZ3m5uq2aurE4w2cdtfNTWT', 1, 2, 8, 'zinia@westacebd.com', 'WES010', '2020-09-14', '1970-01-01', '', NULL, NULL, NULL),
(13, 'MD', 'ABDUR', 'RAHMAN', 'MALE', 'mdabdurinfo17@gamil.com', 'SINGLE', '+8801407070305', '1996-10-29', '', 'MOHAMMAD PUR ,MOHAMMADIA HOUSEING MEAN ROAD LTD', 'MOHAMMAD PUR ,MOHAMMADIA HOUSEING MEAN ROAD LTD', 'Active', 'Regular', '', 2, 1, 1, '$2y$10$tFXK3tF7jm0LWjCA6k8Z4OP4lDTfQx8GLeVSTV5tqXPNGZvvlGDiK', 'EMaV1I9tUo7s5kZhi0hVdGAQUM1j6vHSANiPQXLE4TPVUZCnuEqaqjuTLsOu', 1, 4, 9, 'babu@westacebd.com', 'WES011', '2018-05-15', '1970-01-01', '', 7, NULL, '2020-11-17 07:58:49'),
(16, 'MOHAMMAD', '', 'MIRAZ', 'MALE', 'md.miraz.4000@gmail.com', 'SINGLE', '01710304891', '1996-08-10', '', 'BASHUNDHARA , DHAKA', 'CUMIILLA ADARSHA SADAR ,CUMILLA', 'Active', 'Trainee', '', 2, 1, 1, '$2y$10$XMz6v5jboL.9KESd00IZ1u36MGa02RR8vJl675CraNIOG/BdzS3nK', NULL, 1, 1, 2, 'miraz@evidentbd.com', 'WE009', '2020-09-01', '2020-09-01', '', 1, '2020-11-01 10:55:39', NULL),
(17, 'ANISUR', '', 'RAHMAN', 'MALE', 'anis@afsfashion.co.uk', 'MARRIED', '01407070301', '1988-03-01', '', '6, MOHAMMADIA HOUSING, MOHAMMADPUR, DHAKA', '6, MOHAMMADIA HOUSING, MOHAMMADPUR, DHAKA', 'Active', 'Regular', '', 1, 1, 1, '$2y$10$PXnY/m3Wnu3bNCB2p0ScPuXg5aQI44k4lg5HWuiT2MPL9TR0er4ca', NULL, 1, 4, 11, 'anis@afsfashion.co.uk', 'WES001', '2016-01-01', '1970-01-01', '', 5, '2020-11-02 09:40:39', '2020-11-15 11:50:40'),
(18, 'AL', 'AMIN', 'SHOUROV', 'MALE', 'shourov786@gmail.com', 'MARRIED', '01407070307', '1988-01-01', '', 'UK', 'UK', 'Active', 'Regular', '', 3, 2, 1, '$2y$10$qdwJeJ2IXxskYHSKog4an.l/iLh.hmQCYplMrkh2LJ/qvhflq1O46', 'DhrO71c4Hw0KYRas2LyNoLwSvHFLnqICydKHtW7jNRV8142zjrOVJ65z6iGD', 1, 5, 12, 'shourov786@gmail.com', 'WES000', '2020-11-01', '1970-01-01', '', 1, '2020-11-04 12:35:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report_views`
--

CREATE TABLE `report_views` (
  `id` int UNSIGNED NOT NULL,
  `report_id` int DEFAULT NULL,
  `last_viewed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_types`
--

CREATE TABLE `salary_types` (
  `id` bigint UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_types`
--

INSERT INTO `salary_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Monthly', NULL, NULL),
(2, 'Hourly', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint UNSIGNED NOT NULL,
  `reference` int NOT NULL,
  `schedule_id` int NOT NULL,
  `active_status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `reference`, `schedule_id`, `active_status`, `created_at`, `updated_at`) VALUES
(3, 4, 3, 1, '2020-09-06 11:56:53', NULL),
(4, 6, 3, 1, '2020-09-06 12:02:57', NULL),
(5, 8, 4, 0, '2020-09-07 06:40:48', NULL),
(7, 3, 1, 1, '2020-09-07 07:11:40', NULL),
(11, 2, 1, 1, '2020-10-27 13:03:02', NULL),
(15, 1, 2, 1, '2020-10-29 14:09:46', NULL),
(16, 12, 13, 1, '2020-11-01 09:53:30', NULL),
(17, 10, 16, 1, '2020-11-01 10:43:54', NULL),
(18, 9, 15, 1, '2020-11-01 10:44:22', NULL),
(19, 13, 17, 1, '2020-11-01 10:44:52', NULL),
(20, 16, 1, 1, '2020-11-01 10:55:58', NULL),
(21, 17, 17, 1, '2020-11-02 09:41:26', NULL),
(22, 7, 1, 1, '2020-11-03 07:35:27', NULL),
(23, 8, 14, 1, '2020-11-15 10:17:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_template`
--

CREATE TABLE `schedule_template` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saturday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sunday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wednesday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thursday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `break_allowence` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_template`
--

INSERT INTO `schedule_template` (`id`, `name`, `saturday`, `sunday`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `break_allowence`, `created_at`, `updated_at`) VALUES
(1, '12 PM TO 8 PM -SATURDAY OFF', NULL, '12:00-20:00', '12:00-20:00', '12:00-20:00', '12:00-20:00', '12:00-20:00', '12:00-20:00', '30', '2020-09-02 14:04:38', NULL),
(3, '10 AM TO 6 PM -FRIDAY OFF', '10:00-18:00', '10:00-18:00', '10:00-18:00', '10:00-18:00', '10:00-18:00', '10:00-18:00', NULL, '30', '2020-09-06 11:56:36', NULL),
(4, '11 AM TO 7 PM  -SATURDAY OFF', NULL, '11:00-19:00', '11:00-19:00', '11:00-19:00', '11:00-19:00', '11:00-19:00', '11:00-19:00', '30', '2020-09-07 06:40:29', NULL),
(13, '9:30 AM TO 5:30 PM -FRIDAY OFF', '09:30-17:30', '09:30-17:30', '09:30-17:30', '09:30-17:30', '09:30-17:30', '09:30-17:30', NULL, '30', '2020-11-01 09:07:59', NULL),
(14, '1:30 PM TO 9:30 PM -SATURDAY OFF', NULL, '13:30-21:30', '13:30-21:30', '13:30-21:30', '13:30-21:30', '13:30-21:30', '13:30-21:30', '30', '2020-11-01 09:13:46', NULL),
(15, '9 AM TO 5 PM -FRIDAY OFF', '09:00-17:00', '09:00-17:00', '09:00-17:00', '09:00-17:00', '09:00-17:00', '09:00-17:00', NULL, '30', '2020-11-01 09:20:05', NULL),
(16, '2:30 PM TO 10:30 PM -SATURDAY OFF', NULL, '14:30-22:30', '14:30-22:30', '14:30-22:30', '14:30-22:30', '14:30-22:30', '14:30-22:30', '30', '2020-11-01 09:23:22', NULL),
(17, '3 PM TO 9PM -SUNDAY OFF', '15:00-21:00', NULL, '15:00-21:00', '15:00-21:00', '15:00-21:00', '15:00-21:00', '15:00-21:00', '30', '2020-11-01 09:26:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int UNSIGNED NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clock_comment` int DEFAULT NULL,
  `iprestriction` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opt` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `country`, `timezone`, `clock_comment`, `iprestriction`, `opt`, `company_name`) VALUES
(1, 'Bangladesh', 'Asia/Dhaka', 0, NULL, NULL, 'westace_bd');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int UNSIGNED NOT NULL,
  `reference` int NOT NULL,
  `assigned_by` int NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `done_status` tinyint(1) NOT NULL,
  `finishdate` timestamp NULL DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `reference`, `assigned_by`, `title`, `description`, `done_status`, `finishdate`, `comment`, `created_at`, `updated_at`) VALUES
(18, 17, 18, 'List T-shirt (USA)', 'bismillahirrahmanirrahim\r\n\r\nPlz list round neck & v neck in westAce’s eBay and Amazon', 1, '2020-11-04 18:00:00', 'done. picture lagbe please. i will send email to didar vhai.', '2020-11-04 13:19:14', '2020-11-05 10:16:29'),
(19, 7, 18, 'Cross check sb06 Sep 1 shipment and update IK', 'Sent you an email with sb06 list.\r\nPlease update IK (inventory keeper) accordingly', 1, '2020-11-05 18:00:00', 'Done 05 nov 20', '2020-11-04 13:21:57', '2020-11-06 11:17:34'),
(20, 9, 18, 'Video Collage Needed', 'Sent email.\r\nKindly save them according to the serial they were sent.\r\nThen Collage them together so that it looks like a single clip.', 1, '2020-11-04 18:00:00', 'Video Is Done. I send you mail.', '2020-11-05 08:06:23', '2020-11-05 13:45:43'),
(21, 4, 2, 'test', 'test koro mail pathao', 0, NULL, NULL, '2020-11-05 10:09:14', '2020-11-05 10:09:14'),
(22, 10, 18, 'Change Jacket price on Gantshill eBay', 'Change Jacket Price to $25.99 (Gnatshill eBay).\r\n\r\nLeave Wine colour S as $21.95 plz.', 1, '2020-11-04 18:00:00', 'PRICE CHANGED bhaiya.\r\nThanks', '2020-11-05 11:31:20', '2020-11-05 13:56:48'),
(23, 18, 17, 'please check usa westAce amazon', 'please check usa westAce amazon v-neck and round neck. it says 100% cotton but i did not enter as such, so plz rectify it.', 0, NULL, NULL, '2020-11-05 11:45:04', '2020-11-05 11:45:04'),
(24, 3, 18, 'Additional Button needed on Taska Manager', 'Need to have 3 Button on Task Manager.\r\n\r\nPending Task\r\nCompleted Task\r\nAll Task', 1, '2020-11-09 18:00:00', 'Done', '2020-11-05 11:55:53', '2020-11-10 06:31:43'),
(36, 17, 18, 'Change SB09 price on amazon', 'Plz change SB09 price on amazon \r\n\r\nLIC to £15.95 (Both variations and non variation)\r\n\r\nRT to £15.99 (Both variations and non variation)', 1, '2020-11-05 18:00:00', 'its done', '2020-11-06 11:11:11', '2020-11-06 14:15:08'),
(37, 4, 2, 'Testing', 'A test description', 0, NULL, NULL, '2020-11-07 12:01:24', '2020-11-07 12:01:24'),
(41, 3, 4, 'test', 'test', 0, NULL, NULL, '2020-11-10 06:36:16', '2020-11-10 06:36:16'),
(42, 12, 3, 'A test task. (Please Ignore)', 'Yo bro', 0, NULL, NULL, '2020-11-10 06:37:49', '2020-11-10 06:37:49'),
(45, 9, 17, 'Picture needed please for usa westace', '1. Round neck t-shirt picture including combine picture\r\n2. V-neck t-shirt picture including combine picture\r\n\r\n\r\n3. combine picture needed for v-neck t-shirt. colours are -  [gre+navy]  [navy+white]  [white+grey]\r\n4. combine picture needed for round-neck t-shirt. colours are -  [gre+navy]  [navy+white]  [white+grey]', 1, '2020-11-14 18:00:00', 'DONE PLS MAIL Check', '2020-11-10 10:35:44', '2020-11-15 06:20:42'),
(46, 9, 10, 'Blaurehafen.xyz Website Basis Needed Note', 'Dear sir,\r\n\r\nI put a note on your table for Blaurehafen.xyz website picture. I have already written on the note of what I need.\r\n\r\nI need all pictures please help me to get it, sir.\r\n\r\nThanks.', 1, '2020-11-11 18:00:00', 'done', '2020-11-10 20:49:51', '2020-11-12 09:27:56'),
(47, 18, 13, 'picture', 'westace amazon- SSA20- STONE picture> check email westacecorp@gmail.com\r\n\r\nwestace amazon- SSA19- PINK picture. > check email westacecorp@gmail.com\r\n\r\nwestace amazon- SB05- BEIGE picture. > ask didar bhai on Sunday or Monday\r\n\r\n\r\n\r\n\r\ngantshill amazon- SB09- LIGHT BLUE picture. > DONE\r\n\r\ngantshill amazon- SB09- INDIGO picture. > DONE\r\n\r\nplease bhaiya picture gula onak dorkar.', 1, '2020-11-14 18:00:00', 'done except Didar bhai\'s part (sb05 beige)', '2020-11-11 12:23:13', '2020-11-15 09:57:23'),
(48, 9, 13, 'westace  website logo', 'bhai, please create a new logo.\r\n\r\nwestace, add Eagle logo.', 1, '2020-11-14 18:00:00', 'DONE PLS MAIL CHECK', '2020-11-11 12:36:10', '2020-11-15 06:27:42'),
(49, 9, 18, 'Beige colour needed', 'Please check your email.\r\nPlease deliver to Babu', 1, '2020-11-14 18:00:00', 'DONE, MAIL SEND TO MR. BABU', '2020-11-12 13:18:32', '2020-11-15 08:12:08'),
(50, 17, 18, 'List On Alamo Amazon', 'Please list SB09 on Alamo Amazon. pictures are already on the alamo pc\'s download folder.\r\nPlz make sure to use alamo EAN. In fact use the same ean that was used for sb09 alamo ebay listing', 1, '2020-11-12 18:00:00', 'done', '2020-11-13 01:52:21', '2020-11-13 12:27:27'),
(52, 9, 13, 'Picture', 'bhai SB05 er picture lagba.\r\n\r\nall colors.\r\n\r\nplease bhai.', 1, '2020-11-14 18:00:00', 'Error task', '2020-11-14 11:39:50', '2020-11-15 08:16:54'),
(53, 9, 13, 'Skinny Fit Gabardine Stretch Casual Trouser picture', 'Skinny Fit Gabardine Stretch Casual Trouser - SSA18 er picture lagba \r\n\r\nall colors\r\n\r\nplease bhai.', 1, '2020-11-14 18:00:00', 'Error task', '2020-11-14 13:30:49', '2020-11-15 08:17:00'),
(54, 2, 7, 'do or die', 'sadgh fiaeisu', 0, NULL, NULL, '2020-11-15 07:39:30', '2020-11-15 07:39:30'),
(55, 4, 2, 'test', 'test', 0, NULL, NULL, '2020-11-15 08:35:10', '2020-11-15 08:35:10'),
(56, 4, 2, 'test', 'test', 0, NULL, NULL, '2020-11-15 09:32:12', '2020-11-15 09:32:12'),
(57, 7, 18, 'Update Alamo Linn stock', 'I have sent you an email.\r\nTx-09 Po is the actual packing list that was shipped from BD. However we managed to sent only 52 boxes out of 58 from NY to Texas one we received the shipment in NY.\r\n\r\nSB09-texas Invntry is the file that was received in texas. so deduct any box that is missing in this file from the TX-09 po, it will give you an actual idea of what stock has been received in texas.\r\n\r\nThen update the Alamo default inventory accordingly.', 1, '2020-11-15 18:00:00', 'DONE', '2020-11-15 10:02:06', '2020-11-15 19:49:12'),
(58, 9, 12, 'Graphic images', 'Demo graphics sent through email to make new graphics with tag line.\r\nThanks in advance.', 1, '2020-11-14 18:00:00', 'DONE Pls Check Mail', '2020-11-15 10:32:35', '2020-11-15 10:42:01'),
(59, 9, 12, 'color change', 'please change the color of jacket .', 1, '2020-11-15 18:00:00', 'Done!!!  Pls CHECK YOUR MAIL FOR DESIGN', '2020-11-16 03:41:18', '2020-11-16 04:00:30'),
(60, 9, 12, 'Graphic images and motion video', 'Please check your email and see some demo images. We need total 3 graphics. \r\nWe need them urgently. Kindly do the needful.\r\nThanks in advance.', 0, NULL, NULL, '2020-11-17 04:54:55', '2020-11-17 04:54:55'),
(61, 9, 12, 'jumper images', 'Kindly share with us new jumper collection images  with proper guideline. \r\nThanks in advance.', 1, '2020-11-16 18:00:00', 'DONE PLS CJECK YOUR MAIL', '2020-11-17 06:41:32', '2020-11-17 07:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `task_extension`
--

CREATE TABLE `task_extension` (
  `id` bigint UNSIGNED NOT NULL,
  `task_id` int NOT NULL,
  `new_deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_extension`
--

INSERT INTO `task_extension` (`id`, `task_id`, `reason`, `created_at`, `updated_at`) VALUES
(1, 19, 'need more time to solve this issue', '2020-11-04 17:01:33', NULL),
(2, 18, 'health reason [pressure low]', '2020-11-05 08:25:19', NULL),
(3, 46, 'I have Lots of project', '2020-11-11 07:57:48', NULL),
(4, 41, 'I need more time', '2020-11-14 09:40:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int UNSIGNED NOT NULL,
  `role_id` int DEFAULT NULL,
  `perm_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `role_id`, `perm_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 1),
(4, 2, 3),
(5, 2, 4),
(6, 2, 5),
(7, 2, 6),
(8, 2, 7),
(9, 2, 8),
(10, 2, 9),
(11, 2, 10),
(12, 2, 11),
(13, 2, 12),
(14, 3, 1),
(15, 3, 2),
(16, 3, 22),
(17, 3, 21),
(18, 3, 23),
(19, 3, 24),
(20, 3, 25),
(21, 3, 3),
(22, 3, 31),
(23, 3, 32),
(24, 3, 4),
(25, 3, 41),
(26, 3, 42),
(27, 3, 43),
(28, 3, 44),
(29, 3, 5),
(30, 3, 52),
(31, 3, 53),
(32, 3, 9),
(33, 3, 91),
(34, 3, 7),
(35, 3, 8),
(36, 3, 81),
(37, 3, 82),
(38, 3, 83),
(39, 3, 10),
(40, 3, 101),
(41, 3, 102),
(42, 3, 103),
(43, 3, 104),
(44, 3, 11),
(45, 3, 111),
(46, 3, 112),
(47, 3, 12),
(48, 3, 121),
(49, 3, 122),
(50, 3, 13),
(51, 3, 131),
(52, 3, 132),
(53, 3, 14),
(54, 3, 141),
(55, 3, 142),
(56, 3, 15),
(57, 3, 151),
(58, 3, 152),
(59, 3, 153);

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `role_name`, `state`) VALUES
(1, 'EMPLOYEE', 'Active'),
(2, 'TRAINEE', 'Active'),
(3, 'MANAGER', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `webcam_data`
--

CREATE TABLE `webcam_data` (
  `id` bigint UNSIGNED NOT NULL,
  `reference` int DEFAULT NULL,
  `last_seen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_breaks`
--
ALTER TABLE `daily_breaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_faces`
--
ALTER TABLE `employee_faces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_salary`
--
ALTER TABLE `employee_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobtitle`
--
ALTER TABLE `jobtitle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leavegroup`
--
ALTER TABLE `leavegroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leavetype`
--
ALTER TABLE `leavetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_views`
--
ALTER TABLE `report_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_types`
--
ALTER TABLE `salary_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_template`
--
ALTER TABLE `schedule_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_extension`
--
ALTER TABLE `task_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webcam_data`
--
ALTER TABLE `webcam_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=443;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `daily_breaks`
--
ALTER TABLE `daily_breaks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_faces`
--
ALTER TABLE `employee_faces`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=475;

--
-- AUTO_INCREMENT for table `employee_salary`
--
ALTER TABLE `employee_salary`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobtitle`
--
ALTER TABLE `jobtitle`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `leavegroup`
--
ALTER TABLE `leavegroup`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leavetype`
--
ALTER TABLE `leavetype`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `report_views`
--
ALTER TABLE `report_views`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_types`
--
ALTER TABLE `salary_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `schedule_template`
--
ALTER TABLE `schedule_template`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `task_extension`
--
ALTER TABLE `task_extension`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `webcam_data`
--
ALTER TABLE `webcam_data`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
