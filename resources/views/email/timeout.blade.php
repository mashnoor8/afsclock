# Hello {{ $name }} !<br>

#Your attendance details for {{ $date }} is given below:<br>

<strong>Timein :</strong> {{ $timein }}<br>
<strong>Timeout :</strong> {{ $timeout }}<br>
<strong>Total Hours :</strong> {{ $totalhours }} hours<br>

<p>If you have any query against this record, talk to your admin.</p>

Regards,<br>
<strong>#Attendance Keeper</strong>