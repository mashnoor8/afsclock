
 @component('mail::message')
 # Hello {{$data['firstname']}} {{$data['lastname']}} !

 You have recently requested us to reset your password to login attendance Keeper. <strong>This is the link which help you to reset your password. </strong>  {{$data['link']}} <br>
 Use the given link or the button given below to reset your password. 
    
@component('mail::button', ['url' => $data['link'], 'color' => 'green'])
Reset password 
@endcomponent


Regards,<br>
#Attendance Keeper
@endcomponent






