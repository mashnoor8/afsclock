Hello {{$firstname}} {{$lastname}} <br><br>

Thanks for choosing Attendance Keeper. You have recently unsubscribed to {{ $plan }}. <br>

<strong>Package Information</strong> <br><br>

<strong>Package Name: </strong>{{ $plan }} <br>
<strong>Subscription Start Date :</strong> {{ $start }}
<strong>Subscription Cancellation Date :</strong> {{ $end }}

For further assistance, please send us an email: <a href="info@attendancekeeper.net"> info@attendancekeeper.net</a><br>

#Thanks <br>
- Your friends at Attendance Keeper