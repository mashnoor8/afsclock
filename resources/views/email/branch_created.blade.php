


Hello {{$firstname}} {{$lastname}} <br><br>

Thanks  for choosing Attendance Keeper. Your new attendance keeper branch {{$branch_name}} is successfully created and activated! <br>

<strong>Login Information</strong> <br><br>

<strong>Branch Name: </strong>{{ $branch_name }} <br>
<strong>Access URL: </strong><a href="https://attendancekeeper.net/login">https://attendancekeeper.net/login</a> <br>
<strong>Branch Admin Email: </strong><a href="{{$companyemail}}">{{$companyemail}}</a><br><br>

For further assistance, please send us an email: <a href="info@attendancekeeper.net"> info@attendancekeeper.net</a><br>

#Thanks <br>
- Your friends at Attendance Keeper