@extends('layouts.default')

@section('meta')
<title>Dashboard | Attendance Keeper</title>
<meta name="description" content="Attendance Keeper dashboard, view recent attendance, recent leaves of absence, and newest employees">
@endsection

@section('styles')
<style>
    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 310px;
        max-width: 800px;
        margin: 1em auto;
    }


    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    #chartdiv_attendance {
        width: 100%;
        height: 250px;
    }
  
    .size_chart{
        width: 100%;
        height: 250px;
    }
</style>
@endsection

@section('content')

<div class="container-fluid px-0">
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-title">Dashboard</h2>
        </div>
    </div>


    <div class="row mb-3">
        <div class="col-md-4">
            <div class="info-box shadow-sm p-3">
                <span class="info-box-icon bg-paste"><i class="ui icon user circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text pt-2">EMPLOYEES</span>
                    <div class="progress-group pt-2">

                        <div class="stats_d">
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>Regular</td>
                                        <td>@isset($emp_typeR) {{ $emp_typeR }} @endisset</td>
                                    </tr>
                                    <tr>
                                        <td>Trainee</td>
                                        <td>@isset($emp_typeT) {{ $emp_typeT }} @endisset</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box shadow-sm p-3">
                <span class="info-box-icon  bg-ash"><i class="ui icon clock outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text pt-2">ATTENDANCES</span>
                    <div class="progress-group pt-2">
                        <!-- <div class="progress sm">
                                <div class="progress-bar progress-bar-green" style="width: 100%"></div>
                            </div> -->
                        <div class="stats_d">
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>Total Employees</td>
                                        <td>@isset($is_online_now) {{ $is_online_now }} @endisset</td>
                                    </tr>
                                    <tr>
                                        <td>Present Employees(Today)</td>
                                        <td>@isset($is_offline_now) {{ $is_offline_now }} @endisset</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box shadow-sm p-3">
                <span class="info-box-icon bg-info"><i class="ui icon home"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text pt-2">LEAVES OF ABSENCE</span>
                    <div class="progress-group pt-2">
                        <!-- <div class="progress sm">
                                <div class="progress-bar progress-bar-white" style="width: 100%"></div>
                            </div> -->
                        <div class="stats_d">
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>Approved</td>
                                        <td>@isset($emp_leaves_approve) {{ $emp_leaves_approve }} @endisset</td>
                                    </tr>
                                    <tr>
                                        <td>Pending</td>
                                        <td>@isset($emp_leaves_pending) {{ $emp_leaves_pending }} @endisset</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row px-3">
        
            @if($chart_1  != 0)
            <div class="col-md-6 box box-success box-left shadow-sm  ">
            <div class="box-header with-border p-3">
            <h3 class="box-title">Attendance Superstars : {{ \Carbon\Carbon::now()->format('F') }}  </h3>
                
            </div>
            <div class="box-body">
            <div id="chartdiv_attendance"></div>
 </div>
            @else
            <div class="col-md-6 box box-success text-center box-left shadow-sm d-flex ">
            <div class="box-header with-border p-3">
            <h3 class="box-title">Attendance Superstars : {{ \Carbon\Carbon::now()->format('F') }}  </h3>
                
            </div>
            <div class="box-body p-5 ">
                <div class="box-body text-center m-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Attendance</h4>
                    </div>
                     </div>
            @endif
           
        </div>
        <div class="col-md-6 pr-0  d-flex">
            <div class=" box box-success box-left shadow-sm ">
            <div class="box-header with-border p-3">
            <h3 class="box-title">Task Superstars</h3>
                
            </div>
            <div class="box-body">
            @if($task_count>0)
            
                <figure class="highcharts-figure">
                    <div id="taskchart" style="height: 250px;"></div>
                </figure>

            @else
            <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Task Superstars</h4>
                    </div>
            
            @endif
           
            </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 d-flex">
            <div class="row p-3">
 
  	                @isset($expire_drive)

                @foreach($expire_drive as $drive_ex)


                <div class="alert alert-warning alert-dismissible fade show  shadow-sm" role="alert">
                    <strong>Driving License of </strong>{{$drive_ex->firstname}} @isset($drive_ex->mi) {{$drive_ex->mi}} @endisset {{$drive_ex->lastname}} ( {{$drive_ex->idno}} ) will be expired on {{date('d-M-Y', strtotime($drive_ex->drive_ex_date))}}.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                @endforeach

                @endisset 
 	
 	
 	                @isset($expire_document)

                @foreach($expire_document as $document_ex)


                <div class="alert alert-warning alert-dismissible fade show  shadow-sm" role="alert">
                    <strong>Documents of </strong>{{$document_ex->firstname}} @isset($document_ex->mi) {{$document_ex->mi}} @endisset {{$document_ex->lastname}} ( {{$document_ex->idno}} ) will be expired on {{date('d-M-Y', strtotime($document_ex->document_ex_date))}}.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                @endforeach

                @endisset   
                
                



                <div class="col-12  box box-success box-left shadow-sm ">
                    <div class="box-header with-border p-3">
                        <h3 class="box-title">Latest Pending Tasks</h3>
                        <a class="float-right" href="{{ url('taskmanager') }}"><i class="ui icon arrow alternate circle right"></i></a>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    @if(count($pending_tasks)>0)
                    <div class="box-body">
                        <table class="table table-striped responsive table-hover nobordertop">
                            <thead>
                                <tr>
                                    <th class="text-left">Assigned to</th>
                                    <th class="text-left">Title</th>
                                    <th class="text-left">Deadline</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($pending_tasks)
                                @foreach($pending_tasks as $task)
                                <tr>
                                    <!-- <td onload="pending_task_reminder()">{{ $task->title }}</td> -->
                                    <td class="text-left name-title">{{$task->firstname}} @isset($task->mi) {{$task->mi}} @endisset {{$task->lastname}}</td>
                                    <td class="text-left name-title">{{ $task->title }}</td>

                                    <td> @php echo e(date('M d, Y', strtotime($task->deadline )))@endphp</td>
                                    <!-- <td> @php echo e(date('H:i', strtotime($task->deadline )))@endphp</td> -->
                                </tr>
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Pending Task</h4>
                    </div>
                    @endif
                </div>
                <div class="col-12 box  box-success box-left shadow-sm  mt-3">
                    <div class="box-header with-border p-3">
                        <h3 class="box-title">Newest Employees</h3>
                        <a class="float-right" href="{{ url('employees') }}"><i class="ui icon arrow alternate circle right"></i></a>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    @if(count($newest_employee)>0)
                    <div class="box-body">
                        <table class="table responsive table-striped table-hover nobordertop">
                            <thead>
                                <tr>
                                    <th class="text-left">Name</th>
                                    <th class="text-left">Position</th>
                                    <th class="text-left">Start</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($newest_employee)
                                @foreach ($newest_employee as $data)
                                <tr>
                                    <td class="text-left name-title">{{ $data->firstname }}
                                        {{ $data->lastname }}
                                    </td>
                                    <td class="text-left">{{ $data->jobtitle }}</td>
                                    <td class="text-left">@php echo e(date('M d, Y', strtotime($data->startdate))) @endphp</td>
                                </tr>
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No New Employees</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4 px-3 pt-3 pb-3 d-flex">
        

  
 	
  
           
            <div class="col-12 box box-success shadow-sm">
                <div class="box-header with-border px-3">
                    <h3 class="box-title">Today's Latest Attendances</h3>
                    <a class="float-right" href="{{ url('attendance') }}"><i class="ui icon arrow alternate circle right"></i></a>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                @if(count($today_present)>0)
                <div class="box-body">
                    <table class="table responsive table-striped table-hover nobordertop">
                        <thead>
                            <tr>

                                <th class="text-left">Name</th>
                                <th class="text-left">Status</th>
                                <th class="text-left">Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($today_present)
                            @foreach($today_present as $v)

                            <tr>


                                <td class="text-left name-title">{{$v->employee}}</td>
                                <td>@isset($v->status_timein) {{$v->status_timein }} @endisset / @isset($v->status_timeout) {{$v->status_timeout }} @endisset </td>
                                <td>@php echo e(date('h:i:s A', strtotime($v->timein))); @endphp</td>
                            </tr>


                            @endforeach
                            @endisset

                        </tbody>
                    </table>
                </div>
                @else
                <div class="box-body text-center p-5">
                    <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                    <h4> No Recent Attendance</h4>
                </div>
                @endif
            </div>

           </div>


        <div class="col-md-4 d-flex">
            <div class="row p-3">
                <!-- @isset($expire_res)

                @foreach($expire_res as $brp_ex)


                <div class="alert alert-warning alert-dismissible fade show  shadow-sm" role="alert">
                    <strong>Biometric Residence Permit of </strong>{{$brp_ex->firstname}} @isset($brp_ex->mi) {{$brp_ex->mi}} @endisset {{$brp_ex->lastname}} ( {{$brp_ex->idno}} ) will be expired on {{date('d-M-Y', strtotime($brp_ex->bpr_ex_date))}}.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                @endforeach

                @endisset -->
                
                <!-- @isset($expire_passport)

                    @foreach($expire_passport as $passport_ex)


                    <div class="alert alert-warning alert-dismissible fade show  shadow-sm" role="alert">
                        <strong>Passport of </strong>{{$passport_ex->firstname}} @isset($passport_ex->mi) {{$passport_ex->mi}} @endisset {{$passport_ex->lastname}} ( {{$passport_ex->idno}} ) will be expired on {{date('d-M-Y', strtotime($passport_ex->passport_ex_date))}}.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    @endforeach

                @endisset -->

                <div class="col-12 mb-4 box box-success shadow-sm">
                    <div class="box-header with-border p-3 ">
                        <h3 class="box-title">Documents Expiration</h3>
                        <a class="float-right" href="{{ url('leaves') }}"><i class="ui icon arrow alternate circle right"></i></a>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    @if($expire_documents_employees)
                    <div class="box-body">
                        <table class="table responsive table-striped table-hover nobordertop">
                            <thead>
                                <tr>
                                    <th class="text-left">Name</th>
                                    <th class="text-text">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($expire_documents_employees)
                                @foreach ($expire_documents_employees as $employee)
                                <tr>
                                    <td class="text-left name-title">{{ $employee->firstname }}
                                        {{ $employee->lastname }}
                                    </td>
                                    <td class="text-center">
                                    <i title="Some of the papers about to expire." class="info circle icon text-warning"></i>
                                    </td>
                                    <td class="text-right">
                                    <a class="button ui" href="{{ url('/profile/view/'. $employee->id) }}"><i class="align justify icon"></i> View</a>
                                      
                                    <a class="button ui" href="{{ url('notices/document_expiry_notice/'.$employee->id) }}"><i class="paper plane icon"></i> Notify</a>
                                    </td>
                                </tr>
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Recent Leave Of Absence</h4>
                    </div>
                    @endif
                </div>
                <div class="col-12 mb-4 box box-success shadow-sm">
                    <div class="box-header with-border p-3 ">
                        <h3 class="box-title">Recent Leaves of Absence</h3>
                        <a class="float-right" href="{{ url('leaves') }}"><i class="ui icon arrow alternate circle right"></i></a>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    @if(count($recentleaves)>0)
                    <div class="box-body">
                        <table class="table responsive table-striped table-hover nobordertop">
                            <thead>
                                <tr>
                                    <th class="text-left">Name</th>
                                    <th class="text-left">From</th>
                                    <th class="text-left">To</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($recentleaves)
                                @foreach ($recentleaves as $leaves)
                                <tr>
                                    <td class="text-left name-title">{{ $leaves->firstname }}
                                        {{ $leaves->lastname }}
                                    </td>
                                    <td class="text-left">@php echo e(date('M d, Y', strtotime($leaves->leavefrom))) @endphp</td>
                                    <td class="text-left">@php echo e(date('M d, Y', strtotime($leaves->leaveto))) @endphp</td>
                                </tr>
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Recent Leave Of Absence</h4>
                    </div>
                    @endif
                </div>

                <div class="col-12 mt-2 box box-success shadow-sm">
                    <div class="box-header with-border p-3">
                        <h3 class="box-title">Upcoming Employee's Birthdays</h3>
                        <a class="float-right" href="{{ url('reports/employee-birthdays') }}"><i class="ui icon arrow alternate circle right"></i></a>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    @if(count($recent_birthday)>0)
                    <div class="box-body">
                        <table class="table responsive table-striped table-hover nobordertop">
                            <thead>
                                <tr>
                                    <th class="text-left">Name</th>
                                    <th class="text-left">Birthday</th>
                                    <th class="text-left">Days Left</th>

                                </tr>
                            </thead>
                            <tbody>
                                @isset($recent_birthday)
                                @foreach ($recent_birthday as $birthday)
                                
                                <tr>
                                    <td class="text-left name-title">{{ $birthday->firstname }} @isset( $birthday->mi ) {{ $birthday->mi }} @endisset
                                        {{ $birthday->lastname }}
                                    </td>
                                    <td class="text-left">@php echo e(date('M d', strtotime($birthday->birthday))) @endphp</td>
                                    <td>@if($birthday->left_days == 0) <i class="fa fa-birthday-cake text-danger" title="Today is {{ $birthday->firstname }}'s birthday"></i> @else{{ $birthday->left_days }} days left @endif</td>
                                </tr>
                                
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Recent Leave Of Absence</h4>
                    </div>
                    @endif
                </div>




            </div>

        </div>
    </div>






    @endsection
    @section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

    <!-- Tasks Condition chart -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>


    <!-- Chart code -->
    <script>

        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            
            // Create chart instance
            var chart = am4core.create("chartdiv_attendance", am4charts.XYChart);
            var object_attendance = JSON.parse('<?php echo ($chartData1) ?>');
            var fa = object_attendance.length;

            for (i = 0; i < fa; i++) {
                object_attendance[i]['color'] = chart.colors.next();
                object_attendance[i]['bullet'] = "https://www.amcharts.com/lib/images/faces/A04.png";
            }

           
            // Add data
            chart.data = object_attendance;
           
            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "name";
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.minGridDistance = 0;
            categoryAxis.renderer.inside = false;
            categoryAxis.renderer.labels.template.fill = am4core.color("#000");
            categoryAxis.renderer.labels.template.fontSize = 10;
            categoryAxis.renderer.grid.template.disabled = true;


            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.strokeDasharray = "0,0";
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.min = 0;
            valueAxis.renderer.grid.template.disabled = true;
            // Do not crop bullets
            chart.maskBullets = false;

            // Remove padding
            chart.paddingBottom = 0;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "points";
            series.dataFields.categoryX = "name";
            series.columns.template.propertyFields.fill = "color";
            series.columns.template.propertyFields.stroke = "color";
            series.columns.template.column.cornerRadiusTopLeft = 0;
            series.columns.template.column.cornerRadiusTopRight = 0;
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/b]";

            // Add bullets
            var bullet = series.bullets.push(new am4charts.Bullet());
            var image = bullet.createChild(am4core.Image);
            image.horizontalCenter = "middle";
            image.verticalCenter = "bottom";
            image.dy = 20;
            image.y = am4core.percent(100);
            image.propertyFields.href = "bullet";
            image.tooltipText = series.columns.template.tooltipText;
            image.propertyFields.fill = "color";
            image.filters.push(new am4core.DropShadowFilter());

        }); // end am4core.ready()
        $(document).ready(function(){
$("g[aria-labelledby]").hide();
});
    </script>


    <script>
        var object_task = JSON.parse('<?php echo ($chartData_task) ?>');

        var i;
        var f = object_task.length;
        var fname = [];
        var completed = [];
        var pending = [];
        var failed = [];
        for (i = 0; i < f; i++) {
            fname.push(object_task[i].firstname);
        }
        for (i = 0; i < f; i++) {
            completed.push(parseInt(object_task[i].completed));
        }
        for (i = 0; i < f; i++) {
            pending.push(parseInt(object_task[i].pending));
        }
        for (i = 0; i < f; i++) {
            failed.push(parseInt(object_task[i].failed));
        }

        Highcharts.chart('taskchart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: fname
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Tasks'
                },
                visible: false
            },
            credits: {
               enabled: false
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Completed',
                data: completed
            }, {
                name: 'Pending',
                data: pending
            }, {
                name: 'Failed',
                data: failed
            }]
        });
    </script>
    <script>
        $('#dataTables-example').DataTable({
            responsive: true,
            pageLength: 10,
            lengthChange: false,
            searching: false,
            ordering: true
        });
    </script>

    @endsection
