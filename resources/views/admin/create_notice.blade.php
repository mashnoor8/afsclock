@extends('layouts.default')

@section('meta')
    <title>My Tasks | Attendnace Keeper</title>
    <meta name="description"
          content="Attendance Keeper my schedules, view my schedule records, view present and previous schedules.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')


<section>
    <div class="container-fluid px-0">
    
        <div class="row">
            <div class="col">
                 <h2 class="page-title">Create Notice
                   
                 </h2>
            </div>
        </div>
       
        <div class="content">
            <form id="add_schedule_form" action="{{ url('notices/store') }}" class="ui form" method="post"
                accept-charset="utf-8">
                @csrf
                <div class="field">
                    <label>Employee</label>
                    <select id="selEmployee" class="ui uppercase"  name="reference">
                        <option value="">All</option>
                        @isset($employees)
                            @foreach ($employees as $data)
                                <option class= "" value="{{ $data->id }}"
                                        data-id="{{ $data->id }}">{{ $data->firstname }} @isset($data->mi) {{$data->mi}} @endisset{{ $data->lastname }} @if($data->idno) ( {{ $data->idno }} ) @endif</option>
                                    
                            @endforeach
                        @endisset
                    </select>
                </div>
                
                <div class=" field ">
                    <label>Comment</label>
                    <textarea class="bg-white" rows="5" name="message" required></textarea>
                </div>

                <div class=" field ">
                    <label>Date</label>
                    <input type='text'  id="datetimepicker" name="valid_till" class="form-control bg-white" autocomplete="off"  required>

                </div>

                <div class=" field float-right">
       
                    <button class="ui positive right labeled icon button" type="submit" name="submit"><i class="ui checkmark icon"></i> Create</button>
                </div>

            </form>
        </div>
        

    </div>
</section>

               

@endsection
@section('scripts')
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>
   <script>
    $('#datetimepicker').datetimepicker();
   </script>

@endsection