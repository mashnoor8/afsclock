<!-- <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'> -->
<table width="100%" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee</th>
                                        <th>Description</th>
                                        <th>Leave From</th>
                                        <th>Leave To</th>
                                        <th>Return Date</th>
                                        <th>Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($data)
                                        @foreach ($data as $d)
                                        <tr>
                                        <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>
                                            <td>{{ $d->firstname }} @isset($d->mi) {{ $d->mi }} @endisset {{ $d->lastname }}</td>
                                            <td>{{ $d->type }}</td>
                                            <td>@php echo e(date('D, M d, Y', strtotime($d->leavefrom))) @endphp</td>
                                            <td>@php echo e(date('D, M d, Y', strtotime($d->leaveto))) @endphp</td>
                                            <td>@php echo e(date('M d, Y', strtotime($d->returndate))) @endphp</td>
                                            <td><span class="">{{ $d->status }}</span></td>
                                            <td class="text-center">
                                            @if($d->status != 'Approved')
                                            <a href="{{ url('leaves/edit/'.$d->id) }}" class="ui circular basic icon button tiny"><i class="icon edit outline"></i></a>
                                            @endif
                                               
                                                <!-- <a href="{{ url('leaves/delete/'.$d->id) }}" class="ui circular basic icon button tiny"><i class="icon trash alternate outlin"></i></a> -->
                                                <a  title="Delete" class="ui circular basic icon button tiny delete " onclick="delete_confirmation('{{$d->id}}')"><i class="trash alternate outline icon"></i></a>
                                                @isset($data->comment)
                                                    @if($data->comment != null)
                                                        <button class="ui circular basic icon button tiny uppercase" title='{{ $d->comment }}'><i class="ui icon comment alternate"></i></button>
                                                    @endif
                                                @endisset
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endisset
                                    <tr>
                                    <td colspan="8" align="center">
                                    <div class="row">
                                    <div class="col">
                                    {{ $data->links() }}
                                    </div>

                                    </div>
                                    </td>
                                    </tr>
                                </tbody>
</table>