@extends('layouts.default')

    @section('meta')
        <title>Schedules | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper schedules, view all employee schedules, add schedule or shift, edit, and delete schedules.">
    @endsection

    @section('styles')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/timePicker.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/timepicki.css') }}">


    @endsection

    @section('content')

    <section>
      <div class="py-2">
          <h3 class="page-title">Create Schedule Template
            <a href="{{ url('schedules/templates') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
          </h3>
      </div>
    </section>


<section>
  <div class="container">
    <div class="row">
      <div class="col px-0">
        <div class="col box mt-3">
            <div class="template_form">
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
              <!-- <p>Fill up to the form below to create a schedule template.</p> -->

            <form id="add_schedule_form" action="{{ url('schedules/templates/add') }}" class="ui form my-5" method="post" accept-charset="utf-8">
                @csrf

                <div class="field">
                    <h4>Template Name</h4>
                    <input type="text" placeholder="Template Name" name="template_name" value="{{  old('template_name') }}"  required>
                </div>

                <div class="field">
                    <h4>Saturday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In</label>
                        <input type="text" name="sat_time_in" class="time_element" value="{{  old('sat_time_in') }}" />
                        <!-- <input type="time" id="timepicker" name="sat_time_in"> -->
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="sat_time_out" value="{{  old('sat_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Sunday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In</label>
                        <input type="text" class="time_element" name="sun_time_in" value="{{  old('sun_time_in') }}" >
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="sun_time_out" value="{{  old('sun_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Monday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In </label>
                        <input type="text" class="time_element" name="mon_time_in" value="{{  old('mon_time_in') }}" >
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="mon_time_out" value="{{  old('mon_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Tuesday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In</label>
                        <input type="text" class="time_element" name="tue_time_in" value="{{  old('tue_time_in') }}" >
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="tue_time_out" value="{{  old('tue_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Wednesday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In</label>
                        <input type="text" class="time_element" name="wed_time_in" value="{{  old('wed_time_in') }}" >
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="wed_time_out" value="{{  old('wed_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Thursday</h4>
                    <div class="row">
                      <div class="col-6">
                        <label for="">Time In</label>
                        <input type="text" class="time_element" name="thu_time_in" value="{{  old('thu_time_in') }}" >
                      </div>
                      <div class="col-6">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="thu_time_out" value="{{  old('thu_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                    <h4>Friday</h4>
                    <div class="row">
                      <div class="col-6 ">
                        <label for="">Time In</label>
                        <input type="text"  class="time_element" name="fri_time_in" value="{{  old('fri_time_in') }}" >
                      </div>
                      <div class="col-6 ">
                        <label for="">Time Out</label>
                        <input type="text" class="time_element" name="fri_time_out" value="{{  old('fri_time_out') }}" >
                      </div>
                    </div>
                </div>

                <div class="field">
                  <div class="row">
                    <div class="col-6 ">
                      <h4>Break Allowence</h4>
                      <input type="number" id="appt " name="break_allowence" placeholder="e.g: 30" >
                      
                    </div>
                    <div class="timepicker"></div>
                    <div class="col-6 pt-4">
                      <p class=" pt-1"><i class="exclamation circle icon"></i> Break Allowence will be count in minutes</p>
                      
                    </div>
                  </div>  
                </div>


              <div class="field">
                <button class="ui button green" type="submit" value="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

    

    @endsection

    @section('scripts')
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> -->

<!-- <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script> -->

<script type="text/javascript">

$(document).ready(function(){
    $(".time_element").timepicki();
  });


</script>


<!-- 

<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script> -->

<!-- <script src="{{ asset('/assets/js/timePicker.js') }}"></script> -->
<script src="{{ asset('/assets/js/timepicki.js') }}"></script>


<!-- 
<script>
$('.appt').datepicker({
  language: 'en', dateFormat: 'hh:mm'
});

</script> -->
    @endsection
