@extends('layouts.default')

    @section('meta')
        <title>Employees | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper employees, view all employees, add, edit, delete, and archive employees.">
    @endsection

    @section('content')

    <div class="container-fluid">
        <div class="row">
            <h2 class="page-title">EMPLOYEES
                <a class="btn btn-sm btn_upper float-right" href="{{ url('employees/new') }}"><i class="ui icon plus"></i>Add</a>
            </h2>
        </div>
        

        <div class="row ">
            <div class="col-12 px-0">
            @if(count($data)>0)  
                    <div class="box box-success ">
                        <div class="box-body">
                        <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">
                            <div class="six wide field">
                                <input id="search" type="text" name="searchbox" value="" placeholder="Search anything"  autocomplete="off">
                               
                            </div>
                            <div class="three wide field">
                                    <select id="department" class="ui dropdown uppercase"  name="department">
                                        <option value="all">All departments</option>
                                        @isset($departments)
                                            @foreach($departments as $department)
                                                <option  value="{{ $department->department}}" > {{ $department->department}}</option>
                                            @endforeach
                                        @endisset                                   
                                    </select>
                                </div>
                                <div class="three wide field">
                                    <select  id="job" class="ui dropdown uppercase"  name="job">
                                        <option value="all">All Job title</option>
                                        @isset($jobtitles)
                                            @foreach($jobtitles as $jobtitle)
                                                <option  value="{{ $jobtitle->jobtitle}}" > {{ $jobtitle->jobtitle}}</option>
                                            @endforeach
                                        @endisset                                         
                                    </select>
                                </div>

                           
                        </div>
                        
                        </form>
                    @include('admin.employee-table')
                   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                        </div>
                    </div>
            @else
            
            <div class="row">
                <div class="col-6  offset-3">
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4 class= "py-2">!  <span class="px-1"></span> No Employee Available </h4>
                    </div> 
                </div>
            </div>
                  
                           
            @endif          
            </div>
        </div>

    </div>


    <span id="_url" style="display: none;">{{url('/')}}</span>

    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ],searching: true,ordering: true});
    </script>
    
    <script>

function fetch_data(query, page, department , job)
 {
    var url = document.getElementById("_url").textContent;

  $.ajax({
   url: url + "/employee/search?query="+query + "&page="+page + "&department=" +department+ "&job=" +job,
   success:function(data)
   {
    $('thead').html('');
    $('tbody').html('');
    $('tbody').html(data);
   }
  })
 }

 $( document ).ready(function() {

                $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);

                var query = $('#search').val();
                var department = $('#department').val();
                var job = $('#job').val();

                $('li').removeClass('active');
                $(this).parent().addClass('active');
                fetch_data(query, page , department,job );
                });


    $( "#search" ).keyup(function() {
        var query = $('#search').val();
        var page = $('#hidden_page').val();
        var department = $('#department').val();
        var job = $('#job').val();
        console.log(query , department , job);
        fetch_data(query, page , department, job);
    });
    $( "#department" ).change(function() {
        var query = $('#search').val();
        var page = $('#hidden_page').val();
        var department = $('#department').val();
        var job = $('#job').val();
        console.log(query , department , job);
        fetch_data(query, page, department, job);
    });
    $( "#job" ).change(function() {
        var query = $('#search').val();
        var page = $('#hidden_page').val();
        var department = $('#department').val();
        var job = $('#job').val();
        console.log(query , department , job);
        fetch_data(query, page, department , job);
    });



});




</script>
    @endsection
