@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('styles')
    
@endsection

@section('content')

<section>
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Cases 
                    <a href="javascript::void()" class="ui  btn_upper  button mini show offsettop5 float-right" id="test">Create Case </a>

                </h2>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-7 ">

            <!-- comments -->
                    <div class="ui middle aligned divided list">
                    @foreach($data as $case)
                    <div class="ui comments">
                        <div class="comment">
                            <a class="avatar">
                               <h2> <i class="clipboard icon"></i></h2>
                            </a>
                            <div class="content">
                            
                                <div class="metadata ml-0">
                                   <a href="{{url('case/case_view/' . $case->id) }}"><h3 class="text-uppercase">{{ $case->subject}}</h3></a> 
                                </div>
                                <p class="text">
                                {{ Str::limit($case->comment, 200) }}
                                </p>
                                <div class="col ">
                                    <div class="row pt-2">
                                        <div class="col">
                                            <p><i class="clock icon"></i> {{ $case->created_at }}</p>
                                        </div>
                                        <div class="col">
                                            <p><i class="comment icon"></i> {{ count($case->responses) }} </p>
                                           
                                        </div>
                                        <div class="col">
                                        @if ($case->status == 0)
                                            <p><i class="check circle outline icon"></i> Resolved</p>
                                        @else     
                                            <p><i class="check circle outline icon"></i> Pending</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach    
                    </div>
            </div>
        </div>
    </div>
</section>


<div class="ui modal test">
    <i class="close icon"></i>
    <div class="header">Create Case</div>
    <div class="image content">
        <form id="edit_task_form" action="{{ url('support/create_case') }}" class="ui form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            @csrf              
            <div class="field">
                <div class="field">
                    <label>Subject</label>
                    <input class="uppercase" name="subject" value="" type="text">
                </div>
                <div class=" field">
                    <label>Comment</label>
                    <textarea class="" rows="5" name="comment"></textarea>
                </div>
                <div class="field">
                    <label>Upload Screenshot</label>
                    <input class="is-fullwidth " type="file" name="image" placeholder="image " >
                                    
                </div>
            </div>
            <div class=" field float-right">
                <input type="hidden" name="id">
                <button class="ui positive right labeled icon button" type="submit" name="submit"><i class="ui checkmark icon"></i> Submit</button>
                <!-- <a class="ui black cancel  button" ><i class="ui times icon"></i> Cancel</a> -->
            </div>
        </form>
    </div>
    
  
</div>


@endsection


@section('scripts')
    <script type="text/javascript">

        $(function(){
            $("#test").click(function(){
                $(".test").modal('show');
            });
            $(".test").modal({
                closable: true
            });
        });

   
    </script>

<script>

    function validateFile() {
        var f = document.getElementById("imagefile").value;
        var d = f.lastIndexOf(".") + 1;
        var ext = f.substr(d, f.length).toLowerCase();
        if (ext == "jpg" || ext == "jpeg" || ext == "png") {
        } else {
            document.getElementById("imagefile").value = "";
            $.notify({
                    icon: 'ui icon times',
                    message: "Please upload only jpg/jpeg and png image formats."
                },
                {type: 'danger', timer: 400});
        }
    }

</script>
@endsection 