@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection
@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<section>
  <h3 class="page-title text-left">Salary</h3>
</section>
<section>
  <div class="container ">
    <div class="row">
      <div class="col-6">
        
      </div>
      <div class="col-6 text-right">   
        <!-- <a class="btn btn-sm btn_upper float-right " href="{{ url('admin/salary_types') }}"><i class="ui icon th list"></i>Salary Types</a> -->
        <a class="btn btn-sm btn_upper float-right " href="{{ url('admin/employee_salary') }}"><i class="ui icon money bill alternate outline"></i>Employee Salary</a>
        <!-- <a class="btn btn-sm btn_upper float-right " href="{{ url('admin/holidays') }}"><i class="ui icon flag"></i>Holidays</a> -->
      </div> 
      </div>
  </div>

</section>


<section>
  <div class="container py-5">
    <div class="row">
      <div class="col-5 offset-1 ">
          <div class="container card shadow  p-5">
            <h3 class="text-center py-2"> <i class="fa fa-calendar-check-o pr-2" aria-hidden="true"></i>Monthly Salary Calculation</h3>
            <p class="text-center">Choose the desire month and year to calculate salary.</p>
              <form class="mt-5 ui form" action="{{ url('/admin/calculate_monthly_salary') }}" method="POST">
              @csrf
                  <div class="row pt-3">
                    <div class="col ">
                      <div class="field">                       
                        <select id="month" name="month" class="ui fluid dropdown"  >
                            <option value="">Select Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                      </div>
                    </div>
                    <div class="col">
                      <div class="field">   
                        <select id="month" name="year" class="ui fluid dropdown">
                            <option value="">Select Year</option>
                            
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                        </select>
                      </div>
                    </div>
                    <div class="col">
                      <div class="field pt-4">
                        <button class="btn btn_upper btn-sm ml-2" type="submit"><i class="calculator icon"></i> Calculate Salary</button>
                      </div>
                    </div>
                  </div>
              </form>
          </div>
      </div>
      <div class="col-5 ">
        <div class="container card shadow p-5">
          <h3 class="text-center py-2"><i class="fa fa-clock-o pr-2" aria-hidden="true"></i>Hourly Calculation</h3>
          <p class="text-center">Choose the desire date range to calculate salary</p>
          <div class="row ">
            <div class="col-12 pt-5">
              <form class="ui form mt-3" method="post" action="{{ url('/admin/calculate_hourly_salary')}}">
                @csrf
                  <div class="row">
                    <div class="col-6">
                      <div class="field">
                          <input id="datefrom" type="text" name="start" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                          <i class="ui icon calendar alternate outline calendar-icon"></i>
                      </div>
                    </div>  
                    <div class="col-6">

                      <div class="field ">
                          <input id="dateto" type="text" name="end" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                          <i class="ui icon calendar alternate outline calendar-icon"></i>
                      </div>
                    </div> 
                    <div class="col-12 pt-4">
                       <button class="btn btn_upper btn-sm ml-2 mb-2" type="submit"><i class="calculator icon"></i> Calculate Salary</button>
                    </div> 

                  </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

</section>


<!-- <section class="py-5 mt-5">
 
</section> -->

<section class="">
  
</section>

@endsection

@section('scripts')
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

<script type="text/javascript">
  $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });
</script>

@endsection
