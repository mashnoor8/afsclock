@extends('layouts.default')

@section('meta')
    <title>Application For Missing Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('content')
<div class="container-fluid px-0">
        <div class="row">
          <div class="col">
            <h2 class="page-title">Application For Missing Attendances
              <!-- <a href="{{ url('clock') }}" class="btn btn-sm btn_upper float-right"><i
                          class="ui icon clock"></i>Clock In/Out</a> -->
                          <a href="{{ url('applications/missing_attendances') }}" class="btn btn-sm btn_upper float-right"><i class="ui icon chevron left"></i>Return</a>
            </h2>
          </div>
        </div>


        <div class="row">
       
            <div class="col-12 pt-3">
            <div class="box box-success ">
                <div class="box-body">
                <form action="{{ url('admin/approval/missing_attendance') }}" class="ui form" method="post" accept-charset="utf-8">
                <input type="hidden" name="id"  value="{{ $application->id }}" />

                    @csrf
                <h5><strong>@php echo e(date('M d, Y', strtotime($application->created_at))) @endphp</strong></h5>
                <h5>To,</h5>
                <p>Admin</p>
                <p>@isset($company) {{ $company }} @endisset</p>
                <p><strong>Subject:</strong> Application for approve my missing attendance for @php echo e(date('M d, Y', strtotime($application->date))) @endphp</p>
                <br>
                <p><strong>Dear Sir,</strong></p>
                <p>I, hereby, would like to inform you that i could not give attendance for @php echo e(date('M d, Y', strtotime($application->date))) @endphp . The reason behind my missing attendance is: {{ $application->reason }}.
                I entered into the office at <strong> @php echo e(date('h:i:s a', strtotime($application->timein))) @endphp</strong>.@if($application->timeout != "N\A") On the other hand, I left the office at 
                <strong> @php echo e(date('h:i:s a', strtotime($application->timeout))) @endphp @endif</strong></p>
                <p>I hope you will understand my situation and grant my attendance for @php echo e(date('M d, Y', strtotime($application->date))) @endphp which i missed on that day. I will be glad if you approve my missing attendance.</p>
                <br>
                <h5>Thanking you,</h5>
                <p>Yours sincerely,</p>
                <p>{{ $applicant->firstname }} {{ $applicant->lastname}} ({{ $applicant->idno}})</p>
                <p>{{ $jobtitle }}</p>
                <p>@isset($company) {{ $company }} @endisset</p>
                
                <div class="fields">
                        <div class="sixteen wide field">
                            <label>Comment(if any)</label>
                            <textarea class="" rows="5" name="comment" autocomplete="off" ></textarea>
                        </div>
                </div>
                <div class="fields">
                        <div class="sixteen wide field">
                            <label><strong>Current Status</strong></label>
                            <ul>
                            <li  class="text-danger">
                            
                            @if($attendance_exist != Null)
                            The attendance for this user on @php echo e(date('M d, Y', strtotime($attendance_exist->date))) @endphp has already been exist.
                            @else
                            The attendance for this user on @php echo e(date('M d, Y', strtotime($application->date))) @endphp does not exist.
                            @endif
                            
                            </li>
                         @if($schedule_timein == "Weekend") <li class="text-danger"><strong>@php echo e(date('l', strtotime($application->date))) @endphp is the weekend for this employee. </strong></li> @endif
                            @if($attendance_exist != Null)
                            <li  class="text-danger">The record of the existence attendance for @php echo e(date('M d, Y', strtotime($attendance_exist->date))) @endphp 
                            <ul>
                           @isset($attendance_exist->timein) <li  class="text-info"><strong>Time in: </strong> @php echo e(date('H:i:s A', strtotime($attendance_exist->timein))) @endphp</li> @endisset
                           @if($attendance_exist->timeout) <li  class="text-info"><strong> Time out: </strong> @php echo e(date('H:i:s A', strtotime($attendance_exist->timeout))) @endphp</li> @else <li  class="text-info"> @if($applicant->gender == 'MALE') He @else She @endif didn't clock out against this attendance yet !</li> @endif
                           @isset($attendance_exist->status_timein) <li  class="text-info"><strong>Status Timein: </strong>{{ $attendance_exist->status_timein }}</li> @endisset
                           @if($attendance_exist->status_timeout != Null) <li  class="text-info"><strong>Status Timeout: </strong>{{ $attendance_exist->status_timeout }}</li> @else <li  class="text-info"><strong>Status Timeout:</strong> N/A</li> @endif
                           @isset($schedule_timein) <li  class="text-primary"><strong>Scheduled Timein: </strong>{{ $schedule_timein }}</li> @endisset
                           @isset($schedule_timeout) <li  class="text-primary"><strong>Scheduled Timeout: </strong>{{ $schedule_timeout }}</li> @endisset


                            </ul>
                            </li>
                            @endif
                            </ul>
                        </div>
                </div>
                <br>
                <div class="box-footer bg-white pb-3 ">
                    <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Approve</button>

                    <a  title="Mark done" class="ui red small button tiny delete "  title="Mark done" onclick="delete_confirmation('{{$application->id}}')"><i class="ui times icon"></i> Deny</a>
                    <!-- <a class="ui red small button" href="{{ url('application/missing_attendances/deny/'.$application->id) }}"><i class="ui times icon"></i> Deny</a> -->
                    <a class="ui red small button" href="{{ url('applications/missing_attendances') }}"><i class="ui times icon"></i> Cancel</a>
                </div>
                </form>




                </div>
                </div>
            </div>
        </div>

</div>



 <!-- Delete confirmation modal -->
 <div class="ui modal small delete  " name="denyConfirmation">
      <div class="header">Please leave a reason to deny this application</div>
      <div class="content">
        
            
      <form id="edit_task_form" action="{{ url('admin/applications/deny/reason') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                           
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <label>Reason</label>
                                    <textarea class="" rows="5" name="comment">@isset($application->comment){{ $application->comment }}@endisset</textarea>
                                </div>
                            </div>

                            <div class=" float-right bg-white py-3 pr-4">
                                <input type="text" name="id" id="application_id" value="" hidden>
                                <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Deny </button>
                                
                                <a class="ui red small button" href="{{ url('/applications/'.$application->id) }}"><i class="ui times icon"></i> Cancel</a>
                            </div>
                    </form>
        
         </div>     
      </div>
        
@endsection

@section('scripts')
<script>
// Delete Confirmation 
 function delete_confirmation(applicationID)
        {
        //   console.log(applicationID);

          $( document ).ready(function() {
            $("div[name=denyConfirmation]").modal('show');
          });

          document.getElementById("application_id").value = applicationID ;
          
        }


</script>
@endsection