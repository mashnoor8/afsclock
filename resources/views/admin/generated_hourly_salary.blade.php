@extends('layouts.default')


@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection
@section('styles')
@endsection

@section('content')

<div class="py-2" >
<h1 class="page-title">Total Hourly Salary
<a href="{{ url('admin/salary') }}" class="ui  button btn_upper mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
  
</h1>
</div>


<!-- <section class="py-3">
  <div class="container text-center">
    
  </div>
</section> -->


<section>
  <div class="container">
    <div class="row">
    
      <div class="col p-5 box">
          <!-- <div class="col-6 text-right">
            <button onclick="exportTableToCSV('members.csv')">Export HTML Table To CSV File</button>
          </div> -->
          <div class="col ">
                  <h3 class="text-center py-2"><i class="fa fa-clock-o pr-2" aria-hidden="true"></i>Hourly Salaries </h3>
                  <div class="text-right pb-2">
                    <button class="btn  btn-primary" onclick="exportTableToCSV('members.csv')"> <i class="fa fa-download pr-2" aria-hidden="true"></i>Export </button>
                  </div>
                  

                  <table class="ui celled table table-hover" id="salaryTable">
                    <thead class="thead-salary">
                      <tr>
                      <th>Name</th>
                      <th>IDNO</th>
                      <th>Total Hours</th>
                      <th> Salary/Hour</th>
                      <th>Total Work Days </th>
                      <th>Total Late Arrival </th>
                      <th>Total Early Departure</th>
                      <th>Calculated Salary</th>
                      <th>Currency</th>
                    </tr>
                    </thead>
                    <tbody>
                      @isset($salary_collection)
                      @foreach($salary_collection as $salary)
                      <tr>
                        <td> {{ $salary->employee }}  </td>
                        <td>{{ $salary->idno }}</td>
                        <td data-label="Age">{{ $salary->total_working_hour }}</td>
                        <td data-label="id">{{ $salary->gross_salary }}</td>
                        <td data-label="Job">{{$salary->total_working_days}}</td>
                        
                        
                        <td data-label="Age">{{ $salary->total_late_arrival }}</td>
                        <td data-label="Job">{{ $salary->total_early_departure}}</span></td>
                        <td>{{ number_format((float)$salary->calculated_salary, 2, '.', '')}}</td>
                        <td class="text-uppercase">{{ $salary->currency }}</td>
                      </tr>
                      @endforeach
                      @endisset
                      
                    </tbody>
                  </table>
                  <div class="text-right">
                    <h4 class=""><i class="fa fa-money pr-2" aria-hidden="true"></i> Total: <span class="px-1"></span> {{number_format((float)$sub_total, 2, '.', '')}} /=</h4>
                  </div>
              
          </div>
      </div>
    </div>
   

</div>
</section>





@endsection

@section('scripts')

<script>
function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}



function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

</script>

@endsection
