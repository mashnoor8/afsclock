@extends('layouts.default')

    @section('meta')
        <title>Leave of Absence | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper leave of absence, view all employee leaves of absence, edit, comment, and approve or deny leave requests.">
    @endsection

    @section('content')

    <div class="container-fluid px-0">
    
        <div class="row">
            <div class="col">
                 <h2 class="page-title">Leave of Absence
                    <a href="{{ url('fields/leavetype') }}" class="ui btn_upper  button mini offsettop5 float-right "><i class="ui icon calendar alternate outline"></i>Create Leave Type</a>
                 </h2>
            </div>
        </div>

        <div class="row">
       
                <div class="col-12">
                @if(count($data)>0)
            
                    <div class="box box-success">
                        <div class="box-body">
                        <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">
                            <div class="six wide field">
                                <input id="search" type="text" name="searchbox" value="" placeholder="Search here"  autocomplete="off">
                               
                            </div>
                            <div class="three wide field">
                                <select class="ui dropdown uppercase" id="situation" name="situation">
                                    <option value="all">All </option>
                                    <option value="Approved">Approved</option>
                                    <option value="Pending">Pending</option> 
                                    <option value="Decline">Decline</option>                                       

                                </select>
                            </div>

                           
                        </div>
                        
                        </form>
                    @include('admin.leaves-table')
                   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                        </div>
                    </div>
                @else
                <div class="row">
                    <div class="col-6  offset-3">
                        <div class="box-body text-center p-5">
                            <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                            <h4 class= "py-2">  <span class="px-1"></span> No Leaves </h4>
                        </div> 
                    </div>
                </div>    
                @endif
                </div>  

        </div>
    </div>
    
     <!-- Delete confirmation modal -->
     <div class="ui modal small delete  " name="delete_leave_Confirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->
    
    
    
    <span id="_url" style="display: none;">{{url('/')}}</span>
    <span id="delete_leaves_url" style="display: none;">{{url('leaves/delete')}}</span>
    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],searching: true,ordering: true});
    </script>

 <script>

        function fetch_data(query, page,situation)
        {
            var url = document.getElementById("_url").textContent;
            console.log(situation);

        $.ajax({
        url: url + "/leave/search?query="+query + "&page="+page +"&situation="+situation,
        success:function(data)
        {
            $('thead').html('');
            $('tbody').html('');
            $('tbody').html(data);
        }
        })
        }

        $( document ).ready(function() {

                        $(document).on('click', '.pagination a', function(event){
                        event.preventDefault();
                        var page = $(this).attr('href').split('page=')[1];
                        $('#hidden_page').val(page);

                        var query = $('#search').val();
                        var situation = $('#situation').val();

                        
                        console.log(situation);

                        $('li').removeClass('active');
                        $(this).parent().addClass('active');
                        fetch_data(query, page,situation);
                        });


            $( "#search" ).keyup(function() {
                var query = $('#search').val();
                var page = $('#hidden_page').val();
                var situation = $('#situation').val();

                console.log(situation);
                fetch_data(query, page,situation);
            });
            $( "#situation" ).change(function() {
                var query = $('#search').val();
                var page = $('#hidden_page').val();
                var situation = $('#situation').val();

                console.log(situation);
                fetch_data(query, page, situation);
            });


        });

 // Delete Confirmation 
 function delete_confirmation(leaveID)
        {
          console.log(leaveID);

          $( document ).ready(function() {
            $("div[name=delete_leave_Confirmation]").modal('show');
          });

          var url = document.getElementById('delete_leaves_url').textContent;
          console.log(url);
          url = url+ "/" + leaveID;
          console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }



</script>
    @endsection
