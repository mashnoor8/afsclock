@extends('layouts.default')

@section('meta')
    <title>My Tasks | Attendnace Keeper</title>
    <meta name="description"
          content="Attendance Keeper my schedules, view my schedule records, view present and previous schedules.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')


<section>
    <div class="container-fluid px-0">
    
        <div class="row">
            <div class="col">
                 <h2 class="page-title">Notice
                    <a href="{{ url('/notices/create') }}" class="ui btn_upper  button mini offsettop5 float-right ">Create </a>
                 </h2>
            </div>
        </div>

        <div class="ui grid">
        @isset($notices)
            @foreach($notices as $notice)

                @php 
                    $now =  \Carbon\Carbon::now($timezone);
                    $valid_time =  \Carbon\Carbon::parse($notice->valid_till) ;

                    if( $valid_time->gt($now)){
                        $time =$valid_time->diffInDays($now) . ' Days' ;

                        if($time == 0 ){
                            $time =$valid_time->diffInHours($now) . ' Hours' ;
                        }

                    }
                    else{
                        $time =null;
                    }

                @endphp 

                @isset($time)    
                <div class="four wide column d-flex">
                    <div class="ui card">
                        <div class="content">
                            <div class="header pl-0">{{ $notice->noticeBY->firstname }} {{ $notice->noticeBY->lastname }}   <span class="float-right"> <a href="{{URL('notices/delete/' . $notice->id)}}"> <i class="trash icon"></i></a> <a href="{{URL('notices/edit/' . $notice->id)}}"><i class="edit icon"></i></a></span></div>
                            <div class="meta pt-1">
                                <span> {{  $time  }} </span>
                                <a>@if($notice->reference) 
                                   {{$notice->noticeFor-> firstname}}  {{$notice->noticeFor-> lastname}} 
                                
                                    @else  {{"Everyone"}}

                                    @endif
                                </a>
                            </div>
                            <p class="pt-2">{{$notice->message }} </p>
                        </div>
                   
                @endisset

            @endforeach
        @endisset
        </div>

</div>        

    </div>
</section>

               

@endsection
@section('scripts')

@endsection