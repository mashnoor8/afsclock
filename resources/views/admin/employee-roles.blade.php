@extends('layouts.default')

    @section('meta')
        <title>Employee Roles | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper roles, view all employee roles, add roles, edit roles, and delete roles.">
    @endsection
    
    @section('content')
    @include('admin.modals.modal-add-roles')
    @include('admin.modals.modal-edit-role')

    <div class="container-fluid">
        <div class="row">
            <h2 class="page-title">User Roles
                <button class="ui  button mini btn_upper offsettop5 btn-add float-right"><i class="ui icon plus"></i>Add</button>
                <a href="{{ url('dashboard') }}" class="ui   button btn_upper mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
            </h2>
        </div>

        <div class="row">
            @if ($errors->any())
            <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">There were some errors with your submission</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <div class="row">
            <div class="box box-success">
                <div class="box-body">
                    <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                        <thead>
                            <tr>
                                <th>Role Name</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($roles)
                                @foreach ($roles as $role)
                                <tr>
                                    <td>{{ $role->role_name }}</td>
                                    <td>{{ $role->state }}</td>
                                    <td class="align-right">
                                        <a href="{{ url('/users/roles/permissions/edit') }}/{{ $role->id }}" class="ui circular basic icon button tiny">
                                        <i class="ui icon tasks"></i></a>
                                        <button type="button" class="ui circular basic icon button tiny btn-edit" data-id="{{ $role->id }}"><i class="icon edit outline"></i></button>
                                       
          <a  title="Delete" class="ui circular basic icon button tiny delete "  onclick="delete_confirmation('{{$role->id}}')"><i class="trash alternate outline icon"></i></a>

                                        <!-- <a href="{{ url('/users/roles/delete/'.$role->id) }}" class="ui circular basic icon button tiny"><i class="icon trash alternate outline"></i></a> -->
                                    </td>
                                </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>


 <!-- Delete confirmation modal -->
 <div class="ui modal small delete  " name="deleteConfirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->



    <span id="delete_url" style="display: none;">{{url('/users/roles/delete')}}</span>
    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: true,ordering: true});
    
    $('.btn-edit').click(function(event) {
        var id = $(this).attr('data-id');
        var url = $("#_url").val();
        $.ajax({
            url: url + '/user/roles/get/',type: 'get',dataType: 'json',data: {id: id},headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
            success: function(response) {
                $state = response['state'];
                $('.edit input[name="id"]').val(response['id']);
                $('.edit input[name="role_name"]').val(response['role_name']);
                if ($state == 'Active') {
                    $('.ui.dropdown.state').dropdown({values: [{name: 'Active',value: 'Active', selected : true},{name: 'Disabled',value: 'Disabled'}]});
                } else if($state == 'Disabled') {
                    $('.ui.dropdown.state').dropdown({values: [{name: 'Active',value: 'Active'},{name: 'Disabled',value: 'Disabled', selected : true}]});
                } 
                $('ui.modal.edit').modal('show');
            }
        })
    });



       // Delete Confirmation 
       function delete_confirmation(roleID)
        {
          console.log(roleID);

          $( document ).ready(function() {
            $("div[name=deleteConfirmation]").modal('show');
          });

          var url = document.getElementById('delete_url').textContent;
          console.log(url);
          url = url+ "/" + roleID;
          console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }
    </script>
    
    @endsection