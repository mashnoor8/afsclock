@extends('layouts.default')
    @section('meta')
        <title>Reports | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper reports, view reports, and export or download reports.">
    @endsection

    @section('content')

    <section class="">
        <div class="container-fluid px-0">
            <div class="row pl-3">
                <h2 class="page-title">Reports</h2>
            </div>
            <div class="row">
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5  text-center shadow rounded">
                        <p class="display-4 text-info"><i class="ui icon users"></i></p>
                        <h3 class="pb-3">Employee List Report</h3>
                        <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                        
                        <a class="btn btn-sm btn-info" href="{{ url('reports/employee-list') }}"><i class="fa fa-ellipsis-h" ></i> View Report</a>
                    </div>
                        
                    </div>
                </div>
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5 text-center shadow rounded">
                    <p class="display-4 text-info"><i class="ui icon clock"></i></p>
                    <h3 class="pb-3">Attendance Report</h3>
                    <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                    
                    <a class="btn btn-sm btn-info" href="{{ url('reports/employee-attendance') }}"><i class="fa fa-ellipsis-h" ></i> View Report</a>
                    </div>
                    
                    </div>
                </div>
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5 text-center shadow rounded">
                    <p class="display-4 text-info"><i class="ui icon calendar plus"></i></p>
                    <h3 class="pb-3"> Employee Leave Report</h3>
                    <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                        <a class="btn btn-sm btn-info" href="{{ url('reports/employee-leaves') }}"><i class="fa fa-ellipsis-h" ></i> View Report</a>
                    </div>
                    
                    </div>
                </div>
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5 text-center shadow rounded">
                    <p class="display-4 text-info"><i class="ui icon chart pie"></i></p>
                    <h3 class="pb-3"> Organization's Profile</h3>
                    
                    <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                        <a class="btn btn-sm btn-info" href="{{ url('reports/organization-profile') }}"><i class="fa fa-ellipsis-h" ></i> View Report </a>
                    </div>
                    </div>

                </div>
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5 text-center shadow rounded">
                    <p class="display-4 text-info"><i class="ui icon birthday cake"></i></p>
                    <h3 class="pb-3">  Employee Birthdays</h3>
                    <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                        <a class="btn btn-sm btn-info" href="{{ url('reports/employee-birthdays') }}"><i class="fa fa-ellipsis-h" ></i> View Report </a>
                    </div>
                    </div>
                </div>
                <div class="col-4 pb-5  ">
                    <div class="col bg-white card p-5 text-center shadow rounded">
                    <p class="display-4 text-info"><i class="ui icon address book outline"></i></p>
                    <h3 class="pb-3">  User Account Report</h3>
                    <!-- <p class="small py-3">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to </p> -->
                    <div class="col"> 
                        <a class="btn btn-sm btn-info" href="{{ url('reports/user-accounts') }}"> <i class="fa fa-ellipsis-h" ></i> View Report </a>
                    </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <!-- <div class="container-fluid">
        <div class="row">
            <h2 class="page-title">Reports</h2>
        </div>

        <div class="row">
            <div class="box box-success">
                <div class="box-body">
                <table width="100%" class=" table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                    <thead>
                        <tr>
                            <th>Report name</th>
                            <th class="odd">Last Viewed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="{{ url('reports/employee-list') }}"><i class="ui icon users"></i> Employee List Report</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 1)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ url('reports/employee-attendance') }}"><i class="ui icon clock"></i> Employee Attendance Report</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 2)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ url('reports/employee-leaves') }}"><i class="ui icon calendar plus"></i> Employee Leaves Report</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 3)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                       
                        <tr>
                            <td><a href="{{ url('reports/organization-profile') }}"><i class="ui icon chart pie"></i> Organization's Profile</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 5)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ url('reports/employee-birthdays') }}"><i class="ui icon birthday cake"></i> Employee Birthdays</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 7)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ url('reports/user-accounts') }}"><i class="ui icon address book outline"></i> User Accounts Report</a></td>
                            <td class="odd">
                                @isset($lastviews)
                                    @foreach ($lastviews as $views)
                                        @if($views->report_id == 6)
                                            {{ $views->last_viewed }}
                                        @endif
                                    @endforeach
                                @endisset
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div> -->

    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});
    </script>
    @endsection
