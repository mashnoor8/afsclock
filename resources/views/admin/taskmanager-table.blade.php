<!-- <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 6, "asc" ]]'> -->
<table width="100%" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th width="10%">Assigned to</th>
                                    <th width="10%">Assigned by</th>
                                    <th width="10%">Title</th>
                                    <th width="10%">Deadline</th>
                                    <th width="10%">Finish Date</th>
                                    <th width="5%">Status</th>
                                    <th width="10%">Comment</th>
                                    <th width="10%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($data)
                                    @foreach ($data as $task)

                                        <tr>
                                            <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>
                                            <td>@isset($task->assignedTo->firstname) {{ $task->assignedTo->firstname }}@endisset  @isset($task->assignedTo->mi) {{ $task->assignedTo->mi }}@endisset @isset($task->assignedTo->lastname) {{ $task->assignedTo->lastname }} @endisset</td>
                                            <td>@isset($task->assignedBy->firstname) {{ $task->assignedBy->firstname }}@endisset  @isset($task->assignedBy->mi) {{ $task->assignedBy->mi }}@endisset @isset($task->assignedBy->lastname) {{ $task->assignedBy->lastname }} @endisset</td>
                                            <td>@isset($task->title) {{ $task->title }} @endisset</td>
                                            @if($task->extended_deadline)
                                            <td style="color:#62b504;">@if($task->extended_deadline->new_deadline ) <i class="fa fa-calendar-o" aria-hidden="true"></i> @php echo e(date('M d, Y', strtotime($task->extended_deadline->new_deadline))); @endphp @php echo  nl2br("\n");@endphp <i class="fa fa-clock-o" aria-hidden="true"></i> @php echo e(date('h:i:s A', strtotime($task->extended_deadline->new_deadline))); @endphp <p class="text-primary text-danger mt-2"> <i class="fa fa-hourglass-start mr-2" aria-hidden="true"></i>Extended deadline</p> @else N/A @endif</td>
                                            @else
                                            <td style="color:#f70ad8;">@if($task->deadline ) <i class="fa fa-calendar-o" aria-hidden="true"></i> @php echo e(date('M d, Y', strtotime($task->deadline))); @endphp  @php echo  nl2br("\n");@endphp <i class="fa fa-clock-o" aria-hidden="true"></i> @php  echo e(date('  h:i A', strtotime($task->deadline))); @endphp @else N/A @endif</td>
                                            @endif
                                           
                                            <td style="color:#f70ad8;">@if($task->finishdate ) <i class="fa fa-calendar-o" aria-hidden="true"></i> @php echo e(date('M d, Y', strtotime($task->finishdate)));  @endphp  @php echo  nl2br("\n");@endphp <i class="fa fa-clock-o" aria-hidden="true"></i> @php echo e(date(' h:i A', strtotime($task->finishdate))); @endphp @else N/A @endif</td>
                                            <!-- <td>@isset($task->finishdate){{ $task->finishdate }}@endisset</td> -->
                                            <!-- @php
                                                $color = "red";
                                                $done_status = "Pending";
                                                if(isset($task->finishdate))
                                                    {

                                                        if($task->finishdate>$task->deadline)
                                                        {
                                                            $done_status = "Done (Delayed)";
                                                            $color = "red";
                                                        }
                                                        else
                                                        {
                                                            $done_status = "Done (In Time)";
                                                            $color = "green";
                                                        }
                                                    }

                                            @endphp -->

                                            @php
                                            if(($task->done_status == 0))
                                            {$color = "red";}
                                            else{$color = "blue";}
                                            
                                            @endphp
                                            <!-- <td><span class="{{ $color }}">{{ $done_status }}</span></td> -->
                                            <td><span class="{{ $color }}">@if($task->done_status == 0) <i class="fa fa-spinner" aria-hidden="true"></i> @else <i class="fa fa-check-circle" aria-hidden="true"></i> @endif</span></td>
                                            <td>{{ $task->comment }}</td>
                                            <td>
                                            <a href="{{ url('/task/view/'.$task->id) }}"
                                                class="ui circular basic icon button tiny"><i
                                                          title="View"  class="list ul icon"></i></a>
                                                          @if(($task->done_status == 0)) 
                                                <a href="{{ url('/task/edit/'.$task->id) }}"
                                                class="ui circular basic icon button tiny"><i
                                                title="Edit" class="icon edit outline"></i></a> @endif

                                                            <a  title="Delete" class="ui circular basic icon button tiny delete "  title="Delete" onclick="delete_confirmation('{{$task->id}}')"><i class="trash alternate outline icon"></i></a>
                                                            @if(($task->done_status == 0))   <a href="{{ url('task/extend-deadline/'.$task->id) }}"
                                                                    class="ui circular basic icon button tiny" title="Extend"><i
                                                                                class="icon calendar outline"></i></a> @endif
                                        </tr>

                                    @endforeach
                                    @endisset

                                    <tr>
                                    <td  colspan="8" align="center">
                                    <div class="row">
                                    <div class="col">
                                    {{ $data->links() }}
                                    </div>

                                    </div>
                                    </td>
                                    </tr>


                                
                                </tbody>
                            </table>
                         
