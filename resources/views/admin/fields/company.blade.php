@extends('layouts.default')
    
    @section('meta')
        <title>Companies | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper companies, view companies, and export or download companies.">
    @endsection

    @section('content')
    @include('admin.modals.modal-import-company')

    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">ADD COMPANY
                  <!--  <button class="ui  button mini btn_upper offsettop5 btn-import float-right"><i class="ui icon upload"></i> Import</button> -->
                    <a href="{{ url('export/fields/company' )}}" class="ui  button btn_upper mini offsettop5 btn-export float-right"><i class="ui icon download"></i> Export</a>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-body">
                        @if ($errors->any())
                        <div class="ui error message">
                            <i class="close icon"></i>
                            <div class="header">There were some errors with your submission</div>
                            <ul class="list">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form id="add_company_form" action="{{ url('fields/company/add') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                            <div class="field">
                                <label>Company Name <span class="help">e.g. "Apple Corporation"</span></label>
                                <input class="uppercase" name="company" value="" type="text">
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                            </div>          
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
            <div class="box box-success">
                <div class="box-body">
                    <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($data)
                            @foreach ($data as $company)
                            <tr>
                                <td>{{ $company->company }}</td>
                                <td class="align-right"> 
                                <!-- <a  title="Edit" class="ui circular basic icon button tiny delete " onclick="edit('{{$company->id}}')"><i class="edit icon"></i></a> -->
                                <!-- <a  title="Delete" class="ui circular basic icon button tiny delete " onclick="delete_confirmation('{{$company->id}}')"><i class="trash alternate outline icon"></i></a> -->
                                <a href="{{ url('fields/company/edit/'.$company->id) }}"  title="Edit" class="ui circular basic icon button tiny"><i class="edit icon"></i></a>
                                <a href="{{ url('fields/company/delete/'.$company->id) }}"  title="Delete" class="ui circular basic icon button tiny"><i class="icon trash alternate outline"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>



     <!-- Delete confirmation modal -->
     <div class="ui modal small delete  " name="delete_company_Confirmation">
      <div class="header">Delete</div>
      <div class="content">
      
       
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
        
        
    </div>
   
<!-- Delete confirmation modal ends here -->


  <!-- Edit modal -->
  <div class="ui modal small delete  " name="edit_company">
      <div class="header">Edit Department</div>
      <div class="content">
        
            
      <form id="add_department_form" action="{{ url('fields/company/edit') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                           
                            <input type="hidden" name="id" value="@isset($company->id){{ $company->id }}@endisset">
                            <div class="field">
                                <label>Department Name <span class="help">e.g. "Accounting"</span></label>
                                <input class="uppercase" name="department" value="{{ $company->company }}" type="text">
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                            </div>
                        </form>
        </div>
    </div>
<!-- Edit modal ends here -->

   
    <span id="delete_company_url" style="display: none;">{{url('fields/company/delete')}}</span>


    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: true,ordering: true});
    function validateFile() {
        var f = document.getElementById("csvfile").value;
        var d = f.lastIndexOf(".") + 1;
        var ext = f.substr(d, f.length).toLowerCase();
        if (ext == "csv") { } else {
            document.getElementById("csvfile").value="";
            $.notify({
            icon: 'ui icon times',
            message: "Please upload only CSV file format."},
            {type: 'danger',timer: 400});
        }
    }



     // Delete Confirmation 
     function delete_confirmation(companyID)
        {
          console.log(companyID);

          $( document ).ready(function() {
            $("div[name=delete_company_Confirmation]").modal('show');
          });

          var url = document.getElementById('delete_company_url').textContent;
          console.log(url);
          url = url+ "/" + companyID;
          console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }

             // Edit Company
     function edit(companyID)
        {
          console.log(companyID);

          $( document ).ready(function() {
            $("div[name=edit_company]").modal('show');
          });

         
        }
    </script>

    @endsection


  
