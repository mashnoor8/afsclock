@extends('layouts.default')

    @section('meta')
        <title>Leave Types | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper leave type, view leave types, add or edit leave types and export or download leave types.">
    @endsection

    @section('content')
    @include('admin.modals.modal-import-leavetypes')

    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">ADD LEAVE TYPES
                    <a href="{{ url('fields/leavetype/leave-groups') }}" class="ui mini button btn_upper offsettop5 float-right"><i class="icon calendar check outline"></i>Leave Groups</a>
                   <!-- <button class="ui button btn_upper mini offsettop5 btn-import float-right"><i class="ui icon upload"></i>Import</button> -->
                    <a href="{{ url('export/fields/leavetypes' )}}" class="ui  button btn_upper mini offsettop5 btm-export float-right"><i class="ui icon download"></i>Export</a>
                    <a href="{{ url('leaves') }}" class="ui btn_upper  button mini offsettop5 float-right "><i class="ui icon chevron left"></i>Return</a>
                
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-body">
                        @if ($errors->any())
                        <div class="ui error message">
                            <i class="close icon"></i>
                            <div class="header">There were some errors with your submission</div>
                            <ul class="list">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form id="add_leavetype_form" action="{{ url('fields/leavetype/add') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                            <div class="field">
                                <label>Leave name <span class="help">e.g. "Vacation Leave, Sick Leave"</span></label>
                                <input class="uppercase" name="leavetype" value="" type="text">
                            </div>
                            <div class="field">
                                <label>Credits <span class="help">e.g. "15" (days)</span></label>
                                <input class="" name="credits" value="" type="text">
                            </div>
                            <div class="grouped fields opt-radio">
                                <label class="">Choose Term</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="term" value="Monthly" checked="checked">
                                        <label>Monthly</label>
                                    </div>
                                </div>
                                <div class="field" style="padding:0px!important">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="term" value="Yearly">
                                        <label>Yearly</label>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-body">
                        <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Credits</th>
                                    <th>Term</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($data)
                                @foreach ($data as $leavetype)
                                <tr>
                                    <td>{{ $leavetype->leavetype }}</td>
                                    <td>{{ $leavetype->limit }} days</td>
                                    <td>{{ $leavetype->percalendar }}</td>
                                    <td>
                                    <a href="{{ url('fields/leavetype/edit/'.$leavetype->id) }}"  title="Edit" class="ui circular basic icon button tiny"><i class="edit icon"></i></a>
                                <a href="{{ url('fields/leavetype/delete/'.$leavetype->id) }}"  title="Delete" class="ui circular basic icon button tiny"><i class="icon trash alternate outline"></i></a>
                                    <!-- <a  title="Delete" class="ui circular basic icon button tiny delete " onclick="delete_confirmation('{{$leavetype->id}}')"><i class="trash alternate outline icon"></i></a> -->
                                    </td>
                                    <!-- <td class="align-right"><a href="{{ url('fields/leavetype/delete/'.$leavetype->id) }}" class="ui circular basic icon button tiny"><i class="icon trash alternate outline"></i></a></td> -->
                                </tr>
                                @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


  <!-- Delete confirmation modal -->
  <div class="ui modal small delete  " name="delete_leavetype_Confirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->

   
    <span id="delete_leavetype_url" style="display: none;">{{url('fields/leavetype/delete')}}</span>
    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: true,ordering: true});

    function validateFile() {
        var f = document.getElementById("csvfile").value;
        var d = f.lastIndexOf(".") + 1;
        var ext = f.substr(d, f.length).toLowerCase();
        if (ext == "csv") {} else {
            document.getElementById("csvfile").value = "";
            $.notify({
                icon: 'ui icon times',
                message: "Please upload only CSV file format."
            }, {
                type: 'danger',
                timer: 400
            });
        }
    }




     // Delete Confirmation 
     function delete_confirmation(leavetypeID)
        {
        //   console.log(leavetypeID);

          $( document ).ready(function() {
            $("div[name=delete_leavetype_Confirmation]").modal('show');
          });

          var url = document.getElementById('delete_leavetype_url').textContent;
          console.log(url);
          url = url+ "/" + leavetypeID;
        //   console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }
    </script>
    @endsection
