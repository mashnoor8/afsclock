@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('content')
<section class="py-2">
    <h3 class="page-title">Employee Salary
      <a href="{{ url('admin/salary') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
   
    </h3>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-6 p-5">
          <div class="col box shadow p-5"> 
            <h3 class="text-center py-3"> Assign Salary</h3> 
            @if ($errors->any())
                            <div class="ui error message">
                                <i class="close icon"></i>
                                <div class="header">There were some errors with your submission</div>
                                <ul class="list">
                                    
                                        <li>Oops! The salary for this employee is already exists.</li>
                                   
                                </ul>
                            </div>
                        @endif
            <form class="ui form" method="post" action="{{ url('admin/add_employee_salary')}}">
              @csrf
              <div class="field">
                <h4>Employee</h4>
                <select class="ui search dropdown getid uppercase" name="reference">
                    <option value="">Select Employee</option>
                    @isset($employee)
                        @foreach ($employee as $data)
                            <option value="{{ $data->id }}" data-id="{{ $data->id }}">{{ $data->lastname }}, {{ $data->firstname }}</option>
                        @endforeach
                    @endisset
                </select>
              </div>
              <div class="field">
                <h4>Salary Type</h4>
                <select class="ui search dropdown getid uppercase" name="salary_type">
                    <option value="">Select Salary Type</option>
                     <option value="1">Monthly</option>
                     <option value="2">Hourly</option>
                      
                </select>
              </div>
              <div class="field">
                <h4>Currency</h4>
                <select class="ui search dropdown getid uppercase" name="currency">
                    <option value="">Select Currency Type</option>
@include('currency-list')
                            <!-- <option value="bdt" data-id="bdt">BDT</option>
                            <option value="usd" data-id="usd">USD</option> -->

                </select>
              </div>
              <div class="field">
                <h4>Amount</h4>
                <input type="number"  step="0.001" name="salary_amount" placeholder="Type salary amount">
              </div>

              <button class="ui button blue ml-2" type="submit"><i class="plus square outline icon"></i> Add Salary</button>
            </form>
          </div>  
      </div>
      <div class="col-6  p-5">
        <div class="col box  shadow p-5"> 
          <h3 class="text-center py-3">Salary</h3>
          <table class="ui celled table">
            <thead>
              <tr><th>Employee</th>
              <th>Salary Type</th>
              <th>Amount</th>
              <th>Actions</th>
            </tr></thead>
            <tbody>
              @isset($salary_collection)
                @foreach($salary_collection as $employee_salary)
                  <tr>
                    <td data-label="Employee">{{$employee_salary->employee}}</td>
                    <td data-label="Salary Type">{{$employee_salary->salary_type}}</td>
                    <td data-label="Amount">{{$employee_salary->amount}} <span class="text-uppercase">{{$employee_salary->currency}}</span></td>
                    <td data-label="Actions">
                      <!-- <a href="{{ url('admin/salary_types') }}" class="ui circular basic icon button tiny"><i class="icon edit outline"></i></a> -->
                      <a href="{{ url('admin/delete_employee_salary/'. $employee_salary->id) }}" class="ui circular basic icon button tiny"><i class="icon trash "></i></a>
                    </td>
                  </tr>
                @endforeach
              @endisset
            </tbody>
          </table>
        </div>   
      </div>
    </div>
  </div>
</section>

@endsection

@section('scripts')

@endsection
