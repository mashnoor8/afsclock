@extends('layouts.default')

@section('meta')
    <title>My Tasks | Attendnace Keeper</title>
    <meta name="description"
          content="Attendance Keeper my schedules, view my schedule records, view present and previous schedules.">
@endsection

@section('styles')
<link href="{{ asset('/assets/vendor/datetimepicker/datetimepicker.css') }}" rel="stylesheet">

@endsection

@section('content')


<div class="container-fluid pt-3">
  <div class="row">
    <div class="col-md-12">
      <h2 class="page-title">Extend Deadline
          <a href="{{ url('taskmanager') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>

      </h2>
    </div>
  </div>
  <div class="row">
    <div class="col-6 card offset-3 p-5">
          <div class="container">
          <form id="add_schedule_form" action="{{ url('admin/update-deadline') }}" class="ui form" method="post"
                accept-charset="utf-8">
              @csrf
             
              <div class="field"> 
                <div class="display-4 text-center pb-4"> <i class="fa fa-retweet"></i> </div>
                <h5 class="py-1">Task: <span class="text-primary">{{$task->title}}</span></h5>
                <h5 class="py-1">Assigned to: <span class="text-info">{{$task->assignedBy->firstname}} {{$task->assignedBy->lastname}}</span></h5>
                <p>Present Deadline :  @if($task->extended_deadline) {{ $task->extended_deadline->new_deadline }} @else {{$task->deadline}} @endif</p>
              </div>
              <input type="number" name="task_id" value="{{$task->id}}"  hidden />
              <div class="field">
                  <label for="">Reason</label>
                  <textarea type="text" placeholder="Explain why you need to extend the deadline" name="reason" required></textarea>
              </div>
              <div class="field">
                  <label for="">New Deadline</label>
                  <!-- <input type="text" placeholder="Date" autocomplete="off" name="deadline" class="airdatepicker" data-position="top right"/> -->
                  <div class="form-group">

                      <input type='text' id="datetimepicker" name="new_deadline" class="form-control" required/>

                  </div>
              </div>


              <div class="actions">
                  <input type="hidden" name="id" value="">
                  <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i>
                      Submit
                  </button>
              </div>
          </form>
        </div>

    </div>


  </div>

</div>



  

@endsection
@section('scripts')

<script src="{{ asset('/assets/vendor/datetimepicker/datetimepicker.js') }}"></script>

<script type="text/javascript">

  $('#datetimepicker').datetimepicker();
</script>

@endsection
