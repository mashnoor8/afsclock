@extends('layouts.default')

    @section('meta')
        <title>Schedules | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper schedules, view all employee schedules, add schedule or shift, edit, and delete schedules.">
    @endsection

    @section('styles')
    <link href="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <style>
        /* .ui.active.modal {position: relative !important;} */
        .datepicker {z-index: 999 !important;}
        .datepickers-container {z-index: 9999 !important;}
    </style>
    @endsection

    @section('content')

    @include('admin.modals.modal-schedule-details')
    @include('admin.modals.modal-add-schedule')
   
    
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col">
                <h2 class="page-title">Schedules
                    <button class="btn btn-sm btn_upper float-right assign button"><i class="ui icon plus"></i> Assign Schedule</button>
                    <a href="schedules/templates" class="btn btn-sm btn_upper float-right "><i class="ui icon th list"></i> Schedule Templates</a>
                </h2>
            </div>    
        </div>
        
        <div class="row ">
            <div class="col-12 pt-2">
            @if(count($sorted_active_schedule_collection)>0)  
                <div class="box box-success ">
                    <div class="box-body">
                        <table width="100%" class=" table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th>Schedule</th>
                                    <th>Assigned Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @isset($sorted_active_schedule_collection)
                                @foreach ($sorted_active_schedule_collection as $sched)
                                <tr>
                                    <td>{{ $loop->index +1 }}</td>
                                    <td data-label="Name">{{$sched->employee}}</td>
                                    <td data-label="Age">{{$sched->template}}</td>
                                    <td data-label="Job">{{date('M d, Y', strtotime( $sched->created_at))}}</td>
                                    <td>
                                        
                                        
                                        <button class="btn-sm btn button ui circular basic icon  tiny " onclick="scheduleDetails('{{$sched->employee_id}}')" title="View"> <i class="align justify icon"></i></button>
                                        <div class="ui modal add details tiny" name="md{{$sched->employee_id}}">
                                            <div class="header ui-header text-center" > <h3 class="">Schedule</h3></div>
                                            
                                                <div class="content">
                                                    <div class="container px-5 py-2">
                                                        
                                                        <h3 class="py-1"><i class="fa fa-user-circle pr-2" aria-hidden="true"></i><span class="pr-2"></span>  {{$sched->employee}}</h3>

                                                        <table class="ui table">
                                                        
                                                            <tr>
                                                            <th>Days</th>
                                                            <td>Time</td>
                                                            </tr>  
                                                            <tr>
                                                            <th>Sun</th>
                                                            <td>

                                                            @if($all_templates[$sched->employee_id]->sunday)
                                                                {{$all_templates[$sched->employee_id]->sunday}}
                                                            @else
                                                                Day Off
                                                            @endif

                                                            </td>
                                                            </tr> 
                                                            <tr>
                                                            <th>Mon</th>
                                                            <td>

                                                            @if($all_templates[$sched->employee_id]->monday)
                                                                {{$all_templates[$sched->employee_id]->monday}}
                                                            @else
                                                                Day Off
                                                            @endif

                                                            </td>
                                                            </tr> 
                                                            <tr>
                                                            <th>Tue</th>
                                                            <td>
                                                            
                                                            @if($all_templates[$sched->employee_id]->tuesday)
                                                                {{$all_templates[$sched->employee_id]->tuesday}}
                                                            @else
                                                                Day Off
                                                            @endif

                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <th>Wed</th>
                                                            <td>
                                                            
                                                            @if($all_templates[$sched->employee_id]->wednesday)
                                                                {{$all_templates[$sched->employee_id]->wednesday}}
                                                            @else
                                                                Day Off
                                                            @endif

                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <th>Thu</th>
                                                            <td>

                                                            @if($all_templates[$sched->employee_id]->thursday)
                                                                {{$all_templates[$sched->employee_id]->thursday}}
                                                            @else
                                                                Day Off
                                                            @endif
                                                            
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <th>Fri</th>
                                                            <td>

                                                            @if($all_templates[$sched->employee_id]->friday)
                                                                {{$all_templates[$sched->employee_id]->friday}}
                                                            @else
                                                                Day Off
                                                            @endif

                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <th>Sat</th>
                                                            <td>
                                                             @if($all_templates[$sched->employee_id]->saturday)
                                                            {{$all_templates[$sched->employee_id]->saturday}}
                                                            @else
                                                                Day Off
                                                            @endif
                                                            </td>
                                                            </tr>
                                                            
                                                        </table>
                                                        <div class="actions py-2">
                                                            <div class="ui green deny button float-right mb-5"><i class="fa fa-check-circle pr-2" aria-hidden="true"></i>
                                                            
                                                            okay
                                                            </div>
                                                        </div>
                                                </div>          
                                            </div>   
                                        </div>
                                    
                                        
                                    </td>
                                </tr>
                                @endforeach
                            @endisset
                            </tbody>
                        </table>
                    </div>
                </div>

            @else
            
            <div class="row">
                <div class="col-6  offset-3">
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4 class= "py-2">!  <span class="px-1"></span> No Schedule Availabe </h4>
                    </div> 
                </div>
            </div>
            @endif    
            </div>    
        </div>

        <div>
            <!-- <button class="btn-sm btn button details btn_upper">modal</button> -->
        </div>

    </div>

    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>

    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15, lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ],searching: true,ordering: true});

    $('.jtimepicker').mdtimepicker({ format: 'h:mm:ss tt', hourPadding: true });
    $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });

    $('.ui.dropdown.getid').dropdown({ onChange: function(value, text, $selectedItem) {
        $('select[name="employee"] option').each(function() {
            if($(this).val()==value) {var id = $(this).attr('data-id');$('input[name="id"]').val(id);};
        });
    }});


    $('.ui.modal.add.medium')
      .modal('attach events', '.assign.button', 'show')
        ;
    $('.ui.modal.add.details')
        .modal('attach events', '.details.button', 'show')
        ;
    
    // Finds attendance details for specific row.
    function scheduleDetails($employee_id) {

    $( document ).ready(function() {
    $("div[name="+"md"+$employee_id+"]").modal('show');
    });
    
    }
    
    </script>
    @endsection
