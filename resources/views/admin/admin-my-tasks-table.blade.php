<table width="100%" class="table table-striped table-bordered table-hover mt-3">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Assigned By</th>
                                            <th>Title</th>
                                            <th>Deadline</th>
                                            <th>Finish Date</th>
                                            <th>Status</th>
                                            <th>Comment</th>
                                            <th class="align-right">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @isset($data)
                                            @foreach ($data as $task)
                                                <tr>
                                                <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>
                                                    <td>{{ $task->assignedBy->firstname }} {{ $task->assignedBy->lastname }}</td>
                                                    <td>{{ $task->title }}</td>
                                                    @if($task->extended_deadline)
                                                    <td style="color:#62b504;">@if($task->extended_deadline->new_deadline ) @php echo e(date('Y-m-d h:i:s A', strtotime($task->extended_deadline->new_deadline))); @endphp <sup class="text-primary">Extended deadline</sup> @else N/A @endif</td>
                                                    @else
                                                    <td style="color:#f70ad8;">@if($task->deadline ) @php echo e(date('Y-m-d h:i:s A', strtotime($task->deadline))); @endphp @else N/A @endif</td>
                                                    @endif

                                                    <td>@if($task->finishdate ) @php echo e(date('Y-m-d h:i:s A', strtotime($task->finishdate))); @endphp @else N/A @endif</td>
                                                    <!-- <td>@isset($task->finishdate){{ $task->finishdate }}@endisset</td> -->
                                                    <!-- @php
                                                        $color = "red";
                                                        $done_status = "Pending";
                                                        if(isset($task->finish_date))
                                                            {

                                                                if($task->finish_date>$task->deadline)
                                                                {
                                                                    $done_status = "Done (Delayed)";
                                                                    $color = "red";
                                                                }
                                                                else
                                                                {
                                                                    $done_status = "Done (In Time)";
                                                                    $color = "green";
                                                                }
                                                            }

                                                    @endphp -->
                                                    @php
                                            if(($task->done_status == 0))
                                            {$color = "red";}
                                            else{$color = "blue";}
                                            
                                            @endphp
                                            <!-- <td><span class="{{ $color }}">{{ $done_status }}</span></td> -->
                                            <td><span class="{{ $color }}">@if($task->done_status == 0) Pending @else Completed @endif</span></td>

                                                    <!-- <td><span class="{{ $color }}">{{ $done_status }}</span></td> -->
                                                    <td>{{ $task->comment }}</td>
                                                    <td class="align-right">
                                                    <a href="{{ url('task/view/'.$task->id) }}"
                                                        class="ui circular basic icon button tiny" title="View"><i
                                                                    class="list ul icon"></i></a>
                                                       
                                                                    @if(($task->done_status == 0)) 
                                                         <a  title="Mark done" class="ui circular basic icon button tiny delete "  title="Mark done" onclick="delete_confirmation('{{$task->id}}')"><i
                                                                    class="fa fa-check text-danger"></i></a>
                                                                    @endif

                                                            <a  title="Delete" class="ui circular basic icon button tiny delete "  title="Delete" onclick="delete_confirmation('{{$task->id}}')"><i class="trash alternate outline icon"></i></a>
                                                            @if(($task->done_status == 0))   <a href="{{ url('task/extend-deadline/'.$task->id) }}"
                                                                    class="ui circular basic icon button tiny" title="Extend"><i
                                                                                class="icon calendar outline"></i></a> @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endisset
                                        <tr>
                                    <td colspan="8" align="center">
                                    <div class="row">
                                    <div class="col">
                                    {{ $data->links() }}
                                    </div>

                                    </div>
                                    </td>
                                    </tr>
                                        </tbody>
                                    </table>