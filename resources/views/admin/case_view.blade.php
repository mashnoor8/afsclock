@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('styles')
    
@endsection

@section('content')

<section>
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12 ">

            <!-- comments -->
                <div class="ui middle aligned divided list">
                
                    <div class="ui grid">
                        
                        <div class="sixteen wide column px-0">

                            <div class="card text-center ">
                            <div class="card-header">
                                CASE 
                            </div>
                            <div class="card-body">
                                <h5 class="card-title text-left"> {{ $case->subject}}</h5>
                                <p class="card-text text-left"> {{$case->comment}}</p>
                                <p class="card-text text-left mt-4">Time : {{ date('d-m-Y H:s', strtotime($case->created_at)) }}</p>
                                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            </div>
                            <div class="card-footer text-muted ">
                            @if( $case->status == 1)
                                <a href="{{ url('case/solve/' . $case->id) }}" class="ui orange button float-right py-2 my-2"><i class="ui checkmark icon"></i> Mark As Solve </a>
                            @else
                                <button class="ui green button float-right py-2 my-2">Solved</button>
                            @endif
                            
                            </div>
                            </div>

                        </div>

                        @foreach($case_responses as $r)
                        
                        <div class="ten wide column ">
                            <div class="ui comments">
                                <div class="comment">
                                    <a class="avatar">
                                        <i class="user icon"></i>
                                    </a>
                                    <div class="content">
                                    
                                        <div class="metadata ml-0 ">
                                    
                                            <h6> {{ $r->user->firstname}} {{ $r->user->lastname}} </h6>
                                        </div>
                                        <p class="text">
                                        {{$r->response}}
                                        </p>
                                        
                                        <p>
                                            <div class="metadata ">
                                            <span class="date"><i class="clock icon"></i> {{ date('d-m-Y H:s', strtotime($r->created_at)) }}</span>
                                            </div>
                                        </p>

                                        @if(! empty($r->screenshot))
                                        <p class="text">
                                            <a href="{{url('/response_image/download/' . $r->id )}}" class=" float-left pt-2"><i class="paperclip icon"></i><span class = "text-primary"> Download Attachment </span> </a>
                                        </p>
                                        @endif
                                        
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        @endforeach 
                    
                    </div>

                
                    @if( $case->status == 1)
                
                        <form id="" action="{{ url('admin/case_view/comment') }}" class="ui form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf  
                            <input type="hidden" name="case_id" value= "{{ $case->id}}" >    
                            <input type="hidden" name="user_id" value= "{{ Auth::user()->id }}" > 

                                            
                        
                            <div class=" field">
                                <label>Comment</label>
                                <textarea class="bg-white" rows="5" name="response"></textarea>
                            </div>

                            <div class="field">
                                <label>Upload Screenshot</label>
                                <input class="is-fullwidth bg-white" type="file" name="comment_screenshot" placeholder="image " >               
                            </div>      
                        
                            <div class=" field ">
                                <!-- <input type="hidden" name="id"> -->
                                <button class="ui positive float-right labeled icon button" type="submit" name="submit"><i class="ui checkmark icon"></i> Send</button>
                            </div>
                        </form> 
                    @endif    
                </div>
                    

            </div>
        </div>
    </div>
</section>



@endsection

@section('scripts')
<script>



</script>
@endsection