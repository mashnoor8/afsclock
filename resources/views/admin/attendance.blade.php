@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('content')

@include('admin.modals.modal-delete-confirm')


    <div class="container-fluid px-0">
        <div class="row">
          <div class="col">
            <h2 class="page-title">Attendances
              <!-- <a href="{{ url('clock') }}" class="btn btn-sm btn_upper float-right"><i
                          class="ui icon clock"></i>Clock In/Out</a> -->
                          <a href="{{ url('applications/missing_attendances') }}" class="btn btn-sm btn_upper float-right"><i
                          class="bell outline icon"></i>Applications For Missing Attendances @if($missing>0) ({{ $missing }}) @endif</a>
            </h2>
          </div>
        </div>
        


        <div class="row">
       
            <div class="col-12 pt-3">

         
            @if(count($data)>0)  
                <div class="box box-success ">
                    <div class="box-body">
                    <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">
                            <div class="four wide field">
                                <input id="searchbox" type="text" name="searchbox" value="" placeholder="Search here"  autocomplete="off">
                               
                            </div>

                            <div class="two wide field">
                                <input id="start" type="text" name="start" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>

                            <div class="two wide field">
                                <input id="end" type="text" name="end" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>
                            <button id="filterButton" class="ui button  blue btn_upper"><i class="ui icon filter alternate"></i> Filter</button>
                        </div>
                        
            </form>
                    @include('admin.attendance-table')
                   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                    </div>
                
                </div>
            @else
            
            <div class="row">
                <div class="col-6  offset-3">
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4 class= "py-2">!  <span class="px-1"></span> No Attendance </h4>
                    </div> 
                </div>
            </div>    
            @endif    
            </div>
        </div>


    </div>


   <!-- Delete confirmation modal -->
    <div class="ui modal small delete  " name="deleteConfirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui red button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->

    <span id="_url" style="display: none;">{{url('/')}}</span>
    <span id="delete_url" style="display: none;">{{url('/attendance/delete')}}</span>

@endsection

@section('scripts')
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>


<script> $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });</script>

<script type="text/javascript">
  

  $('#dataTables-example').DataTable({
      responsive: true,
      pageLength: 15,
      lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ],
      searching: true,
      ordering: false
  });


  // // Attendance Details modal
  // $(function(){
  //     $("#attendance-details").click(function(e){
  //         $(".test").modal('show');
  //     });
  //     $(".test").modal({
  //         closable: true
  //     });
  //
  // });

          // Employee attendance search AJAX function
          function fetch_data( query,page, start, end)
          {
              var url = document.getElementById("_url").textContent;
              //  console.log(url);

          $.ajax({
          url: url + "/get/admin/attendance?start="+ start + "&page=" + page + "&end=" + end + "&query=" + query,
          success:function(data)
          {
              // console.log(data);
              $('#attendance_thead').html('');
              $('#attendance_tbody').html('');
              $('#attendance_tbody').html(data);
          }
          })
          }

          $( document ).ready(function() {

          $(document).on('click', '.pagination a', function(event){
          event.preventDefault();
          var page = $(this).attr('href').split('page=')[1];
          $('#hidden_page').val(page);

          var query = $('#searchbox').val();
          var start = $('#start').val();
          var end = $('#end').val();

          $('li').removeClass('active');
          $(this).parent().addClass('active');
          fetch_data( query,page, start, end);
          });


          $('#filterButton').click(function(e){
              e.preventDefault();
              
              var query = $('#searchbox').val();
              var start = $('#start').val();
              var end = $('#end').val();
              var page = $('#hidden_page').val();
              fetch_data( query,page, start, end);
          });
          });

  // Delete Confirmation 
  function delete_confirmation(attendanceID)
  {
    console.log(attendanceID);

    $( document ).ready(function() {
      $("div[name=deleteConfirmation]").modal('show');
    });

    var url = document.getElementById('delete_url').textContent;
    url = url+ "/" + attendanceID;

    console.log( url);
    var buttonID = "deleteButton";

    document.getElementById("deleteButton").setAttribute("href", url);
   

  }



  // Finds attendance details for specific row.
  function getAttendanceDetails(attendanceID) {

    $( document ).ready(function() {
      $("div[name="+"md"+attendanceID+"]").modal('show');
    });

    var BreakTbody = document.getElementById("break_tbody"+attendanceID);
    var url = document.getElementById('_url').textContent;

      $.get(url + '/personal/attendance/details', { attendanceID: attendanceID }, function(data){
          // console.log(data);
          // If there exist break informations
          if (data) {
            BreakTbody.innerHTML = "";

            var breaks = data[0];
            var break_hour = data[1];

            var break_hour_element = document.getElementById("breakHour"+attendanceID);
            break_hour_element.innerHTML = "";
            if (break_hour) {
              break_hour_element.innerHTML += ""+ break_hour + "";
            }else {
                var emoji = document.getElementById("emoji").innerHTML = "<i class='fa fa-smile-o pr-2' aria-hidden='true'></i>";
              break_hour_element.innerHTML += "Did not take a break";
            }

            if (breaks) {

              for(i = 0; i <= breaks.length; i++){
                  if(breaks[i]){

                    var end_time = "";
                    if (breaks[i].end_at) {
                      end_time = breaks[i].end_at;
                      var res = end_time.split(" ");
                      end_time = res[1];
                    }
                    else {
                      end_time = "Ongoing";
                    }

                    var start_time = breaks[i].start_at;
                    var res = start_time.split(" ");
                    start_time = res[1];

                      BreakTbody.innerHTML += "<tr>" +
                          "<td>"+ start_time +"</td>" +
                          "<td>" + end_time + "</td>"
                          +"</tr>";
                  }
              }
            }else{

            }
          }
          // If there exist no break records
          else{
            BreakTbody.innerHTML = "<h1> No break Data found</h1>";

          }
    })

    }

  // delete modal show

  $('.ui.modal.delete')
    .modal('attach events', '.ui.delete.button', 'show')
  ;      



</script>

    <!-- <script>
    var deleteLinks = document.querySelectorAll('.delete');

for (var i = 0; i < deleteLinks.length; i++) {
  deleteLinks[i].addEventListener('click', function(event) {
      event.preventDefault();

      var choice = confirm(this.getAttribute('data-confirm'));

      if (choice) {
        window.location.href = this.getAttribute('href');
      }
  });
}
</script> -->
@endsection
