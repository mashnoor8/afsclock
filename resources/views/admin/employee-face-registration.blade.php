@extends('layouts.default')

@section('content')
<section>
    <div class="py-2">
        <h3 class="page-title">Face Recognition
            <a href="{{ url('employees') }}" class="ui btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
        </h3>
    </div>
</section>

    <div class="container-fluid">
        <div class="row">
            <div class="col-4 px-0 ">
                <div id="my_camera"></div>
                <div id="results pt-2">Your captured image will appear here...</div>
                
                <a class="btn btn-sm btn_capture mt-2" type=button value="Take Snapshot" onClick="take_snapshot()">Take Snapshot</a>
            </div>
            <div class="col-8">
                @isset($employee_photos)
                    <div class="col text-right py-3">
                        <a href="javascript::void()" id="modify"><i class="cog icon"></i> Modify Images</a>
                    </div>
                    
                    <div class="row" id="photos">
                        @foreach($employee_photos as $photo)
                            <div class="card col-3 mx-2" style="width: 18rem;">
                                <img src="{{ asset('assets/faces/'.$photo->image_name)}}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <a href="{{url('/employee/delete_image/'.$photo->id)}}" class="btn btn-sm"><i class="trash alternate outline icon"></i> Delete</a>
                                </div>
                            </div>
                            
                        @endforeach
                    </div>
                @endisset
            </div>
        </div>
        
    </div>



@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/webcam.min.js') }}"></script>
    <script type="text/javascript">
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#my_camera');

        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap(function (data_uri) {
                // display results in page
                // console.log(data_uri)
                var photosElement = document.getElementById("photos");
                var card = document.createElement('div');
                card.classList.add("card");
                card.classList.add("col-3");
                card.classList.add("mx-2");
                var img = document.createElement('img');
                img.setAttribute('src',data_uri);
                img.classList.add("card-img-top");
                card.append(img);
                photosElement.append(card);
                
                                    
                $.ajax({
                    type:'POST',
                    url: '{{ url('employee/registerface/') }}/{{ $id }}' ,
                    data: {'_token' : '{{ csrf_token() }}', 'img_data' : data_uri},
                    success:function(data) {
                        // console.log(data.msg);
                    }
                });


            });
        }

        $("#modify").click(function(){
            location.reload();
        });

    </script>

@endsection
