@extends('layouts.default')

@section('meta')
    <title>New Employee | Attendance Keeper</title>
    <meta name="description" content="Attendance Keeper add new employee, delete employee, edit employee">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

   <!-- <section>
        <div class="container-fluid">
            <div class=" row">
                <h3 class="page-title">Create New </h3>
                <a href="{{ url('employees') }}" class="ui basic blue button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>

            </div>
        </div>    
   </section> -->


    <section>
        <div class="container-fluid">
        <div class="row">
            <h2 class="page-title">Create New 
            <a href="{{ url('employees') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>

            </h2>
           
            @if(count($leavegroup) == 0)
           
            <div class="alert alert-warning alert-dismissible fade show  shadow-sm" role="alert">
           
                   * Please create leavetype <strong><a href="{{ url('/fields/leavetype') }}">here</a> </strong>otherwise you can not assign leavetype to employee.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
     
            @endif
        </div>

        <div class="row">
            <div class="col-md-12 px-0">
                @if ($errors->any())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">There were some errors with your submission</div>
                        <ul class="list">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <form id="add_employee_form" action="{{ url('employee/add') }}" class="ui form custom" method="post"
                  accept-charset="utf-8" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="col-md-6 p-4 float-left">
                    <div class="box box-success  p-3">
                        <div class="box-header with-border">Personal Information</div>
                        <div class="box-body">
                            <div class="two fields">
                                <div class="field">
                                    <label>First Name*</label>
                                    <input type="text" required class="uppercase" name="firstname" value="{{ old('firstname') }}">
                                </div>
                                <div class="field">
                                    <label>Middle Name</label>
                                    <input type="text" class="uppercase" name="mi" value="{{ old('mi') }}">
                                </div>
                            </div>
                            <div class="field">
                                <label>Last Name*</label>
                                <input type="text" required class="uppercase" name="lastname" value="{{ old('lastname') }}">
                            </div>
                            <div class="field">
                                <label>Gender*</label>
                                <select id="gender" name="gender" required class="ui dropdown uppercase">
                                    <option >Select Gender</option>
                                    <option value="MALE" @if(old('gender') == "MALE" ) {{ 'selected' }} @endif >MALE</option>
                                    <option value="FEMALE" @if(old('gender') == "FEMALE") {{ 'selected' }} @endif >FEMALE</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Civil Status*</label>
                                <select  id="civilstatus" name="civilstatus" required class="ui dropdown uppercase">
                                    <option value="">Select Civil Status</option>
                                    <option value="SINGLE" @if(old('civilstatus') == 'SINGLE') {{ 'selected' }} @endif  >SINGLE</option>
                                    <option value="MARRIED" @if(old('civilstatus') == 'MARRIED' ) {{ 'selected' }} @endif >MARRIED</option>
                                    <option value="ANULLED" @if(old('civilstatus') == 'ANULLED' ) {{ 'selected' }} @endif >ANULLED</option>
                                    <option value="WIDOWED" @if(old('civilstatus') == 'WIDOWED') {{ 'selected' }} @endif >WIDOWED</option>
                                    <option value="LEGALLY SEPARATED" @if(old('civilstatus') == 'LEGALLY SEPARATED' ) {{ 'selected' }} @endif >LEGALLY SEPARATED</option>
                                </select>
                            </div>

                           
                                <div class="field">
                                    <label>Email Address (Personal)</label>
                                    <input type="email" name="emailaddress" value="{{ old('emailaddress') }}" class="lowercase" autocomplete="off" placeholder = "email" >
                                </div>
                                <div class="field">
                                    <label>Mobile Number*</label>
                                    <input type="text" required class="" name="mobileno" value="{{ old('mobileno') }}" >
                                </div>
                          
                            <div class="field">


                                <label>Date of Birth</label>
                                <input type="text" name="birthday" autocomplete="off" required value="{{ old('birthday') }}" class="airdatepicker"
                                       data-position="top right" placeholder="Date">
                            </div>
                            <div class="field">
                                <label>National ID</label>
                                <input type="text" class="uppercase"  name="nationalid" value="{{ old('nationalid') }}" placeholder="">
                            </div>
                            <div class="field">
                                <label>Present Address*</label>
                                <input type="text" class="uppercase"  autocomplete="off" required name="birthplace" value="{{ old('birthplace') }}"
                                       placeholder="City, Province, Country">
                            </div>
                            <div class="field">
                                <label>Permanent Address*</label>
                                <input type="text" class="uppercase"  autocomplete="off" name="homeaddress" value="{{ old('homeaddress') }}"
                                       placeholder="House/Unit Number, Building, Street, City, Province, Country">
                            </div>
                            <div class="field">
                                <label>Upload Profile photo</label>
                                <input class="ui file upload" value="{{ old('image') }}" id="imagefile" name="image" type="file"
                                       accept="image/png, image/jpeg, image/jpg" onchange="validateFile()">
                                <small>*Please upload a image less than 1 MB </small>
                                       
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-4 float-left">
                    <div class="box box-success  p-3">
                        <div class="box-header with-border">Employee Details</div>
                        <div class="box-body">
                            <h4 class="ui dividing header">Designation</h4>
                            
                            <div class="field">
                              <label>Company</label>
                              <input type="text" list="company" value ="{{$company->company}}"   name="company" class="ui search uppercase required" autocomplete="off" readonly>
                             
                            </div>
                            <div class="field">
                              <label>Department*</label>
                              <input type="text" list="department" name="department" class="ui search uppercase department" value="{{ old('department') }}" required autocomplete="off">
                              @isset($department)
                              <datalist id="department">
                                @foreach($department as $data)
                                <option>{{$data->department}}</option>
                                @endforeach
                              </datalist>
                              @endisset
                            </div>
                            <div class="field">
                              <label>Job Title / Position*</label>
                              <input type="text" list="jobtitle" name="jobtitle" class="ui search uppercase department" value="{{ old('jobtitle') }}" required autocomplete="off">
                              @isset($jobtitle)
                              <datalist id="jobtitle">
                                @foreach($jobtitle as $data)
                                <option  >{{$data->jobtitle}}</option>
                                @endforeach
                              </datalist>
                              @endisset
                            </div>
                            <!-- <div class="field">
                                <label>Job Title / Position*</label>
                                <div class="ui search dropdown selection uppercase jobposition">
                                    <input type="hidden" name="jobposition">
                                    <i class="dropdown icon" tabindex="1"></i>
                                    <div class="default text">Select Job Title</div>
                                    <div class="menu">
                                        @isset($jobtitle)
                                            @isset($department)
                                                @foreach ($jobtitle as $data)
                                                    @foreach ($department as $dept)
                                                        @if($dept->id == $data->dept_code)
                                                            <div class="item" data-value="{{ $data->id }}"
                                                                 data-dept="{{ $dept->id }}">{{ $data->jobtitle }}</div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @endisset
                                        @endisset
                                    </div>
                                </div>
                            </div> -->
                            <div class="field">
                                <label>ID Number*</label>
                                <input type="text" class="uppercase" name="idno" value="{{ old('idno') }}" autocomplete="off">
                            </div>
                            <div class="field">
                                <label>Email Address* (Company)</label>
                                <input type="email" name="companyemail" value="{{ old('companyemail') }}" class="lowercase" autocomplete="off">
                            </div>
                            <div class="field">
                                    <label>Leave Privilege</label>
                                    <select name="leaveprivilege" class="ui dropdown uppercase">
                                        <option value="">Select Leave Privilege</option>
                                        @isset($leavegroup)
                                            @foreach($leavegroup as $lg)
                                                <option  value="{{ $lg->id }}" @if(old('leaveprivilege') == $lg->id ) {{ 'selected' }} @endif > {{ $lg->leavetype }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            <h4 class="ui dividing header">Employment Information</h4>
                            <div class="field">
                                <label>Employment Type*</label>
                                <select name="employmenttype" required class="ui dropdown uppercase">
                                    <option value="">Select Type</option>
                                    <option value="Regular" @if(old('employmenttype') == "Regular" ) {{ 'selected' }} @endif>Regular</option>
                                    <option value="Trainee" @if(old('employmenttype') == "Trainee" ) {{ 'selected' }} @endif>Trainee</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Employment Status*</label>
                                <select name="employmentstatus" required class="ui dropdown uppercase">
                                    <option value="">Select Status</option>
                                    <option value="Active" @if(old('employmentstatus') == "Active" ) {{ 'selected' }} @endif>Active</option>
                                    <option value="Archived" @if(old('employmentstatus') == "Archived" ) {{ 'selected' }} @endif>Archived</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Official Start Date*</label>
                                <input type="text" name="startdate" required value="{{ old('startdate') }}" class="airdatepicker uppercase"
                                       data-position="top right" autocomplete="off" placeholder="Date" >
                            </div>
                            <div class="field">
                                <label>Date Regularized</label>
                                <input type="text" name="dateregularized" value="{{ old('dateregularized') }}" autocomplete="off" class="airdatepicker uppercase"
                                       data-position="top right" placeholder="Date">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
              </div>
              <div class="row mb-5" >
                <div class="container-fluid p-5 bg-white mx-4 box">
                    <h3>Employee Residence Information </h3>
                    <p>Please fill up the forms bellow</p>
                    <div class="row">
                        <div class="col-6 ">
                            <div class="col py-5">
                                <h4 class="mb-4">Passport</h4>

                                <div class="field">
                                    <label>Passport Number</label>
                                    <input type="text" class="uppercase" name="passport_number" value="{{ old('passport_number') }}" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="passport_issue_date" value="{{ old('passport_issue_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="passport_ex_date"  value="{{ old('passport_ex_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload" value="{{ old('pass_image') }}" id="passimagefile" name="pass_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validatePassFile()">
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-6 py-5">
                        <div class="col">
                                <h4 class="mb-4">Driving Licsense</h4>
                                <div class="field">
                                    <label>Driving Licsense Number</label>
                                    <input type="text" class="uppercase" name="d_license_number" value="{{ old('d_license_number') }}" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="drive_issue_date"  value="{{ old('drive_issue_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="drive_ex_date" value="{{ old('drive_ex_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload" value="{{ old('drive_image') }}" id="driverimagefile" name="drive_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validateDriverFile()">
                                </div> 
                                <!-- <div class="field">
                                    <label>Comment</label>
                                    <textarea class="text_area_size" rows="2" name="comment" Placeholder="tell us you comment....."></textarea>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col px-5">
                        <div class="ui checkbox">
                        <input type="checkbox" name="checked" id="mycheckbox" value="0">
                        <label>Does this person need permission to stay in this country ?</label>
                        </div>
                        </div>
                    </div>

                    <div class="row"  id="mycheckboxdiv" style="display:none">
                        <div class="col-6">
                            <div class="col py-5">
                                <h4 class="mb-4">Biometric Residence Permit</h4>
                            

                                <div class="field">
                                    <label>BRP Number</label>
                                    <input type="text" class="uppercase" name="bpr_number" value="{{ old('bpr_number') }}" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="bpr_issue_date"  value="{{ old('bpr_issue_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="bpr_ex_date"  value="{{ old('bpr_ex_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload" value="{{ old('image') }}" id="bprimagefile" name="bpr_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validateBprFile()">
                                </div>
                                <!-- <div class="field">
                                    <label>Comment</label>
                                    <textarea name="comment" >Enter text here...</textarea>
                                </div> -->
                            </div>
                            
                        </div>
                        <div class="col-6 py-5">
                        <div class="col pt-4">
                                <h4 class="mb-4"></h4>
                                <div class="field">
                                    <label>Current Status (After BRP expires)</label>
                                    <input type="text" class="uppercase" name="current_status" value="{{ old('current_status') }}" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Name of the Document</label>
                                    <input type="text" class="uppercase" name="document_name" value="{{ old('document_name') }}" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="document_issue_date"  value="{{ old('document_issue_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="document_ex_date"  value="{{ old('document_ex_date') }}" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload" value="{{ old('doc_image') }}" id="docimagefile" name="doc_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validateDocFile()">
                                </div>
                                <div class="field">
                                    <label>Comment</label>
                                    <textarea class="text_area_size" rows="2" name="comment" Placeholder="tell us you comment....."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
              </div>
              <div class="row">
                <div class="container-fluid mx-1">


                  <div class=" p-4 box box-success ">
                    <div class="grouped fields opt-radio">
                        <label> Choose Account type </label>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" name="acc_type" value="1">
                                <label>Employee</label>
                            </div>
                        </div>
                        <div class="field" style="padding:0px!important">
                            <div class="ui radio checkbox">
                                <input type="radio" name="acc_type" value="2">
                                <label>Admin</label>
                            </div>
                        </div>
                    </div>

                      <div class="field">
                      <label>Role</label>
                      <!-- <input type="text" list="roles" name="role" class="ui search uppercase department required" value="{{ old('role') }}" autocomplete="off"> -->
                      <select class="ui dropdown uppercase" name="role">
                            <option value=""  selected disabled>Select Status</option>
                            @isset($roles)
                            @foreach($roles as $data)
                            <option value="{{$data->id}}" @if(old('role') == $data->id ) {{ 'selected' }} @endif>{{$data->role_name}}</option>
                        @endforeach
                        @endisset
                            </select>
                    </div>
                    <!-- <div class="fields">
                        <div class="sixteen wide field role">
                            <label for="">Role</label>
                            <select class="ui dropdown uppercase" name="role_id">
                                <option value="">Select Role</option>
                                @isset($roles)
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                    </div> -->
                    <div class="fields">
                        <div class="sixteen wide field">
                            <label>Status</label>
                            <select class="ui dropdown uppercase" name="status">
                                <option value="">Select Status</option>
                                <option value="1" @if(old('status') == "1" ) {{ 'selected' }} @endif >Enabled</option>
                                <option value="0" @if(old('status') == "0" ) {{ 'selected' }} @endif >Disabled</option>
                            </select>
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <label for="">Password</label>
                            <input type="password" name="password" class="" autocomplete="off">
                        </div>
                        <div class="field">
                            <label for="">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="" autocomplete="off">
                        </div>
                    </div>
                  </div>
                  </div>

              </div>
                <div class="col-md-12 float-left">
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header"></div>
                        <ul class="list">
                            <li class=""></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-12 float-left">
                    <div class="action align-right">
                        <button type="submit" name="submit" class="ui green button small"><i
                                    class="ui checkmark icon"></i>Save
                        </button>
                        <a href="{{ url('employees') }}" class="ui red button small"><i class="ui times icon"></i>Cancel</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
        </div>
    
    
    </section>
        
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>
    <script type="text/javascript">
        $('.airdatepicker').datepicker({language: 'en', dateFormat: 'yyyy-mm-dd', autoClose: true});

        $('.ui.dropdown.department').dropdown({
            onChange: function (value, text, $selectedItem) {
                $('.jobposition .menu .item').addClass('hide disabled');
                $('.jobposition .text').text('');
                $('input[name="jobposition"]').val('');
                $('.jobposition .menu .item').each(function () {
                    var dept = $(this).attr('data-dept');
                    if (dept == value) {
                        $(this).removeClass('hide disabled');
                    }
                    ;
                });
            }
        });

        function validateFile() {
                              
      var image = document.getElementById("imagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("imagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select profile image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}

function validatePassFile() {
                              
      var image = document.getElementById("passimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("passimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select passport image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
function validateDriverFile() {
                              
      var image = document.getElementById("driverimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("driverimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select driving license image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
function validateDocFile() {
                              
      var image = document.getElementById("docimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("docimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select document image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
        
        
 function validateBprFile() {
                              
      var image = document.getElementById("bprimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 1) {
    document.getElementById("bprimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select BRP image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
        
        
        function imagetype(){
        var f = document.getElementById("").value;
        var d = f.lastIndexOf(".") + 1;
            var ext = f.substr(d, f.length).toLowerCase();
           
            if (ext == "jpg" || ext == "jpeg" || ext == "png") {
            } else {
                document.getElementById("").value = "";
                $.notify({
                        icon: 'ui icon times',
                        message: "Please upload only jpg/jpeg and png image formats."
                    },
                    {type: 'danger', timer: 400});
            }
        }
    </script>
    
    <script>
$('form#add_employee_form').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
</script>
    
    <script type="text/javascript">
$('#mycheckbox').change(function() {
    $('#mycheckboxdiv').toggle();
});
</script>

@endsection
