@extends('layouts.default')

@section('meta')
        <title>Leave Types | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper leave type, view leave types, add or edit leave types and export or download leave types.">
@endsection


    @section('content')
    
    <div class="container-fluid">
        <div class="row">
            <div class="box box-success col-md-6">
            <div class="box-header with-border">Delete Leave Type</div>
                <div class="box-body">
                @if(!$pleavetype->contains($id))
                    <form action="{{ url('fields/leavetype/delete') }}" class="ui form" method="post" accept-charset="utf-8">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="@isset($id) {{$id}} @endisset">
                        <div class="field">
                        <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
                        </div>
                       
                        <div class="field">
                            <a href="{{ route('leavetype') }}" class="ui black deny button">No</a>
                            <button class="ui positive button approve" type="submit" name="submit"><i class="ui checkmark icon"></i>Yes</button>
                        </div>
                    </form> 
                    @else
        <h4><i class="bullhorn icon"></i><span> You can't delete because this record has some dependencies on other records.</span></h4><br>
        <div class="field"> <a href="{{ route('leavetype') }}" class="ui black deny button">Back</a> </div>
          @endif
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    </script>
    @endsection 