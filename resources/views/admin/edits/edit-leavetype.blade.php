@extends('layouts.default')

@section('meta')
        <title>Leave Types | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper leave type, view leave types, add or edit leave types and export or download leave types.">
    @endsection

@section('content')

<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Edit Leavetype
                    <a href="{{ url('/fields/leavetype') }}" class="btn btn-sm btn_upper float-right"><i class="ui icon chevron left"></i>Return</a>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="box box-success col-md-6">
            <div class="box-header with-border"></div>
                <div class="box-body">
               
                <form id="add_leavetype_form" action="{{ url('fields/leavetype/update') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                            <input type="hidden" name="id" value="@isset($leavetype->id){{ $leavetype->id }}@endisset">
                            <div class="field">
                                <label>Leave name <span class="help">e.g. "Vacation Leave, Sick Leave"</span></label>
                                <input class="uppercase" name="leavetype" value="{{ $leavetype->leavetype }}" type="text">
                            </div>
                            <div class="field">
                                <label>Credits <span class="help">e.g. "15" (days)</span></label>
                                <input class="" name="credits" value="{{ $leavetype->limit }}" type="text">
                            </div>
                            <div class="grouped fields opt-radio">
                                <label class="">Choose Term</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="term" value="Monthly" @if($leavetype->percalendar == "Monthly" ) checked="checked" @endif>
                                        <label>Monthly</label>
                                    </div>
                                </div>
                                <div class="field" style="padding:0px!important">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="term" value="Yearly" @if($leavetype->percalendar == "Yearly" ) checked="checked" @endif>
                                        <label>Yearly</label>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    </script>
    @endsection 