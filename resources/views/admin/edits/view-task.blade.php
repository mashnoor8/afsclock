@extends('layouts.default')

@section('meta')
    <title>View Employee Task | Attendance Keeper</title>
    <meta name="description" content="Attendance Keeper edit employee attendance.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">View Task
                <a href="{{ url('taskmanager') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>

                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-content">
                       
                        <form id="edit_task_form"  class="ui form"  accept-charset="utf-8">
                          
                            <div class="field">
                                <label>Assigned To</label>
                                <input type="text" name="assigned_to" class="readonly" readonly="" value="@isset($task){{ $task->firstname }} @isset($task->mi) {{ $task->mi }}@endisset {{ $task->lastname }}@endisset">
                            </div>

                            <div class="field">
                                <label>Title</label>
                                <input type="text" name="title" class="readonly" readonly="" value="@isset($task->title){{ $task->title }}@endisset" dissabled>
                            </div>
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <label>Description</label>
                                    <textarea class="" rows="5" class="readonly" readonly="" name="description">@isset($task->description){{ $task->description }}@endisset</textarea>
                                </div>
                            </div>

                            <div class="field">
                                <label for="">Deadline</label>
                                <input type="text" placeholder="0000-00-00" autocomplete="off" class="readonly" readonly="" name="deadline" class="airdatepicker" value="@isset($task->deadline){{ $task->deadline }}@endisset"/>
                            </div>
                            @if($task->new_deadline != Null)
                            <div class="field">
                                <label for="">Extended Deadline</label>
                                <input type="text" placeholder="0000-00-00" autocomplete="off" class="readonly" readonly="" name="deadline" class="airdatepicker" value="@isset($task->new_deadline){{ $task->new_deadline }}@endisset"/>
                            </div>
                            @endif
                            @if($task->comment != Null)
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <label>Comment</label>
                                    <textarea class="" rows="5" class="readonly" readonly="" name="comment">@isset($task->comment){{ $task->comment }}@endisset</textarea>
                                </div>
                            </div>
                            @endif
                           
                    </div>


                   
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">
        $('.jtimepicker').mdtimepicker({format:'h:mm:ss tt', theme: 'blue', hourPadding: true});
        $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });
    </script>
@endsection
