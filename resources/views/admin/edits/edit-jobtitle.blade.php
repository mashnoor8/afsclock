@extends('layouts.default')

@section('meta')
        <title>Job Titles | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper job titles, view job titles, and export or download job titles.">
@endsection

@section('content')

<div class="container-fluid">
        <div class="row">
            <div class="box box-success col-md-6">
            <div class="box-header with-border">Edit Job Title</div>
                <div class="box-body">
               
                <form id="add_jobtitle_form" action="{{ url('fields/jobtitle/update') }}" class="ui form" method="post" accept-charset="utf-8">
                        @csrf
                        <input type="hidden" name="id" value="@isset($jobtitle->id){{ $jobtitle->id }}@endisset">
                        <div class="field">
                            <label>Department</label>
                            <select name="department" class="ui search dropdown getdeptcode">
                                <option value="">Select Department</option>
                                @isset($d)
                                    @foreach ($d as $dept)
                                    <option value="{{ $dept->department }}" data-id="{{ $dept->id }}"  @if($dept->department == $jobtitle->department ) selected @endif > {{ $dept->department}}</option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="field">
                            <label>Job Title <span class="help">e.g. "Operations Manager"</span></label>
                            <input class="uppercase" name="jobtitle" value="{{ $jobtitle->jobtitle }}" type="text">
                        </div>
                        <div class="field">
                            <div class="ui error message">
                                <i class="close icon"></i>
                                <div class="header"></div>
                                <ul class="list">
                                    <li class=""></li>
                                </ul>
                            </div>
                        </div>
                        <div class="actions">
                            <input type="hidden" name="dept_code" value="">
                            <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">
 $('.ui.dropdown.getdeptcode').dropdown({
        onChange: function (value, text, $selectedItem) {
            $('select[name="department"] option').each(function () {
                if ($(this).val() == value) {
                    var id = $(this).attr('data-id');
                    $('input[name="dept_code"]').val(id);
                };
            });
        }
    });
    </script>
    @endsection 