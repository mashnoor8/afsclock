@extends('layouts.default')

    @section('meta')
        <title>Edit Employee Profile | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper edit employee profile.">
    @endsection

    @section('styles')
        <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    @endsection

    @section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Edit Employee Profile
                    <a href="{{ url('employees') }}" class="ui btn_upper button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
                </h2>
            </div>
        </div>

        <div class="col-md-12">
            @if ($errors->any())
            <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">There were some errors with your submission</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <div class="row">
                <form id="edit_employee_form" action="{{ url('profile/update') }}" class="ui form custom" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                @csrf

                    <div class="col-md-6 float-left ">
                        <div class="box box-success">
                            <div class="box-header with-border">Personal Information</div>
                            <div class="box-body">
                                <div class="two fields">
                                    <div class="field">
                                        <label>First Name</label>
                                        <input type="text" class="uppercase" name="firstname" value="@isset($person_details->firstname){{ $person_details->firstname }}@endisset">
                                    </div>
                                    <div class="field">
                                        <label>Middle Name</label>
                                        <input type="text" class="uppercase" name="mi" value="@isset($person_details->mi){{ $person_details->mi }}@endisset">
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Last Name</label>
                                    <input type="text" class="uppercase" name="lastname" value="@isset($person_details->lastname){{ $person_details->lastname }}@endisset">
                                </div>
                                <div class="field">
                                    <label>Gender</label>
                                    <select name="gender" class="ui dropdown uppercase">
                                        <option value="">Select Gender</option>
                                        <option value="MALE" @isset($person_details->gender) @if($person_details->gender == 'MALE') selected @endif @endisset>MALE</option>
                                        <option value="FEMALE" @isset($person_details->gender) @if($person_details->gender == 'FEMALE') selected @endif @endisset>FEMALE</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Civil Status</label>
                                    <select name="civilstatus" class="ui dropdown uppercase">
                                        <option value="">Select Civil Status</option>
                                        <option value="SINGLE" @isset($person_details->civilstatus) @if($person_details->civilstatus == 'SINGLE') selected @endif @endisset>SINGLE</option>
                                        <option value="MARRIED" @isset($person_details->civilstatus) @if($person_details->civilstatus == 'MARRIED') selected @endif @endisset>MARRIED</option>
                                        <option value="ANULLED" @isset($person_details->civilstatus) @if($person_details->civilstatus == 'ANULLED') selected @endif @endisset>ANULLED</option>
                                        <option value="WIDOWED" @isset($person_details->civilstatus) @if($person_details->civilstatus == 'WIDOWED') selected @endif @endisset>WIDOWED</option>
                                        <option value="LEGALLY SEPARATED" @isset($person_details->civilstatus) @if($person_details->civilstatus == 'LEGALLY SEPARATED') selected @endif @endisset>LEGALLY SEPARATED</option>
                                    </select>
                                </div>

         
                                <div class="field">
                                    <label>Email Address (Personal)</label>
                                    <input type="email" name="emailaddress" value="@isset($person_details->emailaddress){{ $person_details->emailaddress }}@endisset" class="lowercase">
                                </div>
                                <div class="field">
                                    <label>Mobile Number</label>
                                    <input type="text" class="uppercase" name="mobileno" value="@isset($person_details->mobileno){{ $person_details->mobileno }}@endisset">
                                </div>
                               
                              

                                    <div class="field">
                                        <label>Date of Birth</label>
                                        <input type="text" autocomplete="off" name="birthday" value="@isset($person_details->birthday){{ $person_details->birthday }}@endisset" class="airdatepicker" placeholder="Date">
                                    </div>
                              
                                <div class="field">
                                    <label>National ID</label>
                                    <input type="text" class="uppercase" name="nationalid" value="@isset($person_details->nationalid){{ $person_details->nationalid }}@endisset" placeholder="">
                                </div>
                                <div class="field">
                                    <label>Place of Birth</label>
                                    <input type="text" class="uppercase" name="birthplace" value="@isset($person_details->birthplace){{ $person_details->birthplace }}@endisset" placeholder="City, Province, Country">
                                </div>
                                <div class="field">
                                    <label>Home Address</label>
                                    <input type="text" class="uppercase" name="homeaddress" value="@isset($person_details->homeaddress){{ $person_details->homeaddress }}@endisset" placeholder="House/Unit Number, Building, Street, City, Province, Country">
                                </div>
                                <div class="fields">
                                    <div class="twelve wide field">
                                    <label>Upload Profile photo</label>
                                        <input class="ui file upload" value="" id="imagefile" name="image" type="file"
                                            accept="image/png, image/jpeg, image/jpg" onchange="validateFile()">
                                    </div>
                                    <div class="two wide field ">
                                    </div>
                                    <div class="one wide field pt-4">
                                    @isset($person_details->avatar)
                                        @if($person_details->avatar != '')
                                       
                                            <img id="imageProfile"  class="avatar border-white img-fluid" src="{{ asset('/assets/images/faces/'.$person_details->avatar) }}" alt="profile photo"/>
                                     
                                        @endif
                                    @endisset
                                    </div>
                                    <div class="one wide field ">
                                    </div>

                                   
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 float-left">
                        <div class="box box-success">
                            <div class="box-header with-border">Employee Details</div>
                            <div class="box-body">
                                <h4 class="ui dividing header">Designation</h4>
                                <div class="field">
                                    <label>Company</label>
                                        <input type="text" list="company" value ="{{$company->company}}"   name="company" class="ui search uppercase required" autocomplete="off" readonly> 
                                   
                                </div>
                                <div class="field">
                                    <label>Department</label>
                                    <select name="department" class="ui search dropdown uppercase department">
                                        <option value="">Select Department</option>
                                        @isset($department)
                                            @foreach ($department as $data)
                                                <option value="{{ $data->department }}" @if($data->id == $person_details->department_id) selected @endif> {{ $data->department }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Job Title / Position</label>
                                    <div class="ui search dropdown selection uppercase jobposition">
                                        <input type="hidden" name="jobposition" value="{{ $the_job_title->jobtitle }}">
                                        <i class="dropdown icon"></i>
                                        <div class="text">{{ $the_job_title->jobtitle }}</div>
                                        <div class="menu">
                                        @isset($jobtitle)
                                            @isset($department)
                                                @foreach ($jobtitle as $data)
                                                    @foreach ($department as $dept)
                                                        @if($dept->id == $data->dept_code)
                                                            <div class="item" data-value="{{ $data->jobtitle }}" data-dept="{{ $dept->department }}" @if($data->id == $person_details->job_title_id) selected @endif>{{ $data->jobtitle }}</div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @endisset
                                        @endisset
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>ID Number</label>
                                    <input type="text" class="uppercase" name="idno" value="@isset($person_details->idno){{ $person_details->idno }}@endisset">
                                </div>
                                <div class="field">
                                    <label>Email Address (Company)</label>
                                    <input type="email" name="companyemail" value="@isset($person_details->companyemail){{ $person_details->companyemail }}@endisset" class="lowercase">
                                </div>
                                <div class="field">

                                    <label>Leave Privilege</label>
                                   
                                    @if(is_null($leaves_this_year[0]->diff))
                                    <select name="leaveprivilege" class="ui dropdown uppercase">
                                        <option value="">Select Leave Privilege</option>
                                        @isset($leavegroup)
                                            @foreach($leavegroup as $lg)
                                                <option value="{{ $lg->id }}" @isset($person_details->leaveprivilege) @if($lg->id == $person_details->leaveprivilege) selected @endif @endisset>{{ $lg->leavetype }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                   @else
                                   <select name="leaveprivilege" class="ui dropdown uppercase" disabled>
                                        <option value="">Select Leave Privilege</option>
                                        @isset($leavegroup)
                                            @foreach($leavegroup as $lg)
                                                <option value="{{ $lg->id }}" @isset($person_details->leaveprivilege) @if($lg->id == $person_details->leaveprivilege) selected @endif @endisset>{{ $lg->leavetype }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                    <small id="emailHelp" class="form-text text-muted">Can't change the leavetype for {{ $person_details->firstname }} {{ $person_details->lastname }} because @if($person_details->gender == "MALE") he @else she @endif has already taken leaves under this leavetype.</small>
                                    @endif
                                </div>
                                <h4 class="ui dividing header">Employment Information</h4>
                                <div class="field">
                                    <label>Employment Type</label>
                                    <select name="employmenttype" class="ui dropdown uppercase">
                                        <option value="">Select Type</option>
                                        <option value="Regular" @isset($person_details->employmenttype) @if($person_details->employmenttype == 'Regular') selected @endif @endisset>Regular</option>
                                        <option value="Trainee" @isset($person_details->employmenttype) @if($person_details->employmenttype == 'Trainee') selected @endif @endisset>Trainee</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Employment Status</label>
                                    <select name="employmentstatus" class="ui dropdown uppercase">
                                        <option value="">Select Status</option>
                                        <option value="Active" @isset($person_details->employmentstatus) @if($person_details->employmentstatus == 'Active') selected @endif @endisset>Active</option>
                                        <option value="Archived" @isset($person_details->employmentstatus) @if($person_details->employmentstatus == 'Archived') selected @endif @endisset>Archived</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Official Start Date</label>
                                    <input type="text" name="startdate" value="@isset($person_details->startdate){{ $person_details->startdate }}@endisset" class="airdatepicker" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date Regularized</label>
                                    <input type="text" name="dateregularized" value="@isset($person_details->dateregularized){{ $person_details->dateregularized }}@endisset" class="airdatepicker" placeholder="Date">
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                <div class="row mb-5" >
                <div class="container-fluid p-5 bg-white mx-4 box">
                    <h3>Employee Residence Information </h3>
                    <p>Please fill up the forms bellow</p>
                    <div class="row">
                        <div class="col-6 ">
                            <div class="col py-5">
                                <h4 class="mb-4">Passport</h4>

                                <div class="field">
                                    <label>Passport Number</label>
                                    <input type="text" class="uppercase" name="passport_number" value="@isset($person_details->passport_number){{ $person_details->passport_number }}@endisset" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="passport_issue_date" value="@isset($person_details->passport_issue_date){{ $person_details->passport_issue_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="passport_ex_date"  value="@isset($person_details->passport_ex_date){{ $person_details->passport_ex_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>

                                <div class="fields">
                                    <div class="twelve wide field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                        <input class="ui file upload" value="" id="passimagefile" name="pass_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validatePassFile()">
                                    </div>
                                    <div class="two wide field ">
                                    </div>
                                    <div class="one wide field pt-4">
                                    @isset($person_details->pass_image)
                                        @if($person_details->pass_image != '' || $person_details->pass_image != null)
                                            <img  id="imagePassport" class="avatar border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->pass_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
                                    </div>
                                    <div class="one wide field ">
                                    </div>

                                   
                                </div>
                              
                            </div>
                            
                        </div>
                        <div class="col-6 py-5">
                        <div class="col">
                                <h4 class="mb-4">Driving Licsense</h4>
                                <div class="field">
                                    <label>Driving Licsense Number</label>
                                    <input type="text" class="uppercase" name="d_license_number" value="@isset($person_details->d_license_number){{ $person_details->d_license_number }}@endisset" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="drive_issue_date"  value="@isset($person_details->drive_issue_date){{ $person_details->drive_issue_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="drive_ex_date" value="@isset($person_details->drive_ex_date){{ $person_details->drive_ex_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="fields">
                                    <div class="twelve wide field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload" value="" id="driverimagefile" name="drive_image" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validateDriverFile()">
                                    </div>
                                    <div class="two wide field ">
                                    </div>
                                    <div class="one wide field pt-4">
                                    @isset($person_details->drive_image)
                                        @if($person_details->drive_image != '' || $person_details->drive_image != null)
                                            <img id="imageDriver"  class="avatar border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->drive_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
                                    </div>
                                    <div class="one wide field ">
                                    </div>

                                   
                                </div>
                                <!-- <div class="field">
                                    <label>Comment</label>
                                    <textarea class="text_area_size" rows="2" name="comment" Placeholder="tell us you comment....."></textarea>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col px-5">
                        <div class="ui checkbox">
                        <input type="checkbox" name="checked" id="mycheckbox" value="0" >
                        <label>Does this person need permission to stay in this country ?</label>
                        </div>
                        </div>
                    </div>

                    <div class="row"  id="mycheckboxdiv" style="display:none">
                        <div class="col-6">
                            <div class="col py-5">
                                <h4 class="mb-4">Biometric Residence Permit</h4>
                            

                                <div class="field">
                                    <label>BRP Number</label>
                                    <input type="text" class="uppercase" name="bpr_number" value="@isset($person_details->bpr_number){{ $person_details->bpr_number }}@endisset" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="bpr_issue_date"  value="@isset($person_details->bpr_issue_date){{ $person_details->bpr_issue_date }}@endisset"class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="bpr_ex_date"  value="@isset($person_details->bpr_ex_date){{ $person_details->bpr_ex_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                        
                                <div class="fields">
                                    <div class="twelve wide field">
                                    <label>Upload File (PDF/JPG/PNG)</label>
                                    <input class="ui file upload"  id="bprimagefile" name="image_bpr" type="file"
                                        accept="image/png, image/jpeg, image/jpg" onchange="validateBprFile()">
                                    </div>
                                    <div class="two wide field ">
                                    </div>
                                    <div class="one wide field pt-4">
                                    @isset($person_details->bpr_image)
                                        @if($person_details->bpr_image != '' || $person_details->bpr_image != null)
                                            <img id="imageBRP"  class="avatar border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->bpr_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
                                    </div>
                                    <div class="one wide field ">
                                    </div>

                                   
                                </div>
                                <!-- <div class="field">
                                    <label>Comment</label>
                                    <textarea name="comment" >Enter text here...</textarea>
                                </div> -->
                            </div>
                            
                        </div>
                        <div class="col-6 py-5">
                        <div class="col pt-4">
                                <h4 class="mb-4"></h4>
                                <div class="field">
                                    <label>Current Status (After BRP expires)</label>
                                    <input type="text" class="uppercase" name="current_status" value="@isset($person_details->current_status){{ $person_details->current_status }}@endisset" autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Name of the Document</label>
                                    <input type="text" class="uppercase" name="document_name" value="@isset($person_details->document_name){{ $person_details->document_name }}@endisset"autocomplete="off">
                                </div>
                                <div class="field">
                                    <label>Date of Issue</label>
                                    <input type="text" name="document_issue_date"  value="@isset($person_details->document_issue_date){{ $person_details->document_issue_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                                <div class="field">
                                    <label>Date of Expiry</label>
                                    <input type="text" name="document_ex_date"  value="@isset($person_details->document_ex_date){{ $person_details->document_ex_date }}@endisset" class="airdatepicker uppercase" data-position="top right" autocomplete="off" placeholder="Date">
                                </div>
                            
                                <div class="fields">
                                    <div class="twelve wide field">
                                        <label>Upload File (PDF/JPG/PNG)</label>
                                        <input class="ui file upload"  id="docimagefile" name="doc_image" type="file"
                                            accept="image/png, image/jpeg, image/jpg" onchange="validateDocFile()">
                                    </div>
                                    <div class="two wide field ">
                                    </div>
                                    <div class="one wide field pt-4">
                                    @isset($person_details->document_image)
                                        @if($person_details->document_image != '' || $person_details->document_image != null)
                                           <img id="imageDoc"  class="avatar border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->document_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
                                    </div>
                                    <div class="one wide field ">
                                    </div>

                                   
                                </div>
                                <div class="field">
                                    <label>Comment</label>
                                    <textarea class="text_area_size" rows="2" name="comment" Placeholder="tell us you comment....."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
              </div>
                    <div class="col-md-12 float-left">
                        <div class="ui error message">
                            <i class="close icon"></i>
                            <div class="header"></div>
                            <ul class="list">
                                <li class=""></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 float-left">
                        <div class="action align-right">
                            <input type="hidden" name="id" value="@isset($e_id){{ $e_id }}@endisset">
                            <button type="submit" name="submit" class="ui green button small"><i class="ui checkmark icon"></i> Update</button>
                            <a href="{{ url('employees') }}" class="ui red button small"><i class="ui times icon"></i> Cancel</a>
                        </div>
                    </div>
                </form>
        </div>
    </div>

    <section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Edit User</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-content">
                        @if ($errors->any())
                        <div class="ui error message">
                            <i class="close icon"></i>
                            <div class="header">There were some errors with your submission</div>
                            <ul class="list">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form id="edit_user_form" action="{{ url('users/update/user') }}" class="ui form add-user" method="post" accept-charset="utf-8">
                            @csrf
                            <div class="field">
                                <label>Employee</label>
                                <input type="text" name="employee" value="@isset($u->firstname){{ $u->firstname}} {{ $u->lastname }}@endisset" class="readonly uppercase" readonly>
                            </div>
                            <div class="field">
                                <label>Email</label>
                                <input type="text" name="email" value="@isset($u->companyemail){{ $u->companyemail }}@endisset" class="readonly lowercase" readonly>
                            </div>
                            <div class="grouped fields opt-radio">
                                <label class="">Choose Account type</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="acc_type" value="1" @isset($u->acc_type) @if($u->acc_type == 1) checked @endif @endisset>
                                        <label>Employee</label>
                                    </div>
                                </div>
                                <div class="field" style="padding:0px!important">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="acc_type" value="2" @isset($u->acc_type) @if($u->acc_type == 2) checked @endif @endisset>
                                        <label>Admin</label>
                                    </div>
                                </div>
                            </div>
                            <div class="fields">
                                <div class="sixteen wide field role">
                                    <label for="">Role</label>
                                    <select class="ui dropdown uppercase" name="role_id">
                                        <option value="">Select Role</option>
                                        @isset($r)
                                            @foreach ($r as $role)
                                                <option value="{{ $role->id }}" @if($role->id == $u->role_id) selected @endif>{{ $role->role_name }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="field">
                                <label>Status</label>
                                <select class="ui dropdown uppercase" name="status">
                                    <option value="">Select Status</option>
                                    <option value="1" @isset($u->status) @if($u->status == 1) selected @endif @endisset>Enabled</option>
                                    <option value="0" @isset($u->status) @if($u->status == 0) selected @endif @endisset>Disabled</option>
                                </select>
                            </div>
                            <div class="two fields">
                                <div class="field">
                                    <label for="">New Password</label>
                                    <input type="password" name="password" class="" placeholder="********">
                                </div>
                                <div class="field">
                                    <label for="">Confirm New Password</label>
                                    <input type="password" name="password_confirmation" class="" placeholder="********">
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="field">
                                <a href="{{ url('employee/faceregistration') }}/{{ $u->id }}" class="ui  small button" name="submit"><i class="fa fa-user-circle-o pr-2" aria-hidden="true"></i> Register Faces</a>
                            </div>
                    </div>

                    <div class="box-footer">
                        <input type="hidden" value="@isset($e_id){{ $e_id }}@endisset" name="ref">
                        <button class="ui positive approve small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Update</button>
                        <a href="{{ url('users') }}" class="ui black red small button"><i class="ui times icon"></i> Cancel</a>
                    </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

<!-- Profile Modal -->


<div class="ui tiny modal modalProfile">
  <i class="close icon"></i>
  
  <div class="p-5">
       
    @isset($person_details->avatar)
                                        @if($person_details->avatar != '')
                                       
                                            <img  class=" border-white img-fluid" src="{{ asset('/assets/images/faces/'.$person_details->avatar) }}" alt="profile photo"/>
                                     
                                        @endif
                                    @endisset
  </div>
</div>

<div class="ui tiny modal modalPass">
  <i class="close icon"></i>
  
  <div class="p-5">
       
   @isset($person_details->pass_image)
                                        @if($person_details->pass_image != '' || $person_details->pass_image != null)
                                            <img   class=" border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->pass_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
  </div>
</div>

<div class="ui tiny modal modalDriver">
  <i class="close icon"></i>
  
  <div class="p-5">
       
   @isset($person_details->drive_image)
                                        @if($person_details->drive_image != '' || $person_details->drive_image != null)
                                            <img class=" border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->drive_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
  </div>
</div>

<div class="ui tiny modal modalBRP">
  <i class="close icon"></i>
  
  <div class="p-5">
       
    @isset($person_details->bpr_image)
                                        @if($person_details->bpr_image != '' || $person_details->bpr_image != null)
                                            <img   class=" border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->bpr_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
  </div>
</div>

<div class="ui tiny modal modalDoc">
  <i class="close icon"></i>
  
  <div class="p-5">
       
   @isset($person_details->document_image)
                                        @if($person_details->document_image != '' || $person_details->document_image != null)
                                           <img   class=" border-white img-fluid" src="{{ asset('/assets/employee_info_image/'.$person_details->document_image) }}" alt="profile photo"/>
                                        @endif
                                    @endisset
  </div>
</div>


    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">
        $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });
        $('.ui.dropdown.department').dropdown({ onChange: function(value, text, $selectedItem) {
            $('.jobposition .menu .item').addClass('hide');
            $('.jobposition .text').text('');
            $('input[name="jobposition"]').val('');
            $('.jobposition .menu .item').each(function() {
                var dept = $(this).attr('data-dept');
                if(dept == value) {$(this).removeClass('hide');};
            });
        }});

$( "#imageProfile" )
  .mouseover(function() {
  	$(".modalProfile").modal('show');
  });
  
$( "#imagePassport" )
  .mouseover(function() {
  	$(".modalPass").modal('show');
  });

$( "#imageDriver" )
  .mouseover(function() {
  	$(".modalDriver").modal('show');
  });
  $( "#imageBRP" )
  .mouseover(function() {
  	$(".modalBRP").modal('show');
  });
  $( "#imageDoc" )
  .mouseover(function() {
  	$(".modalDoc").modal('show');
  });
  
  
       function validateFile() {
                              
      var image = document.getElementById("imagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("imagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select profile image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}

function validatePassFile() {
                              
      var image = document.getElementById("passimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("passimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select passport image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
function validateDriverFile() {
                              
      var image = document.getElementById("driverimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("driverimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select driving license image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
function validateDocFile() {
                              
      var image = document.getElementById("docimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 2) {
    document.getElementById("docimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select document image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}
        
        
 function validateBprFile() {
                              
      var image = document.getElementById("bprimagefile");

      if (typeof (image.files) != "undefined") {

          var size = parseFloat(image.files[0].size / (1024 * 1024)).toFixed(2); 

          if(size > 1) {
    document.getElementById("bprimagefile").value = "";
                  $.notify({
                          icon: 'ui icon times',
                          message: "Please select BRP image less than 2 MB."
                      },
                      {type: 'danger', timer: 400});
        

          }else{

        

          }

      } else {

          alert("Network Error Occured!");

      }  

}

        var selected = "@isset($person_details->leaveprivilege){{ $person_details->leaveprivilege }}@endisset";
        var items = selected.split(',');
        $('.ui.dropdown.multiple.leaves').dropdown('set selected', items);
    </script>

    
     
    <script type="text/javascript">

    $('#mycheckbox').change(function() {
        $('#mycheckboxdiv').toggle();
    });
    </script>
    
        <script>
$('form#edit_employee_form').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
</script>

    @endsection
