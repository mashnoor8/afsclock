@extends('layouts.default')

@section('meta')
        <title>Companies | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper companies, view companies, and export or download companies.">
@endsection

@section('content')

<div class="container-fluid">
        <div class="row">
            <div class="box box-success col-md-6">
            <div class="box-header with-border">Delete Company</div>
                <div class="box-body">
               
                <form id="add_department_form" action="{{ url('fields/company/update') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                           
                            <input type="hidden" name="id" value="@isset($company->id){{ $company->id }}@endisset">
                            <div class="field">
                                <label>Company Name <span class="help">e.g. "Apple Corporation"</span></label>
                                <input class="uppercase" name="company" value="{{ $company->company }}" type="text">
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="actions">
                                <button type="submit" class="ui positive button small"><i class="ui icon check"></i> Save</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    </script>
    @endsection 