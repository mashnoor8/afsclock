@extends('layouts.default')

    @section('meta')

        <title>Profile | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper view employee profile, edit employee profile, update employee profile">
    @endsection

    @section('content')
    
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Employee Profile
                    <a href="{{ url('employees') }}" class="ui btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
                </h2>
            </div>    
        </div>

        <div class="row">
            <div class="col-md-4  float-left">
                <div class="box box-success">
                    <div class="box-body employee-info">
                            <div class="author">
                            @if($p->avatar != '')
                                <img class="avatar border-white" src="{{ asset('/assets/images/faces/'.$p->avatar) }}" alt="profile photo"/>
                            @else
                                <img class="avatar border-white" src="{{ asset('/assets/images/faces/default_user.jpg') }}" alt="profile photo"/>
                            @endif
                            </div>
                            <p class="description text-center">
                                <h4 class="title">@if($p->firstname) {{ $p->firstname }} @endisset @isset($p->lastname) {{ $p->lastname }} @else N/A @endif</h4>
                                <table style="width: 100%" class="profile-tbl">
                                    <tbody>
                                        <tr>
                                            <td>Email</td>
                                            <td><span class="p_value">@if($p->emailaddress) {{ $p->emailaddress }} @else N/A @endif</span></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No.</td>
                                            <td><span class="p_value">@if($p->mobileno) {{ $p->mobileno }} @else N/A @endif</span></td>
                                        </tr>
                                        <tr>
                                            <td>ID No.</td>
                                            <td><span class="p_value">@if($p->idno) {{ $p->idno }} @else N/A @endif</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </p>
                    </div>
                </div>
            </div>

            <div class="col-md-8 float-left">
                <div class="box box-success">
                    <div class="box-header with-border">Personal Infomation</div>
                    <div class="box-body employee-info">
                            <table class="tablelist">
                                <tbody>
                                    <tr>
                                        <td><p>Civil Status</p></td>
                                        <td><p>@if($p->civilstatus) {{ $p->civilstatus }} @else N/A @endif</p></td>
                                    </tr>
                                    <!-- <tr>
                                        <td><p>Age</p></td>
                                        <td><p>@isset($p->age) {{ $p->age }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Height <span class="help">(cm)</span></p></td>
                                        <td><p>@isset($p->height) {{ $p->height }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Weight <span class="help">(pounds)</span></p></td>
                                        <td><p>@isset($p->weight) {{ $p->weight }} @endisset</p></td>
                                    </tr> -->
                                    <tr>
                                        <td><p>Gender</p></td>
                                        <td><p>@if($p->gender) {{ $p->gender }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Date of Birth</p></td>
                                        <td>
                                            <p>
                                            @if($p->birthday) 
                                                    @php echo e(date("F d, Y", strtotime($p->birthday))) @endphp
                                            @else N/A @endif
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p>Place of Birth</p></td>
                                        <td><p>@if($p->birthplace) {{ $p->birthplace }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Home Address</p></td>
                                        <td><p>@if($p->homeaddress) {{ $p->homeaddress }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>National ID</p></td>
                                        <td><p>@if($p->nationalid) {{ $p->nationalid }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h4 class="ui dividing header">Designation</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Company</td>
                                        <td>@if($company_data ) {{ $company_data->company }} @else N/A @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Company Email</td>
                                        <td>@if($p->companyemail) {{ $p->companyemail }} @else N/A @endif</td>
                                    </tr>
                                    <tr>
                                        <td><p>Department</p></td>
                                        <td><p>@if($department) {{ $department->department }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td>Position</td>
                                        <td>@if($job_title) {{ $job_title->jobtitle }} @else N/A @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Leave Privilege</td>
                                        
                                        <td><p>@if($leavetype) {{ $leavetype->leavetype }}( {{$leavetype->limit}} days {{$leavetype->percalendar}} ) @else N/A @endif </p></td>
                                       
                                    </tr>
                                                                        <tr>
                                        <td>Total Leaves</td>
                                        
                                        <td><p>@if($total_leaves) {{ $total_leaves }} days @else N/A @endif </p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td>Taken Leaves</td>
                                        
                                        <td><p>@if($p->id) {!! taken_leaves($p->id) !!} days @else N/A @endif </p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td>Available Leaves</td>
                                        
                                        <td><p>@if($p->id) {!! available_leaves($p->id) !!} days @else N/A @endif </p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td><p>Employment Type</p></td>
                                        <td><p>@if($p->employmenttype) {{ $p->employmenttype }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Employement Status</p></td>
                                        <td><p>@if($p->employmentstatus) {{ $p->employmentstatus }} @else N/A @endif</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Official Start Date</p></td>
                                        <td>
                                            <p>
                                            @if($p->startdate) 
                                                @php echo e(date("F d, Y", strtotime($p->startdate))) @endphp
                                                @else N/A @endif
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p>Date Regularized</p></td>
                                        <td>
                                            <p>
                                            @if($p->dateregularized) 
                                                @php echo e(date("F d, Y", strtotime($p->dateregularized))) @endphp
                                            @else N/A @endif
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p>Passport number</p></td>
                                        <td>@if($p->passport_number) {{ $p->passport_number }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>Passport Issue Date</p></td>
                                        <!-- <td>@if($p) {{ $p->passport_issue_date }} @else N/A @endif</td>   -->
                                        <td>@if($p->passport_issue_date) {{ e(date("F d, Y", strtotime( $p->passport_issue_date )))}} @else N/A @endif</td>                                                                             
                                    </tr>
                                    <tr>
                                        <td><p>Passport Expire Date</p></td>
                                        <td>@if($p->passport_ex_date ) {{ e(date("F d, Y", strtotime( $p->passport_ex_date )))    }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>Passport Image</p></td>
                                        <!-- <td>@if($p->pass_image ) <a href="{{url('/response_image/download/'  )}}" class=" float-left "><i class="paperclip icon"></i><span class = "text-primary"> Download Attachment </span> </a>  @else N/A @endif</td> -->
                                        <!-- <td>@if($p->pass_image ) <a href="javascript:void(0)" class=" float-left modal  passport " > View</a>  @else N/A @endif</td>                                                                                -->
                                        <td>@if($p->pass_image )  <button class="ui positive right icon button" id= "passport"> view</button> @endif</td>                                                                               

                                    </tr>
                                    <tr>
                                        <td><p>Driving license_number</p></td>
                                        <td>@if( $p->d_license_number) {{    $p->d_license_number }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>Driving license issue date</p></td>
                                        <td>@if( $p->drive_issue_date ) {{ e(date("F d, Y", strtotime( $p->drive_issue_date )))  }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>Driving license expire</p></td>
                                        <td>@if($p->drive_ex_date ) {{ e(date("F d, Y", strtotime(  $p->drive_ex_date )))   }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>Driving license image</p></td>
                                        <td>@if($p->drive_image ) <button class="ui positive right icon button" id= "drive"> view</button> @else N/A @endif</td>                                                                               
                                    </tr>
                                    
                                    <tr>
                                        <td><p>BRP number</p></td>
                                        <td>@if($p->bpr_number) {{ $p->bpr_number }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>BRP issue date</p></td>
                                        <td>@if($p->bpr_issue_date) {{  e(date("F d, Y", strtotime(  $p->bpr_issue_date)))   }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>BRP expire date</p></td>
                                        <td>@if($p->bpr_ex_date) {{  e(date("F d, Y", strtotime( $p->bpr_ex_date)))  }} @else N/A @endif</td>                                                                               
                                    </tr>
                                    <tr>
                                        <td><p>BRP image</p></td>
                                        <td>@if($p->bpr_image)<button class="ui positive right icon button" id= "bio"> view</button> @else N/A @endif</td>                                                                               
                                    </tr>
                                    @isset($p)
                                        @if(!$p->bpr_number )
                                        <tr>
                                            <td><p>Current status (BPR Expire)</p></td>
                                            <td>@if($p->current_status) {{ $p->current_status }} @else N/A @endif</td>                                                                               
                                        </tr>
                                        <tr>
                                            <td><p>Document name </p></td>
                                            <td>@if($p->passport_number) {{ $p->passport_number }} @else N/A @endif</td>                                                                               
                                        </tr>
                                        <tr>
                                            <td><p>Document issue date </p></td>
                                            <td>@if($p->document_issue_date ) {{ e(date("F d, Y", strtotime( $p->document_issue_date )))  }} @else N/A @endif</td>                                                                               
                                        </tr>
                                        <tr>
                                            <td><p>Document expire date </p></td>
                                            <td>@if($p->document_ex_date ) {{ e(date("F d, Y", strtotime( $p->document_ex_date )))    }} @else N/A @endif</td>                                                                               
                                        </tr>
                                        <tr>
                                            <td><p>Document expire date </p></td>
                                            <td>@if($p->document_ex_date ) {{ e(date("F d, Y", strtotime( $p->document_ex_date )))    }} @else N/A @endif</td>                                                                               
                                        </tr>
                                        <tr>
                                            <td><p>Document image </p></td>
                                            <td>@if($p->document_image)<button class="ui positive right icon button" id= "doc"> view</button> @else N/A @endif</td>                                                                               

                                        </tr>
                                        @endif
                                    @endisset
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="ui modal passport">
    <i class="close icon"></i>
    <div class="header">
        Passport Image
    </div>
    <div class="pt-3 text-center" >
            <img class=" border-white" src="{{ asset('/assets/employee_info_image/'.$p->pass_image ) }}" alt="profile photo"/>
    </div>
    <div class="actions">

        <div class="ui positive right labeled icon button">
            Okay
        <i class="checkmark icon"></i>
        </div>
    </div>
</div>

<div class="ui modal driving">
    <i class="close icon"></i>
    <div class="header">
        Driving license Image
    </div>
    <div class="pt-3 text-center" >
            <img class=" border-white" src="{{ asset('/assets/employee_info_image/'.$p->drive_image ) }}" alt="profile photo"/>
    
    </div>
    <div class="actions">

        <div class="ui positive right labeled icon button">
            Okay
        <i class="checkmark icon"></i>
        </div>
    </div>
    
</div>

<div class="ui modal biometric">
    <i class="close icon"></i>
    <div class="header">
       BPR Image
    </div>
    <div class="pt-3 text-center" >
            <img class=" border-white" src="{{ asset('/assets/employee_info_image/'.$p->bpr_image ) }}" alt="profile photo"/>
    </div>
    <div class="actions">

        <div class="ui positive right labeled icon button">
            Okay
        <i class="checkmark icon"></i>
        </div>
    </div>
    
</div>

<div class="ui modal document">
    <i class="close icon"></i>
    <div class="header">
        Document Image
    </div>
    <div class="pt-3 text-center" >
            <img class=" border-white" src="{{ asset('/assets/employee_info_image/'.$p->document_image ) }}" alt="profile photo"/>
    </div>
    <div class="actions">

        <div class="ui positive right labeled icon button">
            Okay
        <i class="checkmark icon"></i>
        </div>
    </div>
    
</div>

<!-- <div class="ui basic modal biometric">
  <div class="ui icon header">
    
  </div>
  <div class="content text-center">
    <img class="avatar border-white" src="{{ asset('/assets/employee_info_image/'.$p->bpr_image ) }}" alt="profile photo"/>
  </div>
  <div class="actions">
    <div class="ui red basic cancel inverted button">
      <i class="remove icon"></i>
      No
    </div>
    <div class="ui green ok inverted button">
      <i class="checkmark icon"></i>
      Yes
    </div>
  </div>
</div> -->


    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    $("#passport").click(function(){
		$(".passport").modal('show');
	});

	$(".passport").modal({
		closable: true
	});



    $("#drive").click(function(){
		$(".driving").modal('show');
	});

	$(".driving").modal({
		closable: true
	});



    $("#bio").click(function(){
		$(".biometric").modal('show');
	});

	$(".biometric").modal({
		closable: true
	});


    $("#doc").click(function(){
		$(".document").modal('show');
	});

	$(".document").modal({
		closable: true
	});
    </script>
    @endsection 




