<!-- <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "desc" ]]'> -->
<table width="100%" class="table table-striped table-hover" id="employee_attendance">
    <thead>
        <tr>
            <th scope="col"><strong>#</strong></th>
            <th>Date</th>
            <th>Employee Name</th>
            <th>Time In</th>
            <th>Time Out</th>
            <th>Total Hours</th>
            <th>Over Time</th>

        </tr>
    </thead>
    <tbody id="report_tbody">
        @isset($employeeAttendance)
        @foreach ($employeeAttendance as $v)
        <tr id="tr">
            <th scope="row">{{ ($employeeAttendance ->currentpage()-1) * $employeeAttendance ->perpage() + $loop->index + 1 }}</th>
            <td>@isset($v->timein) @php echo e(date('M d,Y', strtotime($v->timein))) @endphp @endisset</td>
            <td>{{ $v->employee }}</td>
            <td>@isset($v->timein) @php echo e(date('h:i:s A', strtotime($v->timein))) @endphp @endisset</td>
            <td>@isset($v->timeout) @php echo e(date('h:i:s A', strtotime($v->timeout))) @endphp @endisset</td>
            <td>@isset($v->totalhours)
                @if($v->totalhours != null)
                @php
                if(stripos($v->totalhours, ".") === false) {
                $h = $v->totalhours;
                } else {
                $HM = explode('.', $v->totalhours);
                $h = $HM[0];
                $m = $HM[1];
                }
                @endphp
                @endif
                @if($v->totalhours != null)
                @if(stripos($v->totalhours, ".") === false)
                {{ $h }} hours
                @else
                {{ $h }} hours {{ $m }} minutes
                @endif
                @endif
                @endisset
            </td>
            <td>

                @php
                $min= $v->overtime_mins;
                if($min < 60){
                    $hours = ($min % 60). ' minutes';                    
                }
                else{
                    $hours = intdiv($min, 60).' hours '. ($min % 60). ' minutes';
                }
                
                @endphp
                {{ $hours }}

            </td>
        </tr>
        @endforeach
        @endisset
        <tr>
            <td colspan="8" align="center">
                <div class="row">
                    <div class="col">
                        {{ $employeeAttendance->links() }}
                    </div>

                </div>
            </td>
        </tr>
    </tbody>
</table>