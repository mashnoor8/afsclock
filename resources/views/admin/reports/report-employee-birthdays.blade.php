@extends('layouts.default')

    @section('meta')
        <title>Reports | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper reports, view reports, and export or download reports.">
    @endsection

    @section('content')

    <div class="container-fluid">
        <div class="row">
            <h2 class="page-title">Employee Birthdays
                <a href="{{ url('export/report/birthdays') }}" class="ui btn_upper button mini offsettop5 btn-export float-right"><i class="ui icon download"></i>Export to CSV</a>
                <a href="{{ url('reports') }}" class="ui btn_upper button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
            </h2>
        </div>

        <div class="row">
            <div class="box box-success shadow">
                <div class="box-body">
                    <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                        <thead>
                            <tr>
                               <th>#</th>
                                <th>Employee Name</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Birthday</th>
                                <th>Contact Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($empBday)
                            @foreach ($empBday as $v)
                                <tr>
                                   <th scope="row">{{ $loop->index + 1 }}</th>
                                    <td>{{ $v->firstname }} {{ $v->mi }} {{ $v->lastname }} </td>
                                    <td>{{ $v->department }}</td>
                                    <td>{{ $v->jobtitle }}</td>
                                    <td>
                                    @php $bdaydate = date('M d, Y', strtotime($v->birthday)); @endphp
                                    @if($v->birthday != null)
                                        {{ $bdaydate }}
                                    @endif
                                    </td>
                                    <td>{{ $v->mobileno }}</td>
                                </tr>
                            @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @endsection

    @section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ],lengthChange: true,searching: true,ordering: true});
    </script>
    @endsection
