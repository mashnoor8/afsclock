@extends('layouts.default')

    @section('meta')
        <title>Reports | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper reports, view reports, and export or download reports.">
    @endsection

    @section('styles')
        <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    @endsection

    @section('content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <h2 class="page-title">Employee Attendance Report
                    <a href="{{ url('reports') }}" class="ui btn_upper button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>
                </h2>
            </div>
        </div>
    </section>


    <div class="container-fluid">

        <div class="row">
            <div class="box box-success shadow">
                <div class="box-body reportstable">
                    <form action="{{ url('export/report/attendance') }}" method="post" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline three fields">
                           

                            <input id="EmployeeIDhiddenField" type="hidden" name="emp_id">
<!-- 
                            <div class="seven wide field">
                                <input id="search" type="text" name="smartsearch" value="" placeholder="Search anything"  autocomplete="off">
                            </div> -->

                            <div class="three wide field">
                                <select name="employee" id="search">
                                    <option value="">Employee</option>
                                    @isset($employee)
                                        @foreach($employee as $e)
                                            <option value="{{ $e->id }}" >{{ $e->firstname }} {{ $e->lastname }}@if($e->idno) ({{ $e->idno }}) @endif</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>

                            <div class="two wide field">
                                <input id="start" type="text" name="start" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>

                            <div class="four wide field">
                                <input id="end" type="text" name="end" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>

                            <!-- <input type="hidden" name="emp_id" value=""> -->
                            <a id="filterButton" class="ui icon button positive small inline-button"  ><i class="ui icon filter alternate"></i> Filter</a>

                            <!-- <button id="btnfilter" class="ui icon button positive small inline-button"><i class="ui icon filter alternate"></i> Filter</button> -->
                            <button type="submit" name="submit" class="ui icon button blue small inline-button" title="Download"><i class="ui icon download"></i>  </button>
                        </div>
                    </form>

                    @include('admin.reports.report-employee-attendance-table')
                    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />

                </div>
            </div>
        </div>

    </div>

    <span id="_url" style="display: none;">{{url('/')}}</span>

    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">


// Employee attendance search AJAX function
function fetch_data(query, page, start, end)
 {
     var url = document.getElementById("_url").textContent;
    //  console.log(url);
    $.ajax({
    url: url + "/reports/get_employee_attendance?query="+query + "&page="+page + "&start="+start + "&end="+end,
    success:function(data)
    {


        $('thead').html('');
        $('tbody').html('');
        $('tbody').html(data);
    }
    })
    }

    $( document ).ready(function() {

    $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);

    var query = $('#search').val();
    var start = $('#start').val();
    var end = $('#end').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(query, page, start, end);
    });


    $('#filterButton').click(function(e){
                e.preventDefault();
        var query = $('#search').val();
        var start = $('#start').val();
        var end = $('#end').val();
        var page = $('#hidden_page').val();
        // console.log(query);
        // console.log(start, end);
        fetch_data(query, page, start, end);
    });
});


// live search by name

function fetch_by_name(query, page, start, end)
        {
            var url = document.getElementById("_url").textContent;

        $.ajax({
        url: url + "/reports/get_employee_name?query="+query + "&page="+page + "&start="+start + "&end="+end ,
        success:function(data)
        {
            $('thead').html('');
            $('tbody').html('');
            $('tbody').html(data);
        }
        })
        }

        $( document ).ready(function() {

                        $(document).on('click', '.pagination a', function(event){
                        event.preventDefault();
                        var page = $(this).attr('href').split('page=')[1];
                        $('#hidden_page').val(page);

                        var query = $('#search').val();
                        var start = $('#start').val();
                        var end = $('#end').val();

                        $('li').removeClass('active');
                        $(this).parent().addClass('active');
                        fetch_by_name (query, page, start, end);
                        });


            $( "#search" ).keyup(function() {
                var query = $('#search').val();
                var page = $('#hidden_page').val();
                var start = $('#start').val();
                var end = $('#end').val();

                console.log(query);
                fetch_by_name(query, page ,start, end);
            });




            // $( "#start" ).on(function() {
            //     var query = $('#search').val();
            //     var start = $('#start').val();
            //     var page = $('#hidden_page').val();
            //     console.log(start);
            //     fetch_by_name(query, page ,start);
            // });



        $("#start")
        .datepicker({
            onSelect: function(dateText) {
                var query = $('#search').val();
                var start = $('#start').val();
                var end = $('#end').val();
                var page = $('#hidden_page').val();
                console.log(start);
                fetch_by_name(query, page ,start, end);
                // console.log("Selected date: " + dateText + "; input's current value: " + this.value);
		        $(this).change();
		    }
		})
        .on("change", function() {
            console.log("Got change event from field");
        });


        $("#end")
        .datepicker({
            onSelect: function(dateText) {
                var query = $('#search').val();
                var start = $('#start').val();
                var end = $('#end').val();
                var page = $('#hidden_page').val();
                console.log(end);
                fetch_by_name(query, page ,start, end);
                // console.log("Selected date: " + dateText + "; input's current value: " + this.value);
		        $(this).change();
		    }
		})
        .on("change", function() {
            console.log("Got change event from field");
        });


        


        });

// Employee attendance search AJAX function ends here



$("#search").select2();


    // $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});

    $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });

    // transfer idno
    // $('.ui.dropdown.getid').dropdown({ onChange: function(value, text, $selectedItem) {
    //     $('select[name="employee"] option').each(function() {
    //         if($(this).val()==value) {var id = $(this).attr('data-id');$('input[name="emp_id"]').val(id);};
    //     });
    // }});

    // $('#btnfilterNone').click(function(event) {
    //     event.preventDefault();
    //     var emp_id = $('input[name="emp_id"]').val();
    //     var date_from = $('#datefrom').val();
    //     var date_to = $('#dateto').val();
    //     var url = $("#_url").val();
    //     var gtr = 0;

    //     $.ajax({
    //         url: url + '/get/employee-attendance/', type: 'get', dataType: 'json', data: {id: emp_id, datefrom: date_from, dateto: date_to}, headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
    //         success: function(response) {
    //             showdata(response);
    //             function showdata(jsonresponse) {
    //                 var employee = jsonresponse;
    //                 var tbody = $('#dataTables-example tbody');

    //                 // clear data and destroy datatable
    //                 $('#dataTables-example').DataTable().destroy();
    //                 tbody.children('tr').remove();

    //                 // append table row data
    //                 for (var i = 0; i < employee.length; i++) {
    //                     gtr += +employee[i].totalhours;
    //                     var time_in = employee[i].timein;
    //                     var t_in = time_in.split(" ");
    //                     var time_out = employee[i].timeout;
    //                     var t_out = time_out.split(" ");

    //                     tbody.append("<tr>"+
    //                                     "<td>"+employee[i].date+"</td>" +
    //                                     "<td>"+employee[i].employee+"</td>" +
    //                                     "<td>"+ t_in[1]+" "+t_in[2] +"</td>" +
    //                                     "<td>"+ t_out[1]+" "+t_out[2] +"</td>" +
    //                                     "<td>"+employee[i].totalhours+"</td>" +
    //                                 "</tr>");
    //                 }

    //                 tbody.append("<tr class='tablefooter'>"+
    //                     "<td colspan='4'><strong>TOTAL HOURS</strong></td>"+
    //                     "<td><strong>"+gtr.toFixed(2)+"</strong></td>"+
    //                     "<td class='hide'></td>"+
    //                     "<td class='hide'></td>"+
    //                     "<td class='hide'></td>"+
    //                     "<td class='hide'></td>"+
    //                     "<td class='hide'></td>"+
    //                 "</tr>");

    //                 // initialize datatable
    //                 $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: false});
    //             }
    //         }
    //     })
    // });



    // // Finds employee attendance for specific row.
    // function getEmployeeAttendance() {

    //       var smartSearchFieldValue = document.getElementById('smartsearch').value;
    //       var datefrom = document.getElementById('datefrom').value;
    //       var dateto = document.getElementById('dateto').value;

    //       var url = document.getElementById('_url').textContent;
    //       var report_tbody = document.getElementById('report_tbody');



    //     $.get(url + '/get/employee-report/search', {searchContent: smartSearchFieldValue, datefrom:datefrom, dateto:dateto}, function(data){

    //       report_tbody.innerHTML = "";

    //       if (data[0]) {
    //         document.getElementById("EmployeeIDhiddenField").value = data[0].reference

    //       }

    //       console.log(data);

    //       for (var i = 0; i < data.length; i++) {
    //         var total_working_hour = data[i].totalhours;
    //         var hr_array = total_working_hour.split(".");

    //         report_tbody.innerHTML += "<tr><td>"+ data[i].timein +"</td><td>"+ data[i].employee +"</td><td>"+ data[i].timeout +"</td><td>"+ data[i].timein +"</td><td>"+hr_array[0]+" hr " + hr_array[1]+ "mins" + "</td></tr>";
    //       }

    //     })
    // }


    // </script>


<!-- 
<script>
//live serarch 
function searchTable() {
    var input, filter, found, table, tr, td, i, j;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("employee_attendance");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");

        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }
        }

        if (found) {
            tr[i].style.display = "";
            found = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}
</script> -->
<!-- 

<script>
//live serarch 
function searchTable() {
    var input, filter, found, table, tr, td, i, j;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("employee_attendance");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }
        }
        if (found) {
            tr[i].style.display = "";
            found = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}
</script> -->

    @endsection
