<table width="100%" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th width="5%">ID</th>
                                    <th width="15%">Employee</th>
                                    <th width="10%">Company</th>
                                    <th width="10%">Department</th>
                                    <th width="10%">Position</th>
                                    <th width="10%" class="text-center">Available Leaves</th>
                                    <th width="13%" class= "text-center">Schedule Status</th>
                                    <th width="5%">Employee Status</th>
                                    <th width="15%" class="text-center">Quick Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($data)
                                @foreach ($data as $employee)
                                @isset($employee->company)
                                
                               
                                    <tr>
                                    <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>
                                    <td>{{ $employee->idno }}</td>
                                    <td>{{ $employee->firstname }} @isset($employee->mi) {{ $employee->mi }} @endisset  {{ $employee->lastname }} </td>
                                    <td>{{ $employee->company }}</td>
                                    <td>{{ $employee->department }}</td>
                                    <td>{{ $employee->jobtitle }}</td>

                                    <td class="text-center">{!! available_leaves($employee->id)  !!}</td>
                                    
                                    <td class="text-center">@if(employee_schedule($employee->id) != NULL) <i class="check icon text-info" title="Schedule exists !"></i> @else <i class="circle icon text-danger" title="Schedule of this employee does not exist ! Please assign a schedule for this employee otherwise no attendance will be counted for this employee."></i><p class="small">No Schedule Assigned </p> @endif</td>
                                    <td>@if($employee->employmentstatus == 'Active') Active @else Archived @endif</td>
                                    <td class="align-right">

                                    <!-- <a href="{{ url('/profile/view/'.$employee->id) }}" class="ui circular basic icon button tiny" title="View Details"><i class="file alternate outline icon"></i></a> -->
            

                                    <a href="{{ url('/profile/view/'.$employee->id) }}" class="ui circular basic icon button tiny" title="View Details"><i class="file alternate outline icon"></i></a>
                                    <a href="{{ url('/employee/faceregistration/'.$employee->id) }}" class="ui circular basic icon button tiny" title="Photos"><i class="images outline icon"></i></a>
                                    <a href="{{ url('/profile/edit/'.$employee->id) }}" class="ui circular basic icon button tiny" title="Edit"><i class="edit outline icon"></i></a>
                                    <a href="{{ url('/profile/delete/'.$employee->id) }}" class="ui circular basic icon button tiny" title="Delete"><i class="trash alternate outline icon"></i></a>
                                    <a href="{{ url('/profile/archive/'.$employee->id) }}" class="ui circular basic icon button tiny" title="Archive"><i class="archive icon"></i></a>
                                    </td>
                                    </tr>
                                @endisset
                                @endforeach
                                @endisset

                                    <tr>
                                    <td colspan="12" align="center">
                                    <div class="row">
                                    <div class="col">
                                    {{ $data->links() }}
                                    </div>

                                    </div>
                                    </td>
                                    </tr>
                            </tbody>
                        </table>
