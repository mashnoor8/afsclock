@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')

<h3 class="page-title">Holidays page</h>

<section class="py-5">
  <div class="container">
    <div class="row">

      <div class="col-6  p-5">
        <div class="col box shadow p-5">
              <h3 class="text-center py-3"><i class="fa fa-calendar-plus-o pr-2" aria-hidden="true"></i> Add new holidays</h3>
              <form class="mt-5 ui form" method="post" action="{{ url('admin/add_holidays') }}">
                @csrf
              <div class="field py-2">
                <h4 class="py-1">Select Month</h4>
                <select class="ui search dropdown getid uppercase" name="month">
                    <option value="01" data-id="JAN">January</option>
                    <option value="02" data-id="FEB">February</option>
                    <option value="03" data-id="MAR">March</option>
                    <option value="04" data-id="JAN">April</option>
                    <option value="05" data-id="JAN">May</option>
                    <option value="06" data-id="JUN">June</option>
                    <option value="07" data-id="JUL">July</option>
                    <option value="08" data-id="AUG">August</option>
                    <option value="09" data-id="SEP">September</option>
                    <option value="10" data-id="OCT">October</option>
                    <option value="11" data-id="NOV">November</option>
                    <option value="12" data-id="DEC">December</option>
                </select>
              </div>
              <div class="field mt-4">
                <h4 class="py-1">Select Date</h4>
                <input type="text" class="form-control date " name="dates" placeholder="Pick the multiple dates" autocomplete="off" required>
              </div>
              <button class="ui button  ml-2 mt-3 btn_upper " type="submit"><i class="plus square outline icon"></i> Add Holidays</button>
            </form>
        </div>    
      </div>

      <div class="col-6 p-5">
      <div class="col box shadow p-5" >
        <h3 class="text-center py-3"> <i class="fa fa-calendar-check-o pr-2" aria-hidden="true"></i> Holidays</h3>
        <table class="ui celled table small">
          <thead class="small"> 
            <tr><th>Month</th>
            <th>Dates</th>
            <th>Total</th>
            <th>Actions</th>
          </tr></thead>
          <tbody class="small">

            @isset($holidays)
          
              @foreach($holidays as $data)
              <tr>
                <!-- <td data-label="Name">{{$data->month}}</td> -->
                <td data-label="Name">@php echo e(date("F", mktime(0, 0, 0, $data->month, 10)))  @endphp</td>
                <td data-label="Age">{{$data->dates}}</td>
                <td>  @php
                        $days = count(explode(',',$data->dates ))
                      @endphp
                    
                      {{$days}}
                  
                  </td>
                <td data-label="Age">
                  <!-- <a href="{{ url('admin/edit_holidays/'.$data->id) }}" class="ui circular basic icon button tiny"><i class="icon edit outline"></i></a> -->
                  <a href="{{ url('admin/delete_holidays/'.$data->id) }}" class="ui circular basic icon button tiny"><i class="icon trash "></i></a>
                </td>
              </tr>
              @endforeach
            @endisset
          </tbody>
        </table>
      </div>  
      </div>
    </div>
  </div>
</section>



@endsection

@section('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.uk.min.js" integrity="sha512-zj4XeRYWp+L81MSZ3vFuy6onVEgypIi1Ntv1YAA6ThjX4fRhEtW7x+ppVnbugFttWDFe/9qBVdeWRdv9betzqQ==" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> -->
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

<script>
$('.date').datepicker({
  language: 'en', dateFormat: 'dd',multipleDates: 'TRUE'
});

</script>


@endsection
