<!-- <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'> -->
<table width="100%" class="table table-striped table-hover">
    <thead id="attendance_thead">
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Employee</th>
            <th>Time In</th>
            <th>Time Out</th>
            {{-- <th>Break In</th>--}}
            {{-- <th>Break Out</th>--}}
            <th>Total Hours</th>
            <th>Overtime</th>
            <!-- <th>Break Duration</th> -->
            <th>Note (In / Out)</th>
            @isset($cc)
            @if($cc == 1)
            <th>Comment</th>
            @endif
            @endisset
            <th class="text-center">Actions</th>
        </tr>
    </thead>
    <tbody id="attendance_tbody">
        @isset($data)
        @foreach ($data as $d)
        <tr>
            <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>
            <td>@isset($d->timein) @php echo e(date('M d, Y', strtotime($d->timein))) @endphp @endisset</td>
            <td>{{ $d->employee }}</td>
            <td>@php $IN = date('h:i:s A', strtotime($d->timein)); echo $IN; @endphp</td>
            <td>
                @isset($d->timeout)
                @php
                $OUT = date('h:i:s A', strtotime($d->timeout));
                @endphp
                @if($d->timeout != NULL)
                {{ $OUT }}
                @endif
                @endisset
            </td>
            {{-- <td>--}}
            {{-- @isset($d->break_in)--}}
            {{-- @php $break_in_time = date('h:i:s A', strtotime($d->break_in)); @endphp--}}
            {{-- {{ $break_in_time }}--}}
            {{-- @endisset--}}

            {{-- </td>--}}
            {{-- <td>--}}
            {{-- @isset($d->break_out)--}}
            {{-- @php $break_out_time = date('h:i:s A', strtotime($d->break_out)); @endphp--}}
            {{-- {{ $break_out_time }}--}}
            {{-- @endisset--}}

            {{-- </td>--}}
            <td>@isset($d->totalhours)
                @if($d->totalhours != null)
                @php
                if(stripos($d->totalhours, ".") === false) {
                $h = $d->totalhours;
                } else {
                $HM = explode('.', $d->totalhours);
                $h = $HM[0];
                $m = $HM[1];
                }
                @endphp
                @endif
                @if($d->totalhours != null)
                @if(stripos($d->totalhours, ".") === false)
                {{ $h }} hours
                @else
                {{ $h }} hours {{ $m }} minutes
                @endif
                @endif
                @endisset
            </td>
            <td>

                @php
                $min= $d->overtime_mins;
                if($min < 60){ $hours=($min % 60). ' minutes' ; } else{ $hours=intdiv($min, 60).' hours '. ($min % 60). ' minutes'; } @endphp {{ $hours }}
            </td>
                
            <td>
                @if($d->status_timein != null OR $d->status_timeout != null)
                @if($d->status_timein == 'Not Scheduled' && $d->status_timeout == 'Not Scheduled')
                <span class="red">Not Scheduled</span>
                @else
                <span class="@if($d->status_timein == 'Late Arrival') orange @else blue @endif">{{ $d->status_timein }}</span>
                /
                @isset($d->status_timeout)
                <span class="@if($d->status_timeout == 'Early Departure') red @else green @endif">
                    {{ $d->status_timeout }}
                </span>
                @endisset
                @endif
                @else
                <span class="blue">{{ $d->status_timein }}</span>
                @endif
            </td>

            @isset($cc)
            @if($cc == 1)
            <td>{{ $d->comment }}</td>
            @endif
            @endisset
            <td class="align-center">
                <button class="ui circular basic icon button tiny" type="button" id="attendance-details" title="View" onclick="getAttendanceDetails('{{$d->id}}')"><i class="list ul icon"></i></button>

                <a href="{{ url('/attendance/edit/'.$d->id) }}" title="Edit" class="ui circular basic icon button tiny"><i class="edit outline icon"></i></a>

                <!-- <a href="{{ url('/attendance/delete/'.$d->id) }}" title="Delete" class="ui circular basic icon button tiny delete "><i class="trash alternate outline icon"></i></a> -->
                <a title="Delete" class="ui circular basic icon button tiny delete " onclick="delete_confirmation('{{$d->id}}')"><i class="trash alternate outline icon"></i></a>


                <div class="ui modal test" name="md{{$d->id}}">
                    <i class="close icon"></i>
                    <div class="header text-center ">
                        <h3 class="py-2 text-center">Attendance Details</h3>

                    </div>
                    <div class="scrolling content">
                        <div class="ui two column grid">
                            <div class="row pl-3">
                                <div class="column ">
                                    <h5 class="py-1"><i class="fa fa-user-circle" aria-hidden="true"></i> {{ $d->employee }}</h5>
                                    <h5 class="py-1"><i class="fa fa-calendar" aria-hidden="true"></i> @isset($d->timein) @php echo e(date('d-M-Y', strtotime($d->timein))) @endphp @endisset</h5>
                                </div>
                                <div class="column">
                                    <p class="display-4 text-center py-2" id="emoji"></p>
                                    <h4 id="breakHour{{$d->id}}" style="text-align: center; font-size: 15px;"></h4>
                                </div>
                            </div>

                            <div class="row pl-3">

                                <div class="column">
                                    <h3 class="py-2">Break In/Out</h3>
                                    <table width="100%" class="table table-bordered table-hover" id="dataTables-example" data-order='[[ 0, "desc" ]]'>
                                        <thead>
                                            <tr>
                                                <th>In</th>
                                                <th>Out</th>
                                            </tr>
                                        </thead>
                                        <tbody id="break_tbody{{$d->id}}">

                                        </tbody>

                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="actions">

                        <div class="ui positive right labeled icon button">
                            Done
                            <i class="checkmark icon"></i>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
        @endisset

        <tr>
            <td colspan="12" align="center">
                <div class="row">
                    <div class="col">
                        {{ $data->links() }}
                    </div>

                </div>
            </td>
        </tr>

    </tbody>
</table>
