@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('styles')
    
@endsection

@section('content')


<section>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">FAQ'S
                    <a href="{{ url('admin/help') }}" class="ui  btn_upper  button mini offsettop5 float-right">Return</a>

               
                </h2>
            </div>
        </div>
    </div>
</section>

<!-- START FAQ -->
<section class="py-5" id="faqs">
    <div class="container-fluid px-5 ">
        <div class="row px-4 pt-5">
            <div class="col-12 pl-4">
                <div class="row">
<!-- First Column  -->
                    <div class="col-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Trial period</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Is your trial period free?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Yes, it's completely free. No credit card required, no upfront costs, no hidden charge, no-obligation and no string attached. We are completely free to walk away if you don’t like our service. We are 100% confident you will choose to stay with us. </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">Is my data available after the trial period?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Yes. During our trial period, you can enjoy as much as you want with all our features. </p>
                                        </div>
                                        <div class="collapse col" id="collapseExample1">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">What happens to my data if I choose not to continue after the trial period ends?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">What happens to my data after my trial period ends and I don’t want to continue with it?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">If you choose not to continue with us, you can close your account and all your data will be deleted. </p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Registration</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">What sort of information you required for an account registration?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Just the usual your Name, work email and password to start with. </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> How long does it take for an account to register and use all your features?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"> Straightaway. You can use all our features right away after you verify your email address and download our software.   </p>
                                        </div>
                                        <div class="collapse" id="collapseExample2">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> During registration which sort of email address do you prefer?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">We recommend using a work email address. </p>
                                            </div>
                                             
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="reg(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="reg(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Task Manager</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Who can assign tasks to each other?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Within a team, anyone can assign tasks to anyone with relevant task details. For example, junior staff can assign tasks to senior staff if required. </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Will the employee be notified about the new task?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"> Yes. The employee will receive an email about tasks and notifications when logged into their employee portal.</p>
                                        </div>
                                        <div class="collapse" id="collapseExample3">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> How do you manage tasks?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">You can manage tasks by assigning them to your employees with relevant details and expected deadlines. </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> Can an employee extend tasks under some circumstances?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes, employees can extend tasks with new expected dates and reasons for delaying. </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> How many times can you extend a task? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Twice, after that task, gets void. </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> Is there any way of knowing the status of assigned tasks?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes, you will see “in progress” status in your task manager dashboard.</p>
                                            </div>
                                             
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Salary Calculations</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">  Is the salary amount on native currency? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"></p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> When is the salary report available? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Our salary system gets updated on real-time. You can view any time you wish to with appropriate dates selected.  </p>
                                        </div>
                                        <div class="collapse col" id="collapseExample4">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> What if my employees are paid by the day?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> In this circumstance, we suggested you calculate them as hourly employees.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">If an employee takes an authorised leave does that reflect on your salary calculation?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes it does reflect on our salary calculations. The employee will be losing payment for that day.  </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">For hourly employee what is your basic constraint?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Minute. We calculate for every minute they invest working.  </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> Does a yearly paid staff receives more pay for an extra day working?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">IT’s NOT A VERY RELEVENT HERE  </p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>  
                    </div>

<!-- Second Column  -->

                    <div class="col-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter " >Installation</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">How big is your Attendance Keeper software?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">How big is your Attendance Keeper software? </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">Would I require any high spec pc or laptop to run attendance keeper?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">No. You don’t need a high spec computer to run our software. </p>
                                        </div>
                                        <div class="collapse" id="collapseExample5">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">How long does it take for the software to install? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> 2 or 3 minutes. </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Who can install Attendance Keeper software on a computer? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> We recommend the Admin/ Manager to complete the installation process .</p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Facial recognition</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">Does your facial recognition work with glasses on or with masks on? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Yes, It works. When you register your face, we recommend taking 7 or 8 pictures from different angles. So our system can dictate face correctly. </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> What if an employee forgets to Clock-out?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"></p>
                                        </div>
                                        <div class="collapse" id="collapseExample6">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Does Attendance Keeper software take random selfies? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> No camera is only in operation during Clock-in and Clock-out when prompted using our Attendance Keeper software.  </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Does the camera stay on all the time? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> No camera is only in operation during Clock-in and Clock-out when prompted using our Attendance Keeper software. </p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample6" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample6" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Location/Branch related</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">What if I have different offices in different locations? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Attendance Keeper is designed to make your work life easier and productive. You can create an unlimited* number of branches located in different locations and consists of different staffs, with the respect of employees available in your subscription. For example, you have 20 employees working for you in 5 locations. You can choose our plan for 7-20 employee plan. That will allow you to create 5 locations or more if needed to accommodate these 20 employees.</p>
                                            <p class="faq-answer mb-4 pb-1 text-muted">*the term unlimited is restricted only for the number of employees you manage through us. No restrictions on the location you create.</p>
                                            
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Can the Attendance Keeper manage different time zones? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">You can have offices in 7 different continents without any problem as long as you are within your limit of employees.</p>
                                        </div>
                                        <div class="collapse" id="collapseExample7">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body"> I can use it with just one location/branch?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> Yes definitely. You can just create one branch.  </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">  “Shift Assign” option for scheduling ? </h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> No camera is only in operation during Clock-in and Clock-out when prompted using our Attendance Keeper software. </p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample7" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample7" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">General </h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">Who owns my employee's data or does Attendance Keeper have access to my employee's data ? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">We don’t own these data and we certainly don’t have access to your data. You are completely in charge of your data..</p>
                                            
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> How you handle privacy? </h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"> We handle our customer privacy with the utmost respect. You can view our privacy policay here .</p>
                                        </div>
                                        <div class="collapse" id="collapseExample9">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Employee clock-in or clock-out grace period.</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> Every company operates differently in this matter. Some companies allow 5,7 or even 10 minutes of a grace period. For example, if an employee clock-ins at 08:55 AM or 09.05 AM, the system will take this extra or less of 5 minutes as a grace period and round it up for 09:00 AM..  </p>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> We work with absolute honesty and simplicity. We simply count for every minute accordingly and generate reports with exact total hours and minutes of clock-in and clock-out time with highlighting late and early departure based upon their aforementioned schedule. We think this is the best solution for both employee and employer.</p>

                                            </div>
                                            
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample9" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample9" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>  
                    </div>
              
<!--  Third Column  -->
                    <div class="col-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Authorised Device Access only</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> How do I know if my device has been authorised?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"> </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Can remote PC/Laptop be authorised?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Yes, as long as the device code matches with our system.  </p>
                                        </div>
                                        <div class="collapse" id="collapseExample10">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Does your device code available online?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">No, to the best of our knowledge. Our device code is system generated. </p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample10" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample10" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Discrete Screenshot</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> What are the requirements for the discrete screenshot work?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Users need to run our Attendance Keeper software in the background. </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Where can I view my employee’s screenshot?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">Once you logged in your account. You can see all the authorised devices, then you can select the one you want to see the screenshot of.  </p>
                                        </div>
                                        <div class="collapse" id="collapseExample11">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Will my employee be notified once a screenshot has been taken?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">No. The whole point of our feature is to be discrete to the user. The user won’t be notified about screenshots.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">How many screenshots does it take during one employee’s shift?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">There’s no definite answer for it. It could be 5 or 10 screenshots during a shift.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">When does Attendance Keeper software take discrete screenshots?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Randomly. There’s no specific timing for the software to take screenshots.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Who has access to these screenshots?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Admin/manager has the access to these screenshots. To do admin/manager needs to log in his web portal and select the device he wants to look into.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Does the Discrete Screenshot feature run 24/7 in the user’s computer?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"></p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">What is the resolution of the screenshots?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"></p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Does the screenshot have a timestamp on them?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes. They have date and time on them.</p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Are these screenshots sharable?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted"> Yes. You can print/save them.</p>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample11" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample11" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card shadow-sm p-4">
                                    <div class="text-center">
                                        <h3 class="pb-3 letter ">Web-based Employee’s Portal</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body"> Why do you need an Employee's Portal ?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted"> </p>
                                        </div>
                                        <div class="col-12">
                                            <div class="faq-question-q-box">Q.</div>
                                            <h4 class="faq-question text-body">  How do employees receive their portal’s credentials ?</h4>
                                            <p class="faq-answer mb-4 pb-1 text-muted">HR manager should be able to provide these credentials. You usually create them when registering new employees.  </p>
                                        </div>
                                        <div class="collapse" id="collapseExample12">
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Who has access to the employee's portal ?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Only the employee has access to the portal. As credentials are private to an individual. </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Can an employee have access to their portal remotely 24/7 ?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes </p>
                                            </div>
                                            <div class="col-12">
                                                <div class="faq-question-q-box">Q.</div>
                                                <h4 class="faq-question text-body">Can an employee update his information from the portal ?</h4>
                                                <p class="faq-answer mb-4 pb-1 text-muted">Yes, he/she update any information required.</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            <a id="unsave" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample12" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" style=" display: none " > <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
                                            <a id="save" class="btn btn-sm float-right" data-toggle="collapse" href="#collapseExample12" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="func(this.id)" > <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> view</a>
                                        
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>  
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

</section>
<!-- END FAQ -->

@endsection

@section('scripts')
<script>

function func(id) {
            if(id == "save"){
               document.getElementById("unsave").style.display="block";
               document.getElementById(id).style.display="none";
             }else{
               document.getElementById("save").style.display="block";
               document.getElementById(id).style.display="none";
             }
             
}

</script>


@endsection