@extends('layouts.default')

    @section('meta')
        <title>Schedules | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper schedules, view all employee schedules, add schedule or shift, edit, and delete schedules.">
    @endsection

    @section('styles')

    @endsection

    @section('content')
<section>
  <div class="py-2">
     <h3 class="page-title">Schedule Templates
     <a href="{{ url('schedules') }}" class="ui  btn_upper  button mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a>

     </h3>
  </div>
</section>

<div class="container"></div>
  <div class="row pb-4"">
    <div class="col-8">
    
      <!-- <p>Manage existing schedule and create new templates.</p> -->
    </div>
    <div class="col-4">
      <a href="{{ url('schedules/templates/create')}}" class="ui button blue float-right"><i class="ui icon plus square outline"></i> Create Template</a>
    </div>
  </div>
  <div class="row">
            <div class="col-12">
            @if(count($templates)>0)  
              <div class="box box-success">
                  <div class="box-body" >
                                  <div class= " py-3">
                                          <table class="table table-striped table-hover " id="dataTables-example"
                            data-order='[[ 0, "asc" ]]'>
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Name</th>
                                                      <th>Sat</th>
                                                      <th>Sun</th>
                                                      <th>Mon</th>
                                                      <th>Tue</th>
                                                      <th>Wed</th>
                                                      <th>Thu</th>
                                                      <th>Fri</th>
                                                      <th>Break</th>
                                                      
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    @isset($templates)
                                                      @foreach ($templates as $t)
                                                        <tr class="tabel-data" style="padding: 50px;">
                                                          <td>{{ $loop->index +1 }}</td>
                                                          <td class="">{{$t->name}}</td>
                                                          <td>
                                                            @if($t->saturday)
                                                            {!!schedule_template($t->saturday) !!}
                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->sunday)
                                            
                                                            {!!schedule_template($t->sunday) !!}
                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->monday)
                                                            {!!schedule_template($t->monday) !!}
                                                          
                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->tuesday)
                                                            {!!schedule_template($t->tuesday) !!}
                                                            
                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->wednesday)
                                                            {!!schedule_template($t->wednesday) !!}

                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->thursday)
                                                            {!!schedule_template($t->thursday) !!}

                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>
                                                            @if($t->friday)
                                                            {!!schedule_template($t->friday) !!}

                                                            @else
                                                            N/A
                                                            @endif
                                                          </td>
                                                          <td>{{$t->break_allowence}} Min</td>

                                                        </tr>
                                                      @endforeach
                                                    @endisset
                                                  </tbody>
                                          </table>
                                  </div>
                  </div>
              </div> 
            @else
            
            <div class="row">
                <div class="col-6  offset-3">
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4 class= "py-2">!  <span class="px-1"></span> No Employee Available </h4>
                    </div> 
                </div>
            </div>               
            @endif     
            </div>                  
  </div>                  
</div>

    @endsection

    @section('scripts')


    <script type="text/javascript">


    </script>
    @endsection
