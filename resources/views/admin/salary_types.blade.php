@extends('layouts.default')

@section('meta')
    <title>Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('content')

<h3 class="page-title px-3">Salaey Types</h3>
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-6   pr-5">
        <div class="col box shadow p-5">
          <h3 class= "text-center py-2"><i class="fa fa-plus-circle pr-2" aria-hidden="true"></i>Add Salary Type</h3>
          <p class="text-center pb-2">Fill the form below to create new type</p>
          <form class="ui form" method="post" action="{{ url('admin/add_salary_types') }}">
            @csrf
            <div class="field">
              <label>Salary Type Name</label>
              <input type="text" name="type_name" placeholder="Type the salary type name. e.g: hourly, monthly" required>
            </div>


            <button class="ui button btn_upper ml-2" type="submit"><i class="icon plus square outline"></i> Create</button>
          </form>
        </div>
      </div>
      <div class="col-6 ">
        <div class="col box shadow p-5">
            <h3 class="py-2 text-center"> <i class="fa fa-money pr-2" aria-hidden="true"></i>Salary Types</h3>
            <table class="ui celled table">
              <thead>
                <tr><th>Type</th>
                <th>Actions</th>

              </tr></thead>
              <tbody>
                @isset($salary_types)
                  @foreach($salary_types as $salary_type)
                  <tr>
                    <td data-label="Name">{{$salary_type->type}}</td>
                    <td data-label="Age">
                      <a href="{{ url('admin/edit_salary_types/'.$salary_type->id) }}" class="ui circular basic icon button tiny"><i class="icon edit outline"></i></a>
                      <a href="{{ url('admin/delete_salary_type/'.$salary_type->id) }}" class="ui circular basic icon button tiny"><i class="icon trash "></i></a>
                    </td>
                  </tr>
                  @endforeach
                @endisset
              </tbody>
            </table>
        </div>
      </div>
    <div>
  </div>
</section>
@endsection

@section('scripts')

@endsection
