<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>  Attendance Keeper</title>
    <link rel="icon" href="manager/images/logo_icon.png" type="image/icon type">

    @yield('meta')

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/semantic-ui/semantic.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/DataTables/datatables.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/semantic-ui/semantic.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/clock.css') }}">
   

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/style.css') }}">
   

    <!-- font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/assets/vendor/html5shiv/html5shiv.min.js') }}></script>
            <script src=" {{ asset('/assets/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
    <style>.highlight{ color:white !important;}</style>
    @yield('styles')
    
        <! -- Mailchamp code -->

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/38052ff9149b91edc49f393c8/1ee71d6e1aacff48fcbfced8c.js");</script>



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-188720658-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js',new Date());
    gtag('config','UA-188720658-1');
    </script>
</head>



<body>

<div class="wrapper">

    <nav id="sidebar">
        <div class="sidebar-header ">
            <div class="logo">
                <a href="{{url('personal/dashboard')}}" class="simple-text">
                    <img src="{{ asset('/assets/images/img/logo-bottom.png') }}">
                </a>
            </div>
        </div>

        <ul class="list-unstyled components">
            <li class="">
                <a  href="{{ url('personal/dashboard') }}" class="@isset($title) @if($title == "dashboard") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal pr-3"></i>
                    Dashboard
                </a>
            </li>
            <li class="">
                <a href="{{ url('personal/attendance/view') }}" class="@isset($title) @if($title == "my_attendances") highlight  @endif @endisset">
                    <i class="ui icon clock outline pr-3"></i>
                    My Attendances
                </a>
            </li>
            <li class="">
                <a href="{{ url('personal/schedules/view') }}" class="@isset($title) @if($title == "my_schedules") highlight  @endif @endisset">
                    <i class="ui icon calendar alternate outline pr-3"></i>
                    My Schedules
                </a>
            </li>

            <li class="">
                <a href="{{ url('personal/tasks/mytasks') }}" class="@isset($title) @if($title == "my_tasks") highlight  @endif @endisset">
                    <i class="ui icon tasks pr-3"></i>
                    My Tasks
                </a>
            </li>

            <li class="">
                <a href="{{ url('personal/leaves/view') }}" class="@isset($title) @if($title == "my_leaves") highlight  @endif @endisset">
                    <i class="ui icon calendar plus outline pr-3"></i>
                    My Leave
                </a>
            </li>
            <!-- <li>
                <a href="{{ url('clock') }}" target="_blank" class="item">
                    <i class="ui icon clock outline pr-3"></i>
                    Clock In/Out
                </a>
            </li> -->
            <li class="">
                <a href="{{ url('personal/notice/view') }}" class="@isset($title) @if($title == "notices") highlight  @endif @endisset">
                    <i class="ui icon calendar plus outline pr-3"></i>
                   Notice
                </a>
            </li>
        </ul>
    </nav>

    <div id="body">
        <nav class="navbar navbar-expand-lg navbar-light bg-lightblue">
            <div class="container-fluid">

                <button type="button" id="slidesidebar" class="ui icon button btn-light-outline">
                    <i class="ui icon bars"></i> Menu
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto navmenu">
                        <li class="nav-item">
                            <div class="ui pointing link dropdown item" tabindex="0">
                                <i class="ui icon linkify"></i> <span class="navmenutext">Quick Access</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" tabindex="-1">
                                    <!-- <a href="{{ url('clock') }}" target="_blank" class="item"><i
                                                class="ui icon clock outline"></i> Clock In/Out</a> -->
                                    <div class="divider"></div>
                                    <a href="{{ url('personal/profile/view') }}" target="_blank" class="item"><i
                                                class="ui icon user outline"></i> My Profile</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="ui pointing link dropdown item" tabindex="0">
                                <i class="ui icon user outline"></i><span
                                        class="navmenutext">@isset(Auth::user()->name) {{ Auth::user()->name }} @endisset</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" tabindex="-1">
                                    <!-- <a href="{{ url('personal/update-user') }}" class="item"><i
                                                class="ui icon user"></i> Update User</a> -->
                                    <a href="{{ url('personal/update-password') }}" class="item"><i
                                                class="ui icon lock"></i> Change Password</a>
                                    <div class="divider"></div>
                                    <a href="{{ url('logout') }}" class="item"><i class="ui icon power"></i> Logout</a>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            @yield('content')
        </div>

        <!-- <input type="hidden" id="_url" value="{{url('/')}}"> -->
        <script>
            //var y = '@isset($var){{$var}}@endisset';
            //var d = {"t":"Activation required!","b":"You need to activate this app otherwise you can't use some features.","a":"You only have 30 days trial to use this app."}
        </script>
    </div>
</div>

<div class="ui tiny modal reminderModal">
    <i class="close icon"></i>
    <div class="header">
        <h3>Hey Deadline is arriving soon for the following task..</h3>
    </div>
    <div class="content">

      <table class="table table-striped nobordertop">
          <thead>
          <tr>
              <th class="text-left">Title</th>
              <th class="text-left">Deadline</th>
          </tr>
          </thead>
          <tbody id="task_info_table">


          </tbody>
      </table>

    </div>
    <div class="actions">

        <div class="ui positive right labeled icon button">
            I'm aware
            <i class="checkmark icon"></i>
        </div>
    </div>
</div></td>

<audio id="bellSound">
  <source src="{{ asset('/assets/audio/bell.mp3') }}" type="audio/mpeg">
</audio>

<span id="_url" style="display: none;">{{url('/')}}</span>



<script src="{{ asset('/assets/vendor/jquery/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('/assets/js/script.js') }}"></script>
<script src="{{ asset('/assets/vendor/momentjs/moment.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/momentjs/moment-timezone-with-data.js') }}"></script>
<script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(window).bind("pageshow", function(event) {
  if (event.originalEvent.persisted) {
      window.location.reload(); 
  }
});
</script>
@if ($success = Session::get('success'))
    <script>
        $(document).ready(function () {
            $.notify({icon: 'ti-check', message: "{{ $success }}"}, {type: 'success', timer: 600});
        });
    </script>
@endif

@if ($error = Session::get('error'))
    <script>
        $(document).ready(function () {
            $.notify({icon: 'ti-close', message: "{{ $error }}"}, {type: 'danger', timer: 600});
        });
    </script>
@endif

@yield('scripts')

<script type="text/javascript">



function taskReminderMonitor(){
  var reference_id = {{Auth::user()->id}};
  reference_id = parseInt(reference_id);
  if (reference_id) {

    var url = document.getElementById('_url').textContent;

    $.get( url + '/personal/dashboard/task_reminder', {reference_id:reference_id}, function(data){

      if (data != "") {


        var task_info_table = document.getElementById('task_info_table');



        task_info_table.innerHTML = "<tr><td>"+ data.title +"</td><td>"+ data.deadline +"</td></tr>";

        $(".reminderModal").modal('show');

        var bellSound = document.getElementById("bellSound");
        bellSound.play();
      }

    })

  }
}


setInterval(taskReminderMonitor, 100000);


</script>

</body>
</html>
