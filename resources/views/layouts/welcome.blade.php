<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>  Attendance Keeper</title>
    <link rel="icon" href="manager/images/logo_icon.png" type="image/icon type">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.6.55/css/materialdesignicons.min.css" rel="stylesheet">
    <!-- fonts -->


    <link rel="preconnect" href="https://fonts.gstatic.com">

    <!-- font awesome  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('manager/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('manager/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css')

    <!-- Font Family -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@600&display=swap" rel="stylesheet">

    <title>Attendance Keeper</title>
    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
  
<! -- Mailchamp code -->

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/38052ff9149b91edc49f393c8/1ee71d6e1aacff48fcbfced8c.js");</script>



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-188720658-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js',new Date());
    gtag('config','UA-188720658-1');
    </script>
  </head>
  
  
  <!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/9348000.js"></script>
<!-- End of HubSpot Embed Code -->
  <body>



<nav class="navbar navbar-expand-lg navbar-light bg-white  fixed-top nav-bar-padding shadow-sm px-5">
    <div class="container-fluid py-1 ">
            <a class="navbar-brand  px-3 py-2" class="logo-dark" height="18"  href="{{url('/')}}"><img src="{{'manager/images/logo-top.png'}}" alt="" class="logo-dark" width="100px" /></a>
            <!-- <a class="" href="{{url('/')}}">  <h4 class="font-weight-600 float-left mr-5 mt-1 text-black">Attendance Keeper</h4></a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>

                <!-- menus -->
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
              <!-- left menu -->
                   <ul class="navbar-nav mr-auto align-items-center">
                        <li class="nav-item mx-1">
                            <a class="nav-link active btn-lg text-gray-500 navbar-menu " href="{{ url('/features') }}"><i class="fa fa-th-large" aria-hidden="true"></i> Features</a>
                        </li>
                        <!-- <li class="nav-item mx-lg-1">
                            <a class="nav-link" href="">Features</a>
                        </li> -->
                        <li class="nav-item mx-1">
                            <a class="nav-link btn-lg text-gray-500 navbar-menu " href="{{ url('/pricing') }}"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Pricing</a>
                        </li>
                        <li class="nav-item mx-lg-1 solution-p">
                            <a class="nav-link btn-lg text-gray-500 navbar-menu" href="{{ url('/solution') }}"><i class="fa fa-comments-o" aria-hidden="true"></i> Solutions</a>
                        </li>
                        
                        <li class="nav-item mx-1 solution">
                        <div class="dropdown show">
                            <a class="nav-link btn-lg text-gray-500 dropdown-toggle navbar-menu" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-comments-o" aria-hidden="true"></i> Solutions
                            </a>

                            <div class="dropdown-menu shadow " aria-labelledby="dropdownMenuLink" id="drop">
                                
                                <div class="container ">
                                    <div class="row x">
                                        <div class="col-12  ">
                                            
                                            <div class="row p-3">
                                                <div class="col-lg-6 col-md-12 p-3">
                                                    <div class="row">
                                                            <div class="col-3 text-center ">
                                                                <h3 class="text-gray "><i class="fa fa-question-circle" aria-hidden="true"></i></h3>

                                                            </div>
                                                            <div class="col-9">
                                                            <a  href="{{url('/faqs')}}"><h3 class="solution-title navbar-menu"> FAQ’s</h3></a> 
                                                                <p class="solution-subtitle">Frequently Asked Questions</p>

                                                            </div>
                                                        </div>
                                                    </div>


                                                <div class="col-6 p-3">
                                                    <div class="row">
                                                            <div class="col-3 text-center">
                                                                <h3 class="text-gray"> <i class="fa fa-laptop" aria-hidden="true"></i></h3>

                                                            </div>
                                                            <div class="col-9">
                                                            <a  href="{{url('/remote-staff')}}"><h3 class="solution-title navbar-menu"> Remote staff management</h3></a>
                                                                <p class="solution-subtitle">See how you can utilise your remote staff </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <div class="col-6 p-3">
                                                    <div class="row">
                                                            <div class="col-3 text-center">
                                                                <h3 class="text-gray"> <i class="fa fa-book" aria-hidden="true"></i></h3>

                                                            </div>
                                                            <div class="col-9">
                                                            <a  href="{{url('/user-manual')}}"><h3 class="solution-title navbar-menu"> User Manuel</h3></a>
                                                                <p class="solution-subtitle">Explaining our features in depth</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <div class="col-6 p-3">
                                                    <div class="row">
                                                            <div class="col-3 text-center ">
                                                                <h3 class="text-gray">  <i class="fa fa-phone-square" aria-hidden="true"></i></h3>

                                                            </div>
                                                            <div class="col-9">
                                                            <a  href="{{url('/call-center')}}"><h3 class="solution-title navbar-menu"> Call centre management </h3></a>
                                                                <p class="solution-subtitle">See how you can manage your staff</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-6 p-3">
                                                    <div class="row">
                                                            <div class="col-3 text-center">
                                                                <h3 class="text-gray"><i class="fa fa-envelope" aria-hidden="true"></i></h3>

                                                            </div>
                                                            <div class="col-9">
                                                            <a  href="{{url('/contact')}}"><h3 class="solution-title navbar-menu"> Contact us </h3></a>
                                                                <p class="solution-subtitle">Contact us form </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-6 p-3">
                                                    <div class="row">
                                                        <div class="col-3 text-center">
                                                            <h3 class="text-gray"> <i class="fa fa-users" aria-hidden="true"></i></h3>
                                                        </div>
                                                        <div class="col-9">
                                                        <a  href="{{url('/warehouse-staff')}}"><h3 class="solution-title navbar-menu"> Warehouse staff management</h3></a>
                                                            <p class="solution-subtitle">See how you can save money</p>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        </li>
                        <!-- <li class="nav-item mx-lg-1">
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Link with href</a>
                            
                        </li> -->
                            <!-- <div class="collapse" id="collapseExample">
                                <div class=" -body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                </div>
                            </div> -->
                    </ul>

              <div class="">
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    @if (Route::has('register'))
                            <li class="nav-item mr-2">
                                <a class="nav-link btn btn-lg font-weight-600 text-base bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-4" href="{{ url('/registration') }}"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{ __('START your 60 days free trial') }}</a>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a class="nav-link btn btn-lg font-weight-600 text-base bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-4" href="{{ route('login') }}"><i class="fa fa-unlock-alt" aria-hidden="true"></i> {{ __('Login') }}</a>
                        </li>
                       
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown " class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                               @if(Auth::user()->role_id == 1)
                                 <a class="dropdown-item" href="{{ url('personal/dashboard') }}">
                               @elseif(Auth::user()->role_id == 2)
                                  <a class="dropdown-item" href="{{ url('dashboard') }}">
                               @elseif(Auth::user()->role_id == 3)
                                 <a class="dropdown-item" href="{{ url('/branches') }}">
                               @endif
                                    Dashboard
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
              </div>
            </div>
          </div>  
</nav>


<!-- <section class="pt-5 mt-4">

</div> -->
@yield('content')


<!-- SuperMan login Modal  -->

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="card p-2" action="{{url('/superman_login')}}" method="post">
                <div class="input-group">
                    <input type="text" class="form-control reedem-color" placeholder="Security Code">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">Redeem</button>
                    </div>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- START FOOTER -->
<!-- <footer class="bg-dark pt-5 pb-3">
    <div class="container px-4">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{'images/logo-bottom.png'}}" alt="" class="logo-dark" height="80px" />
                <p class="text-white-50 mt-4">Hyper makes it easier to build better websites with
                    <br> great speed. Save hundreds of hours of design
                    <br> and development by using it.
                </p>

                <ul class="social-list list-inline mt-3">
                    <li class="list-inline-item text-center">
                        <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                    </li>
                    <li class="list-inline-item text-center">
                        <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                    </li>
                    <li class="list-inline-item text-center">
                        <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
                    </li>
                    <li class="list-inline-item text-center">
                        <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github"></i></a>
                    </li>
                </ul>

            </div>

            <div class="col-lg-2 mt-3 mt-lg-0">
                <h5 class="text-white">Company</h5>

                <ul class="list-unstyled pl-0 mb-0 mt-3">
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">About Us</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Documentation</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Blog</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Affiliate Program</a></li>
                </ul>

            </div>

            <div class="col-lg-2 mt-3 mt-lg-0">
                <h5 class="text-white">Apps</h5>

                <ul class="list-unstyled pl-0 mb-0 mt-3">
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Ecommerce Pages</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Email</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Social Feed</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Projects</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Tasks Management</a></li>
                </ul>
            </div>

            <div class="col-lg-2 mt-3 mt-lg-0">
                <h5 class="text-white">Discover</h5>

                <ul class="list-unstyled pl-0 mb-0 mt-3">
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Help Center</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Our Products</a></li>
                    <li class="mt-2"><a href="javascript: void(0);" class="text-white-50">Privacy</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mt-5">
                    <p class="text-white-50 mt-4 text-center mb-0">©2020 . Design and coded by
                        Evidentbd</p>
                </div>
            </div>
        </div>
    </div>
</footer> -->
        <!-- END FOOTER -->    
<section>
    <div class="container">

        <div class="row  border-top ">
            <div class="col-12 text-center">
                <div class="mt-5 mb-3 ">
                <p class=" mt-4  text-center  "> © {{ \Carbon\Carbon::now()->format('Y') }} AttendanceKeeper is Coded and Designed by Evident BD Ltd. Company Number 13017375.  Registered in United Kingdom. All rights reserved.<br>  
</p>
                </div>
            </div>
           
        </div>
    </div>

</section>        

        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    @yield('script')

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
 
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script></body>
     <script>
$(window).bind("pageshow", function(event) {
  if (event.originalEvent.persisted) {
      window.location.reload(); 
  }
});
</script>
</html>
