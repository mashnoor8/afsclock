<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>  Attendance Keeper</title>
    <link rel="icon" href="manager/images/logo_icon.png" type="image/icon type">
    @yield('meta')

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/semantic-ui/semantic.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/clock.css') }}">
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/DataTables/datatables.min.css') }}">
    <link href="{{ asset('/assets/vendor/datetimepicker/datetimepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

    <style>
#toast-container>.toast-info {

background-color: #313a46;
color: #EEEEEE;
}
#toast-container>.toast-success {
    background-color: orange;
color: black;

}
#toast-container>.toast-warning {

background-color: orange;
color: black;
}
#toast-container>.toast-error {

background-color: red;
color: black;
}

.highlight{
    color: white !important;

}
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/assets/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->
<style>.highlight{ color:white !important;}</style>
    @yield('styles')
    
        <! -- Mailchamp code -->

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/38052ff9149b91edc49f393c8/1ee71d6e1aacff48fcbfced8c.js");</script>



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-188720658-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js',new Date());
    gtag('config','UA-188720658-1');
    </script>
</head>


<body>


<div class="wrapper">

    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <div class="logo">
                <a href="{{ url('branches')}}"  class="simple-text">
                    <img src="{{ asset('/assets/images/img/logo-bottom.png') }}">
                </a>
            </div>
        </div>

        <ul class="list-unstyled components">

            <li class="list">
                <a href="{{ url('branches') }}" class="@isset($title) @if($title == "Branch") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal"></i>
                    Branch
                </a>
            </li>
            <li class="list">
                <a href="{{ url('admins') }}" class="@isset($title) @if($title == "Admin") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal"></i>
                    Admins
                </a>
            </li>
            <li class="list">
                <a href="{{ url('/subscription') }}" class="@isset($title) @if($title == "Subscription") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal"></i>
                    Subscription
                </a>
            </li>
            <li class="list">
                <a href="{{ url('/my_subscription') }}" class="@isset($title) @if($title == "my_subscription") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal"></i>
                    My Subscription
                </a>
            </li>

            <li class="list">
                <a href="{{ url('/desktop_apps') }}" class="@isset($title) @if($title == "applications") highlight  @endif @endisset">
                    <i class="ui icon sliders horizontal"></i>
                    Download
                </a>
            </li>
        </ul>
    </nav>

    <div id="body" class="">
        <nav class="navbar navbar-expand-lg navbar-light bg-lightblue">
            <div class="container-fluid">

                <button type="button" id="slidesidebar" class="ui icon button btn-light-outline">
                    <i class="ui icon bars"></i> Menu
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto navmenu">
                        <!-- <li class="nav-item">
                            <div class="ui pointing link dropdown item" tabindex="0">
                                <i class="ui icon linkify"></i> <span class="navmenutext">Quick Access</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" tabindex="-1">
                                    <a href="{{ url('clock') }}" target="_blank" class="item"><i
                                                class="ui icon clock outline"></i> Clock In/Out</a>
                                    <div class="divider"></div>
                                    <a href="{{ url('employees/new') }}" class="item"><i class="ui icon user plus"></i>
                                        New Employee</a>
                                    <div class="divider"></div>
                                    <a href="{{ url('users/roles') }}" class="item"><i class="ui icon user"></i> Roles</a>
                                    <a href="{{ url('fields/company') }}" class="item"><i
                                                class="ui icon university"></i> Company</a>
                                    <a href="{{ url('fields/department') }}" class="item"><i class="ui icon cubes"></i>
                                        Department</a>
                                    <a href="{{ url('fields/jobtitle') }}" class="item"><i
                                                class="ui icon pencil alternate"></i> Job Title</a>
                                    <a href="{{ url('fields/leavetype') }}" class="item"><i
                                                class="ui icon calendar alternate outline"></i> Leave Type</a>
                                </div>
                            </div>
                        </li> -->
                        <li class="nav-item">
                            <div class="ui pointing link dropdown item" tabindex="0">
                                <i class="ui icon user outline"></i> <span
                                        class="navmenutext">@isset(Auth::user()->name) {{ Auth::user()->name }} @endisset</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" tabindex="-1">
                                    <!-- <a href="{{ url('update-profile') }}" class="item"><i class="ui icon user"></i>
                                        Update Account</a> -->
                                    <a href="{{ url('super_admin/update-password') }}" class="item"><i class="ui icon lock"></i>
                                        Change Password</a>
                                    <a href="{{ url('/super_admin/profile') }}" class="item"><i class="ui icon lock"></i>
                                        Profile</a>
                                    <!-- <a href="{{ url('personal/dashboard') }}" target="_blank" class="item"><i
                                                class="ui icon sign-in"></i> Switch to MyAccount</a> -->
                                    <div class="divider"></div>
                                    <a href="{{ url('logout') }}" class="item"><i class="ui icon power"></i> Logout</a>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            @yield('content')
        </div>

        <input type="hidden" id="_url" value="{{url('/')}}">
        <script>
            //var y = '@isset($var){{$var}}@endisset';
            //var d = {"t":"Activation required!","b":"You need to activate this app otherwise you can't use some features.","a":"You only have 30 days trial to use this app."}
        </script>
    </div>
</div>



<script src="{{ asset('/assets/vendor/jquery/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('/assets/js/script.js') }}"></script>
<script src="{{ asset('/assets/vendor/momentjs/moment.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/momentjs/moment-timezone-with-data.js') }}"></script>
<script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('/assets/vendor/datetimepicker/datetimepicker.js') }}"></script>
<!-- <script>
//this makes the current link containing li of class "active1"
for (var i = 0; i < document.links.length; i++) {
    if (document.links[i].href == document.URL) {
        document.links[i].className = 'list active';
    }
}

</script> -->

<script>
$(window).bind("pageshow", function(event) {
  if (event.originalEvent.persisted) {
      window.location.reload(); 
  }
});
</script>
@if ($success = Session::get('success'))
    <script>
        $(document).ready(function () {
            $.notify({
                    icon: 'ui icon check',
                    message: "{{ $success }}"
                },
                {type: 'success', timer: 400}
            );
        });
    </script>
@endif

@if ($error = Session::get('error'))
    <script>
        $(document).ready(function () {
            $.notify({
                    icon: 'ui icon times',
                    message: "{{ $error }}"
                },
                {type: 'danger', timer: 400});
        });
    </script>
@endif
 <!-- toastr -->
 @if (Session::has('message'))
 <script>


var type = "{{ Session::get('alert-type', 'success') }}"
switch(type){



case 'success':
toastr.options.timeOut = 10000;
toastr.options.closeButton = true;
toastr.options.preventDuplicates = true;
toastr.options.debug = true;
// toastr.options.closeHtml = '<button><i class="far fa-window-close"></i></button>';
toastr.options.showEasing = 'swing';
toastr.options.positionClass = 'toast-bottom-full-width';
toastr.options.hideEasing = 'linear';
toastr.options.showMethod = 'slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'fadeOut';
toastr.success("{{Session::get('message')}}");
// var audio = new Audio('audio.mp3');
// audio.play();
break;

case 'error':
toastr.options.timeOut = 5000;
toastr.options.closeButton = true;
toastr.options.preventDuplicates = true;
toastr.options.debug = false;
// toastr.options.closeHtml = '<button><i class="far fa-window-close"></i></button>';
toastr.options.showMethod = 'slideDown';
toastr.options.positionClass = 'toast-bottom-right-full-width';
toastr.options.showEasing = 'swing';
toastr.options.hideEasing = 'linear';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'fadeOut';
toastr.error("{{Session::get('message')}}");
// var audio = new Audio('audio.mp3');
// audio.play();
break;

}

</script>
@endif
@yield('scripts')



</body>
</html>
