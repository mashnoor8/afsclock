<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Smart Clock | Attendance Keeper</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/semantic-ui/semantic.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/clock.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/jquery.toast.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="{{ asset('/assets/vendor/html5shiv/html5shiv.min.js') }}"></script>
            <script src="{{ asset('/assets/vendor/respond/respond.min.js') }}"></script>
        <![endif]-->

        <style>
#toast-container>.toast-info {

background-color: #313a46;
color: #EEEEEE;
}
#toast-container>.toast-success {
background-color: green;
color: black;

}
#toast-container>.toast-warning {

background-color: orange;
color: black;
}
#toast-container>.toast-error {

background-color: red;
color: black;
}

.highlight{
    color: white !important;

}
    </style>

        @yield('styles')
    </head>
    <body>

    <img src="{{ asset('/assets/images/img/clock-background.png') }}" class="wave">
    <div class="wrapper">
        
        <div id="body">

            <div class="content">

                @yield('content')
             
            </div>

        </div>
    </div>

    <script src="{{ asset('/assets/vendor/jquery/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/momentjs/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/momentjs/moment-timezone-with-data.js') }}"></script>
    <script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
    <script src="{{ asset('/assets/js/script.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.toast.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



   <!-- toastr -->
   <script>

@if (Session::has('message'))
var type = "{{ Session::get('alert-type', 'success') }}"
switch(type){



case 'success':
toastr.options.timeOut = 5000;
toastr.options.closeButton = true;
toastr.options.preventDuplicates = true;
toastr.options.debug = true;
// toastr.options.closeHtml = '<button><i class="far fa-window-close"></i></button>';
toastr.options.showEasing = 'swing';
toastr.options.hideEasing = 'linear';
toastr.options.showMethod = 'slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'fadeOut';
toastr.success("{{Session::get('message')}}");
// var audio = new Audio('audio.mp3');
// audio.play();
break;

case 'error':
toastr.options.timeOut = 5000;
toastr.options.closeButton = true;
toastr.options.preventDuplicates = true;
toastr.options.debug = false;
// toastr.options.closeHtml = '<button><i class="far fa-window-close"></i></button>';
toastr.options.showMethod = 'slideDown';
toastr.options.showEasing = 'swing';
toastr.options.hideEasing = 'linear';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'fadeOut';
toastr.error("{{Session::get('message')}}");
// var audio = new Audio('audio.mp3');
// audio.play();
break;

}
@endif
</script>


 <script>
$(window).bind("pageshow", function(event) {
  if (event.originalEvent.persisted) {
      window.location.reload(); 
  }
});
</script>












    <script>
        var timezone = "@isset($tz){{ $tz }}@endisset";
    </script>

@if ($success = Session::get('success'))
    <script>
        $(document).ready(function () {
            $.notify({icon: 'ti-check', message: "{{ $success }}"}, {type: 'success', timer: 600});
        });
    </script>
@endif

@if ($error = Session::get('error'))
    <script>
        $(document).ready(function () {
            $.notify({icon: 'ti-close', message: "{{ $error }}"}, {type: 'danger', timer: 600});
        });
    </script>
@endif

    @yield('scripts')

    </body>
</html>
