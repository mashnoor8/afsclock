<table width="100%" class="table table-striped table-bordered table-hover delegation mt-2">
                            <thead id="leave_thead" >
                                <tr>
                                    <th>#</th>
                                    <th>Leave Type</th>
                                    <th>Leave From</th>
                                    <th>Leave To</th>
                                    <th>Reason</th>
                                    <th>Return Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody id="leave_tbody">
                                @isset($l)
                                @foreach ($l as $data)

                                
                                <tr> 
                                <th scope="row">{{ ($l->currentpage()-1) * $l->perpage() + $loop->index + 1 }}</th>
                                    <td>{{ $data->type }}</td>
                                    <td>{{ e(date('M d, Y', strtotime($data->leavefrom ))) }}</td>
                                    <td>{{ e(date('M d, Y', strtotime($data->leaveto )))  }}</td>
                                    <td>{{  $data->reason }}</td>
                                    <td>{{ e(date('M d, Y', strtotime($data->returndate ))) }}</td>
                                    <td><span class="">{{ $data->status }}</span></td>
                                    <td>
                                        @if($data->status == 'Approved')
                                       
                                            <button type="button" class="ui circular basic icon tiny teal view button" data-id="{{ $data->id }}" title=" View"><i class="ui icon file alternate"></i></button>
                                        @else
                                            <a href="{{ url('personal/leaves/edit/'.$data->id) }}" class="ui blue icon mini basic button"><i class="ui icon edit"></i> Edit</a>
                                            <a  title="Delete" class="ui red icon mini basic button" onclick="delete_confirmation('{{$data->id}}')"><i class="ui icon trash"></i> Delete</a> 
                                            <!-- <a href="{{ url('personal/leaves/delete/'.$data->id) }}" class="ui red icon mini basic button"><i class="ui icon trash"></i> Delete</a> -->
                                            @isset($data->comment)
                                                <!-- <button data-id="{{ $data->id }}" class="ui grey icon mini basic button uppercase" data-tooltip='{{ $data->comment }}' data-variation='wide' data-position='top right'><i class="ui icon comment alternate"></i></button> -->
                                                <button class="ui circular basic icon button tiny uppercase" title='{{ $data->comment }}'><i class="ui icon comment alternate"></i></button>
                                                    
                                            @endisset
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endisset

                                <tr>
            <td colspan="8" align="center">
                <div class="row">
                    <div class="col">
                        {{ $l->links() }}
                    </div>
                    
                </div>
            </td>
            </tr>
                            </tbody>
                        </table>