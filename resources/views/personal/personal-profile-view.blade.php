@extends('layouts.personal')

    @section('meta')
        <title>My Profile | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper my profile, view my profile, and update my personal information.">
    @endsection

    @section('content')
    
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Profile
                    <a href="{{ url('personal/profile/edit') }}" class="ui basic blue button mini offsettop5 float-right"><i class="ui icon edit"></i>Edit</a>
                </h2>
            </div>    
        </div>

        <div class="row">
            <div class="col-md-4 float-left">
                <div class="box box-success">
                    <div class="box-body employee-info">
                            <div class="author">
                            @if($profile_photo != null)
                                <img class="avatar border-white" src="{{ asset('/assets/faces/'.$profile_photo) }}" alt="profile photo"/>
                            @else
                                <img class="avatar border-white" src="{{ asset('/assets/images/faces/default_user.jpg') }}" alt="profile photo"/>
                            @endif
                            </div>
                            <p class="description text-center">
                                <h4 class="title">@isset($user->firstname) {{ $user->firstname }} @endisset @isset($user->lastname) {{ $user->lastname }} @endisset</h4>
                                <table style="width: 100%" class="profile-tbl">
                                    <tbody>
                                        <tr>
                                            <td>Email</td>
                                            <td><span class="p_value">@isset($user->emailaddress) {{ $user->emailaddress }} @endisset</span></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No.</td>
                                            <td><span class="p_value">@isset($user->mobileno) {{ $user->mobileno }} @endisset</span></td>
                                        </tr>
                                        <tr>
                                            <td>ID No.</td>
                                            <td><span class="p_value">@isset($user->idno) {{ $user->idno }} @endisset</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </p>
                    </div>
                </div>
            </div>

            <div class="col-md-8 float-left">
                <div class="box box-success">
                    <div class="box-header with-border">Personal Information</div>
                    <div class="box-body employee-info">
                            <table class="tablelist">
                                <tbody>
                                    <tr>
                                        <td><p>Civil Status</p></td>
                                        <td><p>@isset($user->civilstatus) {{ $user->civilstatus }} @endisset</p></td>
                                    </tr>
                                   <!--  <tr>
                                        <td><p>Age</p></td>
                                        <td><p>@isset($user->age) {{ $user->age }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Height <span class="help">(cm)</span></p></td>
                                        <td><p>@isset($user->height) {{ $user->height }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Weight <span class="help">(pounds)</span></p></td>
                                        <td><p>@isset($user->weight) {{ $user->weight }} @endisset</p></td>
                                    </tr> -->
                                    <tr>
                                        <td><p>Gender</p></td>
                                        <td><p>@isset($user->gender) {{ $user->gender }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Date of Birth</p></td>
                                        <td><p>
                                            @isset($user->birthday) 
                                                @php echo e(date("F d, Y", strtotime($user->birthday))) @endphp
                                            @endisset
                                        </p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Place of Birth</p></td>
                                        <td><p>@isset($user->birthplace) {{ $user->birthplace }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Home Address</p></td>
                                        <td><p>@isset($user->homeaddress) {{ $user->homeaddress }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>National ID</p></td>
                                        <td><p>@isset($user->nationalid) {{ $user->nationalid }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h4 class="ui dividing header">Designation</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Company</td>
                                        <td>@isset($company_data->company) {{ $company_data->company }} @endisset</td>
                                    </tr>
                                    <tr>
                                        <td><p>Department</p></td>
                                        <td><p>@isset($department) {{ $department->department }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td>Position</td>
                                        <td>@isset($job_title) {{ $job_title->jobtitle }} @endisset</td>
                                    </tr>
                                    <tr>
                                        <td>Leave Privilege</td>
                                        <td><p>@if($leavetype) {{ $leavetype->leavetype }}( {{$leavetype->limit}} days {{$leavetype->percalendar}} ) @else N/A @endif </p></td>
                                       
                                    </tr>

                                    <tr>
                                        <td>Taken Leaves</td>
                                        
                                        <td><p>@if(Auth::user()) {!! taken_leaves(Auth::user()->id) !!} days @else N/A @endif </p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td>Available Leaves</td>
                                        
                                        <td><p>@if(Auth::user()) {!! available_leaves(Auth::user()->id) !!} days @else N/A @endif </p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td><p>Employment Type</p></td>
                                        <td><p>@isset($user->employmenttype) {{ $user->employmenttype }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Employement Status</p></td>
                                        <td><p>@isset($user->employmentstatus) {{ $user->employmentstatus }} @endisset</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Official Start Date</p></td>
                                        <td><p>
                                            @isset($user->startdate) 
                                                @php echo e(date("F d, Y", strtotime($user->startdate))) @endphp
                                            @endisset
                                        </p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Date Regularized</p></td>
                                        <td><p>
                                            @isset($user->dateregularized) 
                                                @php echo e(date("F d, Y", strtotime($user->dateregularized))) @endphp
                                            @endisset
                                        </p></td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    </script>
    @endsection 




