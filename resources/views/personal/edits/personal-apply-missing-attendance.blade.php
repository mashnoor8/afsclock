@extends('layouts.personal')
    
    @section('meta')
        <title>Apply For Missing Attendance | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper edit employee attendance.">
    @endsection 

    @section('styles')
        <link href="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/datetimepicker/datetimepicker.css') }}" rel="stylesheet">

    @endsection

    @section('content')

    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Apply For Missing Attendance
                    <a href="{{ url('personal/attendance/view') }}" class="ui btn_upper  button mini offsettop5 float-right "><i class="ui icon chevron left"></i>Return</a>
                </h2>
            </div>    
        </div>

        <div class="row">
        <div class="col-md-6 offset-3">
            <div class="box box-success">
                <div class="box-content">
                    @if ($errors->any())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">There were some errors with your submission</div>
                        <ul class="list">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ url('personal/apply/missing_attendance') }}" class="ui form" method="post" accept-charset="utf-8">
                    @csrf
                                <div class="field">
                                    <label for="">Date</label>
                                    <input class = "airdatepicker" type="text" autocomplete ="off" placeholder="yyyy-mm-dd" name="date" value="" required/>
                                </div>
                       
                            <div class="field">
                                <label for="">Time In</label>
                                <input type='text' id="datetimepicker" placeholder="yyyy-mm-dd 00:00" autocomplete="off" name="time_in" required/>
                            </div>
                            <div class="field">
                                <label for="">Time Out</label>
                                <input type='text' id="datetimepicker1" placeholder="yyyy-mm-dd 00:00" autocomplete="off" name="time_out" />
                            </div>
                              
                   
                    <div class="fields">
                        <div class="sixteen wide field">
                            <label>Reason</label>
                            <textarea class="" rows="5" name="reason" autocomplete="off" required></textarea>
                        </div>
                    </div>
                   
                
                
                <div class="box-footer bg-white pb-3 ">
                    <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Send</button>
                    <a class="ui red small button" href="{{ url('personal/attendance/view') }}"><i class="ui times icon"></i> Cancel</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('/assets/vendor/datetimepicker/datetimepicker.js') }}"></script>
    
    <script type="text/javascript">
    $('.jtimepicker').mdtimepicker({format:'h:mm:ss tt', theme: 'blue', hourPadding: true});
    $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });
    $('#datetimepicker').datetimepicker();
    $('#datetimepicker1').datetimepicker();

    </script>
    @endsection