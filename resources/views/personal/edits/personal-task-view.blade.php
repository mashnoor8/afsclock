@extends('layouts.personal')

@section('meta')
    <title>View Task | Attendance Keeper</title>
    <meta name="description" content="Attendance Keeper edit employee attendance.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">View Task</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 card offset-3">
                <div class="box box-success">
                    <div class="box-content">
                       
                        <div class="display-4 text-center py-3"><i class="fa fa-info-circle"></i></div>
                        <form id="edit_task_form" action="" class="ui form" accept-charset="utf-8">
                            
                            <div class="field">
                                <label>Assigned By</label>
                                <input type="text"  class="readonly" readonly="" value="@isset($task->assignedTo){{ $task->assignedTo->firstname }} {{ $task->assignedTo->lastname }}@endisset">
                            </div>

                            <div class="field">
                                <label>Title</label>
                                <input type="text" name="title" class="readonly" readonly=""  value="@isset($task->title){{ $task->title }}@endisset">
                            </div>
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <label>Description</label>
                                    <textarea class="" rows="5" class="readonly" readonly="" >@isset($task->description){{ $task->description }}@endisset</textarea>
                                </div>
                            </div>

                            

                            <div class="field">
                                <label for="">Deadline</label>
                                <input type="text" placeholder="0000-00-00" class="readonly" readonly="" value="@isset($task->deadline){{ $task->deadline }}@endisset"/>
                            </div>
                            
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">
        $('.jtimepicker').mdtimepicker({format:'h:mm:ss tt', theme: 'blue', hourPadding: true});
        $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });
    </script>
@endsection
