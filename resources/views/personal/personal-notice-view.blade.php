@extends('layouts.personal')

@section('meta')
    <title>My Tasks | Attendnace Keeper</title>
    <meta name="description"
          content="Attendance Keeper my schedules, view my schedule records, view present and previous schedules.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section>
    <h2 class="page-title py-2">Notices</h2>
</section>
    <div class="container-fluid pt-2 pl-0" >
        <div class="row">
            <div class="col-12 ">

            <div class=" ">
                <div class=" ">
                            
                    <div class="ui secondary blue pointing tabular menu pl-0">
                        <a class="item active pl-0" data-tab="system">Personal notice </a>
                        <a class="item" data-tab="about">Notice for all</a>
                    </div>
                    
                    <div class="ui tab active pl-0" data-tab="system">
                        <div class="col-md-12 pl-0">
                            <div class="tab-content pl-0">
                                <div class="ui grid pl-0">
                                    @isset($notices)
                                        @foreach($notices as $notice)

                                            @php 
                                                $now =  \Carbon\Carbon::now();
                                                $valid_time =  \Carbon\Carbon::parse($notice->valid_till) ;
 
                                                if( $valid_time->gt($now)){

                                    
                                                    $time =$valid_time->diffInDays($now) . ' Days' ;

                                                    if($time == 0 ){
                                                        $time =$valid_time->diffInHours($now) . ' Hours' ;
                                                    }

                                                }
                                                else{
                                                    $time =null;
                                                }
                                            @endphp 


                                            @isset($time)    
                                            <div class="three wide column d-flex pl-0">
                                                <div class="ui card">
                                                    <div class="content">
                                                        <div class="header pl-0">{{ $notice->noticeBY->firstname }} {{ $notice->noticeBY->lastname }}</div>
                                                        <div class="meta pt-1">
                                                            <span> {{  $time  }}   </span>
                                                            <a>@if($notice->reference) 
                                                            {{$notice->noticeFor-> firstname}}  {{$notice->noticeFor-> lastname}} 
                                                            
                                                                @else  {{"Everyone"}}

                                                                @endif
                                                            </a>
                                                        </div>
                                                        <p class="pt-2">{{$notice->message }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endisset

                                        @endforeach
                                    @endisset
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class="ui tab" data-tab="about">
                        <div class="col-md-12 pl-0">
                            <div class="tab-content pl-0">
                                <div class="ui grid pl-0">
                                    @isset($notices_all)
                                        @foreach($notices_all as $notice_all)

                                            @php 
                                                $now =  \Carbon\Carbon::now();
                                                $valid_time =  \Carbon\Carbon::parse($notice_all->valid_till) ;

                                                if( $valid_time->gt($now)){

                                    
                                                    $time =$valid_time->diffInDays($now) . ' Days' ;

                                                    if($time == 0 ){
                                                        $time =$valid_time->diffInHours($now) . ' Hours' ;
                                                    }

                                                }
                                                else{
                                                    $time =null;
                                                }
                                            @endphp 

                                            @isset($time)    
                                            <div class="three wide column d-flex pl-0">
                                                <div class="ui card">
                                                    <div class="content">
                                                        <div class="header pl-0">{{ $notice_all->noticeBY->firstname }} {{ $notice_all->noticeBY->lastname }}</div>
                                                        <div class="meta pt-1">
                                                            <span> {{  $time  }} </span>
                                                            <a>@if($notice_all->reference) 
                                                            {{$notice_all->noticeFor-> firstname}}  {{$notice_all->noticeFor-> lastname}} 
                                                            
                                                                @else  {{"Everyone"}}

                                                                @endif
                                                            </a>
                                                        </div>
                                                        <p class="pt-2">{{$notice_all->message }} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endisset

                                        @endforeach
                                    @endisset
                                </div> 

                            </div>
                        </div>
                    </div>


                </div>
            </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')

<script type="text/javascript">
    $('.menu .item').tab();
</script>

@endsection