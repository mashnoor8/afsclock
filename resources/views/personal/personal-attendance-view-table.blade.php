<table width="100%" class="table table-bordered table-hover" >
    <thead id="attendance_thead" >
        <tr>
        <th>#</th>
            <th>Date</th>
            <th>Time In</th>
            <th>Time Out</th>

            <th>Total Hours</th>
            <!-- <th>Break Duration</th> -->
            <th>Status (In/Out)</th>
            <th>More</th>
        </tr>
    </thead>
    <tbody id="attendance_tbody">
        @isset($data)
        @foreach ($data as $v)
            <tr>
            <th scope="row">{{ ($data->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</th>

                <td>@isset($v->date) @php echo e(date('M d, Y', strtotime($v->timein))) @endphp @endisset</td>
                <td>@if($v->timein ) @php echo e(date('h:i:s A', strtotime($v->timein))); @endphp @else N/A @endif</td>
                    <td>@if($v->timeout ) @php echo e(date('h:i:s A', strtotime($v->timeout))); @endphp  @else N/A @endif</td>

                <td>
                @isset($v->totalhours)
                    @if($v->totalhours != null)
                        @php
                            if(stripos($v->totalhours, ".") === false) {
                                $h = $v->totalhours;
                            } else {
                                $HM = explode('.', $v->totalhours);
                                $h = $HM[0];
                                $m = $HM[1];
                            }
                        @endphp
                    @endif
                    @if($v->totalhours != null)
                        @if(stripos($v->totalhours, ".") === false)
                            {{ $h }} hours
                        @else
                            {{ $h }} hours {{ $m }} minutes
                        @endif
                    @endif
                @endisset
                </td>

                <td>
                    @if($v->status_timein != '' && $v->status_timeout != '')
                        <span class="@if($v->status_timein == 'Late Arrival') orange @else blue @endif">{{ $v->status_timein }}</span> /
                        <span class="@if($v->status_timeout == 'Early Departure') red @else green @endif">{{ $v->status_timeout }}</span>
                    @elseif($v->status_timein == 'Late Arrival')
                        <span class="orange">{{ $v->status_timein }}</span>
                    @else
                        <span class="blue">{{ $v->status_timein }}</span>
                    @endif
                </td>
                <td>
                    <button class="ui circular basic icon button tiny" type="button" id="attendance-details" onclick="getAttendanceDetails('{{$v->id}}')" title="View"><i class="list ul icon"></i></button>
                </td>

            </tr>
        @endforeach
        @endisset
        <tr>
            <td colspan="8" align="center">
                <div class="row">
                    <div class="col">
                        {{ $data->links() }}
                    </div>
                    
                </div>
            </td>
            </tr>
    </tbody>
</table>