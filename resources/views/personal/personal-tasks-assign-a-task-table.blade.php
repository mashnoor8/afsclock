<table width="100%" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Employee</th>
                                    <th>Title</th>
                                    <th>Deadline</th>
                                    <th>Finish Date</th>
                                    <th>Status</th>
                                    <th>Comment</th>
                                    <th class="align-right">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($data)
                                    @foreach ($data as $task)
                                        <tr>
                                            <td>@isset($task->assignedTo->firstname) {{ $task->assignedTo->firstname }} @endisset @isset($task->assignedTo->lastname){{ $task->assignedTo->lastname }} @endisset </td>
                                            <td>{{ $task->title }}</td>
                                            @if($task->extended_deadline)
                                            <td>@if($task->extended_deadline->new_deadline ) @php echo e(date('M d, Y h:i:s A', strtotime($task->extended_deadline->new_deadline))); @endphp @else N/A @endif</td>
                                            @else
                                            <td>@if($task->deadline ) @php echo e(date('M d, Y h:i A', strtotime($task->deadline))); @endphp @else N/A @endif</td>
                                            @endif
                                            <td>@if($task->finishdate ) @php echo e(date('M d, Y h:i A', strtotime($task->finishdate))); @endphp @else N/A @endif</td>
                                            @php
                                                $color = "red";
                                                $done_status = "Pending";
                                                if(isset($task->finishdate))
                                                    {

                                                        if($task->finishdate>$task->deadline)
                                                        {
                                                            $done_status = "Done (Delayed)";
                                                            $color = "red";
                                                        }
                                                        else
                                                        {
                                                            $done_status = "Done (In Time)";
                                                            $color = "green";
                                                        }
                                                    }

                                            @endphp
                                            <td><span class="{{ $color }}">{{ $done_status }}</span></td>
                                            <td>{{ $task->comment }}</td>
                                            <td class="align-right">

                                                <a href="{{ url('personal/tasks/assignatask/edit/'.$task->id) }}"
                                                class="ui circular basic icon button tiny"><i
                                                            class="icon edit outline"></i></a>

                                        <a  title="Delete" class="ui circular basic icon button tiny delete "  title="Delete" onclick="delete_confirmation('{{$task->id}}')"><i class="trash alternate outline icon"></i></a>
                                            

                                                <!-- <a href="{{ url('personal/tasks/assignatask/delete/'.$task->id) }}"
                                                class="ui circular basic icon button tiny"><i
                                                            class="icon trash alternate outline"></i></a> -->
                                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                                <tr>
            <td colspan="8" align="center">
                <div class="row">
                    <div class="col">
                        {{ $data->links() }}
                    </div>
                    
                </div>
            </td>
            </tr>
                                </tbody>
                            </table>