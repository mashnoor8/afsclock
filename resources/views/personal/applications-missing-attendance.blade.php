@extends('layouts.personal')

@section('meta')
    <title>Applications For Missing Attendances | Attendance Keeper</title>
    <meta name="description"
          content="Attendance Keeper attendance, view all employee attendances, clock-in, edit, and delete attendances.">
@endsection

@section('content')




    <div class="container-fluid px-0">
        <div class="row">
          <div class="col">
            <h2 class="page-title">Applications For Missing Attendances
            <a class="btn btn-sm btn_upper float-right" href="{{ url('personal/apply/missing-attendance') }}" role="button"><i class = "ui icon plus"></i>Apply For Missing Attendance</a>
                          <a href="{{ url('personal/attendance/view') }}" class="btn btn-sm btn_upper float-right"><i class="ui icon chevron left"></i>Return</a>
            </h2>
          </div>
        </div>
        


        <div class="row">
       
            <div class="col-12 pt-3">

         
            @if(count($applications) > 0)  
                <div class="box box-success ">
                    <div class="box-body">
                   
                   



<!-- <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'> -->
<table width="100%" class="table table-striped table-hover">
                   <thead id="attendance_thead">
                            <tr>
                                <th></th>                             
                                <th>Applications</th>
                                <th>Status</th>
                                <th>Comment</th>
                                <th>Time</th>


                               

                            </tr>
                            </thead>   
                           <tbody>
                           @isset($applications)
                           @foreach($applications as $application)
                            <tr>
                            <td><i class="star outline icon"></i></td>
                           
                            <td>@if($application->AdminApproved == "Applied") You applied for approving a missing attendance for @php echo e(date('M d, Y', strtotime($application->date))) @endphp. @else Admin responded against your application for approving a missing attendance for @php echo e(date('M d, Y', strtotime($application->date))) @endphp. @endif</td>
                            <td>{{ $application->AdminApproved }}</td>
                            <td>@if($application->comment != Null) {{ $application->comment }} @else N/A @endif</td>
                            <td>@if($application->AdminApproved == "Applied") @php echo e(date('M d, Y H:i:s A', strtotime($application->created_at))) @endphp @else @php echo e(date('M d, Y H:i:s A', strtotime($application->updated_at))) @endphp @endif</td>
                            

                            </tr>





                           @endforeach
                           @endisset
                           </tbody>
                        </table>

                 
                    </div>
                
                </div>
            @else
            
            <div class="row">
                <div class="col-6  offset-3">
                    <div class="box-body text-center p-5">
                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4 class= "py-2">!  <span class="px-1"></span> No Applications For Missing Attendence </h4>
                    </div> 
                </div>
            </div>    
            @endif    
            </div>
        </div>


    </div>

@endsection

