@extends('layouts.personal')

    @section('meta')
        <title>My Attendances | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper my attendance, view all my attendances, and clock-in/out.">
    @endsection

    @section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <style>
        .ui.active.modal {position: relative !important;}
        .datepicker {z-index: 999 !important; }
        .datepickers-container {z-index: 9999 !important;}
    </style>
    @endsection

    @section('content')
     
    <div class="container-fluid  px-0">
        <div class="row ">
            <div class="col-12 py-2">
              <h2 class="page-title">My Attendances
              <div class="ui slider checkbox" style="margin-left: 100px;">
  <input type="checkbox" id="attendance_email" name="newsletter" @isset($email_preference) @if($email_preference == 1) checked="checked" @endif @endisset>
  <label>Accept Email for Confirmation of Attendance</label>
</div>
              <a class="btn btn-sm btn_upper float-right" href="{{ url('personal/apply/missing-attendance') }}" role="button"><i class = "ui icon plus"></i>Apply For Missing Attendance</a>
              </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success shadow-sm p-3">
                <div class="box-body reportstable">
                    <div class="row">
                        <div class="col-9">
                            <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                                {{ csrf_field() }}
                                <div class="inline two fields">


                                    <div class="two wide field">
                                        <input id="start" type="text" name="start" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                                        <i class="ui icon calendar alternate outline calendar-icon"></i>
                                    </div>

                                    <div class="two wide field">
                                        <input id="end" type="text" name="end" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                                        <i class="ui icon calendar alternate outline calendar-icon"></i>
                                    </div>
                                    <button id="filterButton" class="ui button  btn_upper small"><i class="ui icon filter alternate"></i> Filter</button>
                                </div>
                                
                            </form>
                        </div>
                        <div class="col-3">
                
                
                <a href="{{ url('personal/applications/missing_attendances') }}" class="btn btn-sm btn_upper float-right"><i class="bell outline icon"></i>Applications For Missing Attendances</a>
                        </div>
                    </div>
                   @include('personal.personal-attendance-view-table')
                   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                </div>
              </div>
            </div>
        </div>
    </div>

    <section>
    <div class="ui modal test" name="attendanceDetailsModal">
          <i class="close icon"></i>
          <div class="header">
              <h3>Attendance Details</h3>
          </div>
          <div class="content">
              <div class="ui two column grid">
                  <!-- <div class="row">
                      <div class="column">
                          <h5>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h5>
                          <h5></h5>
                      </div>
                  </div> -->
                  <div class="row">
  
                      <div class="column">
                          <h4 class="py-3">Break In/Out</h4>
                          <table width="100%" class="table table-bordered table-hover">
                              <thead>
                              <tr>
                                  <th>In</th>
                                  <th>Out</th>
                              </tr>
                              </thead>
                              <tbody id="break_tbody">

                              </tbody>

                          </table>
                      </div>
                      <div class="column">
                        <h4 id="breakHour" style="text-align: center; font-size: 15px;" class="pt-5"></h4>
                      </div>
                  </div>

              </div>
          </div>
          <div class="actions">

              <div class="ui positive right labeled icon button">
                  Done
                  <i class="checkmark icon"></i>
              </div>
          </div>
      </div>
    </section>
    <span id="url" style="visibility:hidden;">{{url('/')}}</span>

    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});
    $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });


$(document).ready(function(){

var url = document.getElementById('url').textContent;
$("#attendance_email").on('change', function() {
  
    $.ajax({
    url: url + "/personal/attendance/email_preference",
    success:function(data)
    {
        console.log(data);
    }
    });
  
});

});

// Employee attendance search AJAX function
function fetch_data( page, start, end)
 {
     var url = document.getElementById('url').textContent;
     console.log(url);
    $.ajax({
    url: url + "/get/personal/attendance?start="+ start + "&page=" + page + "&end=" + end,
    success:function(data)
    {
        // console.log(data);
        $('#attendance_thead').html('');
        $('#attendance_tbody').html('');
        $('#attendance_tbody').html(data);
    }
    })
}

    $( document ).ready(function() {

    $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);

   
    var start = $('#start').val();
    var end = $('#end').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data( page, start, end);
    });


    $('#filterButton').click(function(e){
        e.preventDefault();
        
        var start = $('#start').val();
        var end = $('#end').val();
        var page = $('#hidden_page').val();
        // console.log(query);
        // console.log(start, end);
        fetch_data( page, start, end);
    });
});





    // $('#filterform').submit(function(event) {
    //     event.preventDefault();
    //     var date_from = $('#datefrom').val();
    //     var date_to = $('#dateto').val();
    //     var url = $("#_url").val();

    //     $.ajax({
    //         url: url + '/get/personal/attendance/',type: 'get',dataType: 'json',data: {datefrom: date_from, dateto: date_to},headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
    //         success: function(response) {
    //             showdata(response);
    //             function showdata(jsonresponse) {
    //                 var employee = jsonresponse;
    //                 var tbody = $('#dataTables-example tbody');

    //                 // clear data and destroy datatable
    //                 $('#dataTables-example').DataTable().destroy();
    //                 tbody.children('tr').remove();

    //                 // append table row data
    //                 for (var i = 0; i < employee.length; i++) {
    //                     var in_status = employee[i].status_timein;
    //                     var out_status = employee[i].status_timeout;

    //                     function t_in_status(in_status) {
    //                         if(in_status == 'Late Arrival'){
    //                             return 'orange';
    //                         } else {
    //                             return 'blue';
    //                         }
    //                     }

    //                     function t_out_status(out_status) {
    //                         if(out_status == 'Early Departure'){
    //                             return 'red';
    //                         } else {
    //                             return 'green';
    //                         }
    //                     }

    //                     function d_status(in_status, out_status) {
    //                         if(in_status != '' && out_status != '') {
    //                             return "<span class=' " + t_in_status(in_status) + "'>" +employee[i].status_timein+ "</span>" + ' / ' + "<span class='" + t_out_status(out_status) + "'>" +employee[i].status_timeout+ "</span>";
    //                         } else if (in_status != '' && out_status == '') {
    //                             return "<span class=' " + t_in_status(in_status) + "'>" +employee[i].status_timein+ "</span>";
    //                         } else {
    //                             return "";
    //                         }
    //                     }
    //                     var time_in = employee[i].timein;
    //                     var t_in = time_in.split(" ");
    //                     var time_out = employee[i].timeout;
    //                     var t_out = time_out.split(" ");

    //                     tbody.append("<tr>"+
    //                                     "<td>"+employee[i].date+"</td>" +
    //                                     "<td>"+t_in[1]+" "+t_in[2]+"</td>" +
    //                                     "<td>"+t_out[1]+" "+t_out[2]+"</td>" +
    //                                     "<td>"+employee[i].totalhours+"</td>" +
    //                                     "<td>"+ d_status(in_status, out_status) +"</td>" +
    //                                 "</tr>");
    //                 }

    //                 // initialize datatable
    //                 $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});
    //             }
    //         }
    //     })
    // });


    // // Attendance Details modal
    // $(function(){
    
    //     $("#attendance-details").click(function(e){
    
    //         $("div[name='abc']").modal('show');
    //     });
    //     $(".test").modal({
    //         closable: true
    //     });
    
    // });




    // Finds attendance details for specific row.
    function getAttendanceDetails(attendanceID) {

      $( document ).ready(function() 
      {
        $("div[name=attendanceDetailsModal]").modal('show');
        console.log(attendanceID);
      });


        var BreakTbody = document.getElementById("break_tbody");

        var url = document.getElementById('_url').textContent;


        $.get(url + '/personal/attendance/details', { attendanceID: attendanceID }, function(data)
        {

            BreakTbody.innerHTML = "";

            var breaks = data[0];
            var break_hour = data[1];

            var break_hour_element = document.getElementById("breakHour");
            break_hour_element.innerHTML = "";

            if (break_hour != null) {
              break_hour_element.innerHTML += "" + break_hour;
            }
            else
            {
              break_hour_element.innerHTML += "Did not take a break.";
            }

            if (breaks) 
            {
              for(i = 0; i <= breaks.length; i++)
              {
                  if(breaks[i]){

                    var end_time = "";
                    if (breaks[i].end_at) 
                    {
                      end_time = breaks[i].end_at;
                      start_time = breaks[i].start_at;

                      var res = end_time.split(" ");
                      var start = start_time.split(" ");
                      end_time = res[1];
                      start_time = start[1];

                      BreakTbody.innerHTML += "<tr>" +
                          "<td>"+ start_time +"</td>" +
                          "<td>" + end_time + "</td>"
                          +"</tr>";
                    }
                  }
              }
            }
        });
      }

    </script>
    @endsection
