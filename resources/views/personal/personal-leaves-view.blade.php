@extends('layouts.personal')

    @section('meta')
        <title>My Leave | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper my leave of absence, view my leave of absence records, edit pending leave request, and request leave of absence">
    @endsection

    @section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <style>
        .ui.active.modal {position: relative !important;}
        .datepicker {z-index: 999 !important; }
        .datepickers-container {z-index: 9999 !important;}
    </style>
    @endsection

    @section('content')
  
    @include('personal.modals.modal-view-leave')

    <div class="container-fluid ">
        <div class="row">
            <h2 class="page-title">My Leave
                <!-- <button class="ui  btn-sm leave button  btn_upper float-right"><i class="icon plus"></i>REQUEST LEAVE</button> -->
                <button class="ui  btn-sm  button leave btn_upper float-right"><i class="icon plus"></i>REQUEST LEAVE</button>

<!-- req modal start  -->
                <div class="ui modal addition medium leave">
                    <div class="header py-3  text-center">Request Leave</div>

                    <div class="content">
                
             @if($lt != NULL)
                
                @if($lt->leavetype!= NULL)

                
                    <div class="content">
                        <div class="ui grid">
                            <div class="eight wide column"><h3><label class="pl-2">  Leave Type</label>: @isset($lt->leavetype) {{ $lt->leavetype }} ({{ $lt->limit }} days)@endisset</h3></div>
                             <div class="four wide column"><h3><label>Taken Leaves</label>: {!! taken_leaves(Auth::user()->id) !!}</h3></div>
                            <div class="four wide column"><h3><label>Remaining Leaves</label>: {!! available_leaves(Auth::user()->id) !!}</h3></div>
                            
                        </div>
                        <div class="ui grid py-3">
                            <form id="request_personal_leave_form" action="{{ url('personal/leaves/request') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                            <input type="text" autocomplete="off" name="available" value="{{ $available_leaves }}" hidden/>

                            <div class="two fields">
                                <div class="field">
                                    <label for="">Leave from</label>
                                    <input id="leavefrom" type="text" autocomplete="off" placeholder="Start date" name="leavefrom" class="airdatepicker uppercase"  />
                                </div>
                                <div class="field">
                                    <label for="">Leave to</label>
                                    <input id="leaveto" type="text" autocomplete="off" placeholder="End date" name="leaveto" class="airdatepicker uppercase" />
                                </div>
                                
                            </div>
                            <p id="leavewarning" class="text-center text-danger"></p>
                            <div class="field">
                                <label for="">Return date</label>
                                <input id="returndate" type="text" autocomplete="off" placeholder="Enter Return date" name="returndate" class="airdatepicker uppercase" />
                            </div>
                            <div class="field">
                                <label>Reason</label>
                                <textarea class="uppercase" rows="5" name="reason" value=""></textarea>
                            </div>
                            <div class="field">
                                <div class="ui error message">
                                    <i class="close icon"></i>
                                    <div class="header"></div>
                                    <ul class="list">
                                        <li class=""></li>
                                    </ul>
                                </div>
                            </div>
                    
                            <div class="actions">
                                <input type="hidden" name="typeid" value="">
                                <button class="ui positive small button float-right" type="submit" id="leave_submit" name="submit"><i class="ui checkmark icon"></i> Send Request</button>
                                <button class="ui red small button cancel float-right" type="button"><i class="ui times icon"></i> Cancel</button>
                            </div>
                        </form>
                        </div>
                    </div>

                    @elseif($lt->leavetype == NULL )
                    
                      
                        <div class="content">
                            <div class="row">
                                <div class="col-6  offset-3">
                                    <div class="box-body text-center">
                                        <h4 class="file-icon"><i class="fa fa-battery-empty" aria-hidden="true"></i></h4>
                                        <h5 class= "py-2"> <span class="px-1"></span>Oops! You don't have any leave previlege yet.</h5>
                                        <small>Please talk to your admin to assign your leavetype otherwise you cannot apply for leave.</small>
                                    </div> 
                                </div>
                            </div>
                            <div class="actions">
                            <button class="ui grey small button cancel float-right" type="button"><i class="ui times icon"></i> Cancel</button>
                            </div>
                        </div>
                    @else                
                    
                        <div class="content">
                            <div class="row">
                                <div class="col-6  offset-3">
                                    <div class="box-body text-center">
                                        <h4 class="file-icon"><i class="fa fa-battery-empty" aria-hidden="true"></i></h4>
                                        <h5 class= "py-2"> <span class="px-1"></span>Oops! You don't have any availabe leaves to get.</h5>
                                        <small>Please talk to your admin to assign your leavetype otherwise you cannot apply for leave.</small>
                                    </div> 
                                </div>
                            </div>
                            <div class="actions">
                            <button class="ui grey small button cancel float-right" type="button"><i class="ui times icon"></i> Cancel</button>
                            </div>
                        </div>
                    @endif
                    @else
                    
                        <div class="content">
                            <div class="row">
                                <div class="col-6  offset-3">
                                    <div class="box-body text-center">
                                        <h4 class="file-icon"><i class="fa fa-battery-empty" aria-hidden="true"></i></h4>
                                        <h5 class= "py-2"> <span class="px-1"></span>Oops! You don't have any leave type assigned yet.</h5>
                                        <p>Please talk to your admin to assign your leavetype otherwise you cannot apply for leave.</p>
                                    </div> 
                                </div>
                            </div>
                            <div class="actions">
                            <button class="ui red small button cancel float-right mb-4" type="button"><i class="ui times icon"></i> Cancel</button>
                            </div>
                        </div>
                    @endif                

                   
                    </div>
                </div>

<!-- req modal end  -->


            </h2>
        </div>
        <div class="row ">
            @if ($errors->any())
            <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">There were some errors with your submission</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="row ">
            <div class="col-12 px-0">
            @if(count($l)>0)      
                <div class="box box-success shadow-sm p-3">
                    <div class="box-body reportstable">
                    <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">


                            <div class="two wide field">
                                <input id="start" type="text" name="start" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>

                            <div class="two wide field">
                                <input id="end" type="text" name="end" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                                <i class="ui icon calendar alternate outline calendar-icon"></i>
                            </div>
                            <button id="filterButton" class="ui button  btn_upper small"><i class="ui icon filter alternate"></i> Filter</button>
                        </div>
                        
                    </form>

<!-- 
                        <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                            {{ csrf_field() }}
                            <div class="inline two fields">
                                <div class="three wide field">
                                    <label>Date Range</label>
                                    <input id="datefrom" type="text" name="" value="" placeholder="Start Date" class="airdatepicker" autocomplete="off">
                                    <i class="ui icon calendar alternate outline calendar-icon"></i>
                                </div>

                                <div class="two wide field">
                                    <input id="dateto" type="text" name="" value="" placeholder="End Date" class="airdatepicker" autocomplete="off">
                                    <i class="ui icon calendar alternate outline calendar-icon"></i>
                                </div>
                                <button id="btnfilter" class="ui btn_upper"><i class="ui icon filter alternate"></i> Filter</button>
                            </div>
                        </form> -->

                        @include('personal.personal-leaves-view-table')
                   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                    </div>
                </div>
            @else
                        <div class="row">
                            <div class="col-6  offset-3">
                                <div class="box-body text-center p-5">
                                    <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                                    <h4 class= "py-2">!  <span class="px-1"></span> No Requested/Approved Leave availabe </h4>
                                </div> 
                            </div>
                        </div>
                    
            @endif    
            </div>          
        </div>

    </div>

     <!-- Delete confirmation modal -->
     <div class="ui modal small delete  " name="delete_personal_leave_Confirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->
    
    
    
    <span id="_url" style="display: none;">{{url('/')}}</span>
    <span id="delete_personal_leaves_url" style="display: none;">{{url('personal/leaves/delete')}}</span>

    @endsection

    @section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});
    $('.airdatepicker').datepicker({ language: 'en', dateFormat: 'yyyy-mm-dd' });

    $('.ui.dropdown.getid').dropdown({ onChange: function(value, text, $selectedItem) {
        $('select[name="type"] option').each(function() {
            if($(this).val()==value) { var id = $(this).attr('data-id'); $('input[name="typeid"]').val(id); };
        });
    }});


    $('.ui.modal.leave')
        .modal('attach events', '.ui.leave.button', 'show')
    ;
    
    // document.getElementById("leave_head").style.color = "#23adb5";

// Employee personal leave search AJAX function
function fetch_data( page, start, end)
 {
    var url = document.getElementById("_url").textContent;

    $.ajax({
    url: url + "/get/personal/leave?start="+ start + "&page=" + page + "&end=" + end,
    success:function(l)
    {
        // console.log(data);
        $('#leave_thead').html('');
        $('#leave_tbody').html('');
        $('#leave_tbody').html(l);
    }
    })
}

    $( document ).ready(function() {

    $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);

   
    var start = $('#start').val();
    var end = $('#end').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data( page, start, end);
    });


    $('#filterButton').click(function(e){
        e.preventDefault();
        
        var start = $('#start').val();
        var end = $('#end').val();
        var page = $('#hidden_page').val();
        // console.log(query);
        console.log(start, end);
        fetch_data( page, start, end);
    });
});

 // Delete Confirmation 
 function delete_confirmation(personalleaveID)
        {
          console.log(personalleaveID);

          $( document ).ready(function() {
            $("div[name=delete_personal_leave_Confirmation]").modal('show');
          });

          var url = document.getElementById('delete_personal_leaves_url').textContent;
          console.log(url);
          url = url+ "/" + personalleaveID;
          console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }


    $( document ).ready(function() {
    $('#leaveto').datepicker({
    onSelect: function() {
       var leavefrom = $('#leavefrom').val();
       var leaveto = $('#leaveto').val();
       var leavefrom_parse = leavefrom.split('-');
       var leaveto_parse = leaveto.split('-');
       if (leavefrom_parse[0] != leaveto_parse[0]){
       $(':input[type="submit"]').prop('disabled', true);
       var str = "*Please select two dates from same year. If you need leaves from two consecutive years at a time, please request two leaves separately.";
       $( "#leavewarning" ).html( str );
       }
       else{
       $(':input[type="submit"]').prop('disabled', false);
       var str = "";
       $( "#leavewarning" ).html( str );
       }
    }
});
    
   }); 
    








    // $('#filterform').submit(function(event) {
    //     event.preventDefault();
    //     var date_from = $('#datefrom').val();
    //     var date_to = $('#dateto').val();
    //     var url = $("#_url").val();

    //     $.ajax({
    //         url: url + '/get/personal/leaves/',type: 'get',dataType: 'json',data: {datefrom: date_from, dateto: date_to},headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
    //         success: function(response) {
    //             console.log(response);

    //             showdata(response);
    //             function showdata(jsonresponse) {
    //                 var leave = jsonresponse;
    //                 var tbody = $('#dataTables-example tbody');

    //                 // clear data and destroy datatable
    //                 $('#dataTables-example').DataTable().destroy();
    //                 tbody.children('tr').remove();

    //                 // append table row data
    //                 for (var i = 0; i < leave.length; i++) {
    //                     var ls = leave[i].status;
    //                     function actions(ls) {
    //                         if (ls == 'Approved') {
    //                             return "<button type='button' data-id='"+leave[i].id+"'"+ "class='ui button icon mini teal view'><i class='ui icon file alternate'></i> View</button>";
    //                         } else {
    //                             if (leave[i].comment !== null) {
    //                                 return "<a href='"+url+"/personal/leaves/edit/"+leave[i].id+"'"+ "class='ui basic blue icon mini button'><i class='ui icon edit'></i> Edit</a>"+
    //                                     "<a href='"+url+"/personal/leaves/delete/"+leave[i].id+"'"+ "class='ui basic red icon mini button'><i class='ui icon trash'></i> Delete</a>"+
    //                                     "<button type='button' data-id='"+leave[i].id+"'"+ "class='ui basic grey icon mini button comment' data-tooltip='"+leave[i].comment+"' data-variation='wide' data-position='top right'><i class='ui icon comment alternate'></i></button>";
    //                             } else {
    //                                 return "<a href='"+url+"/personal/leaves/edit/"+leave[i].id+"'"+ "class='ui basic blue icon mini button'><i class='ui icon edit'></i> Edit</a>"+
    //                                     "<a href='"+url+"/personal/leaves/delete/"+leave[i].id+"'"+ "class='ui basic red icon mini button'><i class='ui icon trash'></i> Delete</a>";
    //                             }
    //                         }
    //                     }
    //                     tbody.append("<tr>"+
    //                                     "<td>"+leave[i].type+"</td>" +
    //                                     "<td>"+leave[i].leavefrom+"</td>" +
    //                                     "<td>"+leave[i].leaveto+"</td>" +
    //                                     "<td>"+leave[i].reason+"</td>" +
    //                                     "<td>"+leave[i].returndate+"</td>" +
    //                                     "<td>"+leave[i].status+"</td>" +
    //                                     "<td>"+ actions(ls) +"</td>" +
    //                                 "</tr>");
    //                 }

    //                 // initialize datatable
    //                 $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthChange: false,searching: false,ordering: true});
    //             }
    //         }
    //     })
    // });

    $(".delegation").on("click", ".view", function () {
        // parent delegation
        var id = $(this).attr('data-id');
        var url = document.getElementById("_url").textContent;
        console.log(url);

        $.ajax({
            url: url + '/view/personal/leave/',type: 'get',dataType: 'json',data: {id: id},headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
            success: function(response) {
                $('.employee').val(response.employee);
                $('.leavetype').val(response.type);
                $('.leavefrom').val(response.leavefrom);
                $('.leaveto').val(response.leaveto);
                $('.returndate').val(response.returndate);
                $('.reason').text(response.reason);
                $('.comment').text(response.comment);
                $('.status').val(response.status);
                $('.ui.modal.view').modal('toggle');
            }
        })
    });
    </script>
    @endsection
