@extends('layouts.personal')

@section('meta')
    <title>My Tasks | Attendnace Keeper</title>
    <meta name="description"
          content="Attendance Keeper my schedules, view my schedule records, view present and previous schedules.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">My Tasks
                <a class="btn btn-sm btn_upper float-right" href="{{ url('personal/tasks/assignatask') }}" role="button">Assign a task</a>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

               
                    
                    @if(count($data)>0)
                        <div class=" box ">
                                <div class="box-body reportstable">
                                <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">
                                <div class="five wide field">
                                    <input id="search" type="text" name="searchbox" value="" placeholder="Search here"  autocomplete="off">
                                </div>
                                <div class="three wide field">
                                    <select class="ui dropdown uppercase" id="situation" name="situation">
                                        <option value="all">All Tasks</option>
                                        <option value="completed">Completed Tasks</option>
                                        <option value="pending">Pending Tasks</option>                                       
                                    </select>
                                </div>
                               
                                <button id="filterButton" class="ui button  blue btn_upper"><i class="ui icon filter alternate"></i> Filter</button>

                           
                        </div>
                        
                        </form>
                                @include('personal.personal-my-tasks-table')
                                <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                                </div>
                        </div>    
                                @else
                                    <div class="row">
                                        <div class="col-6  offset-3">
                                            <div class="box-body text-center p-5">
                                                <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                                                <h4 class= "py-2">!  <span class="px-1"></span> No Task Available </h4>
                                            </div> 
                                        </div>
                                    </div>
                                
                                            
                    @endif    
                    
                
            </div>
        </div>
    </div>
     <!-- Delete confirmation modal -->
 <div class="ui modal small delete  " name="deleteConfirmation">
      <div class="header">Edit</div>
      <div class="content">
        
            
      <form id="edit_task_form" action="{{ url('personal/tasks/update') }}" class="ui form" method="post" accept-charset="utf-8">
                            @csrf
                           
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <label>Comment</label>
                                    <textarea class="" rows="5" name="comment">@isset($task->comment){{ $task->comment }}@endisset</textarea>
                                </div>
                            </div>

                            <div class=" float-right bg-white py-3 pr-4">
                                <input type="text" name="id" id="task_id" value="" hidden>
                                <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Mark Done</button>
                                <a class="ui red small button" href="{{ url('personal/tasks/mytasks') }}"><i class="ui times icon"></i> Cancel</a>
                            </div>
                    </form>
        
         </div>     
      </div>
        <!-- <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div> -->
    </div>
<!-- Delete confirmation modal ends here -->

    <span id="_url" style="display: none;">{{url('/')}}</span>
    <!-- <span id="delete_task_url" style="display: none;">{{url('personal/tasks/edit')}}</span> -->
@endsection
@section('scripts')
    <script type="text/javascript">
    $('#dataTables-example').DataTable({responsive: true,pageLength: 15,lengthMenu: [ [15, 25, 50, -1], [15, 25, 50, "All"] ],lengthChange: true,searching: true,ordering: true});
   

function fetch_data(query, page)
 {
    var url = document.getElementById("_url").textContent;

  $.ajax({
   url: url + "/personal/tasks/search?query="+query + "&page="+page + "&situation=" + situation.value,
   success:function(data)
   {
    $('thead').html('').css({background: "green"});
    $('tbody').html('');
    $('tbody').html(data);
   }
  })
 }

 $( document ).ready(function() {

                $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);

                var query = $('#search').val();
                var situation = $('#situation').val();


                $('li').removeClass('active');
                $(this).parent().addClass('active');
                fetch_data(query, page, situation);
                });


        $('#filterButton').click(function(e){
            e.preventDefault();
        var query = $('#search').val();
        var situation = $('#situation').val();
        var page = $('#hidden_page').val();
        // console.log(query,situation,owner);
        fetch_data(query, page,situation);
    });


});

 // Delete Confirmation 
 function delete_confirmation(taskID)
        {
          console.log(taskID);

          $( document ).ready(function() {
            $("div[name=deleteConfirmation]").modal('show');
          });

          document.getElementById("task_id").value = taskID ;
          
        }


</script>
    @endsection