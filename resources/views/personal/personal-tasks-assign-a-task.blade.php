@extends('layouts.personal')

@section('meta')
    <title>Task Manager | Attendance Keeper</title>
    <meta name="description"
          content="attendance keeper schedules, view all employee schedules, add schedule or shift, edit, and delete schedules.">
@endsection

@section('styles')
    <link href="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/air-datepicker/dist/css/datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/vendor/datetimepicker/datetimepicker.css') }}" rel="stylesheet">

    <style>
        /* .ui.active.modal {position: relative !important;} */
        .datepicker {
            z-index: 999 !important;
        }

        .datepickers-container {
            z-index: 9999 !important;
        }
    </style>
@endsection


@section('content')
    @include('personal.modals.modal-add-task')

    <div class="container-fluid ">
        <div class="row">
            <h2 class="page-title">Tasks
                <button class="ui btn btn-sm btn-add btn_upper float-right"><i class="ui icon plus"></i>Add
                </button>
                <a href="{{ url('personal/tasks/mytasks') }}" class="btn btn-sm btn_upper float-right"><i class="ui icon chevron left"></i>Return</a>

            </h2>
        </div>

        <div class="row">
            <div class="col-12 px-0">
            @if(count($data)>0)
                    <div class="box box-success">
                        <div class="box-body">
                        <form action="" method="get" accept-charset="utf-8" class="ui small form form-filter" id="filterform">
                        {{ csrf_field() }}
                        <div class="inline two fields">
                                <div class="five wide field">
                                    <input id="search" type="text" name="searchbox" value="" placeholder="Search here"  autocomplete="off">
                                </div>
                                <div class="three wide field">
                                    <select class="ui dropdown uppercase" id="situation" name="situation">
                                        <option value="all">All Tasks</option>
                                        <option value="completed">Completed Tasks</option>
                                        <option value="pending">Pending Tasks</option>                                       
                                    </select>
                                </div>
                               
                                <button id="filterButton" class="ui button  blue btn_upper"><i class="ui icon filter alternate"></i> Filter</button>

                           
                        </div>
                        
                        </form>
                                @include('personal.personal-tasks-assign-a-task-table')
                                <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                        </div>
                    </div>

            @else
                            <div class="row">
                                <div class="col-6  offset-3">
                                    <div class="box-body text-center p-5">
                                        <h1 class="file-icon empty-table-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                                        <h4 class= "py-2">!  <span class="px-1"></span> No Task Available </h4>
                                    </div> 
                                </div>
                            </div>
                        
                               


            @endif            
            </div>
        </div>

    </div>

     <!-- Delete confirmation modal -->
 <div class="ui modal small delete  " name="deleteConfirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui secondary button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui grey button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->





    <span id="_url" style="display: none;">{{url('/')}}</span>
    <span id="delete_task_url" style="display: none;">{{url('personal/tasks/assignatask/delete')}}</span>

@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>
    <script src="{{ asset('/assets/vendor/mdtimepicker/mdtimepicker.min.js') }}"></script>
        <script src="{{ asset('/assets/vendor/datetimepicker/datetimepicker.js') }}"></script>

    <script type="text/javascript">

        $('#datetimepicker').datetimepicker();

        $('.jtimepicker').mdtimepicker({format: 'h:mm:ss tt', hourPadding: true});
        $('.airdatepicker').datepicker({language: 'en', dateFormat: 'yyyy-mm-dd'});

        // $('.ui.dropdown.getid').dropdown({
        //     onChange: function (value, text, $selectedItem) {
        //         $('select[name="employee"] option').each(function () {
        //             if ($(this).val() == value) {
        //                 var id = $(this).attr('data-id');
        //                 $('input[name="id"]').val(id);
        //             }

        //         });
        //     }
        // });

       // Initialize select2

$("#selEmployee").select2();

$("#selEmployee").on("select2:open", function(event) {
    $('input.select2-search__field').attr('placeholder', 'Search with name');
});

        $(document).ready(function(){
 
        // Read selected option
        $('#selEmployee').keyup(function(){
        var employee = $('#selEmployee option:selected').text();
        var id = $('#selEmployee').val();
 });
});


        function fetch_data(query, page)
 {
    var url = document.getElementById("_url").textContent;

  $.ajax({
   url: url + "/personal/assigned/tasks/search?query="+query + "&page="+page + "&situation=" + situation.value,
   success:function(data)
   {
    $('thead').html('').css({background: "green"});
    $('tbody').html('');
    $('tbody').html(data);
   }
  })
 }

 $( document ).ready(function() {

                $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);

                var query = $('#search').val();
                var situation = $('#situation').val();


                $('li').removeClass('active');
                $(this).parent().addClass('active');
                fetch_data(query, page, situation);
                });


        $('#filterButton').click(function(e){
            e.preventDefault();
        var query = $('#search').val();
        var situation = $('#situation').val();
        var page = $('#hidden_page').val();
        // console.log(query,situation,owner);
        fetch_data(query, page,situation);
    });


});


// Delete Confirmation 
function delete_confirmation(taskID)
        {
        //   console.log(taskID);

          $( document ).ready(function() {
            $("div[name=deleteConfirmation]").modal('show');
          });

          var url = document.getElementById('delete_task_url').textContent;
        //   console.log(url);
          url = url+ "/" + taskID;
        //   console.log(url);

          var buttonID = "deleteButton";

          document.getElementById('deleteButton').href = url;

        }


    </script>

<script type="text/javascript">

function myFunction() {
    var id = document.getElementById("selEmployee").value;

    var url = document.getElementById("_url").textContent;

        $.ajax({
    url: url + "/personal/task/task_eligible/"+id ,
    success:function(data )
    {  
        // console.log(data['deadline']);
        var x = document.getElementById("message");
        if (data['status'] == 1) {

            document.getElementById("task_count").textContent = data['task']; 
            document.getElementById("predicted_deadline").textContent = data['deadline'];
            console.log("block");
            x.style.display = "block";
        }
        else {
            console.log("none");

            x.style.display = "none";
        }
        // console.log('successfull');
        // console.log(data);
    }

  })

}

</script>

@endsection
