<!-- <div class="ui modal medium leave button">

   @isset($lt)
   
   @if($lt->leavetype!= NULL && $available_leaves >0)
   
   
   <div class="header">Request Leave</div>
    <div class="content">
        <div class="ui grid">
            <div class="eight wide column"><h3><label class="pl-2">  Leave Type</label>: @isset($lt->leavetype) {{ $lt->leavetype }} @endisset</h3></div>
            <div class="eight wide column"><h3><label>Remaining Leaves</label>: @if($available_leaves) {{ $available_leaves }} days @else Leave type is not issued yet. @endif</h3></div>
            
        </div>
        <div class="ui grid py-3">
            <form id="request_personal_leave_form" action="{{ url('personal/leaves/request') }}" class="ui form" method="post" accept-charset="utf-8">
            @csrf
            <div class="two fields">
                <div class="field">
                    <label for="">Leave from</label>
                    <input id="leavefrom" type="text" autocomplete="off" placeholder="Start date" name="leavefrom" class="airdatepicker uppercase" />
                </div>
                <div class="field">
                    <label for="">Leave to</label>
                    <input id="leaveto" type="text" autocomplete="off" placeholder="End date" name="leaveto" class="airdatepicker uppercase" />
                </div>
            </div>
            <div class="field">
                <label for="">Return date</label>
                <input id="returndate" type="text" autocomplete="off" placeholder="Enter Return date" name="returndate" class="airdatepicker uppercase" />
            </div>
            <div class="field">
                <label>Reason</label>
                <textarea class="uppercase" rows="5" name="reason" value=""></textarea>
            </div>
            <div class="field">
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header"></div>
                    <ul class="list">
                        <li class=""></li>
                    </ul>
                </div>
            </div>
       
            <div class="actions">
                <input type="hidden" name="typeid" value="">
                <button class="ui positive small button float-right" type="submit" name="submit"><i class="ui checkmark icon"></i> Send Request</button>
                <button class="ui red small button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
            </div>
         </form>
        </div>
    </div>

    @else
     
         <div class="header">Request Leave</div>
                    <div class="content">
                        <div class="row">
                            <div class="col-6  offset-3">
                                <div class="box-body text-center">
                                    <h4 class="file-icon"><i class="fa fa-battery-empty" aria-hidden="true"></i></h4>
                                    <h5 class= "py-2"> <span class="px-1"></span>Oops! You don't have any leave previlege yet.</h5>
                                    <small>Please talk to your admin to assign your leavetype otherwise you cannot apply for leave.</small>
                                </div> 
                            </div>
                        </div>
                        <div class="actions">
                          <button class="ui grey small button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
                        </div>
                    </div>

    @endif
    @endisset
</div> -->
