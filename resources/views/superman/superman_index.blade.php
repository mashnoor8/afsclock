@extends('layouts.superman_layout')

@section('content')


<div class="container-fluid">
        <div class="row">
            <h2 class="page-title">Super Admins 
                <!-- <button class="ui  button mini btn_upper offsettop5 btn-add float-right"><i class="ui icon plus"></i>Add</button>
                <a href="{{ url('dashboard') }}" class="ui   button btn_upper mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a> -->
            </h2>
        </div>

    @if(count($super_admins) > 0)
        <div class="row">
            <div class="box box-success">
                <div class="box-body">
                    <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Number Of Branches</th>
                                <th>Number Of Employees</th>

                                <th>Created At</th>
                                <!-- <th>Action</th> -->
                            
                            </tr>
                        </thead>
                        <tbody>
                            @isset($super_admins)
                                @foreach ($super_admins as $super_admin)
                                <tr>
                                    <td>{{ $super_admin->firstname }} {{ $super_admin->lastname }}</td>
                                    <td>{{ $super_admin->companyemail }}</td>
                                    <td>{!! branch_number($super_admin->id)  !!}</td>
                                    <td>{!! people_number($super_admin->id)  !!}</td>

                                    <td class="">
                                        {{ $super_admin->created_at }}
                                    </td>
                                    <!-- <th><a href="ui  btn btn-sm "> delete</a></button></th> -->
                                </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Super Admin</h4>
    </div>
    @endif
        
    </div>



@endsection