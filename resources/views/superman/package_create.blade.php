@extends('layouts.superman_layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <h2 class="page-title">Create New Package</h2>
    </div>

    <div class="row">
        <div class="box box-success">
            <div class="box-body">
                <form id="edit_request_personal_leave_form" action="{{ url('/superman/package/create_new') }}" class="ui form" method="post" accept-charset="utf-8">
                    @csrf

                        <div class="field">
                            <label for="">Name</label>
                            <input id="leavefrom" type="text" autocomplete="off" name="name" value="" />
                        </div>
                   

                        <div class="field">
                            <label for="">Limit Of Employees</label>
                            <div class="two fields">
                                <div class="field">
                                 <small>*Max</small>
                                    <input id="start" type="number" autocomplete="off" name="start" value="" />

                                </div>

                                <div class="field">
                                  <small>*Min</small>
                                    <input id="end" type="number" autocomplete="off" name="end"  value="" />
                                </div>
                            </div>

                        </div>
                  
                    <div class="three fields">

                        <div class="field">
                            <label for="">Price</label>
                            <input type="number" step="0.01" autocomplete="off" name="price" value="" />
                        </div>
                        <div class="field">
                            <label>Currency</label>
                            <select class="ui dropdown uppercase" name="currency">

                                @include('currency-list')



                            </select>


                        </div>
                        <div class="field">
                            <label>Interval</label>
                            <select class="ui dropdown uppercase" name="interval">
                                <option value="Week" @isset($interval) @if($interval=='Week' ) selected @endif @endisset>Weekly</option>
                                <option value="Month" @isset($interval) @if($interval=='Month' ) selected @endif @endisset>Monthly</option>
                                <option value="Year" @isset($interval) @if($interval=='Year' ) selected @endif @endisset>Yearly</option>
                            </select>
                        </div>
                    </div>

                    <div class="actions">
                       
                        <button class="ui positive small button" type="submit" name="submit"><i class="ui checkmark icon"></i> Create</button>
                        <a href="{{ url('superman/packages') }}" class="ui grey small button cancel"><i class="ui times icon"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>




@endsection
