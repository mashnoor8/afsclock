@extends('layouts.superman_layout')

@section('content')
<div class="container-fluid">
        <div class="row">
            <h2 class="page-title">
                Packages 
                <a href="{{ url('/superman/package/create') }}" class="ui  button mini btn_upper offsettop5 btn-add float-right"><i class="ui icon plus"></i>Add Package</a>
                <!-- <a href="{{ url('dashboard') }}" class="ui   button btn_upper mini offsettop5 float-right"><i class="ui icon chevron left"></i>Return</a> -->
            </h2>
        </div>

        
@if(count($packages)>0)
        <div class="row">
            <div class="box box-success">
                <div class="box-body">
                    <table width="100%" class="table table-striped table-hover" id="dataTables-example" data-order='[[ 0, "asc" ]]'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Limit Of Employees </th>
                                <th>Price </th>
                                <th>Cirrency</th>

                                <th>Interval</th>
                                <th>Action</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            @isset($packages)
                                @foreach ($packages as $package)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $package->name }} </td>
                                    <td>{{ $package->limit }}</td>
                                    <td>{{Symfony\Component\Intl\Currencies::getSymbol($package->currency) }} {{ $package->price }}</td>
                                    <td>{{ $package->currency }}</td>
                                    <td>{{ $package->interval }}</td>
                                   
                                    <th>
                                    <a href="{{ url('/superman/packages/edit/'.$package->id) }}" title="edit" class="ui inverted primary  button">Edit</a>
                                    <!-- <a href="{{ url('/superman/packages/delete/'.$package->id) }}" class="ui inverted brown button">Delete</a> -->
                                    </th>
                                </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
    <div class="box-body text-center p-5">
                        <h1 class="file-icon"><i class="fa fa-file-text" aria-hidden="true"></i></h1>
                        <h4> No Package</h4>
    </div>
    @endif
        
    </div>

@endsection