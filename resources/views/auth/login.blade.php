@extends('layouts.welcome')

@section('css')

<style>

.card0 {
    box-shadow: 0px 4px 8px 0px #757575;
    border-radius: 0px;
    margin: auto;
    background-color: #EEEEEE;
}

.card2 {
    margin: auto;
    background-color: #00ADB5;
    color:black;
}

.logo {
    width: 100px;
    height: 37px;
    margin:auto;
    margin-top: 20px;
}

.image {
    width: 360px;
    height: 280px
}

.border-line {
    border-right: 1px solid #EEEEEE
}


.line {
    height: 1px;
    width: 45%;
    background-color: #E0E0E0;
    margin-top: 10px
}

.or {
    width: 10%;
    font-weight: bold
}

.text-sm {
    font-size: 14px !important
}

::placeholder {
    color: #BDBDBD;
    opacity: 1;
    font-weight: 300
}

:-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

::-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

input,
textarea {
    padding: 10px 12px 10px 12px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    margin-bottom: 5px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    color: #2C3E50;
    font-size: 14px;
    letter-spacing: 1px
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #304FFE;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

a {
    color: inherit;
    cursor: pointer
}

.btn-blue {
    background-color: #393E46;
    width: 150px;
    color: #fff;
    border-radius: 2px
}

.btn-blue:hover {
    background-color: #EEEEEE;
    cursor: pointer
}

.bg-blue {
    color: #fff;
    background-color: #1A237E
}

@media screen and (max-width: 991px) {
    .logo {
        margin-left: 0px
    }

    .image {
        width: 300px;
        height: 220px
    }

    .border-line {
        border-right: none
    }

    .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0px 15px
    }
}
</style>

@endsection

@section('content')

<!-- <section class="py-5 login_section">
	<div class="container py-5 mt-5">
		<div class="row justify-content-center py-5">
			<div class="col-md-8 py-5">
				<div class="card ">
					<div class="card-header">{{ __('Login') }}</div>

						<div class="card-body">
						@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif
						@if (session('warning'))
						<div class="alert alert-warning">
							{{ session('warning') }}
						</div>
						@endif

						<div class=" text-center pb-3 logo">
							<img class="img" width="70%" src="{{'manager/images/logo-top.png'}}" alt="Attendance Keeper">
						</div>

						<form method="POST" action="{{ route('login') }}" method="POST"">
							@csrf
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

								<div class="col-md-6">
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

									@error('email')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

								<div class="col-md-6">
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

									@error('password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">
										{{ __('Login') }}
									</button>
								</div>
							</div>

							<div class="row" >
								<div class="col-3 offset-4">
									<div class="form-group">
										
											<div class="ui pt-2 checkbox">
												<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
												<label>Remember me</label>
											</div>
										
									</div>
								</div>
								<div class="col-5 float-left ">
								@if (Route::has('password.request'))
									<div class="d-flex justify-content-center links form-group float-left " >
										<a class="btn btn-link text-primary mb-2" href="{{ route('password.request') }}">
											{{ __('Forgot Your Password?') }}
										</a>
									</div>

								@endif
								</div>
							</div>
							<div class="row" >
								<div class="col-8 offset-4 ">
									<div class="form-group">
										
										<p>If you don't have an account please <a class="text-primary" href="{{url('/registration')}}"> Register </a></p>
										
									</div>
								</div>
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>  -->

	
<section class="mx-auto mt-5 py-5 mb-5 container">

<div class="container-fluid px-1 px-md-5 px-lg-5 px-xl-5 py-5 mt-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-md-6">
                <div class="card1 pb-5">
                    
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="https://i.imgur.com/uNGdWHi.png" class="image"> </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card2 m-4 card border-0 px-4 py-2">
                <div class="row"> <img src="{{'manager/images/logo-top.png'}}" class="logo"> </div>
                <div class="row px-3"> <label class="mb-1">
                    @if (session('status'))
						<div class="alert text-white">
							{{ session('status') }}
						</div>
						@endif
						@if (session('warning'))
						<div class="alert text-white">
							{{ session('warning') }}
						</div>
						@endif
		     </div>
		    <form method="POST" action="{{ route('login') }}" method="POST"">
		     @csrf
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm text-white">Email Address</h6>
                        </label>
                        <input id="email" class="mb-2" type="email" name="email" value="{{ old('email') }}" placeholder="Enter a valid email address" required autocomplete="email">

									@error('email')
										<span class="text-white" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
                        </div>
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm text-white">Password</h6>
                        </label>
                        <input id="password" type="password" name="password" placeholder="Enter password" required autocomplete="new-password">

									@error('password')
										<span class="text-white" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
                         </div>
                    <div class="row px-3 mb-4">
                        <div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox"  class="custom-control-input" id="chk1" name="remember" {{ old('remember') ? 'checked' : '' }}> <label for="chk1" class="custom-control-label text-sm text-white">Remember me</label> </div> <a href="{{ route('password.request') }}" class="ml-auto mb-0 text-sm text-white">Forgot Password?</a>
                    </div>
                    <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">Login</button> </div>
                    <div class="row mb-4 px-3"> <small class="font-weight-bold text-white">Don't have an account? Please <a class="text-dark " href="{{url('/registration')}}">Register</a></small> </div>
                </div>
                </form>
            </div>
        </div>
        
    </div>
</div>
</section>	
@endsection

@section('script')
<script src="{{ asset('/assets/vendor/jquery/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('/assets/vendor/semantic-ui/semantic.min.js') }}"></script>
<script>
	$('.ui.checkbox').checkbox('uncheck', 'toggle');
</script>
@endsection
    
