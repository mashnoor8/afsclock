@extends('layouts.support_layout')

@section('content')


<section>
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12 ">
                <table class="ui striped table">
                    <thead>
                        <tr>
                            <th width="10%">Subject</th>
                            <th width="50%">Description</th>
                            <th width="10%">Created At</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $d )
                
                        <tr>
                            <td>{{ $d->subject }}</td>
                            <td>{{ $d->comment }}</td>
                            <td>{{$d->created_at}}</td>
                            <td><a href="{{ url('support/cases/case_view/' . $d->id) }}" class="ui yellow button"> View </a></td>
                        </tr>

                    @endforeach  
                    </tbody>  
                </table>
            <!-- comments -->
                    <!-- <div class="ui middle aligned divided list">
                    @foreach($data as $d)
                    <div class="ui comments">
                        <div class="comment">
                            <a class="avatar">
                                <i class="user icon"></i>
                            </a>
                            <div class="content">
                            
                                <div class="metadata ml-0">
                                   <a href="{{url('case/case_view /' . $d->id) }}"><h3 class="">{{ $d->subject}}</h3></a> 
                                
                                </div>
                                <p class="text">
                                {{$d->comment}}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach    
                    </div> -->
            </div>
        </div>
    </div>
</section>

@endsection