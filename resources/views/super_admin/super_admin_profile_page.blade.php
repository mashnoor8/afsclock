@extends('layouts.super_admin')


@section('content')

<div class="container-fluid">

   
    <div class="row">
        <div class="col-md-4  float-left">
            <div class="box box-success">
                <div class="box-body employee-info">
                        <div class="author">
                        @if($profile_photo != null)
                            <img class="avatar border-white" src="{{ asset('/assets/faces/' . $profile_photo ) }}" alt="profile photo"/>
                        @else
                            <img class="avatar border-white" src="{{ asset('/assets/images/faces/default_user.jpg') }}" alt="profile photo"/>
                        @endif
                        </div>
                        <p class="description text-center">
                            <h4 class="title">@if($p->firstname) {{ $p->firstname }} @endisset @isset($p->lastname) {{ $p->lastname }} @else N/A @endif</h4>
                            <table style="width: 100%" class="profile-tbl">
                                <tbody>
                                    <tr>
                                        <td>Email</td>
                                        <td><span class="p_value">@if($p->emailaddress) {{ $p->emailaddress }} @else N/A @endif</span></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No.</td>
                                        <td><span class="p_value">@if($p->mobileno) {{ $p->mobileno }} @else N/A @endif</span></td>
                                    </tr>
                                    <tr>
                                        <td>ID No.</td>
                                        <td><span class="p_value">@if($p->idno) {{ $p->idno }} @else N/A @endif</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </p>
                </div>
            </div>
        </div>

        <div class="col-md-8 float-left">
            <div class="box box-success">
                <div class="box-header with-border">Personal Infomation</div>
                <div class="box-body employee-info">
                        <table class="tablelist">
                            <tbody>
                                
                                
                                <tr>
                                    <td><p>Gender</p></td>
                                    <td><p>@if($p->gender) {{ $p->gender }} @else N/A @endif</p></td>
                                </tr>
                                <tr>
                                    <td><p>Date of Birth</p></td>
                                    <td>
                                        <p>
                                        @if($p->birthday) 
                                            @php echo e(date("F d, Y", strtotime($p->birthday))) @endphp
                                        @else N/A
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><p>Place of Birth</p></td>
                                    <td><p>@if($p->birthplace) {{ $p->birthplace }} @else N/A @endif</p></td>
                                </tr>
                                <tr>
                                    <td><p>Home Address</p></td>
                                    <td><p>@if($p->homeaddress) {{ $p->homeaddress }} @else N/A @endif</p></td>
                                </tr>
                                
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
















  