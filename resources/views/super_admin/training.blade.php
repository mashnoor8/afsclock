@extends('layouts.super_admin')
@section('content')

<section class="py-5 bg-light">
  <div class="container-fluid px-5">
    <div class="row px-4">
      <div class="col">
        <h3>Train Face Recognition Server</h3>
        <p>Let the machine recognize more people.</p>
      </div>
      <div class="col text-right">
        @if($hasPendingTraining)
          <button class="btn btn-secondary px-3" disabled><i class="lni lni-consulting"></i> Train Now</button>
          <p class="text-primary pt-2">System is being trained. Please wait ...</p>
        @else
          <a class="btn btn-primary px-3" href="{{ url('/train/' . $branch->branch_uid) }}"><i class="lni lni-consulting"></i> Train Now</a>
        @endif
      </div>
    </div>

  </div>
</section>
<section class="mt-3 py-5">
  <div class="container">

    @isset($tainings)
    <h4>Training History</h4>
    @isset($tainings)
@if(count($tainings)==0)
<div class="container   height-100">

</div>
@endif
@endisset
    <ul class="list-group list-group-flush">

        @foreach($tainings as $training)
          @if($training->status)
          <li class="list-group-item text-primary"><i class="lni lni-checkbox"></i> You trained the server at {{$training->updated_at}}</li>
          @else
          <li class="list-group-item text-primary"><i class="lni lni-checkbox"></i> You requested to train the server. {{$training->created_at}}</li>
          @endif
        @endforeach


    </ul>
    @endisset
  </div>
</section>

@endsection
