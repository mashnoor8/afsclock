@extends('layouts.super_admin')



@section('content')

   
<div class="card col-6 offset-3 p-5 mt-5">
<section class="mt-5">
  <div class="container-fluid px-5">
    <div class="row  ">
      <div class="col ">
            <h1 class="mb-3">Download Attendance Keeper Desktop App</h1>
            <p class="mb-5">Click the download link below to download the desktop app.</p>

            
            <a target="_blank" id= "download">
                <div class="card text-white bg-success mb-3 col-7">
                    <div class="card-body">
                        <h5 class="card-title"><i class="download icon"></i> Download Attendance Keeper App </h5>
                    </div>
                </div>
            </a>
      </div>
    </div>
  </div>
</section>


<section class="mt-5">
  <div class="container-fluid px-5">
    <div class="row  ">
      <div class="col  ">
      <h3 class="mb-4">Installation Guideline</h3>
            <p class="mb-3"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam voluptate, deleniti veniam praesentium accusantium nulla deserunt exercitationem explicabo officiis, maxime omnis aliquam, nihil culpa quasi et laboriosam odio dolorem quae.</p>
          
      </div>
    </div>
  </div>
</section>

<section class="mt-5">
  <div class="container-fluid px-5">
    <div class="row  ">
      <div class="col ">
      <h3 class="mb-4">User Menual</h3>
            <p class="mb-3"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam voluptate, deleniti veniam praesentium accusantium nulla deserunt exercitationem explicabo officiis, maxime omnis aliquam, nihil culpa quasi et laboriosam odio dolorem quae.</p>
          
      </div>
    </div>
  </div>
</section>

<section class="mt-5">
  <div class="container-fluid px-5">
    <div class="row  ">
        <div class="col-6  ">
            <a href="https://attendancekeeper.net/ak_mac.zip">
                <div class="card text-white bg-success mb-3" style="max-width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title"><i class="apple icon"></i> Download app for Mac  <i class="caret square down icon pl-2"></i> </h5>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-6  ">
            <a href="https://attendancekeeper.net/ak_setup.exe">
                <div class="card text-white bg-success mb-3" style="max-width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title"><i class="windows icon"></i> Download app for Windows  <i class="caret square down icon pl-2"></i> </h5>
                    </div>
                </div>
            </a>
        </div>
    </div>
  </div>
</section>

</div>



@endsection

@section('scripts')
<script>


var OSName="Unknown OS";
if (navigator.appVersion.indexOf("Win")!=-1)
{
    OSName="Windows";
    var a = document.getElementById('download');
    console.log(a);
    a.href = "https://attendancekeeper.net/ak_setup.exe"; 
} 
if (navigator.appVersion.indexOf("Mac")!=-1)
{
    OSName="Mac";
    var a = document.getElementById('download');
    console.log(a);
    a.href = "https://attendancekeeper.net/ak_mac.zip"; 
} 
if (navigator.appVersion.indexOf("X11")!=-1)
{
    OSName="X11";
    var a = document.getElementById('download');
    console.log(a);
    a.href = "https://attendancekeeper.net/ak_setup.exe";
} 
if (navigator.appVersion.indexOf("Linux")!=-1) 
{
    OSName="Linux";
    var a = document.getElementById('download');
    console.log(a);
    a.href = "https://attendancekeeper.net/ak_setup.exe";
} 

console.log(OSName);

$("#download_text").text(OSName);

</script>
@endsection