@extends('layouts.super_admin')



@section('content')
<section class=" pt-5">
  <div class="container-fluid px-5">
    <div class="row px-3">
      <div class="col">
        <h4><i class="fa fa-university pr-3" aria-hidden="true"></i>{{$branch->branch_name}} </h4>
        
        <!-- <span class="small text-info pr-3 border-right">Created at : @isset($branch->created_at) @php echo e(date('d-m-Y', strtotime($branch->created_at))) @endphp @endisset</span>
        <span class="small pl-3 text-info">Admin: {{$branch->admin_email}}</span> -->

        <!-- <p class="mt-4"><a href="/home"><i class="lni lni-arrow-left"></i> Go back to the Dashboard</a></p> -->
      </div>
      <div class="col text-right">
        <!-- <a class="btn btn-sm btn_upper" href="/home"><i class="lni lni-arrow-left"></i> Go back</a> -->
        <a class="btn btn-sm btn_upper" href="{{ url('/training/' . $branch->id) }}"><i class="lni lni-consulting"></i> Train</a>
        
        <a href="" class="btn btn-sm btn_upper"  id="delete" > Delete</a>
        <button class="  create_btn btn btn-sm btn_upper" type="button" id="test"><i class="plus square icon"></i>Add Device</button>
      
      </div>
    </div>
  </div>
</section>

<section class="py-3">
  <div class="container-fluid px-5 ">
    <div class="row">
      <div class="col-12 text-center">
        <h5 class="pt-2"> <i class="fa fa-tablet pr-3" aria-hidden="true"></i>Connected Devices</h5>
      </div>
      <div class="col-6 text-right">
        <!-- <a class="btn btn-sm btn_upper" href="{{ url('/new_device/' . $branch->id) }}" data-toggle="modal" data-target="#addDeviceModal"><i class="lni lni-plus"></i> Add Device</a> -->
      </div>



<div class="ui tiny modal test">
  <i class="close icon"></i>
  <div class="header">
  Register new device

  </div>
  <div class="p-5">
        <form class=" py-3" action="{{url('/add_device')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="">Device Name</label>
                <input type="text" name="device_name" class="form-control" id="" placeholder="Device name here">
            </div>
            <div class="form-group">
                <label for="">Device Type</label>
                <select id="" name="device_type" class="form-control">
                <option selected>Mobile</option>
                <option>Tablet</option>
                <option value="laptop">Laptop</option>
                <option value="desktop">Desktop</option>
                <option>Unknown</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Device Code : </label>
                <input type="text" name="device_code" class="form-control" id="" placeholder="Device IMEI here">
            </div>
            <input type="number" value="{{$branch->id}}" name="branch_id" class="form-control" id="" hidden>
            <button class="btn btn-sm btn_upper float-right" type="submit" name="submit" value="submit"><i class="lni lni-checkmark"></i> Register Device</button>
        </form>

  
  </div>
  <!-- <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    <div class="ui positive right labeled icon button">
      Yep, that's me
      <i class="checkmark icon"></i>
    </div>
  </div> -->
</div>







<div class="ui tiny modal delete">
  <i class="close icon"></i>
  <div class="header">
  Delete Confirmation

  </div>
  <div class="p-5">
       
  <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this branch?</h4>
    <input type="number" value="{{$branch->id}}" name="branch_id" class="form-control" id="" hidden>    
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    
    <a class="ui positive right labeled icon button" href="{{ url('/branch/delete/' . $branch->id) }}">Delete <i class="checkmark icon"></i></a>

  </div>
</div>





       

    </div>

  
  </div>
  
</section>

<!-- @isset($devices)
@if(count($devices)==0)
<div class="container  height-100">

</div>
@endif
@endisset -->


@if( count($devices) > 0)
<section class="">
  <div class="container-fluid px-5  mb-3">
    <div class="row">
        <div class="col-12">
          <div class="row  py-3">
          @foreach($devices as $device)
            <div class="col-4 card-deck mb-5">  
            
              <div class="card shadow rounded p-5">
                <div class="row">
                  <div class="col-12 p-1 text-left">
                    <!-- <h5 class="text-uppercase pl-3"><a class="text-info" href="{{ url('/branch/' . $branch->id) }}">{{$branch->branch_name}}</a></h5> -->
                  </div>
                  <div class="col-12 p-1 ">
                      <h5 class="  pl-3 "><i class="fa fa-tablet pr-3" aria-hidden="true"></i> {{$device->device_name}}</h5> 
 
                  </div>

                  <div class="col-12 p-1 ">
                  @if($device->status)
                  <p class=" pl-3  "><i class="fa fa-circle-o pr-3" aria-hidden="true"></i> Device Active</p>
                    @else
                      <p class=" pl-3  "><i class="fa fa-ban pr-3" aria-hidden="true"></i> Device Deactivated</p>
                    @endif
 
                  </div>                 
                  <div class="col-12 p-1">
                    <p class="   pl-3"><i class="fa fa-ellipsis-v pr-4" aria-hidden="true"></i>Type: {{$device->device_type}}</p>
                  </div>
                  <div class="col-12 p-1">
                    <p class="   pl-3"><i class="fa fa-list-ol pr-3" aria-hidden="true"></i></i>Device Code: {{$device->device_code}}</p>
                  </div>
                  <!-- <div class="col-12 p-2">
                    <p class="   pl-3"><i class="fa fa-user-circle pr-3" aria-hidden="true"></i>Status: {{$device->status}}</p>
                  </div> -->
                  <div class="col-12 p-1">
                    <p class="   pl-3"><i class="fa fa-calendar pr-3" aria-hidden="true"></i>Date: {{$device->created_at}} </p>
                  </div>
                  
                  <div class="col-12 p-1 text-left pl-3">
                  @if($device->status)
                    <a class="btn btn-sm  btn-danger pl-3" href="{{ url('/device/deactivate/' . $device->id) }}">  <i class="close icon"></i>  Deactivate</a>
                  @else
                    <a class="btn btn-sm btn-success pl-3" href="{{ url('/device/activate/' . $device->id) }}"><i class="checkmark icon"></i> Activate</a>
                  @endif
                  <a class="btn btn-sm btn-primary" href="{{ url('/screenshots/'. $device->id) }}"><i class="camera icon"></i>Screenshots </a>

                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" onclick="delete_confirmation('{{ $device->id }}')"  ><i class="trash icon"></i>Delete </a>

                  </div>

                
                </div>

              </div>
             
            </div>
            @endforeach
          </div>
        </div>
    </div>
  </div>
 
</section>
@else
<section class="my-5">
  <div class="container">
    <div class="row">
      <div class="col-6 offset-2 text-center">
        <!-- <img class="col-8" src="{{ url('manager/images/empty.png') }}" alt=""> -->
        <img src="https://img.icons8.com/fluent/96/000000/device-shop.png"/>
        <h2>There are no active devices</h2>
        <p class="py-2">Click the button below to add a new device.</p>
        <!-- <button class="btn btn-primary mt-3 text-uppercase" id="test" ><i class="plus square icon"></i> Create New Branch</button> -->
        <button class="btn btn-primary mt-3 text-uppercase  " type="button" id="add_device"><i class="plus square icon"></i>Add new Device</button>

      </div>
    </div>
  </div>
</section>
@endif


 <!-- Delete confirmation modal -->
 <div class="ui modal small delete_branch  " name="deleteConfirmation">
      <div class="header">Delete</div>
      <div class="content">
        
            
            <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently remove this record?</h4>
        
              
      </div>
        <div class="actions">
          
            <a href="" class="ui positive button approve" id="deleteButton" type="submit"><i class="ui checkmark icon"></i> Delete</a>
            <button class="ui red button cancel" type="button"><i class="ui times icon"></i> Cancel</button>
        </div>
    </div>
<!-- Delete confirmation modal ends here -->
<span id="delete_url" style="display: none;">{{url('/device/delete')}}</span>


@endsection

@section('scripts')

<script>

$(function(){
	$("#test").click(function(){
		$(".test").modal('show');
	});
	$(".test").modal({
		closable: true
	});
});

$(function(){
	$("#add_device").click(function(){
		$(".test").modal('show');
	});
	$(".test").modal({
		closable: true
	});
});

$(function(){
	$("#delete").click(function(){
		$(".delete").modal('show');
	});
	$(".delete").modal({
		closable: true
	});
});
</script>


<script>
 function delete_confirmation(branch_id)
    {

      $(".delete_branch").modal('show');
      console.log(branch_id);
      var url = document.getElementById('delete_url').textContent;
      console.log(url);
      url = url+ "/" + branch_id;
      console.log(url);

    //   var buttonID = "deleteButton";

      document.getElementById('deleteButton').href = url;

    }

</script>

@endsection

