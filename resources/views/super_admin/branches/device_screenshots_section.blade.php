<section class="py-5" id="data_table">
    <div class="container">
        <h4>Screenshots : </h4>
        <div class="row">
            @if(count($data) > 0)
              @foreach($data as $screenshot)
                  <div class="col-3 p-3">
                      @php $image_url = $screenshot->image . '.jpg' @endphp
                     
                      <a  class="btn btn-sm btn_upper" onclick="open_modal('{{$image_url}}')" >
                          <img class="img-fluid" src="{{ asset('/manager/screenshots/'. $image_url) }}" alt="{{ $screenshot->created_at }}">
                      </a>
                      <p class="text-center mt-2">@php echo e(date('M d, Y h:i A', strtotime($screenshot->created_at))) @endphp</p>
                     
                  </div>
              @endforeach 
            @else
                <p>No screenshot available for the following device.</p>
            @endif
        </div>
       @php echo $data->render(); @endphp
    </div>
</section>



