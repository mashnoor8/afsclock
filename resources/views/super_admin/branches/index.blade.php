@extends('layouts.super_admin')


@section('content')


<section class="mt-5">
  <div class="container-fluid px-5">
    <div class="row px-4 ">
      <div class="col-6 text-right py-4">
        @if(count($branches) > 0)
        <h3 class="float-left Branch pl-2 "><i class="lni lni-cloud-network"></i> Branches</h3>
        @endif
      </div>
      <div class="col-6">
        @if($condition != 'over')
        <h3 class="float-left py-2">Your trial period has {{$days_left}} left </h3>
        @else
        <h3 class="float-left py-2">Your subscription is expired before {{$days_left}}. Please <a href="{{url('/subscription')}}">subscribe</a>
          @endif
          @if(count($branches) > 0)
          <a class="btn btn-sm btn_upper float-right mt-4" href="{{url('/branches/setup')}}"><i class="plus square icon"></i> Create new Branch</a>
          @endif

      </div>
    </div>
  </div>
</section>



@if(count($branches) > 0)
<section class="mb-5">
  <div class="container-fluid px-5 mb-5 ">
    <div class="row px-4">
      <div class="col-12">
        <div class="row px-1 py-3">
          @foreach($branches as $branch)
          <div class="col-6 ">

            <div class="card shadow rounded p-5">
              <div class="row">
                <div class="col-12">
                  <div class="row">
                    <div class="col-md-6 p-2  float-left">
                      <h3 class="text-uppercase pl-3"><a class="text-info" href="{{ url('/branch/' . $branch->branch_uid) }}">{{$branch->branch_name}}</a></h3>
                    </div>
                    <div class="col-md-6  py-2 " >
                      <div class="ui slider checkbox text-right float-right">
                        <input type="checkbox" class="float-right " id ="scren_prefer.{{ $branch->id }}" onchange="screenshot_prefer('{{ $branch->id }}')" name="newsletter" @isset($branch->screenshot_preference) @if($branch->screenshot_preference==1) checked="checked" @endif @endisset>
                        <label class="text-uppercase">Store Screenshot</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 py-4">
                  <div class="col">
                  <div class="row bg-light py-3">
                    <div class="col text-center">
                     
                        <h2 class="text-success">{!! employee_count($branch->id) !!}</h2>
                        <p class="text-uppercase">Employees</p>
                      
                    </div>
                    <div class="col border-left text-center">
                    
                      <h2 class="text-success">{!! admin_count($branch->id) !!}</h2>
                      <p class="text-uppercase">Admin</p>
                    
                    </div>
                    <div class="col border-left text-center">
                      <h2 class="text-success">{!! device_count($branch->id) !!}</h2>
                      <p class="text-uppercase">Devices</p>
                    </div>
                  </div>
                  </div>
                  
                </div>

                <div class="col-12 pb-4 ">
                <div class="row">
                <div class="col ">
                    <p class=""><i class="fa fa-user-circle pr-3" aria-hidden="true"></i> {{$branch->companyemail}}</p>
                  </div>
                  <div class="col text-right">
                  <p class="text-uppercase text-primary"><i class="sync icon pr-3 "></i>
                    Last Trained {!! traing_time($branch->id) !!} ago                    
                    </p>
                  </div>
                    <!-- <p class=""><i class="fa fa-calendar pr-3" aria-hidden="true"></i>@isset($branch->created_at) @php echo e(date('d-M-Y', strtotime($branch->created_at))) @endphp @endisset</p> -->
                  </div>
                  
                  <div class="col-12">
                    

                </div>
                </div>

                
                
                <div class="col-12">
                  <a class=" tiny ui button  " href="{{ url('/branch/' . $branch->branch_uid) }}"> <i class="tablet alternate icon"></i>Device</a>
                  <a class="tiny ui button " href="{{ url('/training/' . $branch->branch_uid) }}"><i class="user circle outline icon"></i>Train</a>
                  <a class="button ui tiny float-right" href="{{ url('/branch/super_admin_to_branch/' . $branch->id) }}"><i class="unlock icon"></i> Login As admin</a>
                </div>

              </div>

            </div>

          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@else
<section class="my-5">
  <div class="container">
    <div class="row">
      <div class="col-6 offset-2 text-center">
        <img class="col-8" src="{{ url('manager/images/empty.png') }}" alt="">
        <h2>There are no active branches</h2>
        <p class="py-2">Click the button below to create a new branch.</p>
        <a class="btn btn-primary mt-3 text-uppercase" href="{{url('/branches/setup')}}"><i class="plus square icon"></i> Create New Branch</a>

      </div>
    </div>
  </div>
</section>
<!-- <section class="mb-5">
<div class="container row py-5">
<div class="col-md-6 offset-5">
<div class="card">
  <h2 class="card-header">Thank You For Keeping Belief on Us</h2>
  <div class="card-body">
    <h3 class="card-title "><strong>You must <a href="{{url('/branches/setup')}}" class="text-primary">create</a> branch in order to manage your employees.</strong></h3>
    

    
  </div>
</div>
</div>
</div>
</section> -->
@endif
<span id="url" style="visibility:hidden;">{{url('/')}}</span>
@endsection

@section('scripts')
<script>
  function screenshot_prefer(branch_id) {
    var url = document.getElementById('url').textContent;
    $.ajax({
      url: url + "/branch/superadmin/screenshot_preference_change?branch_id=" + branch_id,
      success: function(data) {
        // if (data = 1){
        //   var id = '#scren_prefer'+branch_id
        //   var isChecked = $(id).attr('checked')?true:false;
        //   if (isChecked = 'false'){
        //     $(id).prop("checked", true);
        //   }
        //   else{
        //     $(id).prop("checked", false);
        //   }
          
        // }
        
      }
    });

  }
</script>


@endsection