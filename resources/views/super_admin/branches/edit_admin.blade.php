@extends('layouts.super_admin')

    @section('meta')
        <title>Employees | Attendance Keeper</title>
        <meta name="description" content="Attendance Keeper view employees, delete employees, edit employees, add employees">
    @endsection

    @section('content')
    
    <div class="container-fluid">
        <div class="row">
            <div class="box box-success col-md-6">
            <div class="box-header with-border">Edit</div>
                <div class="box-body">
                <form action="{{ url('/admins/edit') }}" class="ui form" method="post" accept-charset="utf-8">
                    @csrf
                    <div class="field">
                        <label>First Name</label>
                        <input type="text" value="@isset($admin->firstname){{ $admin->firstname}} @endisset" name="firstName" placeholder="First Name">
                    </div>
                    <div class="field">
                        <label>Last Name</label>
                        <input type="text" value="@isset($admin->firstname){{ $admin->lastname}} @endisset" name="lastName" name="lastName" placeholder="Last Name">
                    </div>
                    <div class="field">
                        <label>Email</label>
                        <input type="text" value="@isset($admin->companyemail){{ $admin->companyemail }}@endisset"  name="email" placeholder="Email ">
                    </div>
                    <div class="field">
                        <label>New Password</label>
                        <input type="password" name="password" placeholder="********">
                    </div>
                    <div class="field">
                        <label>Confirm Passsword</label>
                        <input type="password" name="password_confirmation" placeholder="********">
                    </div>
                    <input type="text" name="id" value="@isset($admin->id){{ $admin->id}} @endisset" hidden>
                    
                    <button class="ui button" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection
    
    @section('scripts')
    <script type="text/javascript">

    </script>
    @endsection 