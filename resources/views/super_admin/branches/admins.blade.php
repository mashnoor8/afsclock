@extends('layouts.super_admin')



@section('content')
<section class=" pt-5">
  <div class="container-fluid px-5">
    <div class="row px-3">
      <div class="col">
        <h4><i class="fa fa-university pr-3" aria-hidden="true"></i>Admins</h4>
       
      </div>
      <div class="col text-right">
    
       
      
      </div>
    </div>
  </div>
</section>

<section class="py-3">
  
</section>



@isset($admins)
<section class="">
  <div class="container-fluid px-5  mb-3">
    <div class="row">
        <div class="col-12">
          <div class="row  py-3">
           
                
            <table width="100%" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th >#</th>
                        
                        <th >Admin</th>
                        <th >Email</th>
                        <th >Company</th>
                        <th >Branch </th>
                        <th >Country</th>
                        <th >Time Zone</th>
                        <th >Created At</th>
                        <th >Action</th>
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                    @isset($admins)
                        @foreach ($admins as $admin)
                        
                            <tr>
                            <th scope="row">{{ ($admins->currentpage()-1) * $admins ->perpage() + $loop->index + 1 }}</th>
                            <td>{{ $admin->firstname }} {{ $admin->lastname }}</td>
                            <td>{{ $admin->companyemail }}</td>
                            <td>{{ $admin->company_name }}</td>
                            <td>{{ $admin->branch_name }}</td>
                            <td>{{ $admin->country }}</td>
                            <td>{{ $admin->timezone }}</td>
                            <td>@isset( $admin->created_at)
                            @php echo e(date('Y-m-d h:i:s A', strtotime($admin->created_at))); @endphp
                            @endisset
                              </td>
                              <td><a class="btn btn-sm btn-primary" id="edit" href="{{url('admins/edit/'. $admin->id )}}">Edit</a>
   
                              </td>
                            </tr>
                        
                        @endforeach
                    @endisset

                        <tr>
                        <td colspan="12" align="center">
                        <div class="row">
                        <div class="col">
                        {{ $admins->links() }}
                        </div>

                        </div>
                        </td>
                        </tr>
                </tbody>
            </table>

          </div>
        </div>
    </div>
  </div>
</section>
@endisset



                                         
@endsection

@section('scripts')


@endsection

