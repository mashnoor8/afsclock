@extends('layouts.super_admin')



@section('content')


<section class="bg-info">
    <div class="container py-4 text-white">
        <div class="row">
            <div class="col">
                <h4>Device Name : {{$device->device_name}}</h2>
                <h5>Device Type :{{ $device->device_type }}</h3>
                <p>Device Code : {{ $device->device_code }}</p>
            </div>
            <input class="input" type="number" id="device_id" name="height" value="{{ $device->id }}" hidden>
            <div class="col">
                <div class="col-12">
                    <select class="form-control form-control-sm" id="screenshot_search_change" name="screenshot_search_value">
                        <option value="all">All</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="last_week">Last week</option>
                        <option value="last_month">Last Month</option>
                    </select>
                </div>
                <div class="col-12 pt-3">
                    <div class="row">
                        
                        <div class="col text-right">
                        <a class="text-white" data-toggle="collapse" href="#date_range" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="lni lni-circle-plus"></i>Advance Search</a>
                        
                        </div>
                    </div>
                    <form action="" class="collapse" id="date_range">
                        <div class="row ">
                            <div class="col-md-5">
                                <div class="">
                                    <label class="text-white" for="">From</label>
                                    <input class="form-control" type="date" id="start" name="">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="">
                                    <label class="text-white" for="">From</label>
                                    <input class="form-control" type="date" id="end" name="">
                                </div>
                            </div>
                            <div class="col-md-2 pt-4">
                                <button type="submit" id="search_action" class="btn btn-light mt-1 col-12" title="search"><i class="lni lni-search-alt"></i>Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('super_admin.branches.device_screenshots_section')




<!-- <section class=" pt-5">
  <div class="container-fluid px-5">
    <div class="row px-3">
      <div class="col text-right">
        <a class="btn btn-sm btn_upper"  id="delete" > Delete</a>
      </div>
    </div>
  </div>
</section> -->

<div class="ui large modal view">
  <i class="close icon"></i>
      <img class="img-fluid " id="screenshot" alt="" style="width:100%;"></a>
</div>



<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
<span id="_url" style="display: none;">{{url('/')}}</span>




@endsection

@section('scripts')

<script>

function open_modal(id){

    var url = document.getElementById("_url").textContent + '/manager/screenshots/' + id;
    // console.log(id, url);
    $(".view").modal('show');
    document.getElementById("screenshot").src = url;
}

</script>




<script>


function fetch_data(page, search_value, device_id)
 {
    var url = document.getElementById("_url").textContent;

  $.ajax({
   url: url + "/screenshot/search?page="+ page + "&search_value=" + screenshot_search_change.value + "&device_id=" + device_id,
   success:function(data)
   {
    //    console.log(data);
    
    $('#data_table').html(data);
   }
  })
 }

 $( document ).ready(function() {

    $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);

    
    var search_value = $('#screenshot_search_change').val();
    var device_id = $('#device_id').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, search_value, device_id);
    });


    $( '#screenshot_search_change' ).change(function() {
    var search_value = $('#screenshot_search_change').val();
    var page = $('#hidden_page').val();
    var device_id = $('#device_id').val();
    // console.log(search_value,device_id);
    fetch_data(search_value, page,device_id);
    });
});




</script>

<script>





function fetch_data_advance(page, start, end, device_id)
 {
    var url = document.getElementById("_url").textContent;

  $.ajax({
   url: url + "/advance/screenshot/search?page="+ page + "&start=" + start.value + "&end=" + end.value + "&device_id=" + device_id,
   success:function(data)
   {
    
    $('#data_table').html(data);
   }
  })
 }

 $( document ).ready(function() {

                $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);

               
                var start = $('#start').val();
                var end = $('#end').val();
                var device_id = $('#device_id').val();

                $('li').removeClass('active');
                $(this).parent().addClass('active');
                fetch_data_advance(page, start, end, device_id);
                });


                $('#search_action').click(function(e){
                e.preventDefault();
                var searchData = $('#date_range').serialize();
                var page = $('#hidden_page').val();
                var device_id = $('#device_id').val();
                fetch_data_advance(page, start, end,device_id);


            });


});

</script>




@endsection

