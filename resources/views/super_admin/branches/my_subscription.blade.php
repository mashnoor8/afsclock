@extends('layouts.super_admin')



@section('content')


    <section class="mt-2">
        <div class="container-fluid px-5">
            <div class="row px-4">      
                <div class="col-6 text-right py-4">
                    <h3 class="float-left Branch pl-2 "><i class="lni lni-cloud-network"></i> Subscription Details</h3>
                   
                </div>
                <div class="col-6 text-right py-4">
                   @isset($data) @if($data->stripe_subscription_id != "trial") <a class="btn btn-sm btn_upper float-right mt-4" id="cancel_subscription"><i class="lni lni-plus"></i>Cancel Subscription</a> @endif @endisset
                 
                </div>  
            </div>    
        </div>
    </section>
  
        
   @if($data != null)
    <section class="mb-5">
        <div class="container-fluid px-5 mt-5 ">
            <div class="row px-4">
                <div class="col-6 offset-3">
                    <div class="row px-1 py-3 card ">
                        
                    <div class="col-md-12 order-md-2 mb-4">
                        <h3 class="text-center text-info py-2">Package Details</h3>
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Payment Method</h6>
                               
                            </div>
                            <span class="text-muted">{{$data->payment_method}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Email</h6>
                                
                            </div>
                            <span class="text-muted">{{$data->payer_email}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> Amount </h6>
                                
                            </div>
                            <span class="text-muted">{{$data->price}} @if($data->stripe_subscription_id == 'trial') {{ $currency }} @else {{$data->currency}} @endif</span>
                            </li>
                            
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> Employee Limit </h6>
                                
                            </div>
                            <span class="text-muted">{{$data->limit}}</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> Status </h6>
                               
                            </div>
                            <span class="text-muted"> {{$data->status}}</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> Start Date </h6>
                               
                            </div>
                            <span class="text-muted">@isset($data->plan_period_start) @php echo e(date('d-M-Y', strtotime($data->plan_period_start))) @endphp @endisset</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> End Date</h6>
                                
                            </div>
                            <span class="text-muted">@isset($data->plan_period_end) @php echo e(date('d-M-Y', strtotime($data->plan_period_end))) @endphp @endisset</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"> Days left </h6>
                                
                            </div>
                            <span class="text-muted"> {{$days_left}} </span>
                            </li>

                            
                            
                        </ul>

                        
                    </div>
                       
                    </div>
                </div>

                <div class= "col-12">
                    
                </div>
            </div>
        </div>
    </section>
@else

<section>
    <div class="row pl-5">
        <div class="col-6 ml-5 box shadow-sm p-5">
            <h3 class="text-primary">Your are currently under 60 days trial package.</h3>
            <!-- <p> <strong>Employee Limit : </strong> 6 </p> -->
            <p class="mt-3"><strong>After finishing this trial period, you have to subscribe to a package.</strong></p>

            <h4 class="mt-5">Interested to subscribe to a package?</h4>
            <a class="btn btn-primary mt-3" href="{{ url('/subscription')}}">View All Packages</a>
        </div>
    </div>
</section>

@endif

<div class="ui tiny modal cancel_subscription_modal">
  <i class="close icon"></i>
  <div class="header">
  Unsubscription Confirmation

  </div>
  <div class="p-5">
       
  <h4><i class="question circle icon text-danger"></i> Are you sure you want to permanently unsubscribe this package?</h4>
     
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Nope
    </div>
    
    <a class="ui positive right labeled icon button" href="{{url('/my_subscription/cancel')}}">unsubscribe <i class="checkmark icon"></i></a>

  </div>
</div>
@endsection

@section('scripts')

<script>



$(function(){
	$("#cancel_subscription").click(function(){
		$(".cancel_subscription_modal").modal('show');
	});
	$(".cancel_subscription_modal").modal({
		closable: true
	});
});
</script>

@endsection
