@extends('layouts.super_admin')

@section('css')

@endsection

@section('content')
<main class="mt-3 container-fluid">
    <div class="row  mt-2 pt-3 px-2">
      @isset($packages)
       @foreach($packages as $package)
    
        <div class="col-md-4">
            <div class="card card-pricing shadow pb-5 m-5 zoom">
            <div class="card-header text-center">
            <h3 class="card-pricing-plan-name header-small font-weight-600 pt-2 text-uppercase"><i class="cloud icon"></i>{{ $package->name }} </h3>
            </div>
                <div class="card-body text-center">

                    <i class="card-pricing-icon dripicons-user text-primary"></i>


                    <h2 class="card-pricing-price "> {{Symfony\Component\Intl\Currencies::getSymbol($package->currency) }} {{ $package->price }} <span>/ {{ $package->interval }} </span></h2>
                    <p class="mt-1 text-lg leading-7 text-gray pt-3">For companies with</p>
                    <p class="mt-1 text-lg leading-7 text-gray pt-1">{{ $package->limit}} Employees</p>
                    <div class="mt-3"></div>
                    <a href="{{ url('/subscription_checkout/'.$package->id)}}" class="btn mt-2 btn btn-lg font-weight-600 text-base bg-gray text-white  border  border-transparent letter-space-sm shadow-sm py-3 px-3 rounded"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Choose Plan</a>
                </div>
            </div>
           
        </div>
        
        @endforeach
        @endisset

       

    </div>

    <div class="row mt-5 px-5">
        <div class="col-12 pt-5">
            <h6 class="float-right leading-7 text-gray"> *If more than 50 employees please contact us at <span class="text-primary">sales@attendancekeeper.net</span></h6>
        </div>
    </div>
</main>


@endsection
