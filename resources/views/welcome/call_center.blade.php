@extends('layouts.welcome')

@section('content')

<section class="mt-5 px-5">
    <div class="container-fluid p-5">
        <div class="col text-center pt-5">
            <h1>Call Centre Management</h1>
            <p>Attendance Keeper is designed to manage your call centre in an organised way and also monitor your agents who are on site or working remotely.</p>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container-fluid p-5">
        <div class="row px-5">
            <div class="col-7 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Complete HR Solutions</h2>
                    <p>With one web-based centralised HR database, it is easier and practical to keep all employees data in one place. It is accessible 24/7 and no need for paper handling instead you can upload documents on the cloud, making it completely paperless and secured. We have included the right to work information for each employee’s if required. Our system will notify the admin about any documents expiring soon or any documents that need updating.
</p>
                </div>
            </div>
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/hr.png' }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="px-5 mt-5">
    <div class="container-fluid p-5">
        <div class="row">
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/portal.png' }}" alt="">
            </div>
            <div class="col-7 d-flex align-items-center">
                <div class="col pl-5">
                    <h2 class="display-4">Web-based Employee Portal</h2>
                    <p>Large Call Centres with more than 10 agents communicate with each agent could be challenging. An individual web-based agent's portal communication could be reliably fast-tracked and productive as there is no need for daily monotonous admin task. Instead, each agent can log in in their portal for information such as attendance reports, schedules for next week, holiday/leave requests and assigned task.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 my-5">
    <div class="container-fluid p-5">
        <div class="row px-5">
            <div class="col-7 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Generate Real-time Reports</h2>
                    <p>Able to Generate real-time reports could be key to an informed decision for call centre managers. You can generate an attendance report or employee's past leave reports to look for a pattern to understand better about their behaviour. </p>
                </div>
            </div>
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/counting.png' }}" alt="">
            </div>
        </div>
    </div>
</section>



@endsection
