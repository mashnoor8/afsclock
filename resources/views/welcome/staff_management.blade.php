@extends('layouts.welcome')
@section('content')

<section class="mt-5 px-5">
    <div class="container-fluid p-5">
        <div class="col text-center pt-5">
            <h1>Remote Staff Management</h1>
            <p>Attendance Keeper is designed to keep your remote staff operate the best possible way especially with these features</p>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container-fluid p-5">
        <div class="row px-5">
            <div class="col-7 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Facial Recognition System</h2>
                    <p>Remote staff can Clock-in and Clock-out using our facial recognition system with arrival and departure status according to their scheduled working hours.</p>
                </div>
            </div>
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/clock-in.png' }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="px-5 mt-5">
    <div class="container-fluid p-5">
        <div class="row">
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/devices.png' }}" alt="">
            </div>
            <div class="col-7 d-flex align-items-center">
                <div class="col pl-5">
                    <h2 class="display-4">Authorised Device Access</h2>
                    <p>Many employers are providing laptops to their employee for better productivity and security. With our Attendance Keeper software, an employee has to use this particular authorised device provided by the employer for work during working hours.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 my-5">
    <div class="container-fluid p-5">
        <div class="row px-5">
            <div class="col-7 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Live Activity Screenshot</h2>
                    <p>Since there’s no way of knowing if a remote employee being attentive towards the job, we have a feature that will allow you to determine easily and effectively. Attendance Keeper will take discrete screenshots of the employees desktop during working hours between clock-in and clock-out. Theses screenshots are available 24/7 through the manager’s portal when logged in our site.</p>
                </div>
            </div>
            <div class="col-5">
                <img class="img-fluid" src="{{ 'manager/images/screenshot.png' }}" alt="">
            </div>
        </div>
    </div>
</section>



@endsection
