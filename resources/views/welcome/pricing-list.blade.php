
        @isset($packages)
        @foreach($packages as $package)
            <div class="col-md-4">
                <div class="card card-pricing shadow pb-5 mx-5">
                    <div class="card-body text-center">
                        <h3 class="card-pricing-plan-name header-small font-weight-600 text-uppercase">{{ $package->name }}  </h3>
                        <i class="card-pricing-icon dripicons-user text-primary"></i>
                        <h2 class="card-pricing-price ">{{Symfony\Component\Intl\Currencies::getSymbol($package->currency) }} {{ $package->price }}</h2>
                            <p class="mt-1 text-lg leading-7 text-gray pt-3">For companies with</p>
                            <p class="mt-1 text-lg leading-7 text-gray pt-1">{{ $package->limit}} Employees</p>
                  
                            <!-- <li>1 User</li>
                            <li>Email Support</li>
                            <li>24x7 Support</li> -->
                    
                        <button class="btn mt-2 btn btn-lg font-weight-600 text-base bg-org text-white border  border-transparent letter-space-sm shadow-sm py-3 px-3 rounded"  data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Choose Plan</button>
                    </div>
                </div>
                
            </div>
        @endforeach
        @endisset 

            




