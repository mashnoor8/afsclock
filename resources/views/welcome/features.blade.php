@extends('layouts.welcome')

@section('content')


         <!-- START FEATURES 2 -->
<section class="py-5">
    <div class="container-fluid px-5 ">
       
        <div class="row mt-2 py-5 align-items-center px-4" id="facial_rec">
            <div class="col-lg-7 pb-5 pl-4 pr-5">
                <h1 class=" header-large font-weight-800">Facial Recognition System</h1>
                <p class="text-muted mt-3">This will allow an employee to Clock-in and Clock-out using our Facial Recognition System easily and effectively. </p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray"> We use the modern way to map out 91% facial accuracy. This won’t allow any fraudulent activities associated with employee’s Clock-in and Clock-out.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Not possible to do any proxy Clock-in or Clock-out.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Our Facial Recognition System* only works in an authorised device assigned by the employer. In order for an employee to Clock-in and Clock-out. The employee needs to use this device with the help of our desktop app**. This can be installed in a work station or on a personal computer.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> User will receive a notification via email*** every time they complete Clock-in and Clock-out.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Upon completion, our system will automatically update attendance timesheet instantly.</li>

                       
                    </ul>
                   
                                   
                </div>

                <div class=" mt-3">
                <p class="small text-muted">
                *In order for our software to work properly the owner of the device needs to grant permission for the webcam or install external USB webcam.<br>
                **This desktop app can be replaced for a browser-based app if needed<br>
                ***Company registered email address will be used<br>
          
                </p>
                </div>

                <!-- <a href="" class="btn btn-primary btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            <div class="col-lg-5 py-5">
                    <video width="100%" height="" controls  muted>
                        <source src="{{'manager/videos/AttendanceFace.mp4'}}" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                        <!-- Your browser does not support the video tag. -->
                    </video>
            </div>
            
        </div>

        <div class="row pb-3 pt-5 align-items-center px-4" id="work_attendance">
            
            <div class="col-lg-5 pl-4 py-5">

                <!-- carusal bootstrap  start-->

                <video width="100%" height="" controls poster="{{'manager/images/screenshot.png'}}" preload="none"  muted>
                    <source src="{{'manager/videos/Discrete .mp4'}}" type="video/mp4">
                    <source src="movie.ogg" type="video/ogg">
                    <!-- Your browser does not support the video tag. -->
                </video>

                <!-- carusal bootstrap  end-->

            <!-- <img src="{{'images/report.png'}}" class="img-fluid" width="80%" alt=""> -->
            </div>

            <div class="col-lg-7 py-5 pl-5">
                <h1 class=" header-large font-weight-800">Live Activity Screenshot </h1>
                <p class="text-muted mt-3">It’s important to know if an employee is really working during his/her scheduled working hours </p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray"> This feature will capture* sudden screenshot of an employee’s desktop, as a proof of their attendance towards their responsibilities during working hour*** while at work or working from home and pass these screenshots to upper management.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> This will allow an employee to be more attentive to their responsibilities.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Employee’s performance can be measured based upon their work attentive over a set of time.</li>
                        

                       
                    </ul>

                    <!-- <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i> This feature will capture* sudden screenshot of an employee’s desktop*, as a proof of
                        their attendance towards their responsibilities during working hour** while at work
                        or working from home and pass this screenshot to manager/supervisor. </p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>  This will allow an employee to be more attentive towards their job. </p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i> Employee’s performance can be measured based upon their work attentive over a set
                        of time. </p> -->
                    <!-- <p class="text-muted"><i class="mdi mdi-circle-medium text-success"></i> Detailed Documentation</p> -->
                </div>

                <div class=" mt-3">
                <p class="small text-muted">
                *permission needed to be granted while installing our software to access the owner’s desktop.<br>
                **Employee desktop means his/her personal computer or work station that has our software installed and been authorised to this designated person<br>
                *** Working hours mean time between Clocking-in and Clocking-out using our software.  <br>
           
                </p>
                </div>

                <!-- <a href="" class="btn btn-success btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            
        </div>


        <div class="row mt-2 py-5 align-items-center px-4" id="authorized-device">

            <div class="col-lg-7 py-5 pl-4 pr-5">
                <h1 class="  header-large font-weight-800"> Authorised Device Access Only</h1>
                <p class="text-muted mt-3">Attendance Keeper can provide valuable insights into every device that accessing company data through authorising devices</p>

                <div class="mt-4">

                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray"> This will secure your highly-sensitive company data, as an employee has to use an authorised device* during working hours.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> The employer can protect data, as well as the identities and devices that access the data.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Better security with less overhead costings.</li>
                        

                       
                    </ul>

                    
                </div>

                <div class=" mt-3">
                <p class="small text-muted">
                *Authorised device refers to a personal computer or a company workstation that, has our Attendance Keeper software installed with a valid license key. 
 <br>

               
           
                </p>
                </div>

                <!-- <a href="" class="btn btn-primary btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            
            <div class="col-lg-5 py-5">
             

             <!-- carusal bootstrap  start-->


             <div id="carouselExampleControls" class="carousel slide" width="80%" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100" src="{{'manager/images/AD01.png'}}" alt="demo-img"
                                class="img-fluid shadow-sm rounded">
                    <!-- <img class="d-block w-100" src="..." alt="First slide"> -->
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="{{'manager/images/AD02.png'}}" alt="demo-img"
                                class="img-fluid shadow-sm rounded">
                    <!-- <img class="d-block w-100" src="..." alt="Second slide"> -->
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="{{'manager/images/AD03.png'}}" alt="demo-img"
                                class="img-fluid shadow-sm rounded">
                    <!-- <img class="d-block w-100" src="..." alt="Second slide"> -->
                    </div>
                  
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>

                <!-- carusal bootstrap  end-->
            </div>
            
        </div>

        <div class="row pb-3 pt-5 align-items-center px-4" id="complete_hr">
            <div class="col-lg-5 pl-4 py-5">
                            <video width="100%" height="" controls  muted>
                            <source src="{{'manager/videos/CompleteHR.mp4'}}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            <!-- Your browser does not support the video tag. -->
            </div>
            <div class="col-lg-7 py-5 pl-5">
                <h1 class="text-l weight-800 space-25">Complete HR Solutions</h1>
                <p class="text-muted mt-3">All your employee information in one secured place for all HR-related tasks</p>

                <div class="mt-4">

                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray"> All your employee data in one localised place safeguarded. Manager can handle all personal data centrally on a cloud-based database.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">No more physical copy required as Manager can store documents on the cloud securely and safely with reminding notification before any documents expire.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Keep track of right to work information with reminder and documents storage facility.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Receive notifications before any documents expire and to be able to update current status into our system.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Keep track of employee’s sick/leave days to predict a pattern and take necessary steps to improve attendance. Manager can approve or disapprove any sick/leave requests.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Manage employee’s holidays easily and smoothly instead of emailing back and forth or handling holiday request form.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Create an employee’s schedule as per required by the business on timely manners and make it available.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Manager can create a username and password for an employe’s own web-based portal.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Manager can run any queries for an informative report to conduct an audit within a team.</li>    
                    </ul>
                    <!-- <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i> All your employee data in one localised place safeguarded. Manager can handle all personal data centrally on a cloud
based system.  </p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>No more physical copy required as Manager can store documents on the cloud securely and safely with reminding
notification before any documents expires.</p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i> Keep track of right to work information with reminder and documents storage facility.</p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Receive notifications before any documents expires and to be able to update current status in to our system. </p>
                    
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Keep track of employe’s sick/leave days to predict a pattern and take necessary steps to improve attendance. Manager
can approve or disapprove any sick/leave requests. </p>
                
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Manage employe’s holidays easily and smoothly instead of emailing back and forth or handle holiday request form.</p>

                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Create employe’s schedule as per required by the business on timely manners and make it available.</p>
                
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Manager can create an username and password for an employe’s own web based portal.</p>
                    <p class="mt-3 text-base  text-gray"><i class="mdi mdi-circle-medium text-success"></i>Manager can run any queries for an informative reports to conduct any audit within a team.</p> -->
                
                </div>

              

                <!-- <a href="" class="btn btn-success btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            
        </div>

        <div class="row mt-2 py-5 align-items-center px-4" id="task_manager">
            <div class="col-lg-7 py-5 pl-4 pr-5">
                <h1 class="text-l weight-800 space-25">Task Manager </h1>
                <p class="text-muted mt-3">Task manager would allow you to manage your tasks more efficiently and productively.</p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray"> An employee will receive notification of assign task with details.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> An employee can either complete the assigned task in realtime with notification sent or can extend* deadline as required.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Any employee can assign the task to any employee within an organisation as a business required. For example, a team member can assign a task to upper management.</li>
                       
                        <li class="mt-3 text-base  leading-7 text-gray">Manager can easily audit a team’s performance-based upon their professional productivity and work discipline.</li>

                 
                       
                    </ul>

                    
                
                </div>


                <div class=" mt-3">
                <p class="small text-muted">
               * Each task a person can extend only twice, if still not done task gets void

               
           
                </p>
                </div>

                <!-- <a href="" class="btn btn-primary btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            <div class="col-lg-5 py-5">
                            <video width="100%" height="" controls  muted>
                            <source src="{{'manager/videos/Task.mp4'}}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            <!-- Your browser does not support the video tag. -->
            </div>
            
        </div>

        <div class="row pb-3 pt-5 align-items-center px-4" id="employees_portal">
            <div class="col-lg-5 py-5 pl-4">
                            <video width="100%" height="" controls  muted>
                            <source src="{{'manager/videos/EmployeesPortal.mp4'}}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            <!-- Your browser does not support the video tag. -->
            </div>
            <div class="col-lg-7 py-5 pl-5">
                <h1 class="text-l weight-800 space-25">Employee’s Portal</h1>
                <p class="text-muted mt-3">An employee own web-based personal portal* for all the relevant information needed in one place  </p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray">An employee can Clock-in and Clock- out using our facial recognition system** from their own web-based portal or by using our software***</li>
                        <li class="mt-3 text-base  leading-7 text-gray">An employee can see all their recent and past attendances with the status of their arrivals and departures. 
 </li>
                        <li class="mt-3 text-base  leading-7 text-gray">Current and future work schedule available in the portal.</li>
                        <li class="mt-3 text-base  leading-7 text-gray"> Submit and manage holiday/leave requests.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">An employee can view their assigned task and submit their progress or assign tasks to other colleagues with.
</li>

                       
                    </ul>
                    
                    
                </div>
                <div class=" mt-3">
                <p class="small text-muted">
               *Manager needs to provide credentials for personal portal ie username and password   <br>
               ** Camera/webcam required<br>
               *** Attendance Keeper software needs to be installed in an authorised device beforehand
               
           
                </p>
                </div>

                <!-- <a href="" class="btn btn-success btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            
        </div>

        <div class="row mt-2 py-5 align-items-center px-4" id="generate_report">
            
            <div class="col-lg-7 py-5 pl-4 pr-5">
                <h1 class="text-l weight-800 space-25">Generate Reports</h1>
                <p class="text-muted mt-3">Manager can generate various practical and real-time reports* to make an informative and realistic decision. </p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray">At any time manager, can generate attendance reports for all the employees with their start and finish time in real-time over any set of timelines highlighting arrival and departure status.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Employee turnover for a set of the period to see the overhead costs.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Organisational profiling reports for an overview of employees working based upon their gender, age and department.</li>
                    </ul>
                    
              
                </div>

                <div class=" mt-3">
                <p class="small text-muted">
                * These reports are only for guidance and can be customised if required  <br>
               
           
                </p>
                </div>


                <!-- <a href="" class="btn btn-primary btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            <div class="col-lg-5 py-5">
               
               <!-- carusal bootstrap  start-->


               <div id="carouselExampleControls" class="carousel slide" width="80%" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100" src="{{'manager/images/RP01.png'}}" alt="demo-img"
                                class="img-fluid shadow-sm rounded">
                    <!-- <img class="d-block w-100" src="..." alt="First slide"> -->
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="{{'manager/images/RP02.png'}}" alt="demo-img"
                                class="img-fluid shadow-sm rounded">
                    <!-- <img class="d-block w-100" src="..." alt="Second slide"> -->
                    </div>
                    
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>

                <!-- carusal bootstrap  end-->
            </div>
        </div>

        <div class="row pb-3 pt-5 align-items-center px-4" id="Salary">
            <div class="col-lg-5 py-5 pl-4">
            <video width="100%" height="" controls  muted>
                <source src="{{'manager/videos/HrSalary.mp4'}}" type="video/mp4">
                <!-- <source src="movie.ogg" type="video/ogg"> -->
                Your browser does not support the video tag.
            </video>
            </div>
            <div class="col-lg-7 py-5 pl-5">
                <h1 class="text-l weight-800 space-25">Salary calculation</h1>
                <p class="text-muted mt-3">You can easily calculate your employee’s gross salary in realtime based on their Clock-in and Clock-out time.
</p>

                <div class="mt-4">
                    <ul>
                        <li class="mt-3 text-base  leading-7 text-gray">Attendance Keeper will automatically deduct employees unapproved leave hours.</li>
                        <li class="mt-3 text-base  leading-7 text-gray">Allows you to generate salary reports both for annual paid staff and hourly paid staff at any given time.   </li>
                        <li class="mt-3 text-base  leading-7 text-gray">You can export this report to CSV format.</li>
                       

                       
                    </ul>
                    
                    
                </div>
                <div class=" mt-3">
                <!-- <p class="small text-muted">
                *Manager need to provide credentials for personal portal ie username and password  <br>
                ** Camera/webcam required<br>
                *** Attendance Keeper software needs to be installed in an authorised device beforehand
               
           
                </p> -->
                </div>

                <!-- <a href="" class="btn btn-success btn-rounded mt-3">Read More <i class="mdi mdi-arrow-right ml-1"></i></a> -->

            </div>
            
        </div>

       
    </div>
</section>
        <!--  FEATURES  -->

        <section>
    <div class="container-fluid py-3">

        <div class="row padding-lg align-items-center bg-dark">
            <div class="col-lg-12 text-center">
                <h1 class="header-large font-weight-800 text-white">Still deciding ?</h1>
                <h4 class="mt-3 text-lg leading-7 text-white">See how you can save time and money </h4>

                <a class="btn btn-lg font-weight-600 font-para-xl bg-org text-white  rounded-md border-transparent letter-space-sm shadow-sm py-3 px-5 mt-3" href="{{ route('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a>
                                                                            
                <!-- <a href="" class="btn btn-primary btn-rounded mt-3">Signup <i class="mdi mdi-arrow-right ml-1"></i></a> -->
            

            </div>
            <!-- <div class="col-lg-5 offset-lg-1">
            <img src="{{'images/report.png'}}" class="img-fluid" width="80%" alt="">
            </div> -->
        </div>

        

    </div>
    
</section>
    
  
           <!-- START CONTACT -->
<section class="p-5    border-bottom border-light" id="contact">
    <div class="container-fluid px-5">
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3 class="header-large font-weight-800">Get In Touch</h3>
                    <p class="mt-3   ">Please fill out the following form and we will get back to you shortly. For more
                        <br>information please contact us.</p>
                </div>
            </div>
        </div>

        <div class="row my-5  ">
            <!-- <div class="col-md-4 mt-4 pt-1">
                <p class="text-muted"><span class="font-weight-bold">Customer Support:</span><br> <span class="d-block mt-1">+1 234 56 7894</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Email Address:</span><br> <span class="d-block mt-1">info@gmail.com</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Office Address:</span><br> <span class="d-block mt-1">4461 Cedar Street Moro, AR 72368</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Office Time:</span><br> <span class="d-block mt-1">9:00AM To 6:00PM</span></p>
            </div> -->

            <div class="col-md-7">
                <form action="{{url('/contact/send')}}" method="post"> 
                @csrf
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="fullname">Your Name</label>
                                <input class="form-control form-control-light" type="text" name="name" id="fullname" placeholder="Name...">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="emailaddress">Your Email</label>
                                <input class="form-control form-control-light" type="email" name="email" required="" id="emailaddress" placeholder="Enter you email...">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="subject">Your Subject</label>
                                <input class="form-control form-control-light" type="text" name="subject" id="subject" placeholder="Enter subject...">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="comments">Message</label>
                                <textarea id="comments" rows="4" class="form-control form-control-light" name="message" placeholder="Type your message here (1 to 500 character) ..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-lg font-weight-600 font-para-xl bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-4"><i class="fa fa-paper-plane " aria-hidden="true"></i> Send a Message</button>
                        </div>
                    </div>
                </form>
                
            </div>

            <div class="col-md-5  text-center ">
                <img src="{{'manager/images/contact.jpg'}}" alt="" class="img-fluid" height="" />
               <!-- <a href="{{url('/')}}"><img src="{{'images/logo-top.png'}}" alt="" class="logo-dark ml-5" height="150px" /> </a>
               <a href="https://attendancekeeper.net/ak_setup.exe" target="_blank" class="btn  font-weight-600 font-para-xl bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-3 ml-5 mt-5"><i class="fa fa-download" aria-hidden="true"></i> Download Desktop App </a> -->
            </div>
            
        </div>
    </div>
</section>
<!-- END CONTACT -->

@endsection
