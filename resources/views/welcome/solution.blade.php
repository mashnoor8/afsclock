@extends('layouts.welcome')

@section('content')


 <!-- <section>
    <div class="container ">
        <div class="row ">

            <div class="col-8 offset-2 mt-5 pt-5">

                <div class="row p-2 ">
                    <div class="col-6 p-2 ">
                           
                        <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-3 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-9 p-3">
                                <h4 class="font-weight-normal "> FAQs</h4>
                            </div>

                            </div>
                            
                        </div>
                        <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-3 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-9 p-3">
                                <h4 class="font-weight-normal ">User Manuel</h4>
                            </div>

                            </div>
                            
                        </div>
                        <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-3 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-9 p-3">
                                <h4 class="font-weight-normal ">Contact us </h4>
                            </div>

                            </div>
                            
                        </div>


                    </div>
                    <div class="col-6  p-2  ">
                           
                       <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-2 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-10 p-3">
                                <h4 class="font-weight-normal ">Remote staff management</h4>
                            </div>

                            </div>
                            
                        </div>
                        <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-3 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-9 p-3">
                                <h4 class="font-weight-normal ">Call centre management </h4>
                            </div>

                            </div>
                            
                        </div>
                        <div class="col-12 shadow card">
                            <div class="row">
                            <div class="col-3 p-3">
                                <h3 class="font-weight-normal "><i class="lni lni-question-circle"></i></h3>
                            </div>
                            <div class="col-9 p-3">
                                <h4 class="font-weight-normal "> Warehouse staff management</h4>
                            </div>

                            </div>
                            
                        </div>
   
   
                    </div>
                </div>


            </div>
        </div>

    </div>

 </section> -->



 <section>

    <div class="container">

        <div class="row">

            <div class="col-10 offset-1 mt-25">

            <div class="col-12 card">

                <div class="row p-5">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                                <div class="col-3 text-center ">
                                    <h3 class="header-small text-gray "><i class="fa fa-question-circle" aria-hidden="true"></i></h3>

                                </div>
                                <div class="col-9">
                                   <a  href="{{url('/faqs')}}"><h3 class="text-lg text-gray "> FAQ’s</h3></a> 
                                    <p>Frequently Asked Questions</p>

                                </div>
                            </div>
                        </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                                <div class="col-3 text-center">
                                    <h3 class="header-small text-gray "> <i class="fa fa-book" aria-hidden="true"></i></h3>

                                </div>
                                <div class="col-9">
                                <a  href="{{url('/user-manual')}}"><h3 class="text-lg text-gray "> User Manuel</h3></a>
                                    <p>Explaining our features in depth</p>
                                </div>
                            </div>
                        </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                                <div class="col-3 text-center">
                                    <h3 class="header-small text-gray "> <i class="fa fa-laptop" aria-hidden="true"></i></h3>

                                </div>
                                <div class="col-9">
                                <a  href="{{url('/remote-staff')}}"><h3 class="text-lg text-gray "> Remote staff management</h3></a>
                                    <p>See how you can utilise your remote staff </p>
                                </div>
                            </div>
                        </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                                <div class="col-3 text-center ">
                                    <h3 class="header-small text-gray ">  <i class="fa fa-phone-square" aria-hidden="true"></i></h3>

                                </div>
                                <div class="col-9">
                                <a  href="{{url('/call-center')}}"><h3 class="text-lg text-gray "> Call centre management </h3></a>
                                    <p>See how you can manage your staff</p>
                                </div>
                            </div>
                        </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                                <div class="col-3 text-center">
                                     <h3 class="header-small text-gray "><i class="fa fa-envelope" aria-hidden="true"></i></h3>

                                </div>
                                <div class="col-9">
                                <a  href="{{url('/contact')}}"><h3 class="text-lg text-gray "> Contact us </h3></a>
                                    <p>Contact us form </p>
                                </div>
                            </div>
                        </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-3">
                        <div class="row">
                            <div class="col-3 text-center">
                                <h3 class="header-small text-gray "> <i class="fa fa-users" aria-hidden="true"></i></h3>
                            </div>
                            <div class="col-9">
                            <a  href="{{url('/warehouse-staff')}}"><h3 class="text-lg text-gray "> Warehouse staff management</h3></a>
                                <p>See how you can save money</p>
                            </div>
                        </div>
                           
                    </div>
                </div>
            
            </div>


            </div>
        </div>
    </div>
 </section>

@endsection