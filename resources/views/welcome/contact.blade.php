@extends('layouts.welcome')

@section('content')
           <!-- START CONTACT -->
<section class="p-5    border-bottom border-light" id="contact">
    <div class="container-fluid px-5">
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3 class="header-large font-weight-800">Get In Touch</h3>
                    <p class="mt-3   ">Please fill out the following form and we will get back to you shortly. For more
                        <br>information please contact us.</p>
                </div>
            </div>
        </div>

        <div class="row my-5  ">
            <!-- <div class="col-md-4 mt-4 pt-1">
                <p class="text-muted"><span class="font-weight-bold">Customer Support:</span><br> <span class="d-block mt-1">+1 234 56 7894</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Email Address:</span><br> <span class="d-block mt-1">info@gmail.com</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Office Address:</span><br> <span class="d-block mt-1">4461 Cedar Street Moro, AR 72368</span></p>
                <p class="text-muted mt-4"><span class="font-weight-bold">Office Time:</span><br> <span class="d-block mt-1">9:00AM To 6:00PM</span></p>
            </div> -->

            <div class="col-md-7">
                <form action="{{url('/contact/send')}}" method="post"> 
                @csrf
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="fullname">Your Name</label>
                                <input class="form-control form-control-light" type="text" name="name" id="fullname" placeholder="Name...">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="emailaddress">Your Email</label>
                                <input class="form-control form-control-light" type="email" name="email" required="" id="emailaddress" placeholder="Enter you email...">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="subject">Your Subject</label>
                                <input class="form-control form-control-light" type="text" name="subject" id="subject" placeholder="Enter subject...">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="comments">Message</label>
                                <textarea id="comments" rows="4" class="form-control form-control-light" name="message" placeholder="Type your message here (1 to 500 character) ..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-lg font-weight-600 font-para-xl bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-4"><i class="fa fa-paper-plane " aria-hidden="true"></i> Send a Message</button>
                        </div>
                    </div>
                </form>
                
            </div>

            <div class="col-md-5  text-center ">
                <img src="{{'manager/images/contact.jpg'}}" alt="" class="img-fluid" height="" />
               <!-- <a href="{{url('/')}}"><img src="{{'images/logo-top.png'}}" alt="" class="logo-dark ml-5" height="150px" /> </a>
               <a href="https://attendancekeeper.net/ak_setup.exe" target="_blank" class="btn  font-weight-600 font-para-xl bg-org text-white border rounded-md border-transparent letter-space-sm shadow-sm py-2 px-3 ml-5 mt-5"><i class="fa fa-download" aria-hidden="true"></i> Download Desktop App </a> -->
            </div>
            
        </div>
    </div>
</section>
<!-- END CONTACT -->
@endsection