@extends('layouts.welcome')
@section('content')

<section class="mt-5 px-5">
    <div class="container-fluid p-3">
        <div class="col text-center pt-5">
            <h1>Warehouse Staff Management</h1>
            <p>Attendance Keeper is built to supervise your warehouses in different locations from one portal.</p>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container-fluid p-3">
        <div class="row px-5">
            <div class="col-lg-7 col-md-7 col-sm-12 col-12  d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Facial Recognition System</h2>
                    <p>Our face recognition system will prevent any proxy clock-in or clock-out. This will help your warehouse staffs to be more punctual and productive. Our system can update your attendance report in real-time so you can analyse any time you wish to.</p>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-12 p-5 ">
                <img class="img-fluid" src="{{ 'manager/images/clock-in.png' }}" alt="">
            </div>
            
        </div>
    </div>
</section>

<section class="px-5 mt-5">
    <div class="container-fluid p-3">
        <div class="row px-5">
            <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-4">
                <img class="img-fluid pl-2" src="{{ 'manager/images/hr.png' }}" alt="">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-12 d-flex align-items-center">
                <div class="col pl-5">
                    <h2 class="display-4">Complete HR Solution</h2>
                    <p>As the company gets larger and larger storing all employees sensitive data securely could be daunting. Attendance Keeper can keep all your employee's data in one web-based centralised database with documents storage digitally. In this way, you can access that data remotely and 24/7.
</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 my-5">
    <div class="container-fluid p-3">
        <div class="row px-5">
            <div class="col-lg-7 col-md-7 col-sm-12 col-12 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Task Manager</h2>
                    <p>Task Manager can organise your team in a more manageable way. Any staff can assign the task to anyone in the team. And the manager can view all the past and ongoing tasks details and status. The manager can have a precise overall opinion about a team’s performance and productivity.
</p>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-12 p-5">
                <img class="img-fluid" src="{{ 'manager/images/task-manager.png' }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="py-5 my-5">
    <div class="container-fluid p-3">
        <div class="row px-5">
        <div class="col-lg-5 col-md-5 col-sm-12 col-12 pl-4">
                <img class="img-fluid pl-2" src="{{ 'manager/images/salary.png' }}" alt="">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-12 d-flex align-items-center">
                <div class="col pr-5">
                    <h2 class="display-4">Salary Calculation</h2>
                    <p>With Attendance Keeper you can calculate your yearly and hourly staffs salary with just a few clicks at any given time.</p>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection
